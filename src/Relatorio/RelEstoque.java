

package Relatorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;


public class RelEstoque extends javax.swing.JFrame {
 public Date Data_Inicial;
    public Date Data_Final;
    MovimentoEstoque M;
    private SimpleDateFormat sdf;
    public String TesteData;

    public RelEstoque() {
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JCData = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        JTFData_Inicial = new javax.swing.JFormattedTextField();
        JTFData_Final = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        JCProduto = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        JTFCodProduto = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        JBConverter_Trans = new javax.swing.JButton();
        JTFProduto = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório de Estoque");

        jLabel1.setText("Escolha os Filtros Desejados");

        JCData.setText("Período");
        JCData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCDataActionPerformed(evt);
            }
        });

        jLabel2.setText("Data Inicial");

        JTFData_Inicial.setEditable(false);
        try {
            JTFData_Inicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JTFData_Final.setEditable(false);
        try {
            JTFData_Final.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Data Final");

        JCProduto.setText("Produto Movimentados");
        JCProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCProdutoActionPerformed(evt);
            }
        });

        jLabel4.setText("Código");

        JTFCodProduto.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.setEnabled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Produto");

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));

        JBConverter_Trans.setText("Gerar Relatório");
        JBConverter_Trans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBConverter_TransActionPerformed(evt);
            }
        });
        jPanel2.add(JBConverter_Trans);

        JTFProduto.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JCData)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(JTFData_Inicial, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JCProduto)
                            .addComponent(jLabel4))
                        .addGap(69, 69, 69))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFProduto, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(JTFData_Final, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JCData)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFData_Inicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData_Final, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JCProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(275, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap(402, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCDataActionPerformed
        if (JCData.isSelected() == true) {
            JTFData_Final.setEditable(true);
            JTFData_Inicial.setEditable(true);
        } else if (JCData.isSelected() == false) {
            JTFData_Final.setText(null);
            JTFData_Inicial.setText(null);
            JTFData_Final.setEditable(false);
            JTFData_Inicial.setEditable(false);
        }
    }//GEN-LAST:event_JCDataActionPerformed

    private void JCProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCProdutoActionPerformed

        if (JCProduto.isSelected() == true) {

            jButton1.setEnabled(true);
        } else if (JCProduto.isSelected() == false) {
            JTFCodProduto.setEditable(false);
            jButton1.setEnabled(false);
            JTFCodProduto.setText("");
            JTFProduto.setText("");
        }
    }//GEN-LAST:event_JCProdutoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        final ConsultarProdutos Pesquisa = new ConsultarProdutos();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodProduto.setText(Pesquisa.Codigo);
                    JTFProduto.setText(Pesquisa.Descr);
                }
            }
        });
    }//GEN-LAST:event_jButton1ActionPerformed

    private void JBConverter_TransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBConverter_TransActionPerformed
        SimpleDateFormat Teste = new SimpleDateFormat("yyyy");
        int Opcao = 0;
        if ((JCData.isSelected() == true) && (JCProduto.isSelected() == false)) {
            Opcao = 1;
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            M = new MovimentoEstoque(1, JTFData_Inicial.getText(), JTFData_Final.getText(), 0);

        } else if ((JCProduto.isSelected() == true) && (JCData.isSelected() == false)) {
            if (JTFProduto.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Informe o Produto");
                return;
            } else {
                M = new MovimentoEstoque(2, JTFData_Inicial.getText(), JTFData_Final.getText(), Integer.parseInt(JTFCodProduto.getText()));
            }
        } else if ((JCProduto.isSelected() == true) && (JCData.isSelected() == true)) {
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            if (JTFProduto.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Informe o Produto");
                return;
            }
            M = new MovimentoEstoque(3, JTFData_Inicial.getText(), JTFData_Final.getText(), Integer.parseInt(JTFCodProduto.getText()));
        } else if ((JCData.isSelected() == false) && (JCProduto.isSelected() == false)) {
            M = new MovimentoEstoque(0, JTFData_Inicial.getText(), JTFData_Final.getText(), 0);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_JBConverter_TransActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RelEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RelEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RelEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RelEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RelEstoque().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBConverter_Trans;
    private javax.swing.JCheckBox JCData;
    private javax.swing.JCheckBox JCProduto;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JFormattedTextField JTFData_Final;
    private javax.swing.JFormattedTextField JTFData_Inicial;
    private javax.swing.JTextField JTFProduto;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
public boolean Validar_Data() {
        try {
            Data_Inicial = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }
}
