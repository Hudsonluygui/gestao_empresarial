package Relatorio;

import CONEXAO.ConexaoOracle;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class RelatorioVendas {

    ConexaoOracle conecta_oracle;

    public RelatorioVendas() {
        conecta_oracle = new ConexaoOracle();
    }

    public void Geral() {
        try {
            conecta_oracle.conecta(0);
            conecta_oracle.executeSQL("SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY') AS DATA, TO_CHAR(V.DATA_VENCIMENTO,'DD/MM/YYY') AS DATA_VENCIMENTO, V.TOTAL_VENDA"
                    + " FROM VENDA V"
                    + " JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA ORDER BY DATA_VENCIMENTO DESC");
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
          JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica\\src\\Relatorio"
                        + "\\04.Jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void Geral(String Data_Inicial, String Data_Final) {
        try {
            conecta_oracle.conecta(0);
            conecta_oracle.executeSQL("SELECT V.ID_VENDAS, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYY') AS DATA_VENDA, V.VALOR_VENDA FROM VENDAS V"
                    + " JOIN CAD_PESSOA P ON V.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE DATA_VENDA BETWEEN '" + Data_Inicial + "' AND '" + Data_Final + "' ORDER BY V.ID_VENDAS ASC");
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("D:\\NetBeans\\TCC\\src\\Relatorios"
                    + "\\VENDA.Jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void Geral(int Codigo) {
        try {
            conecta_oracle.conecta(0);
            conecta_oracle.executeSQL("SELECT V.ID_VENDAS, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYY') AS DATA_VENDA, V.VALOR_VENDA "
                    + " FROM VENDAS V "
                    + " JOIN CAD_PESSOA P ON V.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE V.ID_PESSOA = " + Codigo + " ORDER BY V.ID_VENDAS ASC");
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("D:\\NetBeans\\TCC\\src\\Relatorios"
                    + "\\VENDA.Jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void Geral(int Codigo, String Data_Inicial, String Data_Final) {
        try {
            conecta_oracle.conecta(0);
            conecta_oracle.executeSQL("SELECT V.ID_VENDAS, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYY') AS DATA_VENDA, V.VALOR_VENDA "
                    + " FROM VENDAS V "
                    + " JOIN CAD_PESSOA P ON V.ID_PESSOA = P.ID_PESSOA"
                    + " WHERE V.ID_PESSOA = " + Codigo
                    + " AND DATA_VENDA BETWEEN '" + Data_Inicial + "' AND '" + Data_Final + "' ORDER BY V.ID_VENDAS ASC"
            );
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("D:\\NetBeans\\TCC\\src\\Relatorios"
                    + "\\VENDA.Jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

}
