package Relatorio;

import CONEXAO.ConexaoOracle;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class RelatorioCompras {

    ConexaoOracle conecta_oracle;

    public RelatorioCompras() {
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);
    }

    public void Geral() {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT N.NR_NF, P.NOME, TO_CHAR(N.DATA_EMISSAO, 'DD/MM/YYYY') AS DATA_EMISSAO, "
                    + " TO_CHAR(N.DATA_VENCIMENTO, 'DD/MM/YYYY') AS DATA_VENCIMENTO, N.VALOR_NF"
                    + " FROM MOV_NF N"
                    + " JOIN FORNECEDOR F ON N.ID_FORNECEDOR = F.ID_FORNECEDOR"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = F.ID_PESSOA"
                    + " ORDER BY N.DATA_VENCIMENTO ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\SistemaRetifica\\src\\Relatorio\\"
                    + "01.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

}
