package Relatorio;

import CONEXAO.ConexaoOracle;
import Classes.ClasseOrdem;
import Classes.ClasseOrdemAssociativa;
import Classes.ClasseOrdemProdutos;
import Classes.ClasseVenda;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class NotaVenda {

    ConexaoOracle conecta_oracle;

    public NotaVenda() {
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);
    }

    public void Geral(ClasseVenda obj) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT V.ID_VENDA, C.ID_CLIENTE, P.NOME,"
                    + " TO_CHAR(V.DATA_VENDA,'DD/MM/YYYY')AS DATA,"
                    + " TO_CHAR(V.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " V.TOTAL_VENDA,"
                    + " VP.ID_PRODUTO,P.DS_PRODUTO,VP.QUANT,VP.VL_UNITARIO,VP.VL_TOTAL,V.DESCR"
                    + " FROM VENDA V"
                    + " JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                    + " JOIN VENDA_PRODUTOS VP ON V.ID_VENDA = VP.ID_VENDA"
                    + " LEFT JOIN CAD_PRODUTO P ON VP.ID_PRODUTO = P.ID_PRODUTO"
                    + " WHERE V.ID_VENDA = " + obj.getID_VENDA()
                    + " AND VP.CODIGO = P.CODIGO"
                    + " ORDER BY P.DS_PRODUTO ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "Venda.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void OrdemGeral(int Codigo) {

        String SQL = "SELECT O.ID_ORDEM, CP.NOME, CP.TEL_FIXO,CP.NUM_CASA,CP.CIDADE,CP.ESTADO,O.OPERACAO,"
                + "CP.TEL_CEL,LOGRADOURO,TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + "TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,CP.DS_PRODUTO,"
                + "TO_CHAR(OP.VL_UNITARIO,'999G999G999D99') AS VALOR_UNIT,OP.QUANT, "
                + "TO_CHAR(OP.VL_TOTAL,'999G999G999D99') AS VALOR_TOTAL,"
                + "(SELECT TO_CHAR((SUM(VALOR_TOTAL)),'999G999G999D99') FROM ORDEM WHERE ID_ORDEM = "+Codigo+ " ) AS TOTAL_GERAL "
                + "FROM ORDEM O "
                + "JOIN CAD_PESSOA CP ON O.ID_CLIENTE = CP.ID_PESSOA "
                + "JOIN ORDEM_PRODUTO OP ON O.ID_ORDEM = OP.ID_ORDEM "
                + "JOIN CAD_PRODUTO CP ON OP.ID_PRODUTO = CP.ID_PRODUTO WHERE O.ID_ORDEM = " + Codigo;

        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(conecta_oracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "Ordem.jasper", new HashMap(), jrRs);
            /*
            JasperViewer viewer = new JasperViewer(jasperPrint, true);
           viewer.setVisible(true);
             */
            JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\Sistema-2019\\src\\Relatorio\\Ordem.pdf");
            Runtime.getRuntime().exec("cmd /c start C:\\Sistema-2019\\src\\Relatorio\\Ordem.pdf");

        } catch (JRException | IOException e) {
            System.out.println(e);

        }

    }

    public int Cont(ClasseOrdemAssociativa obj) {
        conecta_oracle.conecta(0);
        int A = 0;
        String SQL = "SELECT COUNT (ID_PRODUTO) AS CONT "
                + " FROM ORDEM_PRODUTO WHERE ID_ORDEM = " + obj.getID_ORDEM();

        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            A = conecta_oracle.resultSet.getInt("CONT");

        } catch (Exception ex) {
            A = 0;
        }
        return A;

    }

    public void OrdemProduto(ClasseOrdem obj) {

        conecta_oracle.conecta(0);
        String SQL = "SELECT DISTINCT O.ID_ORDEM, C.ID_CLIENTE, P.NOME,"
                + "TO_CHAR(DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,P.TEL_FIXO,P.TEL_CEL,"
                + "O.VALOR_TOTAL,"
                + "PO.CODIGO,PO.DS_PRODUTO,OP.QUANT,OP.VL_UNITARIO, OP.VL_TOTAL,OP.TOTAL_GERAL,O.DESCR"
                + " FROM ORDEM O JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " JOIN ORDEM_PRODUTO OP ON O.ID_ORDEM = OP.ID_ORDEM"
                + " JOIN CAD_PRODUTO PO ON OP.ID_PRODUTO = PO.CODIGO"
                + " WHERE O.ID_ORDEM = " + obj.getID_ORDEM()
                + " ORDER BY PO.DS_PRODUTO ASC";
        try {
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "OrdemProduto.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(false);
            try {
                JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\Sistema-2019\\src\\Relatorio\\"
                        + "OrdemProduto.pdf");
                Runtime.getRuntime().exec("cmd /c start C:\\Sistema-2019\\src\\Relatorio\\"
                        + "OrdemProduto.pdf");
                File file = new File("C:\\Sistema-2019\\src\\Relatorio\\"
                        + "OrdemProduto.pdf");
                file.deleteOnExit();
            } catch (JRException e) {
                System.out.println(e);
            }
        } catch (Exception erro) {
            System.out.println(erro);
        }

    }

    public void RelatorioCliente(ClasseOrdem obj) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE() + ") AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE() + " ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }

    }

    public void RelatorioOrdem() {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO,(SELECT SUM(OS.VALOR_TOTAL) FROM ORDEM OS) AS SOMA FROM ORDEM O"
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }

    }

    public void RelatorioOrdemAberta() {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO FROM ORDEM O"
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE O.SITUACAO LIKE 'ABERTA'"
                    + " ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }

    }

    public void RelatorioOrdemFechada() {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO,(SELECT SUM(OS.VALOR_TOTAL) FROM ORDEM OS WHERE OS.SITUACAO LIKE 'FECHADA') AS SOMA"
                    + " FROM ORDEM O "
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE O.SITUACAO LIKE 'FECHADA' "
                    + " ORDER BY O.ID_ORDEM ASC";

            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }

    }

    public void RelatorioOrdemData(String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO FROM ORDEM O"
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE DATA_ABERTA BETWEEN  '" + A + "' AND '" + B + "' "
                    + " ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioOrdemDataAberta(String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO FROM ORDEM O"
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE DATA_ABERTA BETWEEN  '" + A + "' AND '" + B + "' AND"
                    + " O.SITUACAO = 'ABERTA'"
                    + " ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioOrdemDataFechada(String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "SELECT O.ID_ORDEM, C.ID_CLIENTE, P.NOME, TO_CHAR(O.DATA_ABERTA,'DD//MM/YYYY') AS DATA_ABERTA, "
                    + " NVL(TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY'),' ') AS DATA_FECHA,"
                    + " O.VALOR_TOTAL, O.SITUACAO FROM ORDEM O"
                    + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA "
                    + " WHERE DATA_ABERTA BETWEEN  '" + A + "' AND '" + B + "' AND"
                    + " O.SITUACAO = 'FECHADA'"
                    + " ORDER BY O.ID_ORDEM ASC";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdem.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioClienteFechada(ClasseOrdem obj) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "(SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE() + " AND SITUACAO = 'FECHADA') "
                    + " AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE() + ""
                    + " AND O.SITUACAO = 'FECHADA')";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioClienteAberta(ClasseOrdem obj) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "(SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE() + " AND SITUACAO = 'ABERTA' ) AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE() + ""
                    + " AND O.SITUACAO = 'ABERTA')";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioClienteAbertaData(ClasseOrdem obj, String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "(SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE() + " AND SITUACAO = 'ABERTA' "
                    + " AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "') AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE()
                    + " AND O.SITUACAO = 'ABERTA' AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "')";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioClienteFechadaData(ClasseOrdem obj, String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "(SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE() + " AND SITUACAO = 'FECHADA' "
                    + " AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "') AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE()
                    + " AND O.SITUACAO = 'FECHADA' AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "')";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }

    public void RelatorioClienteData(ClasseOrdem obj, String A, String B) {
        try {
            conecta_oracle.conecta(0);
            String SQL = "(SELECT O.ID_ORDEM, O.ID_CLIENTE,TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA,"
                    + " P.NOME,"
                    + " TO_CHAR (O.DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA, TO_CHAR(O.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                    + " O.DESCR,O.SITUACAO,O.ID_CARRO,O.PERC,O.VALOR_BRUTO,O.VALOR_TOTAL,"
                    + "(SELECT SUM(VALOR_TOTAL) FROM ORDEM "
                    + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE()
                    + " AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "') AS VALOR_GERAL FROM ORDEM O"
                    + " JOIN CLIENTE C ON C.ID_CLIENTE = O.ID_CLIENTE"
                    + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                    + " WHERE O.ID_CLIENTE = " + obj.getID_CLIENTE()
                    + " AND DATA_ABERTA BETWEEN '" + A + "' AND '" + B + "')";
            conecta_oracle.executeSQL(SQL);
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio\\"
                    + "RelatorioOrdemCliente.jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);

        } catch (Exception erro) {
            System.out.println(erro);
        }
    }
}
