package Relatorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class RelVendas extends javax.swing.JFrame {

    private SimpleDateFormat sdf;
    public Date Data_Inicial;
    public Date Data_Final;
    public String TesteData;
    RelatorioVendas RO;

    public RelVendas() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JCData = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        JTFData_Inicial = new javax.swing.JFormattedTextField();
        JTFData_Final = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        JCliente = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        JTFCodCliente = new javax.swing.JTextField();
        JBPesquisaCliente = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        JTFCliente = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFCPF_CNPJ = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        JBConverter_Trans = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JCData1 = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        JTFData_Inicial1 = new javax.swing.JFormattedTextField();
        JTFData_Final1 = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        JCliente1 = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();
        JTFCodCliente1 = new javax.swing.JTextField();
        JBPesquisaCliente1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        JTFCliente1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        JTFCPF_CNPJ1 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        JBConverter_Trans1 = new javax.swing.JButton();

        JCData.setText("Período");
        JCData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCDataActionPerformed(evt);
            }
        });

        jLabel2.setText("Data Inicial");

        JTFData_Inicial.setEditable(false);
        try {
            JTFData_Inicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JTFData_Final.setEditable(false);
        try {
            JTFData_Final.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Data Final");

        JCliente.setText("Cliente");
        JCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JClienteActionPerformed(evt);
            }
        });

        jLabel4.setText("Cliente");

        JTFCodCliente.setEditable(false);

        JBPesquisaCliente.setText("Pesquisar");
        JBPesquisaCliente.setEnabled(false);
        JBPesquisaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaClienteActionPerformed(evt);
            }
        });

        jLabel5.setText("Nome");

        JTFCliente.setEditable(false);

        jLabel6.setText("CPF/CNPJ");

        JTFCPF_CNPJ.setEditable(false);

        jPanel3.setBackground(new java.awt.Color(153, 153, 153));

        JBConverter_Trans.setText("Gerar Relatório");
        JBConverter_Trans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBConverter_TransActionPerformed(evt);
            }
        });
        jPanel3.add(JBConverter_Trans);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório de Vendas");
        setResizable(false);

        jLabel1.setText("Escolha os Filtros Desejados");

        JCData1.setText("Período");
        JCData1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCData1ActionPerformed(evt);
            }
        });

        jLabel7.setText("Data Inicial");

        JTFData_Inicial1.setEditable(false);
        try {
            JTFData_Inicial1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JTFData_Final1.setEditable(false);
        try {
            JTFData_Final1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel8.setText("Data Final");

        JCliente1.setText("Cliente");
        JCliente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCliente1ActionPerformed(evt);
            }
        });

        jLabel9.setText("Cliente");

        JTFCodCliente1.setEditable(false);

        JBPesquisaCliente1.setText("Pesquisar");
        JBPesquisaCliente1.setEnabled(false);
        JBPesquisaCliente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaCliente1ActionPerformed(evt);
            }
        });

        jLabel10.setText("Nome");

        JTFCliente1.setEditable(false);

        jLabel11.setText("CPF/CNPJ");

        JTFCPF_CNPJ1.setEditable(false);

        jPanel4.setBackground(new java.awt.Color(153, 153, 153));

        JBConverter_Trans1.setText("Gerar Relatório");
        JBConverter_Trans1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBConverter_Trans1ActionPerformed(evt);
            }
        });
        jPanel4.add(JBConverter_Trans1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JCData1)
                            .addComponent(jLabel1)
                            .addComponent(JCliente1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(JTFData_Inicial1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(90, 90, 90)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(JTFData_Final1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel8)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(91, 91, 91)
                                        .addComponent(jLabel10))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFCodCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JBPesquisaCliente1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFCPF_CNPJ1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(0, 93, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addComponent(JCData1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFData_Inicial1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData_Final1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(JCliente1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBPesquisaCliente1)
                    .addComponent(JTFCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCPF_CNPJ1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCDataActionPerformed
        if (JCData.isSelected() == true) {
            JTFData_Final.setEditable(true);
            JTFData_Inicial.setEditable(true);
        } else if (JCData.isSelected() == false) {
            JTFData_Final.setEditable(false);
            JTFData_Inicial.setEditable(false);
            JTFData_Final.setText(null);
            JTFData_Inicial.setText(null);
        }
    }//GEN-LAST:event_JCDataActionPerformed

    private void JClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JClienteActionPerformed
        if (JCliente.isSelected() == true) {
            JBPesquisaCliente.setEnabled(true);
        } else if (JCliente.isSelected() == false) {
            JTFCodCliente.setText("");
            JTFCliente.setText("");
            JTFCPF_CNPJ.setText("");
            JBPesquisaCliente.setEnabled(false);
        }
    }//GEN-LAST:event_JClienteActionPerformed

    private void JBPesquisaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaClienteActionPerformed

    }//GEN-LAST:event_JBPesquisaClienteActionPerformed

    private void JBConverter_TransActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBConverter_TransActionPerformed

        if ((JCData.isSelected() == true)
                && (JCliente.isSelected() == false)) {
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            RO = new RelatorioVendas();
            RO.Geral(JTFData_Inicial.getText(), JTFData_Final.getText());
        } else if ((JCData.isSelected() == false) && (JCliente.isSelected() == true)) {
            RO = new RelatorioVendas();
            RO.Geral(Integer.parseInt(JTFCodCliente.getText()));
        } else if ((JCData.isSelected() == true)
                && (JCliente.isSelected() == true)) {
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            RO = new RelatorioVendas();
            RO.Geral(Integer.parseInt(JTFCodCliente.getText()), JTFData_Inicial.getText(), JTFData_Final.getText());
        } else if ((JCData.isSelected() == false) && (JCliente.isSelected() == false)) {
            RO = new RelatorioVendas();
            RO.Geral();
        }
    }//GEN-LAST:event_JBConverter_TransActionPerformed

    private void JCData1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCData1ActionPerformed

        if (JCData1.isSelected() == true) {
            JTFData_Final1.setEditable(true);
            JTFData_Inicial1.setEditable(true);
        } else if (JCData1.isSelected() == false) {
            JTFData_Final1.setEditable(false);
            JTFData_Inicial1.setEditable(false);
            JTFData_Final1.setText(null);
            JTFData_Inicial1.setText(null);
        }
    }//GEN-LAST:event_JCData1ActionPerformed

    private void JCliente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCliente1ActionPerformed
        if (JCliente1.isSelected() == true) {
            JBPesquisaCliente1.setEnabled(true);
        } else if (JCliente1.isSelected() == false) {
            JTFCodCliente1.setText("");
            JTFCliente1.setText("");
            JTFCPF_CNPJ1.setText("");
            JBPesquisaCliente1.setEnabled(false);
        }
    }//GEN-LAST:event_JCliente1ActionPerformed

    private void JBPesquisaCliente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaCliente1ActionPerformed
        final ConsultarClienteVenda Pesquisa = new ConsultarClienteVenda();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodCliente1.setText(Pesquisa.Codigo);
                    JTFCliente1.setText(Pesquisa.Nome);
                }
            }
        });


    }//GEN-LAST:event_JBPesquisaCliente1ActionPerformed

    private void JBConverter_Trans1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBConverter_Trans1ActionPerformed

        if ((JCData.isSelected() == true)
                && (JCliente.isSelected() == false)) {
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            RO = new RelatorioVendas();
            RO.Geral(JTFData_Inicial.getText(), JTFData_Final.getText());
        } else if ((JCData.isSelected() == false) && (JCliente.isSelected() == true)) {
            RO = new RelatorioVendas();
            RO.Geral(Integer.parseInt(JTFCodCliente.getText()));
        } else if ((JCData.isSelected() == true)
                && (JCliente.isSelected() == true)) {
            TesteData = JTFData_Inicial.getText();
            if (Validar_Data()) {
                TesteData = JTFData_Final.getText();
                if (Validar_Data()) {
                    try {
                        Data_Inicial = sdf.parse(JTFData_Inicial.getText());
                        Data_Final = sdf.parse(JTFData_Final.getText());
                        if (Data_Final.compareTo(Data_Inicial) < 0) {
                            JOptionPane.showMessageDialog(null, "Data Final deve ser Maior ou Igual a Data Inicial");
                            JTFData_Final.setText(null);
                            return;
                        } else if (Data_Inicial.compareTo(Data_Final) > 0) {
                            JOptionPane.showMessageDialog(null, "A Data Inicial deve ser Menor ou Igual a Data Final");
                            JTFData_Inicial.setText(null);
                            return;
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Data Final Inválida");
                    JTFData_Inicial.setText(null);
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data Inicial Inválida");
                JTFData_Inicial.setText(null);
                return;
            }
            RO = new RelatorioVendas();
            RO.Geral(Integer.parseInt(JTFCodCliente.getText()), JTFData_Inicial.getText(), JTFData_Final.getText());
        } else if ((JCData.isSelected() == false) && (JCliente.isSelected() == false)) {
            RO = new RelatorioVendas();
            RO.Geral();
        }
    }//GEN-LAST:event_JBConverter_Trans1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RelVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RelVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RelVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RelVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RelVendas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBConverter_Trans;
    private javax.swing.JButton JBConverter_Trans1;
    private javax.swing.JButton JBPesquisaCliente;
    private javax.swing.JButton JBPesquisaCliente1;
    private javax.swing.JCheckBox JCData;
    private javax.swing.JCheckBox JCData1;
    private javax.swing.JCheckBox JCliente;
    private javax.swing.JCheckBox JCliente1;
    private javax.swing.JTextField JTFCPF_CNPJ;
    private javax.swing.JTextField JTFCPF_CNPJ1;
    private javax.swing.JTextField JTFCliente;
    private javax.swing.JTextField JTFCliente1;
    private javax.swing.JTextField JTFCodCliente;
    private javax.swing.JTextField JTFCodCliente1;
    private javax.swing.JFormattedTextField JTFData_Final;
    private javax.swing.JFormattedTextField JTFData_Final1;
    private javax.swing.JFormattedTextField JTFData_Inicial;
    private javax.swing.JFormattedTextField JTFData_Inicial1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables

    public boolean Validar_Data() {
        try {
            Data_Inicial = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }
}
