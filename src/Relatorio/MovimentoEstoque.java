package Relatorio;

import CONEXAO.ConexaoOracle;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class MovimentoEstoque {

    ConexaoOracle conecta_oracle;

    public MovimentoEstoque(int Opcao, String Data_Inicial, String Data_Final, int Codigo) {
        conecta_oracle = new ConexaoOracle();
        if (Opcao == 1) { // Data
            try {
                conecta_oracle.conecta(0);
                String SQL = "SELECT M.ID_PRODUTO, P.DS_PRODUTO, P.MARCA, P.MODELO, P.TAMANHO, "
                        + " M.EST_ANTERIOR, M.EST_ATUAL,M.QUANT_MOVIDA, M.DESCR,TO_CHAR(M.DATA,'DD/MM/YYYY') AS DATA, P.DS_UNIDADE "
                        + " FROM MOV_PRODUTOS M JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO "
                        + " WHERE DATA BETWEEN '" + Data_Inicial + "' AND '" + Data_Final + "' ORDER BY DATA ASC";
                conecta_oracle.executeSQL(SQL);
                JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
                JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio"
                        + "\\02.Jasper", new HashMap(), jrRs);
                JasperViewer viewer = new JasperViewer(jasperPrint, false);
                viewer.setVisible(true);
            } catch (Exception erro) {
                System.out.println(erro);
            }

        } else if (Opcao == 2) { // Produto
            try {
                String SQL = "SELECT M.ID_PRODUTO, P.DS_PRODUTO, P.MARCA, P.MODELO, P.TAMANHO, "
                        + " M.EST_ANTERIOR, M.EST_ATUAL,M.QUANT_MOVIDA, M.DESCR,TO_CHAR(M.DATA,'DD/MM/YYYY') AS DATA, P.DS_UNIDADE "
                        + " FROM MOV_PRODUTOS M JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO "
                        + " WHERE M.ID_PRODUTO = "+Codigo+ " ORDER BY DATA DESC";
                conecta_oracle.conecta(0);
                conecta_oracle.executeSQL(SQL);
                JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
                JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio"
                        + "\\02.Jasper", new HashMap(), jrRs);
                JasperViewer viewer = new JasperViewer(jasperPrint, false);
                viewer.setVisible(true);
            } catch (Exception erro) {
                System.out.println(erro);
            }

        } else if (Opcao == 3) { //Produto e Data
            try {
                conecta_oracle.conecta(0);
                 String SQL = "SELECT M.ID_PRODUTO, P.DS_PRODUTO, P.MARCA, P.MODELO, P.TAMANHO, "
                        + " M.EST_ANTERIOR, M.EST_ATUAL,M.QUANT_MOVIDA, M.DESCR,TO_CHAR(M.DATA,'DD/MM/YYYY') AS DATA, "
                         + "P.DS_UNIDADE "
                        + " FROM MOV_PRODUTOS M JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO "
                        + " WHERE DATA BETWEEN '" + Data_Inicial + "' AND '" + Data_Final + "' AND M.ID_PRODUTO = " + Codigo + " ORDER BY DATA,"
                         + "P.DS_PRODUTO ASC";
                conecta_oracle.executeSQL(SQL);
                JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
                JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio"
                        + "\\02.Jasper", new HashMap(), jrRs);
                JasperViewer viewer = new JasperViewer(jasperPrint, false);
                viewer.setVisible(true);
            } catch (Exception erro) {
                System.out.println(erro);
            }

        } else if (Opcao == 0) {// NADA
            try {
                conecta_oracle.conecta(0);
                String SQL = "SELECT M.ID_PRODUTO, P.DS_PRODUTO, P.MARCA, P.MODELO, P.TAMANHO, "
                        + " M.EST_ANTERIOR, M.EST_ATUAL,M.QUANT_MOVIDA, M.DESCR,TO_CHAR(M.DATA,'DD/MM/YYYY') AS DATA, P.DS_UNIDADE "
                        + " FROM MOV_PRODUTOS M JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO "
                        + " ORDER BY DATA,P.DS_PRODUTO DESC";
                conecta_oracle.executeSQL(SQL);
                JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
                JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio"
                        + "\\02.Jasper", new HashMap(), jrRs);
                JasperViewer viewer = new JasperViewer(jasperPrint, false);
                viewer.setVisible(true);
            } catch (Exception erro) {
                System.out.println(erro);
            }
        }
    }
}
