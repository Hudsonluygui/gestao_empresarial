package Interfaces;

import Classes.ClasseOrdem;
import Classes.ClasseReceberVendas;
import Classes.ClasseVenda;
import Controles.ControleOrdem;
import Controles.ControleReceberOrdens;
import Controles.ControleReceberVendas;
import Controles.ControleVendas;
import Validações.ConverterDecimais;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class ParcelaOrdem_1 extends javax.swing.JFrame {
    
    ControleReceberOrdens Controle_ReceberOrdem;
    ControleVendas Controle_Venda;
    ClasseReceberVendas Classe_Receber;
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    LimparTabelas Limpar_Tabela;
    ConverterDecimais converter = new ConverterDecimais();
    ClasseVenda Classe_Venda;
    
    public static String Codigo;
    public static String Valor;
    public static String Data;
    public static String Descr;
//Receber Valores da Outra tela

    public String Codigo_Parcela = null;
    public Double Valor_Parcela;

    //Pegar o Código da Parcela da Tabela
    public ParcelaOrdem_1() {
        initComponents();
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        JTFCodOrdem.setText(Codigo);
        JTFData.setText(Data);
        JTFValorOrdem.setText(Valor);
        
        Limpar_Tabela = new LimparTabelas();
        Classe_Receber = new ClasseReceberVendas();
        Controle_ReceberOrdem = new ControleReceberOrdens();
        Controle_Venda = new ControleVendas();
        Classe_Venda = new ClasseVenda();
        
        Classe_Receber.setIDENTIF(JTFCodOrdem.getText());
        double Soma = Controle_ReceberOrdem.ValorPago(Classe_Receber);
        JTFValor_Pago.setText(String.valueOf(Soma));
        Classe_Receber.setDESCR("VENDA");
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_ReceberOrdem.PesquisarParcelas(Classe_Receber));
        
        Tamanho_Jtable(JTPesquisa, 0, 100);
        Tamanho_Jtable(JTPesquisa, 1, 150);
        Tamanho_Jtable(JTPesquisa, 2, 150);
        Tamanho_Jtable(JTPesquisa, 3, 150);
        Tamanho_Jtable(JTPesquisa, 4, 150);
        Tamanho_Jtable(JTPesquisa, 5, 150);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCodOrdem = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFData = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFValorOrdem = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFValor_Pago = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFValor_Recebido = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        JTFDataPaga = new javax.swing.JFormattedTextField();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        JCPercJuros = new javax.swing.JCheckBox();
        JTFPerc_Juros = new javax.swing.JTextField();
        JTFJuros = new javax.swing.JTextField();
        JCValorJuros = new javax.swing.JCheckBox();
        JCPercDesconto = new javax.swing.JCheckBox();
        JTFPercDesconto = new javax.swing.JTextField();
        JTFValorDesconto = new javax.swing.JTextField();
        JCValorDesconto = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        JTFTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        JTFDif = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Vendas");
        setResizable(false);

        jLabel1.setText("Venda Nº");

        JTFCodOrdem.setEditable(false);

        jLabel2.setText("Data Aberta");

        JTFData.setEditable(false);
        try {
            JTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Valor da Ordem");

        JTFValorOrdem.setEditable(false);

        jLabel4.setText("*Valor que o Cliente ja Pagou");

        JTFValor_Pago.setEditable(false);

        jLabel5.setText("Valor sendo Pago");

        JTFValor_Recebido.setEditable(false);

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Parcela Nº", "Valor", "Valor Pago", "Situação", "Data Vencimento", "Data Paga"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPesquisa);

        jLabel6.setText("Data Paga");

        JTFDataPaga.setEditable(false);
        try {
            JTFDataPaga.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBAlterar.setText("Liquidar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir Liq.");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        JCPercJuros.setText("% Juros");
        JCPercJuros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPercJurosActionPerformed(evt);
            }
        });

        JTFPerc_Juros.setEditable(false);
        JTFPerc_Juros.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFPerc_JurosFocusLost(evt);
            }
        });

        JTFJuros.setEditable(false);
        JTFJuros.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFJurosFocusLost(evt);
            }
        });

        JCValorJuros.setText("Valor Juros");
        JCValorJuros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCValorJurosActionPerformed(evt);
            }
        });

        JCPercDesconto.setText("% Desconto");
        JCPercDesconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPercDescontoActionPerformed(evt);
            }
        });

        JTFPercDesconto.setEditable(false);
        JTFPercDesconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFPercDescontoFocusLost(evt);
            }
        });

        JTFValorDesconto.setEditable(false);
        JTFValorDesconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFValorDescontoFocusLost(evt);
            }
        });

        JCValorDesconto.setText("Valor Desconto");
        JCValorDesconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCValorDescontoActionPerformed(evt);
            }
        });

        jLabel7.setText("Total");

        JTFTotal.setEditable(false);

        jLabel8.setText("Valor da Diferença");

        JTFDif.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 779, Short.MAX_VALUE)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(77, 77, 77)
                                        .addComponent(jLabel2))
                                    .addComponent(JTFValor_Recebido))
                                .addGap(24, 24, 24))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFCodOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                                .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(JTFValorOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(89, 89, 89)
                                .addComponent(jLabel4))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFDataPaga, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(JTFDif))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(113, 113, 113)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(JCValorDesconto)
                                    .addComponent(JTFValor_Pago, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JCPercJuros)
                            .addComponent(JTFPerc_Juros, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFJuros, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JCValorJuros))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(JTFPercDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(JTFValorDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(JCPercDesconto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(JTFTotal))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValorOrdem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValor_Pago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFValor_Recebido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataPaga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JCPercJuros)
                    .addComponent(JCValorJuros)
                    .addComponent(JCPercDesconto)
                    .addComponent(JCValorDesconto)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPerc_Juros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFJuros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFPercDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValorDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        int index = JTPesquisa.getSelectedRow();
        
        String Valor = (String) JTPesquisa.getValueAt(index, 1);
        JTFTotal.setText(Valor);
        Valor_Parcela = Double.parseDouble(Valor);
        JTFValor_Recebido.setText(Valor);
        String Situacao = (String) JTPesquisa.getValueAt(index, 3);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        if (Situacao.equals("LIQUIDADA")) {
            String Data = (String) JTPesquisa.getValueAt(index, 5);
            JTFDataPaga.setText(Data);
            JTFDataPaga.setEditable(false);
            JTFValor_Recebido.setEditable(false);
            jBExcluir.setEnabled(true);
        } else {
            JTFDataPaga.setText(Controle_ReceberOrdem.Data());
            JTFDataPaga.setEditable(false);
            JTFValor_Recebido.setEditable(false);
            jBAlterar.setEnabled(true);
        }
        Codigo_Parcela = (String) JTPesquisa.getValueAt(index, 0);
        JTFTotal.setText(String.valueOf(Valor_Parcela));
        double Diferenca;
        Diferenca = Double.parseDouble(JTFValorOrdem.getText()) - Double.parseDouble(JTFValor_Pago.getText());
        JTFDif.setText(String.valueOf(Diferenca));
    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        JTFValor_Recebido.setEditable(true);
        JTFDataPaga.setEditable(true);
        jBAlterar.setEnabled(false);
        jBGravar.setEnabled(true);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        int index = JTPesquisa.getSelectedRow();
        String Data = (String) JTPesquisa.getValueAt(index, 4);
        
        Classe_Receber.setDATA_PAGO("");
        Classe_Receber.setVALOR_PAGO(0.00);
        Classe_Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
        Classe_Receber.setSITUACAO("ABERTA");
        Classe_Receber.setIDENTIF(JTFCodOrdem.getText());
        Classe_Receber.setDATA_VENCIMENTO(Data);
        Classe_Receber.setDESCR("VENDA");
        
        Controle_ReceberOrdem.Liquidar(Classe_Receber);
        
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_ReceberOrdem.PesquisarParcelas(Classe_Receber));
        
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        JTFDataPaga.setEditable(false);
        JTFValor_Recebido.setEditable(false);
        
        double Soma = Controle_ReceberOrdem.ValorPago(Classe_Receber);
        
        JTFValor_Pago.setText(String.valueOf(Soma));
        double Diferenca;
        Diferenca = Double.parseDouble(JTFValorOrdem.getText()) - Double.parseDouble(JTFValor_Pago.getText());
        JTFDif.setText(String.valueOf(Diferenca));
        
        ControleReceberVendas Controle_Receber = new ControleReceberVendas();
        
        Classe_Venda.setID_VENDA(Integer.parseInt(JTFCodOrdem.getText()));
        int Contar_Parc = Controle_Receber.Contar_Parcelas(Classe_Receber);
        int Contar_Pago = Controle_Receber.Contar_Pagos(Classe_Receber);
        if (Contar_Pago == Contar_Parc) {
            Classe_Venda.setPAGO("S");
            Controle_Venda.Pago(Classe_Venda);
        } else {
            Classe_Venda.setPAGO("N");
            Controle_Venda.Pago(Classe_Venda);
        }
        JTFTotal.setText("");
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        double A, B = 0;
        A = Double.parseDouble(JTFValor_Recebido.getText());
        B = Double.parseDouble(JTFTotal.getText());
        double Ordem = Controle_ReceberOrdem.ValorOrdem(Classe_Receber);
        if (A < B) {
            Classe_Receber.setIDENTIF(JTFCodOrdem.getText());
            Classe_Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
            Classe_Receber.setSITUACAO("ABERTA");
            Classe_Receber.setVALOR_PARCELA(B);
            Classe_Receber.setVALOR_PAGO(A);
            Classe_Receber.setDATA_PAGO(JTFDataPaga.getText());
            if (Controle_ReceberOrdem.Alterar(Classe_Receber)) {
                JOptionPane.showMessageDialog(null, "Valor ALterado");
            }
            Controle_ReceberOrdem.AlterarValor(Ordem, Integer.parseInt(Classe_Receber.getIDENTIF()));
            
        } else {
            if (Controle_ReceberOrdem.Liquidar(GetCampos())) {
                JOptionPane.showMessageDialog(null, "Valor Liquidado");
                
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu algum Erro");
            }
        }
        ClasseOrdem Classe_Ordem = new ClasseOrdem();
        ControleOrdem Controle_Ordem = new ControleOrdem();
        
        int Contar_Parc = Controle_ReceberOrdem.Contar_Parcelas(Classe_Receber);
        int Contar_Pago = Controle_ReceberOrdem.Contar_Pagos(Classe_Receber);
        Classe_Venda.setID_VENDA(Integer.parseInt(JTFCodOrdem.getText()));
        if (Contar_Pago == Contar_Parc) {
            Classe_Venda.setPAGO("S");
            Controle_Venda.Pago(Classe_Venda);
        } else {
            Classe_Venda.setPAGO("N");
            Controle_Venda.Pago(Classe_Venda);
        }
        
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_ReceberOrdem.PesquisarParcelas(Classe_Receber));
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        JTFDataPaga.setEditable(false);
        JTFValor_Recebido.setEditable(false);
        double Soma = Controle_ReceberOrdem.ValorPago(Classe_Receber);
        JTFValor_Pago.setText(String.valueOf(Soma));
        double Diferenca;
        Diferenca = Double.parseDouble(JTFValorOrdem.getText()) - Double.parseDouble(JTFValor_Pago.getText());
        JTFDif.setText(String.valueOf(Diferenca));
        JTFTotal.setText("");

    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        this.dispose();

    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JCPercJurosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPercJurosActionPerformed
        
        if (JCPercJuros.isSelected() == true) {
            JTFPerc_Juros.setEditable(true);
            JTFJuros.setEditable(false);
            JCValorJuros.setSelected(false);
        } else if (JCPercJuros.isSelected() == false) {
            JTFPerc_Juros.setEditable(false);
            double valor;
            if (!JTFJuros.equals("")) {
                valor = Double.parseDouble(JTFJuros.getText());
                valor = Double.parseDouble(JTFTotal.getText()) - valor;
                JTFTotal.setText(String.valueOf(valor));
                JTFPerc_Juros.setText("");
                JTFJuros.setText("");
            }
        }

    }//GEN-LAST:event_JCPercJurosActionPerformed

    private void JCValorJurosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCValorJurosActionPerformed
        
        if (JCValorJuros.isSelected() == true) {
            JTFPerc_Juros.setEditable(false);
            JTFJuros.setEditable(true);
            JCPercJuros.setSelected(false);
        } else if (JCValorJuros.isSelected() == false) {
            JTFJuros.setEditable(false);
            double valor;
            if (!JTFJuros.equals("")) {
                valor = Double.parseDouble(JTFJuros.getText());
                valor = Double.parseDouble(JTFTotal.getText()) - valor;
                JTFTotal.setText(String.valueOf(valor));
                JTFPerc_Juros.setText("");
                JTFJuros.setText("");
            }
        }
    }//GEN-LAST:event_JCValorJurosActionPerformed

    private void JCPercDescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPercDescontoActionPerformed
        
        if (JCPercDesconto.isSelected() == true) {
            JTFPercDesconto.setEditable(true);
            JTFValorDesconto.setEditable(false);
            JCValorDesconto.setSelected(false);
        } else if (JCPercDesconto.isSelected() == false) {
            JTFPercDesconto.setEditable(false);
            double valor;
            if (!JTFValorDesconto.equals("")) {
                valor = Double.parseDouble(JTFValorDesconto.getText());
                valor = Double.parseDouble(JTFTotal.getText()) + valor;
                JTFTotal.setText(String.valueOf(valor));
                JTFPercDesconto.setText("");
                JTFValorDesconto.setText("");
            }
            
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JCPercDescontoActionPerformed

    private void JCValorDescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCValorDescontoActionPerformed
        
        if (JCValorDesconto.isSelected() == true) {
            JTFValorDesconto.setEditable(true);
            JTFPercDesconto.setEditable(false);
            JCPercDesconto.setSelected(false);
        } else if (JCValorDesconto.isSelected() == false) {
            JTFValorDesconto.setEditable(false);
            double valor;
            if (!JTFValorDesconto.equals("")) {
                valor = Double.parseDouble(JTFValorDesconto.getText());
                valor = Double.parseDouble(JTFTotal.getText()) + valor;
                JTFTotal.setText(String.valueOf(valor));
                JTFPercDesconto.setText("");
                JTFValorDesconto.setText("");
            }
        }

    }//GEN-LAST:event_JCValorDescontoActionPerformed

    private void JTFPerc_JurosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFPerc_JurosFocusLost
        double Perc = Double.parseDouble(JTFPerc_Juros.getText().replace(",", "."));
        if ((Perc != 0.00) && (JCPercJuros.isSelected() == true)) {
            double valor = 0;
            try {
                Perc = Double.parseDouble(JTFPerc_Juros.getText().replaceAll(",", "."));
                valor = Valor_Parcela;
                valor = valor * Perc / 100;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFJuros.setText(String.valueOf(valor));
                Perc = Double.parseDouble(JTFJuros.getText().replace(",", "."));
                valor = Valor_Parcela;
                valor = valor + Perc;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFTotal.setText(String.valueOf(valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                JTFJuros.setText("");
                JTFJuros.requestFocus();
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFPerc_JurosFocusLost

    private void JTFJurosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFJurosFocusLost
        
        if ((!JTFJuros.getText().equals("") && (JCValorJuros.isSelected() == true))) {
            try {
                double Valor = Valor_Parcela;
                double Juros = Double.parseDouble(JTFJuros.getText().replaceAll(",", "."));
                double Perc = 0;
                Juros = Juros * 100;
                Perc = Juros / Valor;
                Perc = converter.converterDoubleDoisDecimais(Perc);
                JTFPerc_Juros.setText(String.valueOf(Perc));
                Juros = Juros / 100;
                Valor = Valor + Juros;
                Valor = converter.converterDoubleDoisDecimais(Valor);
                JTFTotal.setText(String.valueOf(Valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                return;
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFJurosFocusLost

    private void JTFPercDescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFPercDescontoFocusLost
        if (!JTFPercDesconto.equals("")) {
            double Desconto = Double.parseDouble(JTFPercDesconto.getText().replace(",", "."));
            if ((Desconto != 0.00) && (JCPercDesconto.isSelected() == true)) {
                double Perc = 0;
                double valor = 0;
                try {
                    Perc = Double.parseDouble(JTFPercDesconto.getText().replaceAll(",", "."));
                    valor = Double.parseDouble(JTFTotal.getText());
                    valor = valor * Perc / 100;
                    valor = converter.converterDoubleDoisDecimais(valor);
                    JTFValorDesconto.setText(String.valueOf(valor));
                    Perc = Double.parseDouble(JTFValorDesconto.getText().replace(",", "."));
                    valor = Double.parseDouble(JTFTotal.getText());
                    valor = valor - Perc;
                    valor = converter.converterDoubleDoisDecimais(valor);
                    JTFTotal.setText(String.valueOf(valor));
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                    JTFPercDesconto.setText("");
                    JTFPercDesconto.requestFocus();
                }
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFPercDescontoFocusLost

    private void JTFValorDescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFValorDescontoFocusLost
        
        if ((!JTFValorDesconto.getText().equals("") && (JCValorDesconto.isSelected() == true))) {
            try {
                double Valor = Double.parseDouble(JTFTotal.getText().replaceAll(",", "."));
                double Desconto = Double.parseDouble(JTFValorDesconto.getText().replaceAll(",", "."));
                double Perc = 0;
                Desconto = Desconto * 100;
                Perc = Desconto / Valor;
                Perc = converter.converterDoubleDoisDecimais(Perc);
                JTFPercDesconto.setText(String.valueOf(Perc));
                Desconto = Desconto / 100;
                Valor = Valor - Desconto;
                Valor = converter.converterDoubleDoisDecimais(Valor);
                JTFTotal.setText(String.valueOf(Valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                JTFValorDesconto.setText("");
                JTFValorDesconto.requestFocus();
                return;
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFValorDescontoFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ParcelaOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ParcelaOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ParcelaOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ParcelaOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ParcelaOrdem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox JCPercDesconto;
    private javax.swing.JCheckBox JCPercJuros;
    private javax.swing.JCheckBox JCValorDesconto;
    private javax.swing.JCheckBox JCValorJuros;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JTextField JTFCodOrdem;
    private javax.swing.JFormattedTextField JTFData;
    private javax.swing.JFormattedTextField JTFDataPaga;
    private javax.swing.JTextField JTFDif;
    private javax.swing.JTextField JTFJuros;
    private javax.swing.JTextField JTFPercDesconto;
    private javax.swing.JTextField JTFPerc_Juros;
    private javax.swing.JTextField JTFTotal;
    private javax.swing.JTextField JTFValorDesconto;
    private javax.swing.JTextField JTFValorOrdem;
    private javax.swing.JTextField JTFValor_Pago;
    private javax.swing.JTextField JTFValor_Recebido;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
    
    public ClasseReceberVendas GetCampos() {
        Classe_Receber.setDATA_PAGO(JTFDataPaga.getText());
        Classe_Receber.setVALOR_PAGO(Double.parseDouble(JTFValor_Recebido.getText()));
        Classe_Receber.setIDENTIF(JTFCodOrdem.getText());
        Classe_Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
        Classe_Receber.setSITUACAO("LIQUIDADA");
        Classe_Receber.setDESCR("VENDA");
        return Classe_Receber;
    }
}
