package Interfaces;

import Classes.ClasseMovProdutos;
import Classes.ClasseProdutos;
import Controles.ControleMovProdutos;
import Controles.ControleProdutos;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class CadastroProdutos extends javax.swing.JFrame {

    public boolean CodigoExistente = false;
    public int Inclusao;
    public int estado = Rotinas.Padrao;
    public int CodigoBanco = 0;

    private ClasseProdutos Classe_Produtos;
    private ClasseMovProdutos Classe_Movimento;
    private ControleMovProdutos Controle_Mov;
    private ControleProdutos Controle_Produtos;
    private LimparTabelas Limpar_Tabela = new LimparTabelas();

    ConverterDecimais converter = new ConverterDecimais();
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();

    public CadastroProdutos() {
        initComponents();
        Valida_Botao.ValidaCamposCancelar(JPainel_Cadastro, JPAINELBOTAO);
        JBAlterar.setEnabled(false);
        Classe_Produtos = new ClasseProdutos();
        Classe_Movimento = new ClasseMovProdutos();
        Controle_Mov = new ControleMovProdutos();
        Controle_Produtos = new ControleProdutos();

        JCPerc.setEnabled(false);
        JCValor.setEnabled(false);

        Botao();
        Tamanho_Jtable(JTConsultaProd, 0, 100);
        Tamanho_Jtable(JTConsultaProd, 1, 250);
        Tamanho_Jtable(JTConsultaProd, 2, 150);
        Tamanho_Jtable(JTConsultaProd, 3, 150);
        Tamanho_Jtable(JTConsultaProd, 4, 150);
        Tamanho_Jtable(JTConsultaProd, 5, 120);
        Tamanho_Jtable(JTConsultaProd, 6, 120);
        Tamanho_Jtable(JTConsultaProd, 7, 120);
        Tamanho_Jtable(JTConsultaProd, 8, 100);
        Tamanho_Jtable(JTConsultaProd, 9, 120);
        Tamanho_Jtable(JTConsultaProd, 10, 100);
        Tamanho_Jtable(JTConsultaProd, 11, 120);
        Double_Click();

        Inf.setText(" ");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPainel_Cadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCodProduto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFDescr = new javax.swing.JTextField();
        JLPesquisa = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        JTFQuant = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFVL_Custo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFTotal = new javax.swing.JTextField();
        JTFPerc = new javax.swing.JTextField();
        JTFVenda = new javax.swing.JTextField();
        JCPerc = new javax.swing.JCheckBox();
        JCValor = new javax.swing.JCheckBox();
        jLabel6 = new javax.swing.JLabel();
        JTFUnidade = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        JTFTamanho = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        JTFData = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        JTFMarca = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        JTFModelo = new javax.swing.JTextField();
        JLCodigo = new javax.swing.JLabel();
        Inf = new javax.swing.JLabel();
        JPainel_Consulta = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        JBPesquisa = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTConsultaProd = new javax.swing.JTable();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        JBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastrar Produtos");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Código");

        JTFCodProduto.setEditable(false);
        JTFCodProduto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCodProdutoFocusLost(evt);
            }
        });

        jLabel2.setText("Descrição");

        JLPesquisa.setText(" ");

        jLabel3.setText("Quantidade");

        JTFQuant.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                JTFQuantFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFQuantFocusLost(evt);
            }
        });

        jLabel4.setText("Valor Unitário(Custo)");

        JTFVL_Custo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFVL_CustoFocusLost(evt);
            }
        });

        jLabel5.setText("<--Valor Total");

        JTFTotal.setEditable(false);

        JTFPerc.setEditable(false);
        JTFPerc.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFPercFocusLost(evt);
            }
        });

        JTFVenda.setEditable(false);
        JTFVenda.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFVendaFocusLost(evt);
            }
        });

        JCPerc.setText("% Lucro");
        JCPerc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPercActionPerformed(evt);
            }
        });

        JCValor.setText("Valor de Venda");
        JCValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCValorActionPerformed(evt);
            }
        });

        jLabel6.setText("Unidade de Medida");

        jLabel7.setText("Tamanho");

        jLabel8.setText("Data do Cadastro");

        JTFData.setEditable(false);
        try {
            JTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel9.setText("Marca");

        jLabel10.setText("Modelo");

        JLCodigo.setText(" ");

        Inf.setText("jLabel11");

        javax.swing.GroupLayout JPainel_CadastroLayout = new javax.swing.GroupLayout(JPainel_Cadastro);
        JPainel_Cadastro.setLayout(JPainel_CadastroLayout);
        JPainel_CadastroLayout.setHorizontalGroup(
            JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                        .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                .addComponent(JTFTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(Inf))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                        .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                    .addGap(393, 393, 393)
                                    .addComponent(JCPerc)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                    .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3)
                                        .addComponent(JTFQuant, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(JTFDescr)
                                        .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                            .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                                    .addComponent(jLabel4)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel5))
                                                .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                                    .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(JTFVL_Custo, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel9))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                                            .addComponent(JTFTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(JTFPerc, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jLabel10))))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(JCValor)
                                                .addComponent(JTFVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(0, 0, Short.MAX_VALUE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(92, 92, 92)
                                .addComponent(jLabel2)
                                .addGap(445, 445, 445)))
                        .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFUnidade)
                                    .addComponent(JTFData, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(JLCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(226, 226, 226)
                                .addComponent(JLPesquisa)
                                .addGap(164, 164, 164))
                            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel6))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
        );
        JPainel_CadastroLayout.setVerticalGroup(
            JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPainel_CadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel8)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JLPesquisa)
                    .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JLCodigo)
                    .addComponent(JTFDescr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JCPerc)
                    .addComponent(JCValor)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFVL_Custo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFPerc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_CadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 264, Short.MAX_VALUE)
                .addComponent(Inf)
                .addGap(139, 139, 139))
        );

        jTabbedPane1.addTab("Cadastro", JPainel_Cadastro);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Cód. Produto", "Descr. Produto" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        JBPesquisa.setText("Pesquisar");
        JBPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaActionPerformed(evt);
            }
        });

        JTConsultaProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código Produto", "Descrição", "Quantidade", "Valor Unitário", "Valor Total", "% Lucro", "Valor de Venda", "Marca", "Modelo", "Tamanho", "Unidade de Medida", "Data Cadastro", " "
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTConsultaProd.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTConsultaProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTConsultaProdMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTConsultaProd);

        javax.swing.GroupLayout JPainel_ConsultaLayout = new javax.swing.GroupLayout(JPainel_Consulta);
        JPainel_Consulta.setLayout(JPainel_ConsultaLayout);
        JPainel_ConsultaLayout.setHorizontalGroup(
            JPainel_ConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPainel_ConsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPainel_ConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPainel_ConsultaLayout.createSequentialGroup()
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JBPesquisa))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 971, Short.MAX_VALUE)
                    .addGroup(JPainel_ConsultaLayout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 860, Short.MAX_VALUE)))
                .addContainerGap())
        );
        JPainel_ConsultaLayout.setVerticalGroup(
            JPainel_ConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPainel_ConsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPainel_ConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBPesquisa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Consulta", JPainel_Consulta);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        JBAlterar.setText("Alterar");
        JBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(JBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, 1000, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addGap(18, 18, 18)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCValorActionPerformed
        if (JCValor.isSelected() == true) {
            JTFVenda.setEditable(true);
            JTFPerc.setEditable(false);
            JCPerc.setSelected(false);

        } else if (JCValor.isSelected() == false) {
            JTFVenda.setEditable(false);
        }

    }//GEN-LAST:event_JCValorActionPerformed

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        Inf.setText("");
        JTFCodProduto.requestFocus();
        estado = Rotinas.Incluir;
        limpar.LimparCampos(JPainel_Cadastro);
        Valida_Botao.ValidaCamposIncluir(JPainel_Cadastro, JPAINELBOTAO);
        JCPerc.setEnabled(true);
        JCValor.setEnabled(true);
        JTFCodProduto.setEditable(false);
        JLCodigo.setText(" ");
        Inclusao = 1;
        CodigoBanco = Controle_Produtos.Codigo(Classe_Produtos);
        JTFCodProduto.setText("");
        Classe_Produtos.setCODIGO(CodigoBanco);
        JTFData.setText(Controle_Produtos.Data());
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsultaProd);

        JTFCodProduto.setText(Controle_Produtos.Ultima().getUtl());
        JTFDescr.requestFocus();
        JTFMarca.setText(" ");
        JTFModelo.setText(" ");
        JTFUnidade.setText(" ");
        JTFTamanho.setText(" ");
    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar Exclusão?");
        if (opcao == JOptionPane.YES_OPTION) {
            int cont = Controle_Produtos.Contador(Classe_Produtos.getID_PRODUTO(), Classe_Produtos.getCODIGO());
            if ((cont == 1) || (cont == 0)) {
                if (Controle_Produtos.ExcluirProduto(Classe_Produtos)) {
                    JOptionPane.showMessageDialog(null, "Produto Excluido");
                } else {
                    JOptionPane.showMessageDialog(null, Classe_Produtos.getERRO());
                }
            } else {
                JOptionPane.showMessageDialog(null, "Produto não pôde ser removido pois já foi movimentado(Vendas/Ordens...)");
            }
        }
        LimparTUDO();

    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (JTFDescr.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe uma Descrição");
            JTFDescr.requestFocus();
            return;
        }
        if (JTFQuant.equals("")) {
            JOptionPane.showMessageDialog(null, "Informe a Quantidade");
            JTFDescr.requestFocus();
            return;
        }
        if (Inclusao == 2) {
            if (CodigoExistente == true) {
                int opcao = JOptionPane.showConfirmDialog(null, "Esse Registro e Estoque será Substituído, Deseja Continuar?");
                if (opcao == JOptionPane.YES_OPTION) {
                    if (Controle_Produtos.Alterar(GetCampos())) {
                        JOptionPane.showMessageDialog(null, "Registro Alterado");
                        CodigoExistente = false;
                    } else {
                        JOptionPane.showMessageDialog(null, Classe_Produtos.getERRO());
                        return;
                    }
                }
            }
        } else {
            if (Controle_Produtos.Cadastrar(GetCampos())) {
                JOptionPane.showMessageDialog(null, "Produto Cadastrado com Sucesso");
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu o Seguinte Erro: " + Classe_Produtos.getERRO());
                return;
            }

            LimparTUDO();
        }
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        LimparTUDO();


    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JCPercActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPercActionPerformed
        if (JCPerc.isSelected() == true) {
            JTFPerc.setEditable(true);
            JCValor.setSelected(false);
            JTFVenda.setEditable(false);
        } else {
            JTFPerc.setEditable(false);
        }

    }//GEN-LAST:event_JCPercActionPerformed

    private void JTFPercFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFPercFocusLost
        if ((!JTFPerc.getText().equals("")) && (JCPerc.isSelected() == true)) {
            try {
                double Juros = Double.parseDouble(JTFPerc.getText().replace(",", "."));
                double Perc = 0;
                double valor = 0;
                Perc = Double.parseDouble(JTFPerc.getText().replaceAll(",", "."));
                JTFPerc.setText(String.valueOf(Perc));
                valor = Double.parseDouble(JTFVL_Custo.getText());
                valor = valor * Perc / 100;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFVenda.setText(String.valueOf(valor));
                Perc = Double.parseDouble(JTFVenda.getText().replace(",", "."));
                valor = Double.parseDouble(JTFVL_Custo.getText());
                valor = valor + Perc;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFVenda.setText(String.valueOf(valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                JTFPerc.setText("");
                JTFPerc.requestFocus();
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JTFPercFocusLost

    private void JTFQuantFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFQuantFocusLost
        try {
            Double A = Double.parseDouble(JTFQuant.getText().replaceAll(",", "."));
            JTFQuant.setText(String.valueOf(A));
        } catch (NumberFormatException ex) {
            JTFQuant.setText("");
        }
        if (!JTFQuant.getText().equals("") && (!JTFVL_Custo.getText().equals(""))) {
            Double A, B;
            try {
                A = Double.parseDouble(JTFQuant.getText().replaceAll(",", "."));

                B = Double.parseDouble(JTFVL_Custo.getText().replaceAll(",", "."));
                A = A * B;
                JTFTotal.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Quantidade Inválida");
                JTFQuant.setText("");
                JTFQuant.requestFocus();
                return;
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JTFQuantFocusLost

    private void JTFVL_CustoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFVL_CustoFocusLost
        try {
            Double teste = Double.parseDouble(JTFVL_Custo.getText().replaceAll(",", "."));
        } catch (NumberFormatException ex) {
            JTFVL_Custo.setText("");
            return;
        }
        if (!JTFQuant.getText().equals("") && (!JTFVL_Custo.getText().equals(""))) {
            Double A, B;
            try {
                A = Double.parseDouble(JTFQuant.getText().replaceAll(",", "."));
                B = Double.parseDouble(JTFVL_Custo.getText().replaceAll(",", "."));
                JTFVL_Custo.setText(String.valueOf(B));
                A = A * B;
                A = converter.converterDoubleDoisDecimais(A);
                JTFTotal.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                JTFVL_Custo.setText("");
                JTFVL_Custo.requestFocus();
                return;
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFVL_CustoFocusLost

    private void JTFVendaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFVendaFocusLost
        if ((!JTFVenda.getText().equals("") && (JCValor.isSelected() == true))) {
            try {
                double Valor = Double.parseDouble(JTFVL_Custo.getText().replaceAll(",", "."));
                double Lucro = Double.parseDouble(JTFVenda.getText().replaceAll(",", "."));
                JTFVenda.setText(String.valueOf(Lucro));
                double Perc = 0;
                Lucro = Lucro * 100;
                Perc = (Lucro / Valor) - 100;
                Perc = converter.converterDoubleDoisDecimais(Perc);
                JTFPerc.setText(String.valueOf(Perc));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                return;
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFVendaFocusLost

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed
        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 4) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }

    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void JBPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaActionPerformed
        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(Integer.parseInt(JTFPesquisar.getText())));
                break;
            }
            case 2: {
                preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(JTFPesquisar.getText().toUpperCase()));
                break;
            }
        }


    }//GEN-LAST:event_JBPesquisaActionPerformed

    private void JTFCodProdutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCodProdutoFocusLost
        if (!JTFCodProduto.getText().equals("0")) {
            Inf.setText(" ");
            if (Inclusao == 1) {
                if (CodigoExistente = Controle_Produtos.Existe(JTFCodProduto.getText())) {
                    Inf.setText("*Código já Existe!");
                    Inf.setForeground(Color.red);
                    CodigoBanco = Integer.parseInt(JTFCodProduto.getText());
                    return;
                } else {
                    Inf.setText(" ");
                    CodigoExistente = false;
                }
            }
        } else {
            JTFCodProduto.setText("");
            JTFCodProduto.requestFocus();
            Inf.setText("Código não pode ser zero!");
            Inf.setForeground(Color.red);
        }

    }//GEN-LAST:event_JTFCodProdutoFocusLost

    private void JTConsultaProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTConsultaProdMouseClicked
        int index = JTConsultaProd.getSelectedRow();
        String CodigoProd = (String) JTConsultaProd.getValueAt(index, 0);
        String CodBD = (String) JTConsultaProd.getValueAt(index, 12);
        Classe_Produtos.setID_PRODUTO(CodigoProd);
        Classe_Produtos.setCODIGO(Integer.parseInt(CodBD));;
    }//GEN-LAST:event_JTConsultaProdMouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        LimparTUDO();
        JTFCodProduto.setText("");
        Inf.setText("");
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsultaProd);
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void JTFQuantFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFQuantFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFQuantFocusGained

    private void JBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBAlterarActionPerformed
        JTFCodProduto.setEditable(false);
        JCPerc.setEnabled(true);
        JCValor.setEnabled(true);
        if (JTFCodProduto.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");
        } else {
            CodigoExistente = true;
            Valida_Botao.ValidaCamposIncluir(JPainel_Cadastro, JPAINELBOTAO);
            estado = Rotinas.Alterar;
        }
        Inclusao = 2;
        // TODO add your handling code here:
    }//GEN-LAST:event_JBAlterarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroProdutos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Inf;
    private javax.swing.JButton JBAlterar;
    private javax.swing.JButton JBPesquisa;
    private javax.swing.JCheckBox JCPerc;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JCheckBox JCValor;
    private javax.swing.JLabel JLCodigo;
    private javax.swing.JLabel JLPesquisa;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPainel_Cadastro;
    private javax.swing.JPanel JPainel_Consulta;
    private javax.swing.JTable JTConsultaProd;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JFormattedTextField JTFData;
    private javax.swing.JTextField JTFDescr;
    private javax.swing.JTextField JTFMarca;
    private javax.swing.JTextField JTFModelo;
    private javax.swing.JTextField JTFPerc;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTextField JTFQuant;
    private javax.swing.JTextField JTFTamanho;
    private javax.swing.JTextField JTFTotal;
    private javax.swing.JTextField JTFUnidade;
    private javax.swing.JTextField JTFVL_Custo;
    private javax.swing.JTextField JTFVenda;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public ClasseProdutos GetCampos() {
        if (CodigoBanco != 0) {
            Classe_Produtos.setCODIGO(CodigoBanco);
        }

        Classe_Produtos.setDS_UNIDADE(JTFUnidade.getText().toUpperCase());
        Classe_Produtos.setTAMANHO(JTFTamanho.getText().toUpperCase());
        Classe_Produtos.setMARCA(JTFMarca.getText().toUpperCase());
        Classe_Produtos.setMODELO(JTFModelo.getText().toUpperCase());
        
        Classe_Produtos.setDS_PRODUTO(JTFDescr.getText().toUpperCase());

        Classe_Produtos.setPERC_LUCRO(Double.parseDouble(JTFPerc.getText()));
        Classe_Produtos.setQUANT(Double.parseDouble(JTFQuant.getText()));
        Classe_Produtos.setTOTAL_CUSTO(Double.parseDouble(JTFTotal.getText()));
        Classe_Produtos.setVL_CUSTO(Double.parseDouble(JTFVL_Custo.getText()));
        Classe_Produtos.setVL_VENDA(Double.parseDouble(JTFVenda.getText()));
        Classe_Produtos.setID_PRODUTO(JTFCodProduto.getText().toUpperCase());
        Classe_Produtos.setDATA_CADASTRO(JTFData.getText());
        return Classe_Produtos;
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Botao() {
        JBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        JCPerc.setEnabled(false);
        JCValor.setEnabled(false);
    }

    public void retornar(String Cod) {
        Controle_Produtos.Recuperar(getPosicao(Cod));
        jTabbedPane1.setSelectedIndex(0);
        JTFPesquisar.setText(null);
        JTFPesquisar.setEditable(false);
        Valida_Botao.ValidaCamposCancelar(JPainel_Cadastro, JPAINELBOTAO);
        JCPesquisa.setSelectedIndex(0x0);
        JTFData.setEditable(false);
        JTFData.setText(Controle_Produtos.Pesquisar_Data(Classe_Produtos.getID_PRODUTO()));
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsultaProd);
        JPainel_Cadastro.isShowing();
        JTFCodProduto.setText(String.valueOf(Classe_Produtos.getID_PRODUTO()));
        JTFDescr.setText(Classe_Produtos.getDS_PRODUTO());
        JTFMarca.setText(Classe_Produtos.getMARCA());
        JTFModelo.setText(Classe_Produtos.getMODELO());
        JTFPerc.setText(String.valueOf(Classe_Produtos.getPERC_LUCRO()));
        JTFQuant.setText(String.valueOf(Classe_Produtos.getQUANT()));
        JTFTamanho.setText(Classe_Produtos.getTAMANHO());
        JTFTotal.setText(String.valueOf(Classe_Produtos.getTOTAL_CUSTO()));
        JTFUnidade.setText(Classe_Produtos.getDS_UNIDADE());
        JTFVL_Custo.setText(String.valueOf(Classe_Produtos.getVL_CUSTO()));
        JTFVenda.setText(String.valueOf(Classe_Produtos.getVL_VENDA()));

    }

    public ClasseProdutos getPosicao(String posicao) {
        Classe_Produtos.setID_PRODUTO(posicao);
        return Classe_Produtos;
    }

    public void Double_Click() {
        JTConsultaProd.addMouseListener(
                new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                // Se o botão direito do mouse foi pressionado  
                if (e.getClickCount() == 2) {
                    //Se for 2x
                    retornar(String.valueOf(Classe_Produtos.getID_PRODUTO()));
                }
            }
        });
    }

    public void LimparTUDO() {
        Inclusao = 0;
        Valida_Botao.ValidaCamposCancelar(JPainel_Cadastro, JPAINELBOTAO);
        Botao();
        limpar.LimparCampos(JPainel_Cadastro);
        JCPerc.setEnabled(false);
        JCValor.setEnabled(false);
        JTFDescr.requestFocus();
        JTFCodProduto.setEditable(true);
        JLCodigo.setText(" ");
    }

}
