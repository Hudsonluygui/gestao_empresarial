package Interfaces;

import Classes.ClasseFornecedor;
import Classes.ClasseFornecedorProdutos;
import Classes.ClasseJuridica;
import Classes.ClassePessoa;
import Classes.ClasseProdutos;
import Controles.ControleFornecedor;
import Controles.ControleFornecedorProdutos;
import Controles.ControleJuridica;
import Controles.ControlePessoa;
import Controles.ControleProdutos;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import Validações.ValidarCNPJ;
import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CadastroFornecedor extends javax.swing.JFrame {

    public int inclusao = 0;
    public int Contador = 0;
    public int Cont_Banco = 0;
    public int teste_remover = 0;
    public int CODIGO = 0;
    public int Vetor_Codigo[];
    public int Vetor_Codigo2[];
    public int Vetor_Produto[];
    public int Vetor_Produto2[];
    public boolean Valida_CNPJ = false;
    public boolean Teste_Geral = false;
    public int estado = Rotinas.Padrao;
    //
    private final ValidarCNPJ Validar_CNPJ;
    ClasseFornecedorProdutos Classe_FornecedorProdutos;
    ControleFornecedorProdutos Controle_FornProdutos;
    ClasseFornecedor Classe_Fornecedor;
    ClassePessoa Classe_Pessoa;
    ClasseJuridica Classe_Juridica;
    ControleFornecedor Controle_Fornecedor;
    ControleJuridica Controle_Juridica;
    ControlePessoa Controle_Pessoa;
    ClasseProdutos Classe_Produtos;
    ControleProdutos Controle_Produtos;
    //
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();

    LimparTabelas Limpar_Tabela;

    public CadastroFornecedor() {
        initComponents();

        Controle_Produtos = new ControleProdutos();
        Classe_Produtos = new ClasseProdutos();
        Validar_CNPJ = new ValidarCNPJ();
        Classe_Fornecedor = new ClasseFornecedor();
        Classe_Pessoa = new ClassePessoa();
        Classe_Juridica = new ClasseJuridica();
        Controle_Fornecedor = new ControleFornecedor();
        Controle_Pessoa = new ControlePessoa();
        Controle_Juridica = new ControleJuridica();
        Classe_FornecedorProdutos = new ClasseFornecedorProdutos();
        Controle_FornProdutos = new ControleFornecedorProdutos();
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPProdutos, JPAINELBOTAO);
        //   Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Double_Click();
        Botao();
        Limpar_Tabela = new LimparTabelas();
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Codigo2 = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
        Cont_Banco = Controle_Produtos.Criar_Vetor();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPCadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCod = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFLogradouro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFNum = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFBairro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFCidade = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        JTFCEP = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        JTFEmail = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        JTFTelFixo = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        JTFTel_Cel = new javax.swing.JFormattedTextField();
        JLCNPJ = new javax.swing.JLabel();
        JTFCNPJ = new javax.swing.JFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        JTFRazao = new javax.swing.JTextField();
        JPProdutos = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        JTFCodProduto = new javax.swing.JTextField();
        JBPesquisa = new javax.swing.JButton();
        JTFDescrProduto = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        JTFUnidade = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        JTFTamanho = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        JTFMarca = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        JTFModelo = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTProdutos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        JTPesquisaProdutos = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastrar Fornecedor");

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Código do Fornecedor");

        JTFCod.setEditable(false);

        jLabel2.setText("Nome Fantasia");

        jLabel3.setText("Endereço");

        jLabel4.setText("Número");

        jLabel5.setText("Bairro");

        jLabel6.setText("Cidade");

        jLabel7.setText("CEP");

        try {
            JTFCEP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel8.setText("Email");

        jLabel9.setText("Telefone");

        try {
            JTFTelFixo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel10.setText("Tel. 2");

        try {
            JTFTel_Cel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JLCNPJ.setText("CNPJ");

        try {
            JTFCNPJ.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        JTFCNPJ.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCNPJFocusLost(evt);
            }
        });

        jLabel12.setText("Razão Social");

        javax.swing.GroupLayout JPCadastroLayout = new javax.swing.GroupLayout(JPCadastro);
        JPCadastro.setLayout(JPCadastroLayout);
        JPCadastroLayout.setHorizontalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(JTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFTelFixo, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFTel_Cel, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JTFNome)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(JTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFRazao))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(JTFBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFCidade))
                            .addComponent(JTFLogradouro)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(JTFCod, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(203, 203, 203)
                                        .addComponent(jLabel6))
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(206, 206, 206)
                                        .addComponent(jLabel9))
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addComponent(JLCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(73, 73, 73)
                                        .addComponent(jLabel12)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(JTFNum, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel7)
                                .addComponent(JTFCEP, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                            .addComponent(jLabel4))))
                .addContainerGap())
        );
        JPCadastroLayout.setVerticalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jLabel1)
                .addGap(6, 6, 6)
                .addComponent(JTFCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jLabel7)))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(JTFCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JTFTelFixo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(JTFTel_Cel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JLCNPJ)
                    .addComponent(jLabel12))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFRazao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jTabbedPane1.addTab("Cadastro", JPCadastro);

        jLabel13.setText("Código do Produto");

        JTFCodProduto.setEditable(false);

        JBPesquisa.setText("Pesquisar");
        JBPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaActionPerformed(evt);
            }
        });

        JTFDescrProduto.setEditable(false);

        jLabel14.setText("Produto");

        jLabel15.setText("Unidade de Medida");

        JTFUnidade.setEditable(false);
        JTFUnidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFUnidadeActionPerformed(evt);
            }
        });

        jLabel16.setText("Tamanho");

        JTFTamanho.setEditable(false);

        jLabel17.setText("Marca");

        JTFMarca.setEditable(false);

        jLabel18.setText("Modelo");

        JTFModelo.setEditable(false);

        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Adicionar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        JTProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Selecionar", "Cód. Produto", "Produto", "Unidade de Medida", "Tamanho", "Marca", "Modelo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(JTProdutos);

        javax.swing.GroupLayout JPProdutosLayout = new javax.swing.GroupLayout(JPProdutos);
        JPProdutos.setLayout(JPProdutosLayout);
        JPProdutosLayout.setHorizontalGroup(
            JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPProdutosLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPProdutosLayout.createSequentialGroup()
                        .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPProdutosLayout.createSequentialGroup()
                                .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JBPesquisa))
                            .addComponent(jLabel13)
                            .addComponent(JTFUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPProdutosLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel14)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(JPProdutosLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFDescrProduto)
                                    .addGroup(JPProdutosLayout.createSequentialGroup()
                                        .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(JTFTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel16))
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())))
                    .addGroup(JPProdutosLayout.createSequentialGroup()
                        .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(JPProdutosLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(245, 245, 245)
                                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(JTFMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFModelo)
                                    .addGroup(JPProdutosLayout.createSequentialGroup()
                                        .addComponent(jLabel18)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPProdutosLayout.createSequentialGroup()
                                        .addComponent(jButton2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                                        .addComponent(jButton1)))))
                        .addContainerGap())))
        );
        JPProdutosLayout.setVerticalGroup(
            JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPProdutosLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addGap(5, 5, 5)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBPesquisa)
                    .addComponent(JTFDescrProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18))
                .addGap(6, 6, 6)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPProdutosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Produtos", JPProdutos);

        jCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Cód. Fornecedor", "Nome Fantasia", "Razao Social", "CNPJ" }));
        jCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton3.setText("Pesquisar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código Fornecedor", "Nome Fantasia", "Razao Social", "Telefone 1", "Telefone 2", "Email"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTPesquisa);

        JTPesquisaProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código Produto", "Produto", "Unidade de Medida", "Tamanho", "Marca", "Modelo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(JTPesquisaProdutos);

        jLabel19.setText("Produtos Vendidos por este Fornecedor Selecionado");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addComponent(jButton3))
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Consulta", jPanel3);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JTFUnidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFUnidadeActionPerformed

// TODO add your handling code here:
    }//GEN-LAST:event_JTFUnidadeActionPerformed

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        inclusao = 1;
        JTFNome.requestFocus();
        estado = Rotinas.Incluir;
        limpar.LimparCampos(JPCadastro);
        limpar.LimparCampos(JPProdutos);
        Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposIncluir(JPProdutos, JPAINELBOTAO);
    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        inclusao = 2;
        Contador = 0;
        if (JTFCod.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");
        } else {
            Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
            Valida_Botao.ValidaCamposIncluir(JPProdutos, JPAINELBOTAO);
            estado = Rotinas.Alterar;
            JTProdutos.setEnabled(true);
            JTFNome.requestFocus();
            Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
            Vetor_Codigo2 = new int[Controle_Produtos.Criar_Vetor()];
            Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
            Cont_Banco = Controle_Produtos.Criar_Vetor();
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed

        Classe_Fornecedor.setID_FORNECEDOR(Integer.parseInt(JTFCod.getText()));
        Classe_Fornecedor.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmetne Excluir?");
        if (opcao == JOptionPane.YES_OPTION) {
            if (Controle_FornProdutos.Exluir_TUDO(Classe_FornecedorProdutos)) {
                Controle_Pessoa.Excluir(Classe_Pessoa);
                JOptionPane.showMessageDialog(null, "Registro Excluído com Sucesso");
                Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProdutos);
                Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
                Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
                limpar.LimparCampos(JPCadastro);
                limpar.LimparCampos(JPProdutos);
                Botao();
            } else {
                JOptionPane.showMessageDialog(null, "Cliente sendo utilizado em algum Cadastro/Movimento");
            }
        }


    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (inclusao == 1) {
            if (Controle_Pessoa.Cadastrar(getCampos())) {
                if (Controle_Juridica.Cadastrar(GetJuridica())) {
                    Classe_Fornecedor.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
                    if (Controle_Fornecedor.Cadastrar(Classe_Fornecedor)) {
                        JTFCod.setText(String.valueOf(Classe_Fornecedor.getID_FORNECEDOR()));
                        int linha = JTProdutos.getRowCount();
                        if (linha != 0) {
                            ControleFornecedorProdutos Gravar = GetProdutos();
                        }
                        JOptionPane.showMessageDialog(null, "Registro Incluso com Sucesso");

                        limpar.LimparCampos(JPCadastro);
                        limpar.LimparCampos(JPProdutos);
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro");
                return;
            }
        } else if (inclusao == 2) {
            if (Controle_Pessoa.Alterar(getCampos())) {
                int linha = JTProdutos.getRowCount();
                if (linha != 0) {
                    ControleFornecedorProdutos Alterar = GetProdutos();
                }
                JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
                limpar.LimparCampos(JPCadastro);
                limpar.LimparCampos(JPProdutos);
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro");
                return;
            }
        }
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Codigo2 = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
        Cont_Banco = Controle_Produtos.Criar_Vetor();
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPProdutos, JPAINELBOTAO);
        Botao();
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProdutos);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
        Contador = 0;
        JLCNPJ.setText("CNPJ");
        JLCNPJ.setForeground(Color.black);

    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPProdutos, JPAINELBOTAO);
        Botao();
        limpar.LimparCampos(JPCadastro);
        limpar.LimparCampos(JPProdutos);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProdutos);
        JTProdutos.setEnabled(false);
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Codigo2 = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
        Cont_Banco = Controle_Produtos.Criar_Vetor();
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JTFCNPJFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCNPJFocusLost
        if (!JTFCNPJ.getText().equals("  .   .   /    -  ")) {
            String CNPJ = Validar_CNPJ.Retirar_Pontos(JTFCNPJ.getText());
            if (Validar_CNPJ.Validar_CNPJ(CNPJ) == true) {
                JLCNPJ.setText("CNPJ OK");
                JLCNPJ.setForeground(Color.blue);
                Valida_CNPJ = true;
            } else {
                JLCNPJ.setForeground(Color.red);
                JLCNPJ.setText("*CNPJ Inválido!");
                Valida_CNPJ = false;
                return;
            }
        }
    }//GEN-LAST:event_JTFCNPJFocusLost

    private void JBPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaActionPerformed
        final ConsultaProdutos Pesquisa = new ConsultaProdutos();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    Classe_Produtos.setID_PRODUTO(Pesquisa.Codigo);
                    Classe_Produtos.setDATA_CADASTRO(Pesquisa.Data);
                    Classe_Produtos.setDS_PRODUTO(Pesquisa.Descr);
                    Classe_Produtos.setDS_UNIDADE(Pesquisa.Unidade_Medida);
                    Classe_Produtos.setMARCA(Pesquisa.Marca);
                    Classe_Produtos.setMODELO(Pesquisa.Modelo);
                    Classe_Produtos.setPERC_LUCRO(Double.parseDouble(Pesquisa.Perc_Lucro));
                    Classe_Produtos.setQUANT(Double.parseDouble(Pesquisa.Quantidade));
                    Classe_Produtos.setTAMANHO(Pesquisa.Tamanho);
                    Classe_Produtos.setTOTAL_CUSTO(Double.parseDouble(Pesquisa.Valor_Total));
                    Classe_Produtos.setVL_CUSTO(Double.parseDouble(Pesquisa.Valor_Unitario));
                    Classe_Produtos.setVL_VENDA(Double.parseDouble(Pesquisa.Valor_Venda));

                    Vetor_Produto[Contador] = Controle_Produtos.Pesquisar_Codigo(Classe_Produtos);
                    JTFCodProduto.setText(Pesquisa.Codigo);
                    JTFMarca.setText(Pesquisa.Marca);
                    JTFModelo.setText(Pesquisa.Modelo);
                    JTFUnidade.setText(Pesquisa.Unidade_Medida);
                    JTFTamanho.setText(Pesquisa.Tamanho);
                    JTFDescrProduto.setText(Pesquisa.Descr);
                    Contador++;
                }
            }
        });

// TODO add your handling code here:
    }//GEN-LAST:event_JBPesquisaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        Adicionar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        switch (jCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Fornecedor.PesquisarGeral());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquiasr");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Fornecedor.PesquisarCodigo(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Nome Fantasia");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Pessoa.PesquisaNomeFantasia(JTFPesquisar.getText().toUpperCase()));
                }
                break;
            }
            case 3: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe a Razao Social Cadastrada");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Juridica.PesquisarRazao(JTFPesquisar.getText().toUpperCase()));

                }
                break;
            }
            case 4: {
                String Pesquisar, Aux = null;
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o CNPJ");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    int tamanho = JTFPesquisar.getText().length();
                    if (tamanho > 14) {
                        Pesquisar = Validar_CNPJ.Retirar_Pontos(JTFPesquisar.getText());
                        Aux = Pesquisar;
                    } else {
                        Pesquisar = JTFPesquisar.getText();
                    }
                    if (Validar_CNPJ.Validar_CNPJ(Pesquisar) == false) {
                        JOptionPane.showMessageDialog(null, "CNPJ Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
                    } else {
                        if (Aux != null) {
                            Pesquisar = (Aux.substring(0, 2)
                                    + "." + Aux.substring(2, 5)
                                    + "." + Aux.substring(5, 8)
                                    + "/" + Aux.substring(8, 12)
                                    + "-" + Aux.substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Juridica.Pesquisar_CNPJ_Forn(Pesquisar));
                        } else {
                            Pesquisar = (JTFPesquisar.getText().substring(0, 2)
                                    + "." + JTFPesquisar.getText().substring(2, 5)
                                    + "." + JTFPesquisar.getText().substring(5, 8)
                                    + "/" + JTFPesquisar.getText().substring(8, 12)
                                    + "-" + JTFPesquisar.getText().substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Juridica.Pesquisar_CNPJ_Forn(Pesquisar));
                        }
                    }
                }
                break;
            }
        }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCPesquisaActionPerformed
        if (jCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jCPesquisa.getSelectedIndex() == 4) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(false);
        }
    }//GEN-LAST:event_jCPesquisaActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        int index = JTPesquisa.getSelectedRow();
        String Cod = (String) JTPesquisa.getValueAt(index, 0);
        Classe_FornecedorProdutos.setID_FORNECEDOR(Integer.parseInt(Cod));
        if (Controle_FornProdutos.PesquisarExistente(Classe_FornecedorProdutos)) {
            preencher.Preencher_JTableGenerico(JTPesquisaProdutos, Controle_FornProdutos.PesquisarProdutos(Integer.parseInt(Cod)));
        }

        Classe_Fornecedor.setID_FORNECEDOR(Integer.parseInt(Cod));
        Classe_Fornecedor.setID_PESSOA(Controle_Fornecedor.Pesquisar_Pessoa(Classe_Fornecedor));
        Classe_FornecedorProdutos.setID_FORNECEDOR(Classe_Fornecedor.getID_FORNECEDOR());
        Classe_FornecedorProdutos.setID_PESSOA(Classe_Fornecedor.getID_PESSOA());
        Cont_Banco = Controle_FornProdutos.Contador(Classe_FornecedorProdutos);
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Codigo = new int[Cont_Banco];
    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged

// TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        Remover_Produto();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBPesquisa;
    private javax.swing.JLabel JLCNPJ;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPCadastro;
    private javax.swing.JPanel JPProdutos;
    private javax.swing.JTextField JTFBairro;
    private javax.swing.JFormattedTextField JTFCEP;
    private javax.swing.JFormattedTextField JTFCNPJ;
    private javax.swing.JTextField JTFCidade;
    private javax.swing.JTextField JTFCod;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JTextField JTFDescrProduto;
    private javax.swing.JTextField JTFEmail;
    private javax.swing.JTextField JTFLogradouro;
    private javax.swing.JTextField JTFMarca;
    private javax.swing.JTextField JTFModelo;
    private javax.swing.JTextField JTFNome;
    private javax.swing.JTextField JTFNum;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTextField JTFRazao;
    private javax.swing.JTextField JTFTamanho;
    private javax.swing.JFormattedTextField JTFTelFixo;
    private javax.swing.JFormattedTextField JTFTel_Cel;
    private javax.swing.JTextField JTFUnidade;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JTable JTPesquisaProdutos;
    private javax.swing.JTable JTProdutos;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox jCPesquisa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
 public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Botao() {
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    public ClassePessoa getCampos() {
        int tamanho = 0;
        String teste = null;
        if (estado == Rotinas.Alterar) {
            Classe_Fornecedor.setID_FORNECEDOR(Integer.parseInt(JTFCod.getText()));
            Classe_FornecedorProdutos.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        }
        Classe_Pessoa.setNUM_CASA(JTFNum.getText());
        tamanho = JTFNome.getText().length();
        if (tamanho > 100) {
            teste = JTFNome.getText().substring(0, 60).toUpperCase();
            Classe_Pessoa.setNOME(teste);
        } else {
            Classe_Pessoa.setNOME(JTFNome.getText().toUpperCase());
        }
        tamanho = JTFBairro.getText().length();
        if (tamanho > 40) {
            teste = JTFBairro.getText().substring(0, 40).toUpperCase();
            Classe_Pessoa.setBAIRRO(teste);
        } else {
            Classe_Pessoa.setBAIRRO(JTFBairro.getText().toUpperCase());
        }
        tamanho = JTFEmail.getText().length();
        if (tamanho > 100) {
            teste = JTFEmail.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setEMAIL(teste);
        } else {
            Classe_Pessoa.setEMAIL(JTFEmail.getText().toUpperCase());
        }
        tamanho = JTFLogradouro.getText().length();
        if (tamanho > 100) {
            teste = JTFLogradouro.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setLOGRADOURO(teste);
        } else {
            Classe_Pessoa.setLOGRADOURO(JTFLogradouro.getText().toUpperCase());
        }
        tamanho = JTFCidade.getText().length();
        if (tamanho > 100) {
            teste = JTFCidade.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setCIDADE(teste);
        } else {
            Classe_Pessoa.setCIDADE(JTFCidade.getText().toUpperCase());
        }
        Classe_Juridica.setCNPJ(JTFCNPJ.getText());
        tamanho = JTFRazao.getText().length();
        if (tamanho > 100) {
            teste = JTFRazao.getText().substring(0, 100).toUpperCase();
            Classe_Juridica.setRAZAO(teste);
        } else {
            Classe_Juridica.setRAZAO(JTFRazao.getText().toUpperCase());
        }
        Classe_Pessoa.setTEL_CEL(JTFTel_Cel.getText());
        Classe_Pessoa.setTEL_FIXO(JTFTelFixo.getText());
        Classe_Pessoa.setTP_PESSOA("J");
        return Classe_Pessoa;
    }

    public ClasseJuridica GetJuridica() {
        Classe_Juridica.setCNPJ(JTFCNPJ.getText());
        Classe_Juridica.setRAZAO(JTFRazao.getText().toUpperCase());
        Classe_Juridica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        return Classe_Juridica;

    }

    public ControleFornecedorProdutos GetProdutos() {
        Contador = 0;
        int totlinha = JTProdutos.getRowCount();
        int i = 0;
        int x = 0;
        int Resultado = 0;
        for (i = 1; i <= totlinha; i++) {
            String cod = (String) JTProdutos.getValueAt(x, 1);
            int Codigo = Vetor_Produto[Contador];
            Classe_FornecedorProdutos.setCODIGO(Codigo);
            Classe_FornecedorProdutos.setID_FORNECEDOR(Integer.parseInt(JTFCod.getText()));
            Classe_FornecedorProdutos.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
            Classe_FornecedorProdutos.setID_PRODUTO(cod);
            if (inclusao == 2) {
                cod = (String) JTProdutos.getValueAt(x, 1);

                String Produto = (String) JTProdutos.getValueAt(x, 2);
                String Unidade = (String) JTProdutos.getValueAt(x, 3);
                String Tamanho = (String) JTProdutos.getValueAt(x, 4);
                String Marca = (String) JTProdutos.getValueAt(x, 5);
                String Modelo = (String) JTProdutos.getValueAt(x, 6);

                Classe_Produtos.setDS_PRODUTO(Produto);
                Classe_Produtos.setDS_UNIDADE(Unidade);
                Classe_Produtos.setTAMANHO(Tamanho);
                Classe_Produtos.setMARCA(Marca);
                Classe_Produtos.setMODELO(Modelo);
                Classe_Produtos.setID_PRODUTO(cod);
                int Codigo_Gerado = Controle_Produtos.Pesquisar_CodigoFornecedor(Classe_Produtos);
                Classe_FornecedorProdutos.setCODIGO(Codigo_Gerado);
            }
            Resultado = Controle_FornProdutos.PesquisarExistentes(Classe_FornecedorProdutos);
            if (Resultado != 0) {
                Controle_FornProdutos.Cadastrar(Classe_FornecedorProdutos);
            }

            Contador++;
            x = x + 1;
        }
        if (teste_remover != 0) {
            for (Contador = 0; Contador < Cont_Banco; Contador++) {
                if (Vetor_Codigo2[Contador] != 0) {
                    String Codigo_Produto = String.valueOf(Vetor_Codigo2[Contador]);
                    int CODIGO_Gerado = Vetor_Produto2[Contador];
                    Classe_FornecedorProdutos.setID_PRODUTO(Codigo_Produto);
                    Classe_FornecedorProdutos.setCODIGO(CODIGO_Gerado);
                    Classe_FornecedorProdutos.setID_FORNECEDOR(Integer.parseInt(JTFCod.getText()));
                    Resultado = Controle_FornProdutos.PesquisarExistentes(Classe_FornecedorProdutos);
                    if (Resultado == 0) {
                        if (!Controle_FornProdutos.RemoverProdeuto(Classe_FornecedorProdutos)) {
                            JOptionPane.showMessageDialog(null, "Registro sendo Utilizado em outro Cadastro/Movimento");
                            Teste_Geral = false;
                            break;
                        } else {
                            Teste_Geral = true;
                        }
                    }
                }
            }
        }
        return Controle_FornProdutos;
    }

    public void Adicionar() {
        if (JTFCodProduto.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Nada a ser Adicionado");
            return;
        } else {
            int conta = 0;
            DefaultTableModel TabelaTeste = (DefaultTableModel) JTProdutos.getModel();
            int totlinha = JTProdutos.getRowCount();    // pega quantiade de linhas da grid
            int i, vai = 0;
            int cod, index = 0;
            int x = JTProdutos.getRowCount();
            if (vai == 0) {
                TabelaTeste.setNumRows(x + 1);
                TabelaTeste.setValueAt(JTFCodProduto.getText(), x, 1);
                TabelaTeste.setValueAt(JTFDescrProduto.getText(), x, 2);
                TabelaTeste.setValueAt(JTFUnidade.getText(), x, 3);
                TabelaTeste.setValueAt(JTFTamanho.getText(), x, 4);
                TabelaTeste.setValueAt(JTFMarca.getText(), x, 5);
                TabelaTeste.setValueAt(JTFModelo.getText(), x, 6);
                TabelaTeste.setValueAt((false), x, 0);
                JTProdutos.grabFocus();
                JTFCodProduto.setText("");
                JTFDescrProduto.setText("");
                JTFUnidade.setText("");
                JTFTamanho.setText("");
                JTFMarca.setText("");
                JTFModelo.setText("");
            }
        }
    }

    public ClasseFornecedor getPosicao(String posicao) {
        Classe_Fornecedor.setID_FORNECEDOR(Integer.parseInt(posicao));
        return Classe_Fornecedor;
    }

    public void SetCampos(ClassePessoa obj, ClasseJuridica obj2) {
        JTFBairro.setText(obj.getBAIRRO());
        JTFCEP.setText(obj.getCEP());
        JTFCNPJ.setText(obj2.getCNPJ());
        JTFCidade.setText(obj.getCIDADE());
        JTFEmail.setText(obj.getEMAIL());
        JTFLogradouro.setText(obj.getLOGRADOURO());
        JTFNome.setText(obj.getNOME());
        JTFNum.setText(obj.getNUM_CASA());
        JTFRazao.setText(obj2.getRAZAO());
        JTFTelFixo.setText(obj.getTEL_FIXO());
        JTFTel_Cel.setText(obj.getTEL_CEL());
        JTFCNPJ.setEditable(false);
    }

    public void retornar() {
        Controle_Fornecedor.RecuperarFornecedor(Classe_Fornecedor, Classe_Pessoa);
        jCPesquisa.setSelectedIndex(0x0);
        if (Controle_FornProdutos.PesquisarExistente(Classe_FornecedorProdutos)) {
            preencher.Preencher_JTableGenerico(JTProdutos, Controle_FornProdutos.PesquisarProdutos2(Classe_Fornecedor.getID_FORNECEDOR()));
        }
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProdutos);
        JPCadastro.isShowing();
        Controle_Juridica.RetornarCNPJ_RAZAO(Classe_Fornecedor.getID_FORNECEDOR(), Classe_Juridica);
        JTFCod.setText(String.valueOf(Classe_Fornecedor.getID_FORNECEDOR()));
        SetCampos(Classe_Pessoa, Classe_Juridica);
        jTabbedPane1.setSelectedIndex(0);
        JTProdutos.setEnabled(false);
        JTFPesquisar.setText(null);
        JTFPesquisar.setEditable(false);
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Classe_Pessoa.setID_PESSOA(Classe_Fornecedor.getID_PESSOA());
        Classe_Juridica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        JTFCNPJFocusLost(null);

    }

    public void Double_Click() {
        JTPesquisa.addMouseListener(
                new java.awt.event.MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        // Se o botão direito do mouse foi pressionado  
                        if (e.getClickCount() == 2) {
                            //Se for 2x
                            retornar();
                        }
                    }
                });
    }

    public void Remover_Produto() {
        teste_remover = 1;
        DefaultTableModel tabela = (DefaultTableModel) JTProdutos.getModel();
        int totlinha = JTProdutos.getRowCount();    // pega quantiade de linhas da grid   
        int i = 0;
        int x = 0;
        int CODIGO = 0;
        String Cod = null;
        Boolean sel = false;
        for (i = totlinha - 1; i >= 0; i--) {
            Boolean selecionado = (Boolean) JTProdutos.getValueAt(i, 0);
            Cod = (String) JTProdutos.getValueAt(i, 1);
            if (selecionado == null) {
                selecionado = false;
            }
            if (selecionado == true) {
                sel = true;
                if (!JTFCod.getText().equals("")) {
                    String Codigo = (String) JTProdutos.getValueAt(i, 1);
                    String Produto = (String) JTProdutos.getValueAt(i, 2);
                    String Unidade = (String) JTProdutos.getValueAt(i, 3);
                    String Tamanho = (String) JTProdutos.getValueAt(i, 4);
                    String Marca = (String) JTProdutos.getValueAt(i, 5);
                    String Modelo = (String) JTProdutos.getValueAt(i, 6);

                    Classe_Produtos.setDS_PRODUTO(Produto);
                    Classe_Produtos.setDS_UNIDADE(Unidade);
                    Classe_Produtos.setTAMANHO(Tamanho);
                    Classe_Produtos.setMARCA(Marca);
                    Classe_Produtos.setMODELO(Modelo);
                    Classe_Produtos.setID_PRODUTO(Codigo);
                    int Codigo_Gerado = Controle_Produtos.Pesquisar_CodigoFornecedor(Classe_Produtos);
                    Classe_FornecedorProdutos.setCODIGO(Codigo_Gerado);
                    Classe_FornecedorProdutos.setID_PRODUTO(Classe_Produtos.getID_PRODUTO());
                    int Resultado = Controle_FornProdutos.PesquisarExistentes(Classe_FornecedorProdutos);
                    if (Resultado == 0) {
                        Vetor_Produto2[Contador] = Classe_FornecedorProdutos.getCODIGO();
                        Vetor_Codigo2[Contador] = Integer.parseInt(Codigo);
                    }
                }
                Contador++;
                tabela.removeRow(i);
            }
        }
        if (!sel == true) {
            JOptionPane.showMessageDialog(null, "Nao ha nenhum registro selecionado !");
        }
    }

}
