package Interfaces;

import Classes.ClasseProdutos;
import Controles.ControleProdutos;
import Validações.Preencher_JTableGenerico;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class ConsultaProdutos extends javax.swing.JFrame {

    int A = 0;
    private ControleProdutos Controle_Produtos;
    private ClasseProdutos Classe_Produtos;
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    String Codigo;
    String Descr;
    String Marca;
    String Modelo;
    String Unidade_Medida;
    String Tamanho;
    String Valor_Unitario;
    String Valor_Total;
    String Perc_Lucro;
    String Valor_Venda;
    String Data;
    String Quantidade;
    String Codigo_Banco;
    public static String Produto_;

    public ConsultaProdutos() {
        initComponents();
        Controle_Produtos = new ControleProdutos();
        Classe_Produtos = new ClasseProdutos();
        if (Produto_ == null) {
            preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar());
        } else {
            preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(Produto_.toUpperCase()));
        }
        Tamanho_Jtable(JTConsultaProd, 0, 100); //Código
        Tamanho_Jtable(JTConsultaProd, 1, 180); //Descrição
        Tamanho_Jtable(JTConsultaProd, 2, 100); //Quantidade
        Tamanho_Jtable(JTConsultaProd, 3, 100); //Valor Unitário
        Tamanho_Jtable(JTConsultaProd, 4, 100); //Valor Total
        Tamanho_Jtable(JTConsultaProd, 5, 100); // %Lucro
        Tamanho_Jtable(JTConsultaProd, 6, 100); //Valor de Venda
        Tamanho_Jtable(JTConsultaProd, 7, 100); //Data do Cadastro

        Double_Click();
        Enter(JTConsultaProd);
        Enter1(JTFPesquisar);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTConsultaProd = new javax.swing.JTable();
        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Produtos");
        setResizable(false);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Cód. Produto", "Descr. Produto" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTConsultaProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código Produto", "Descrição", "Quantidade", "Valor Unitário", "Valor Total", "% Lucro", "Valor de Venda", "Data Cadastro", "Cód. Banco"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTConsultaProd.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTConsultaProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTConsultaProdMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTConsultaProd);

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 897, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JTConsultaProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTConsultaProdMouseClicked
        int index = JTConsultaProd.getSelectedRow();
        Codigo = (String) JTConsultaProd.getValueAt(index, 0);
        Descr = (String) JTConsultaProd.getValueAt(index, 1);
        Quantidade = (String) JTConsultaProd.getValueAt(index, 2);
        Valor_Unitario = (String) JTConsultaProd.getValueAt(index, 3);
        Valor_Total = (String) JTConsultaProd.getValueAt(index, 4);
        Perc_Lucro = (String) JTConsultaProd.getValueAt(index, 5);
        Valor_Venda = (String) JTConsultaProd.getValueAt(index, 6);
        Data = (String) JTConsultaProd.getValueAt(index, 7);
        Codigo_Banco = (String) JTConsultaProd.getValueAt(index, 8);
        A = 1;
        Produto_ = JTFPesquisar.getText();

    }//GEN-LAST:event_JTConsultaProdMouseClicked

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 4) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
            JTFPesquisar.setText("");
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisa");
                    return;
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe a Descroção");
                    return;
                } else {
                    preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(JTFPesquisar.getText().toUpperCase()));
                }
                break;
            }

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultaProdutos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JTable JTConsultaProd;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Double_Click() {
        JTConsultaProd.addMouseListener(
                new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                // Se o botão direito do mouse foi pressionado  
                if (e.getClickCount() == 2) {
                    //Se for 2x
                    dispose();
                }
            }
        });
    }

    private void Enter(final JTable Tabela) {
        Tabela.getInputMap(Tabela.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "enterAction");
        Tabela.getActionMap().put("enterAction", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int index = JTConsultaProd.getSelectedRow();
                Codigo = (String) JTConsultaProd.getValueAt(index, 0);
                Descr = (String) JTConsultaProd.getValueAt(index, 1);
                Marca = (String) JTConsultaProd.getValueAt(index, 2);
                Modelo = (String) JTConsultaProd.getValueAt(index, 3);
                Unidade_Medida = (String) JTConsultaProd.getValueAt(index, 4);
                Tamanho = (String) JTConsultaProd.getValueAt(index, 5);
                Quantidade = (String) JTConsultaProd.getValueAt(index, 6);
                Valor_Unitario = (String) JTConsultaProd.getValueAt(index, 7);
                Valor_Total = (String) JTConsultaProd.getValueAt(index, 8);
                Perc_Lucro = (String) JTConsultaProd.getValueAt(index, 9);
                Valor_Venda = (String) JTConsultaProd.getValueAt(index, 10);
                Data = (String) JTConsultaProd.getValueAt(index, 11);
                Codigo_Banco = (String) JTConsultaProd.getValueAt(index, 12);
                A = 1;
                dispose();
            }
        });
    }

    private void Enter1(final JTextField campo) {
        campo.getInputMap(campo.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "enterAction");
        campo.getActionMap().put("enterAction", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                switch (JCPesquisa.getSelectedIndex()) {
                    case 0: {
                        preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar());
                        break;
                    }
                    case 1: {
                        if (JTFPesquisar.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisa");
                            return;
                        } else {
                            try {
                                preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(Integer.parseInt(JTFPesquisar.getText())));
                            } catch (NumberFormatException ex) {
                                JOptionPane.showMessageDialog(null, "Código Inválido");
                            }
                        }
                        break;
                    }
                    case 2: {
                        if (JTFPesquisar.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Informe a Descroção");
                            return;
                        } else {
                            preencher.Preencher_JTableGenerico(JTConsultaProd, Controle_Produtos.Pesquisar(JTFPesquisar.getText().toUpperCase()));
                        }
                        break;
                    }
                }
            }

        });
    }

}
