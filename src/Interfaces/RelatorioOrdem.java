package Interfaces;

import Classes.ClasseOrdem;
import Relatorio.NotaVenda;

public class RelatorioOrdem extends javax.swing.JFrame {
    
    NotaVenda Relatorio = new NotaVenda();
    ClasseOrdem Classe = new ClasseOrdem();

    /**
     * Creates new form RelatorioOrdem
     */
    public RelatorioOrdem() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        JCAberta = new javax.swing.JCheckBox();
        JCFechada = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        JCPeriodo = new javax.swing.JCheckBox();
        JTFDataInicio = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        JTFDataFInal = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        JBConverter_Trans1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        JTFCodCliente = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        JTFCliente = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório Ordens");

        JCAberta.setText("Ordens Abertas");

        JCFechada.setText("Ordens Fechadas");

        jLabel1.setText("Escolha os Filtros Desejados");

        JCPeriodo.setText("Período");
        JCPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPeriodoActionPerformed(evt);
            }
        });

        JTFDataInicio.setEditable(false);
        try {
            JTFDataInicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel2.setText("Data Inicial");

        jLabel3.setText("Data Final");

        JTFDataFInal.setEditable(false);
        try {
            JTFDataFInal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jPanel4.setBackground(new java.awt.Color(153, 153, 153));

        JBConverter_Trans1.setText("Gerar Relatório");
        JBConverter_Trans1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBConverter_Trans1ActionPerformed(evt);
            }
        });
        jPanel4.add(JBConverter_Trans1);

        jLabel4.setText("Cliente");

        JTFCodCliente.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JCPeriodo)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(JTFCodCliente, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JCAberta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(JTFDataInicio, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(93, 93, 93)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(JCFechada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3)
                                    .addComponent(JTFDataFInal))
                                .addGap(0, 172, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFCliente)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JCAberta)
                    .addComponent(JCFechada))
                .addGap(28, 28, 28)
                .addComponent(JCPeriodo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataFInal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(JTFCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JBConverter_Trans1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBConverter_Trans1ActionPerformed
        
        if (JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == false) {
            Relatorio.RelatorioOrdemAberta();
            return;
        }
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == false)
                && JCFechada.isSelected() == true && JCPeriodo.isSelected() == false) {
            Relatorio.RelatorioOrdemFechada();
            return;
        }
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == false)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == false) {
            Relatorio.RelatorioOrdem();
            return;
            
        }
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == true) && JCPeriodo.isSelected() == false) {
            Relatorio.RelatorioOrdem();
            return;
        }
        //ATE AQUI SEM DATA

        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == false)
                && JCFechada.isSelected() == false && (JCPeriodo.isSelected() == true)) {
            Relatorio.RelatorioOrdemData(JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
        
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == false)) {
            Relatorio.RelatorioOrdemDataAberta(JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == false
                && (JCFechada.isSelected() == true))) {
            Relatorio.RelatorioOrdemDataFechada(JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
        if (JTFCodCliente.getText().equals("") && (JCAberta.isSelected() == true)
                && JCFechada.isSelected() == true && (JCPeriodo.isSelected() == true)) {
            Relatorio.RelatorioOrdemData(JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
        //ATE AQUI SEM O CLIENTE
        
        if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == false)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == false) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioCliente(Classe);
            return;
        }
        if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == false) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteAberta(Classe);
            return;
        }if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == false)
                && (JCFechada.isSelected() == true) && JCPeriodo.isSelected() == false) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteFechada(Classe);
            return;
        }
        if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == true) && JCPeriodo.isSelected() == false) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioCliente(Classe);
            return;
        }  if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == true) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteAbertaData(Classe,JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
         if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == false)
                && (JCFechada.isSelected() == true) && JCPeriodo.isSelected() == true) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteFechadaData(Classe,JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        } if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == true)
                && (JCFechada.isSelected() == true) && JCPeriodo.isSelected() == true) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteData(Classe,JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        } if (!JTFCodCliente.getText().equals("")
                && (JCAberta.isSelected() == false)
                && (JCFechada.isSelected() == false) && JCPeriodo.isSelected() == true) {
            Classe.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
            Relatorio.RelatorioClienteData(Classe,JTFDataInicio.getText(), JTFDataFInal.getText());
            return;
        }
    }//GEN-LAST:event_JBConverter_Trans1ActionPerformed

    private void JCPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPeriodoActionPerformed
        if (JCPeriodo.isSelected() == false) {
            JTFDataInicio.setEditable(false);
            JTFDataFInal.setEditable(false);
        } else {
            JTFDataFInal.setEditable(true);
            JTFDataInicio.setEditable(true);
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JCPeriodoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        final ConsultarClienteRel Pesquisa = new ConsultarClienteRel();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodCliente.setText(Pesquisa.Cod);
                    JTFCliente.setText(Pesquisa.Nome);
                }
            }
        });

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RelatorioOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RelatorioOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RelatorioOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RelatorioOrdem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RelatorioOrdem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBConverter_Trans1;
    private javax.swing.JCheckBox JCAberta;
    private javax.swing.JCheckBox JCFechada;
    private javax.swing.JCheckBox JCPeriodo;
    private javax.swing.JTextField JTFCliente;
    private javax.swing.JTextField JTFCodCliente;
    private javax.swing.JFormattedTextField JTFDataFInal;
    private javax.swing.JFormattedTextField JTFDataInicio;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
