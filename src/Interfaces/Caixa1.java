
package Interfaces;

import Classes.ClasseCaixa;
import Controles.ControleCaixa;
import Validações.ConverterDecimais;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hudson
 */
public class Caixa1 extends javax.swing.JFrame {
   ControleCaixa Controle;
    ClasseCaixa Caixa;
    ConverterDecimais Converter;
    LimparTabelas Limpar_Tabela;
    Preencher_JTableGenerico preencher;
    int Gravado = 0;
    
    public Caixa1() {
        initComponents();
           Controle = new ControleCaixa();
        String data = Controle.Data();
        JTFData.setText(data);
        Caixa = new ClasseCaixa();
        Limpar_Tabela = new LimparTabelas();
        preencher = new Preencher_JTableGenerico();
        JBExcluir.setEnabled(false);
        
        //**************************************************************************//
        Tamanho_Jtable(JTCadastro, 0, 45);
        Tamanho_Jtable(JTCadastro, 1, 250);
        Tamanho_Jtable(JTCadastro, 2, 80);
        Tamanho_Jtable(JTCadastro, 3, 80);
        Tamanho_Jtable(JTCadastro, 4, 100);
        Tamanho_Jtable(JTCadastro, 5, 110);

        Tamanho_Jtable(JTPesquisa, 0, 320);
        Tamanho_Jtable(JTPesquisa, 1, 110);
        Tamanho_Jtable(JTPesquisa, 2, 100);
        Tamanho_Jtable(JTPesquisa, 3, 110);
        Tamanho_Jtable(JTPesquisa, 4, 110);
        Tamanho_Jtable(JTPesquisa, 5, 50);

        JREntrada.setSelected(true);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFDescr = new javax.swing.JTextField();
        JTFData = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        JTFValor = new javax.swing.JTextField();
        JREntrada = new javax.swing.JRadioButton();
        JRSaida = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTCadastro = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        JCForma = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        jButton5 = new javax.swing.JButton();
        JTFDATA_INICIO = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFData_FInal = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        JPAINELBOTAO1 = new javax.swing.JPanel();
        JBGravar = new javax.swing.JButton();
        JBExcluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Caixa");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Descrição");

        try {
            JTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel2.setText("Data");

        jLabel3.setText("Valor");

        JREntrada.setText("Entrada");

        JRSaida.setText("Saida");

        JTCadastro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sel.", "Descrição", "Valor", "Data", "Operação", "Forma"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(JTCadastro);

        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Adicionar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel6.setText("Forma");

        JCForma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DINHEIRO", "CHEQUE", "CARTÃO" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JREntrada)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JRSaida)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addGap(2, 2, 2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(JTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(9, 9, 9)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(JCForma, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(JTFDescr))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFData, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFDescr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JCForma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(JREntrada)
                            .addComponent(JRSaida)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton2)
                        .addComponent(jButton1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Dados", jPanel1);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Data" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Descrição", "Valor", "Data", "Operação", "Forma", "Nº"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTPesquisa);

        jButton5.setText("Pesquisar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        JTFDATA_INICIO.setEditable(false);
        try {
            JTFDATA_INICIO.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel4.setText("Data Inicial");

        JTFData_FInal.setEditable(false);
        try {
            JTFData_FInal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel5.setText("Data Final");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 777, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFDATA_INICIO, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5))
                            .addComponent(jLabel5)
                            .addComponent(JTFData_FInal, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFDATA_INICIO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFData_FInal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Consulta", jPanel2);

        JPAINELBOTAO1.setBackground(new java.awt.Color(153, 153, 153));

        JBGravar.setText("Gravar");
        JBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO1.add(JBGravar);

        JBExcluir.setText("Excluir");
        JBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO1.add(JBExcluir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(JPAINELBOTAO1, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jTabbedPane1)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(658, Short.MAX_VALUE)
                .addComponent(JPAINELBOTAO1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(63, Short.MAX_VALUE)))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
  int tamanho = JTCadastro.getRowCount();
        if (tamanho <= 0) {
            JOptionPane.showMessageDialog(null, "Nada a ser Removido");
            return;
        } else {
            Remover();
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

  if ((JREntrada.isSelected() == false) && (JRSaida.isSelected() == false)) {
            JOptionPane.showMessageDialog(null, "Informe se é Entrada ou Saida");
            return;
        }
        Double Valor;
        try {
            Valor = Double.parseDouble(JTFValor.getText());
            Valor = Converter.converterDoubleDoisDecimais(Valor);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "Valor Inválido");
            JTFValor.setText("");
            JTFValor.requestFocus();
            return;
        }
        Adicionar();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFDATA_INICIO.setEditable(true);
            JTFData_FInal.setEditable(true);
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFDATA_INICIO.setEditable(false);
            JTFData_FInal.setEditable(false);
            JTFDATA_INICIO.setText("");
            JTFData_FInal.setText("");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
 JBExcluir.setEnabled(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
  switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar(JTFDATA_INICIO.getText(), JTFData_FInal.getText()));
                break;
            }
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
  if (jPanel1.isShowing()) {
            JBExcluir.setEnabled(false);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        } else if (jPanel2.isShowing()) {
            JBExcluir.setEnabled(false);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
            JCPesquisa.setSelectedIndex(0x0);
        }        // TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void JBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBGravarActionPerformed

        int Tamanho = JTCadastro.getRowCount();
        if (Tamanho <= 0) {
            JOptionPane.showMessageDialog(null, "Nada a ser Gravado");
        } else {
            GetCampos();
            if (Gravado != 0) {
                JOptionPane.showMessageDialog(null, "Financeiro Gravado");
                JTFData.setText(null);
                JTFDescr.setText("");
                JTFValor.setText("");

                Limpar_Tabela.Limpar_tabela_pesquisa(JTCadastro);

            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JBGravarActionPerformed

    private void JBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBExcluirActionPerformed
int index = JTPesquisa.getSelectedRow();
        String Codigo = (String) JTPesquisa.getValueAt(index, 5);
        Caixa.setID_CAIXA(Integer.parseInt(Codigo));
        int opcao = JOptionPane.showConfirmDialog(null, "Confirmar Exclusão?");
        if (opcao == JOptionPane.YES_OPTION) {
            if (Controle.Excluir(Caixa)) {
                JOptionPane.showMessageDialog(null, "Registro Excluido!");
            }
        }
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle.Pesquisar());
        JBExcluir.setEnabled(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_JBExcluirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Caixa1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Caixa1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Caixa1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Caixa1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Caixa1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBExcluir;
    private javax.swing.JButton JBGravar;
    private javax.swing.JComboBox JCForma;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JPanel JPAINELBOTAO1;
    private javax.swing.JRadioButton JREntrada;
    private javax.swing.JRadioButton JRSaida;
    private javax.swing.JTable JTCadastro;
    private javax.swing.JFormattedTextField JTFDATA_INICIO;
    private javax.swing.JFormattedTextField JTFData;
    private javax.swing.JFormattedTextField JTFData_FInal;
    private javax.swing.JTextField JTFDescr;
    private javax.swing.JTextField JTFValor;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
 public void Adicionar() {
        DefaultTableModel Tabela = (DefaultTableModel) JTCadastro.getModel();
        int vai = 0;
        int X = Tabela.getRowCount();
        if (vai == 0) {
            Tabela.setNumRows(X + 1);
            Tabela.setValueAt(JTFDescr.getText().toUpperCase(), X, 1);
            Tabela.setValueAt(JTFValor.getText(), X, 2);
            Tabela.setValueAt(JTFData.getText(), X, 3);
            switch (JCForma.getSelectedIndex()) {
                case 0: {
                    Tabela.setValueAt("DINHEIRO", X, 5);
                    break;
                }
                case 1: {
                    Tabela.setValueAt("CHEQUE", X, 5);
                    break;
                }
                case 2: {
                    Tabela.setValueAt("CARTÃO", X, 5);
                    break;
                }
            }

            String Teste = null;
            if (JREntrada.isSelected() == true) {
                Teste = "ENTRADA";
            } else {
                Teste = "SAIDA";
            }
            Tabela.setValueAt(Teste, X, 4);
            JTFDescr.setText("");
            JTFValor.setText("");
            JTFDescr.requestFocus();
        }
    }

    public void Remover() {
        DefaultTableModel tabela = (DefaultTableModel) JTCadastro.getModel();
        int totlinha = tabela.getRowCount();    // pega quantiade de linhas da grid   
        int i = 0;
        for (i = totlinha - 1; i >= 0; i--) {
            Boolean selecionado = (Boolean) tabela.getValueAt(i, 0);
            if (selecionado == null) {
                selecionado = false;
            }
            if (selecionado == true) {
                tabela.removeRow(i);
            }
        }
    }

    public ControleCaixa GetCampos() {
        int totlinha = JTCadastro.getRowCount();
        int i = 0;
        int x = 0;
        for (i = 1; i <= totlinha; i++) {
            String Descr = (String) JTCadastro.getValueAt(x, 1);
            String Valor = (String) JTCadastro.getValueAt(x, 2);
            String Data = (String) JTCadastro.getValueAt(x, 3);
            String Operacao = (String) JTCadastro.getValueAt(x, 4);
            String Forma = (String) JTCadastro.getValueAt(x, 5);

            Caixa.setDATA(Data);
            Caixa.setDESCR(Descr.toUpperCase());
            Caixa.setOPERACAO(Operacao);
            Caixa.setVALOR(Double.parseDouble(Valor));
            Caixa.setFORMA(Forma.toUpperCase());
            if (Controle.Cadastrar(Caixa)) {
                Gravado = 1;
            } else {
                Gravado = 0;
                break;
            }
            x++;
        }
        return Controle;
    }

    public final void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
}
