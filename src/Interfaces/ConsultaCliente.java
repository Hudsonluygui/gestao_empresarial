package Interfaces;

import Classes.ClasseCliente;
import Controles.ControleCliente;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.ValidarCNPJ;
import Validações.ValidarCPF;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class ConsultaCliente extends javax.swing.JFrame {

    ClasseCliente Classe_Cliente;
    ControleCliente Controle_Cliente;
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    private final ValidarCPF Validar_CPF;
    private final ValidarCNPJ Validar_CNPJ;
    LimparTabelas Limpar_Tabela = new LimparTabelas();
    public static String Codigo = null;
    public static String Nome = null;
    public static String RG = null;
    public static String CPF = null;
    public static String CNPJ = null;
    public int A = 0;

    public ConsultaCliente() {
        initComponents();
        Validar_CNPJ = new ValidarCNPJ();
        Validar_CPF = new ValidarCPF();
        Classe_Cliente = new ClasseCliente();
        Controle_Cliente = new ControleCliente();
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisaGeral2());
        Tamanho_Jtable(JTPesquisa, 0, 50);
        Tamanho_Jtable(JTPesquisa, 1, 220);
        Tamanho_Jtable(JTPesquisa, 2, 150);
        Tamanho_Jtable(JTPesquisa, 3, 150);
        Tamanho_Jtable(JTPesquisa, 4, 150);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consultar Cliente");
        setResizable(false);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Código", "Nome", "CPF", "CNPJ", "RG" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "CPF", "CNPJ", "RG"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPesquisa);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        switch (jComboBox1.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisaGeral2());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisar");
                    JTFPesquisar.requestFocus();
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisaCodigo(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisar.requestFocus();
                        JTFPesquisar.setText("");
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Nome para Pesquisar");
                    JTFPesquisar.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarNome(JTFPesquisar.getText().toUpperCase()));
                }
                break;
            }
            case 3: {
                String Pesquisar, Aux = null;
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o CPF");
                    JTFPesquisar.requestFocus();
                } else {
                    int tamanho = JTFPesquisar.getText().length();
                    if (tamanho > 11) {
                        Pesquisar = Validar_CPF.Retirar_Pontos(JTFPesquisar.getText());
                        Aux = Pesquisar;
                    } else {
                        Pesquisar = JTFPesquisar.getText();
                    }
                    if (Validar_CPF.Validar_CPF(Pesquisar) == false) {
                        JOptionPane.showMessageDialog(null, "CPF Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);

                    } else {
                        if (Aux != null) {
                            Pesquisar = (Aux.substring(0, 3)
                                    + "." + Aux.substring(3, 6)
                                    + "." + Aux.substring(6, 9)
                                    + "-" + Aux.substring(9, 11));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarCPF(Pesquisar));
                        } else {
                            Pesquisar = (JTFPesquisar.getText().substring(0, 3)
                                    + "." + JTFPesquisar.getText().substring(3, 6)
                                    + "." + JTFPesquisar.getText().substring(6, 9)
                                    + "-" + JTFPesquisar.getText().substring(9, 11));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarCPF(Pesquisar));
                        }
                    }
                }
                break;
            }
            case 4: {
                String Pesquisar, Aux = null;
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o CNPJ");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    int tamanho = JTFPesquisar.getText().length();
                    if (tamanho > 14) {
                        Pesquisar = Validar_CNPJ.Retirar_Pontos(JTFPesquisar.getText());
                        Aux = Pesquisar;
                    } else {
                        Pesquisar = JTFPesquisar.getText();
                    }
                    if (Validar_CNPJ.Validar_CNPJ(Pesquisar) == false) {
                        JOptionPane.showMessageDialog(null, "CNPJ Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
                    } else {
                        if (Aux != null) {
                            Pesquisar = (Aux.substring(0, 2)
                                    + "." + Aux.substring(2, 5)
                                    + "." + Aux.substring(5, 8)
                                    + "/" + Aux.substring(8, 12)
                                    + "-" + Aux.substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarCNPJ(Pesquisar));
                        } else {
                            Pesquisar = (JTFPesquisar.getText().substring(0, 2)
                                    + "." + JTFPesquisar.getText().substring(2, 5)
                                    + "." + JTFPesquisar.getText().substring(5, 8)
                                    + "/" + JTFPesquisar.getText().substring(8, 12)
                                    + "-" + JTFPesquisar.getText().substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarCNPJ(Pesquisar));
                        }
                    }
                }
                break;
            }
            case 5: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o RG");
                    JTFPesquisar.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Cliente.PesquisarRG(JTFPesquisar.getText()));
                }
                break;
            }
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        if (jComboBox1.getSelectedIndex() == 1) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jComboBox1.getSelectedIndex() == 2) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jComboBox1.getSelectedIndex() == 3) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jComboBox1.getSelectedIndex() == 4) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jComboBox1.getSelectedIndex() == 5) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (jComboBox1.getSelectedIndex() == 0) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(false);
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        int index = JTPesquisa.getSelectedRow();
        Codigo = (String) JTPesquisa.getValueAt(index, 0);
        Nome = (String) JTPesquisa.getValueAt(index, 1);
        CPF = (String) JTPesquisa.getValueAt(index, 2);
        CNPJ = (String) JTPesquisa.getValueAt(index, 3);
        RG = (String) JTPesquisa.getValueAt(index, 4);
        A = 1;
        dispose();
    }//GEN-LAST:event_JTPesquisaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultaCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

}
