package Interfaces;

import Controles.ControleProdutos;
import Validações.Preencher_JTableGenerico;
import java.awt.HeadlessException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Hudson
 */
public class PesquisarProdutos extends javax.swing.JFrame {

    ControleProdutos Controle_Produtos;
    Preencher_JTableGenerico preencher;
    public static String Codigo;
    public static String Descr;
    public static String Quant;
    public static String CodigoBanco;
    public int A = 0;

    public PesquisarProdutos(ControleProdutos Controle_Produtos, Preencher_JTableGenerico preencher, JComboBox<String> JCPesquisa, JTextField JTFPesquisar, JTable JTPesquisa, JButton jButton1, JScrollPane jScrollPane1) throws HeadlessException {
        this.Controle_Produtos = Controle_Produtos;
        this.preencher = preencher;
        this.JCPesquisa = JCPesquisa;
        this.JTFPesquisar = JTFPesquisar;
        this.JTPesquisa = JTPesquisa;
        this.jButton1 = jButton1;
        this.jScrollPane1 = jScrollPane1;
    }

    public PesquisarProdutos() {
        initComponents();
        Controle_Produtos = new ControleProdutos();
        preencher = new Preencher_JTableGenerico();
        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Produtos.PesquisarProduto());

        Tamanho_Jtable(JTPesquisa, 0, 50);
        Tamanho_Jtable(JTPesquisa, 1, 350);
        Tamanho_Jtable(JTPesquisa, 2, 150);
        Tamanho_Jtable(JTPesquisa, 3, 150);
        Tamanho_Jtable(JTPesquisa, 4, 150);
        Tamanho_Jtable(JTPesquisa, 5, 0);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        JCPesquisa = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Pesquisar");
        setResizable(false);

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cód.", "Produto", "Qntd. Atual", "Valor Custo", "Valor Venda", "null"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTPesquisa);
        if (JTPesquisa.getColumnModel().getColumnCount() > 0) {
            JTPesquisa.getColumnModel().getColumn(5).setResizable(false);
        }

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Geral", "Código", "Descr" }));
        JCPesquisa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                JCPesquisaItemStateChanged(evt);
            }
        });
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 837, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCPesquisaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_JCPesquisaItemStateChanged

        // TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Produtos.PesquisarProduto());
                break;
            }
            case 1: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Produtos.PesquisarProduto(JTFPesquisar.getText()));
                break;
            }
            case 2: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Produtos.PesquisarProdutoNome(JTFPesquisar.getText().toUpperCase()));
                break;
            }

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed
        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(false);
            JTFPesquisar.requestFocus();
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked

        int index = JTPesquisa.getSelectedRow();
        Codigo = (String) JTPesquisa.getValueAt(index, 0);
        Descr = (String) JTPesquisa.getValueAt(index, 1);
        Quant = (String) JTPesquisa.getValueAt(index, 2);
        CodigoBanco = (String) JTPesquisa.getValueAt(index, 5);
        A = 1;
        dispose();
        // TODO add your handling code here:
    }//GEN-LAST:event_JTPesquisaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PesquisarProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PesquisarProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PesquisarProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PesquisarProdutos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PesquisarProdutos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JCPesquisa;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
}
