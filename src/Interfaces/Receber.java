package Interfaces;

import Classes.ClasseOrdem;
import Classes.ClasseReceberVendas;
import Classes.ClasseVenda;
import Controles.ControleOrdem;
import Controles.ControleReceberVendas;
import Controles.ControleVendas;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Receber extends javax.swing.JFrame {

    LimparTabelas Limpar_Tabela;
    public String TesteData;
    public Date data = null;
    public boolean clicado = false;
    public int Codigo_Parcela = 0;
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao;
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    ConverterDecimais converter = new ConverterDecimais();
    public int estado = Rotinas.Padrao;
    private SimpleDateFormat sdf;
    private ClasseReceberVendas Receber = new ClasseReceberVendas();
    private ControleReceberVendas Controle_Receber;
    private ClasseVenda Classe_Venda;
    private ControleVendas Controle_Venda;
    private ClasseOrdem Classe_Ordem;
    private ControleOrdem Controle_Ordem;
    LimparTabelas LimparTabela = new LimparTabelas();

    public Receber() {
        initComponents();
        Controle_Receber = new ControleReceberVendas();
        Classe_Venda = new ClasseVenda();
        Controle_Venda = new ControleVendas();
        Classe_Ordem = new ClasseOrdem();
        Controle_Ordem = new ControleOrdem();
        preencher.Preencher_JTableGenerico(JTParcelas_Cadastro, Controle_Receber.Pesquisar_ParcelasGeral());
        Valida_Botao = new ValidaEstadoBotoes();
        Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        jBCancelar.setEnabled(false);

        JCPerc_Juros.setEnabled(false);
        JCValor_Juros.setEnabled(false);
        JCPerc_Desconto.setEnabled(false);
        JCValor_Desconto.setEnabled(false);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);

        Tamanho_Jtable(JTParcelas_Cadastro, 0, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 1, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 2, 200);
        Tamanho_Jtable(JTParcelas_Cadastro, 3, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 4, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 5, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 6, 100);
        Tamanho_Jtable(JTParcelas_Cadastro, 7, 250);
        Tamanho_Jtable(JTParcelas_Cadastro, 8, 150);
        Tamanho_Jtable(JTParcelas_Cadastro, 10, 150);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPANEL = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JCTipo = new javax.swing.JComboBox();
        JTFCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFData = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFValorGeral = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFValorPago = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFJuros = new javax.swing.JTextField();
        JCPerc_Juros = new javax.swing.JCheckBox();
        JCValor_Juros = new javax.swing.JCheckBox();
        JTFValor_Juros = new javax.swing.JTextField();
        JTFDesconto = new javax.swing.JTextField();
        JCPerc_Desconto = new javax.swing.JCheckBox();
        JTFValor_Desconto = new javax.swing.JTextField();
        JCValor_Desconto = new javax.swing.JCheckBox();
        JTFDataVenc = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        JTFTotal = new javax.swing.JTextField();
        JTFValor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        JTFDataPago = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        JTFSituacao = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTParcelas_Cadastro = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Contas a Receber");
        setResizable(false);

        jLabel1.setText("Operação");

        JCTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ordem de Serviço", "Venda", "NF" }));

        JTFCodigo.setEditable(false);

        jLabel2.setText("Código");

        try {
            JTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        JTFData.setEnabled(false);

        jLabel3.setText("Data");

        JTFValorGeral.setEditable(false);

        jLabel4.setText("Valor Geral");

        JTFValorPago.setEditable(false);

        jLabel5.setText("Valor Pago(Total)");

        JTFJuros.setEditable(false);
        JTFJuros.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFJurosFocusLost(evt);
            }
        });

        JCPerc_Juros.setText("% Juros");
        JCPerc_Juros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPerc_JurosActionPerformed(evt);
            }
        });

        JCValor_Juros.setText("Valor Juros");
        JCValor_Juros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCValor_JurosActionPerformed(evt);
            }
        });

        JTFValor_Juros.setEditable(false);
        JTFValor_Juros.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFValor_JurosFocusLost(evt);
            }
        });

        JTFDesconto.setEditable(false);
        JTFDesconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFDescontoFocusLost(evt);
            }
        });

        JCPerc_Desconto.setText("% Desconto");
        JCPerc_Desconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPerc_DescontoActionPerformed(evt);
            }
        });

        JTFValor_Desconto.setEditable(false);
        JTFValor_Desconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFValor_DescontoFocusLost(evt);
            }
        });

        JCValor_Desconto.setText("Valor Desconto");
        JCValor_Desconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCValor_DescontoActionPerformed(evt);
            }
        });

        JTFDataVenc.setEditable(false);
        try {
            JTFDataVenc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel6.setText("Data de Vencimento");

        jLabel7.setText("*Valor Total");

        JTFTotal.setEditable(false);

        JTFValor.setEditable(false);
        JTFValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFValorActionPerformed(evt);
            }
        });

        jLabel8.setText("Valor sendo Pago");

        JTFDataPago.setEditable(false);
        try {
            JTFDataPago.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel9.setText("Data Pagamento");

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBAlterar.setText("Liquidar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir Liq.");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        jLabel10.setText("Situação");

        JTFSituacao.setEditable(false);

        JTParcelas_Cadastro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº Conta", "Código ", "Operacao", "Valor Total", "Valor Pago", "Juros", "Desconto", "Cliente", "Data de Vencimento", "Data Pago", "Forma de Pgto", "Situação", "Valor Normal"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTParcelas_Cadastro.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        JTParcelas_Cadastro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTParcelas_CadastroMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTParcelas_Cadastro);

        jLabel11.setText("A Receber");

        javax.swing.GroupLayout JPANELLayout = new javax.swing.GroupLayout(JPANEL);
        JPANEL.setLayout(JPANELLayout);
        JPANELLayout.setHorizontalGroup(
            JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPANELLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPANELLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(JPANELLayout.createSequentialGroup()
                        .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, 583, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(JPANELLayout.createSequentialGroup()
                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(81, 81, 81)
                                        .addComponent(jLabel2))
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(JCPerc_Juros)
                                        .addGap(63, 63, 63)
                                        .addComponent(JCValor_Juros))
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(JCTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(JTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(JCPerc_Desconto)
                                        .addGap(18, 18, 18)
                                        .addComponent(JCValor_Desconto))
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(85, 85, 85)
                                        .addComponent(jLabel4)
                                        .addGap(63, 63, 63)
                                        .addComponent(jLabel5))
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(JTFValorGeral, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(JTFValorPago, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(JPANELLayout.createSequentialGroup()
                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                                    .addComponent(JTFJuros))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(JTFValor_Juros, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                                    .addComponent(JTFValor))
                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(JPANELLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(JTFDataPago, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(JTFSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPANELLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(JTFDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel9))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(JPANELLayout.createSequentialGroup()
                                                .addComponent(JTFValor_Desconto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel6)
                                                    .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addComponent(jLabel10)))))
                            .addGroup(JPANELLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(71, 71, 71)
                                .addComponent(jLabel8)))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(JPANELLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        JPANELLayout.setVerticalGroup(
            JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPANELLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel3)
                        .addComponent(jLabel5)))
                .addGap(6, 6, 6)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JCTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValorGeral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValorPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JCPerc_Juros)
                    .addComponent(JCValor_Juros)
                    .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JCValor_Desconto)
                        .addComponent(JCPerc_Desconto)
                        .addComponent(jLabel6)))
                .addGap(2, 2, 2)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFJuros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValor_Juros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValor_Desconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataVenc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(jLabel9)))
                .addGap(6, 6, 6)
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JTFDataPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(JTFValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JTFSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("Dados", JPANEL);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JTFJurosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFJurosFocusLost
        double Juros = Double.parseDouble(JTFJuros.getText().replace(",", "."));
        if ((Juros != 0.00) && (JCPerc_Juros.isSelected() == true)) {
            double Perc = 0;
            double valor = 0;
            try {
                Perc = Double.parseDouble(JTFJuros.getText().replaceAll(",", "."));
                valor = Double.parseDouble(JTFTotal.getText());
                valor = valor * Perc / 100;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFValor_Juros.setText(String.valueOf(valor));
                Perc = Double.parseDouble(JTFValor_Juros.getText().replace(",", "."));
                valor = Double.parseDouble(JTFTotal.getText());
                valor = valor + Perc;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFTotal.setText(String.valueOf(valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                JTFJuros.setText("");
                JTFJuros.requestFocus();
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFJurosFocusLost

    private void JCPerc_JurosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPerc_JurosActionPerformed
        if (JCPerc_Juros.isSelected() == true) {
            JTFJuros.setEditable(true);
            JCValor_Juros.setSelected(false);
            JTFValor_Juros.setEditable(false);
        } else {
            JTFJuros.setEditable(false);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JCPerc_JurosActionPerformed

    private void JCValor_JurosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCValor_JurosActionPerformed
        if (JCValor_Juros.isSelected() == true) {
            JTFJuros.setEditable(false);
            JCPerc_Juros.setSelected(false);
            JTFValor_Juros.setEditable(true);
        } else {
            JTFValor_Juros.setEditable(false);
        }
    }//GEN-LAST:event_JCValor_JurosActionPerformed

    private void JTFValor_JurosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFValor_JurosFocusLost
        if ((!JTFValor_Juros.getText().equals("") && (JCValor_Juros.isSelected() == true))) {
            try {
                double Valor = Double.parseDouble(JTFTotal.getText().replaceAll(",", "."));
                double Juros = Double.parseDouble(JTFValor_Juros.getText().replaceAll(",", "."));
                double Perc = 0;
                Juros = Juros * 100;
                Perc = Juros / Valor;
                Perc = converter.converterDoubleDoisDecimais(Perc);
                JTFJuros.setText(String.valueOf(Perc));
                Juros = Juros / 100;
                Valor = Valor + Juros;
                Valor = converter.converterDoubleDoisDecimais(Valor);
                JTFTotal.setText(String.valueOf(Valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                return;
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFValor_JurosFocusLost

    private void JTFDescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFDescontoFocusLost
        double Desconto = Double.parseDouble(JTFDesconto.getText().replace(",", "."));
        if ((Desconto != 0.00) && (JCPerc_Desconto.isSelected() == true)) {
            double Perc = 0;
            double valor = 0;
            try {
                Perc = Double.parseDouble(JTFDesconto.getText().replaceAll(",", "."));
                valor = Double.parseDouble(JTFTotal.getText());
                valor = valor * Perc / 100;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFValor_Desconto.setText(String.valueOf(valor));
                Perc = Double.parseDouble(JTFValor_Desconto.getText().replace(",", "."));
                valor = Double.parseDouble(JTFTotal.getText());
                valor = valor - Perc;
                valor = converter.converterDoubleDoisDecimais(valor);
                JTFValorGeral.setText(String.valueOf(valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                JTFDesconto.setText("");
                JTFDesconto.requestFocus();
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFDescontoFocusLost

    private void JCPerc_DescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPerc_DescontoActionPerformed
        if (JCPerc_Desconto.isSelected() == true) {
            JCValor_Desconto.setSelected(false);
            JTFValor_Desconto.setEditable(false);
            JTFDesconto.setEditable(true);
        } else {
            JTFDesconto.setEditable(false);
        }
    }//GEN-LAST:event_JCPerc_DescontoActionPerformed

    private void JTFValor_DescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFValor_DescontoFocusLost
        if ((!JTFValor_Desconto.getText().equals("") && (JCValor_Desconto.isSelected() == true))) {
            try {
                double Valor = Double.parseDouble(JTFValorGeral.getText().replaceAll(",", "."));
                double Desconto = Double.parseDouble(JTFValor_Desconto.getText().replaceAll(",", "."));
                double Perc = 0;
                Desconto = Desconto * 100;
                Perc = Desconto / Valor;
                Perc = converter.converterDoubleDoisDecimais(Perc);
                JTFDesconto.setText(String.valueOf(Perc));
                Desconto = Desconto / 100;
                Valor = Valor - Desconto;
                Valor = converter.converterDoubleDoisDecimais(Valor);
                JTFValorGeral.setText(String.valueOf(Valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido");
                JTFValor_Desconto.setText("");
                JTFValor_Desconto.requestFocus();
                return;
            }
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFValor_DescontoFocusLost

    private void JCValor_DescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCValor_DescontoActionPerformed
        if (JCValor_Desconto.isSelected() == true) {
            JCPerc_Desconto.setSelected(false);
            JTFValor_Desconto.setEditable(true);
            JTFDesconto.setEditable(false);
        } else {
            JTFValor_Desconto.setEditable(false);
        }
    }//GEN-LAST:event_JCValor_DescontoActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        JTFDataPago.setText(Controle_Ordem.Data());
        JTFCodigo.setEditable(false);
        JCTipo.setEnabled(false);
        Valida_Botao.ValidaCamposIncluir(JPANEL, JPAINELBOTAO);
        estado = Rotinas.Alterar;
        jBAlterar.setEnabled(false);
        JCPerc_Juros.setEnabled(true);
        JCValor_Juros.setEnabled(true);
        JCPerc_Desconto.setEnabled(true);
        JCValor_Desconto.setEnabled(true);
        if (JCTipo.getSelectedIndex() == 0) {
            if (Receber.getSITUACAO().equals("LIQUIDADA")) {
                JTFSituacao.setText("LIQUIDADA");
                jBExcluir.setEnabled(true);
                JCTipo.setEnabled(false);
            } else {
                JTFSituacao.setText("ABERTA");
                jBExcluir.setEnabled(false);
                JTFValor.setEditable(true);
                JTFDataPago.setEditable(true);
                JCTipo.setEnabled(false);
            }
        } else if (JCTipo.getSelectedIndex() == 1) {
            if (Receber.getSITUACAO().equals("LIQUIDADA")) {
                JTFSituacao.setText("LIQUIDADA");
                jBExcluir.setEnabled(true);
                JCTipo.setEnabled(false);
            } else {
                jBExcluir.setEnabled(false);
                JTFValor.setEditable(true);
                JTFDataPago.setEditable(true);
            }
        } else if (JCTipo.getSelectedIndex() == 2) {
            if (Receber.getSITUACAO().equals("LIQUIDADA")) {
                JTFSituacao.setText("LIQUIDADA");
                jBExcluir.setEnabled(true);
                JCTipo.setEnabled(false);
            } else {
                jBExcluir.setEnabled(false);
                JTFValor.setEditable(true);
                JTFDataPago.setEditable(true);
            }
            JTFData.setEditable(false);
            JTFValorPago.setEditable(false);
        }
        JCTipo.setEnabled(false);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        int Selecionada = JTParcelas_Cadastro.getSelectedRow();
        int tamanho = JTParcelas_Cadastro.getRowCount();
        int X;
        boolean teste = false;
        boolean teste_remover = false;
        String Codigo_Parcela = null;
        String Codigo_Parcela1 = null;
        String Codigo_Origem = null;
        Codigo_Parcela = (String) JTParcelas_Cadastro.getValueAt(Selecionada, 0);
        if (JCTipo.getSelectedIndex() == 0) {
            Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
            Receber.setVALOR_TOTAL(Double.parseDouble(JTFTotal.getText().replaceAll(",", ".")));
            int opcao = JOptionPane.showConfirmDialog(null, "Apenas a Liquidação será Excluída, Deseja Continuar?");
            if (opcao == JOptionPane.YES_OPTION) {
                Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
                Receber.setIDENTIF(JTFCodigo.getText());
                if (Controle_Receber.Excluir_Liquidação(Receber)) {
                } else {
                    teste_remover = false;
                    return;
                }
            }
        } else if (JCTipo.getSelectedIndex() == 1) {
            Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
            Receber.setVALOR_TOTAL(Double.parseDouble(JTFTotal.getText().replaceAll(",", ".")));
            Receber.setIDENTIF(JTFCodigo.getText());
            int opcao = JOptionPane.showConfirmDialog(null, "Apenas a Liquidação será Excluída, Deseja Continuar?");
            if (opcao == JOptionPane.YES_OPTION) {
                Receber.setID_PARCELA(Integer.parseInt(Codigo_Parcela));
                if (Controle_Receber.Excluir_Liquidação(Receber)) {
                    teste_remover = true;
                    JOptionPane.showMessageDialog(null, "Liquidação Removida");
                    LimparTabela.Limpar_tabela_pesquisa(JTParcelas_Cadastro);
                    preencher.Preencher_JTableGenerico(JTParcelas_Cadastro, Controle_Receber.Pesquisar_ParcelasGeral());
                } else {
                    teste_remover = false;
                    return;
                }
            }
        }


    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        int index = JTParcelas_Cadastro.getRowCount();
        if (index == 0) {
            JOptionPane.showMessageDialog(null, "Nada a Ser Gravado");
            JTFCodigo.requestFocus();
            return;
        }
        int opcao;
        if (JTFValorPago.getText().equals("") && (Receber.getSITUACAO().equals("ABERTA"))) {
            JOptionPane.showMessageDialog(null, "Informe o Valor Pago");
            return;
        }
        double C;
        double B;
        C = Double.parseDouble(JTFValorPago.getText());
        B = Double.parseDouble(JTFTotal.getText());
        if (C > B) {
            JOptionPane.showMessageDialog(null, "Valor Pago Informado é Maior que o da Parcela Gerada");
            return;
        }
        TesteData = JTFDataPago.getText();
        if (!Validar_Data()) {
            JOptionPane.showMessageDialog(null, "Data de Pagamento Inválida");
            JTFDataPago.setText(null);
            return;
        } else {
            opcao = JOptionPane.showConfirmDialog(null, "Confirmar Alteração?");
            if (opcao == 0) {
                Receber.setVALOR_PARCELA(Double.parseDouble(JTFValorGeral.getText()));
                if (JCTipo.getSelectedIndex() == 0) {
                    double valor_pago = Double.parseDouble(JTFValor.getText().replaceAll(",", "."));
                    double valor_final = Double.parseDouble(JTFTotal.getText().replaceAll(",", "."));
                    valor_pago = converter.converterDoubleDoisDecimais(valor_pago);
                    valor_final = converter.converterDoubleDoisDecimais(valor_final);
                    Receber.setVALOR_PAGO(valor_pago);
                    Receber.setVALOR_TOTAL(valor_final);
                    Receber.setSITUACAO("LIQUIDADA");
                    Receber.setDATA_PAGO(JTFDataPago.getText());
                    Receber.setDATA_VENCIMENTO(JTFDataVenc.getText());
                    Receber.setDESCONTO(Double.parseDouble(JTFDesconto.getText()));
                    Receber.setDESCR("ORDEM SERVICO");
                    String FORMA = JOptionPane.showInputDialog("Informe a Forma de Pagamento");
                    Receber.setDS_FORMA(FORMA.toUpperCase());
                    if (Controle_Receber.Alterar(Receber)) {
                        JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
                        LimparTabela.Limpar_tabela_pesquisa(JTParcelas_Cadastro);
                        preencher.Preencher_JTableGenerico(JTParcelas_Cadastro, Controle_Receber.Pesquisar_ParcelasGeral());
                    }
                } else if (JCTipo.getSelectedIndex() == 1) {
                    double valor_pago = Double.parseDouble(JTFValor.getText().replaceAll(",", "."));
                    double valor_final = Double.parseDouble(JTFTotal.getText().replaceAll(",", "."));
                    valor_pago = converter.converterDoubleDoisDecimais(valor_pago);
                    valor_final = converter.converterDoubleDoisDecimais(valor_final);
                    Receber.setVALOR_PAGO(valor_pago);
                    Receber.setVALOR_TOTAL(valor_final);
                    Receber.setSITUACAO("LIQUIDADA");
                    Receber.setDATA_PAGO(JTFDataPago.getText());
                    Receber.setDATA_VENCIMENTO(JTFDataVenc.getText());
                    Receber.setDESCONTO(Double.parseDouble(JTFDesconto.getText()));
                    Receber.setDESCR("VENDA");
                    String FORMA = JOptionPane.showInputDialog("Informe a Forma de Pagamento");
                    Receber.setDS_FORMA(FORMA.toUpperCase());
                    if (Controle_Receber.Alterar(Receber)) {
                        JOptionPane.showMessageDialog(null, "Registor Alterado com Sucesso");
                        LimparTabela.Limpar_tabela_pesquisa(JTParcelas_Cadastro);
                        preencher.Preencher_JTableGenerico(JTParcelas_Cadastro, Controle_Receber.Pesquisar_ParcelasGeral());
                    }
                } else if (JCTipo.getSelectedIndex() == 2) {
                    double valor_pago = Double.parseDouble(JTFValor.getText().replaceAll(",", "."));
                    double valor_final = Double.parseDouble(JTFTotal.getText().replaceAll(",", "."));
                    valor_pago = converter.converterDoubleDoisDecimais(valor_pago);
                    valor_final = converter.converterDoubleDoisDecimais(valor_final);
                    Receber.setVALOR_PAGO(valor_pago);
                    Receber.setVALOR_TOTAL(valor_final);
                    Receber.setSITUACAO("LIQUIDADA");
                    Receber.setDATA_PAGO(JTFDataPago.getText());
                    Receber.setDATA_VENCIMENTO(JTFDataVenc.getText());
                    Receber.setDESCONTO(Double.parseDouble(JTFDesconto.getText()));
                    Receber.setDESCR("COMPRA");
                    String FORMA = JOptionPane.showInputDialog("Informe a Forma de Pagamento");
                    Receber.setDS_FORMA(FORMA.toUpperCase());
                    if (Controle_Receber.Alterar(Receber)) {
                        JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
                        LimparTabela.Limpar_tabela_pesquisa(JTParcelas_Cadastro);
                        preencher.Preencher_JTableGenerico(JTParcelas_Cadastro, Controle_Receber.Pesquisar_ParcelasGeral());
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nada Gravado, REVERTIDO!");
                limpar.LimparCampos(JPANEL);
            }
        }
        limpar.LimparCampos(JPANEL);

        Limpar_Tabela.Limpar_tabela_pesquisa(JTParcelas_Cadastro);
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed

        Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
        jBGravar.setEnabled(false);
        jBCancelar.setEnabled(false);
        JCPerc_Juros.setSelected(false);
        JCValor_Juros.setSelected(false);
        JCPerc_Desconto.setSelected(false);
        JCValor_Desconto.setSelected(false);
        JCValor_Desconto.setSelected(false);
        JCPerc_Desconto.setSelected(false);
        limpar.LimparCampos(JPANEL);
        JCPerc_Desconto.setEnabled(false);
        JCValor_Desconto.setEnabled(false);
        JCValor_Desconto.setEnabled(false);
        JCPerc_Desconto.setEnabled(false);
        JTFValor_Desconto.setEditable(false);

    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JTParcelas_CadastroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTParcelas_CadastroMouseClicked
        clicado = true;
        JTFValorPago.setEditable(false);
        int index = JTParcelas_Cadastro.getSelectedRow();
        String strVetor = (String) JTParcelas_Cadastro.getValueAt(index, 0);
        Codigo_Parcela = Integer.parseInt(strVetor);
        String Codigo = (String) JTParcelas_Cadastro.getValueAt(index, 1);
        JTFCodigo.setText(Codigo);

        Receber.setIDENTIF(JTFCodigo.getText());
        Receber.setID_PARCELA(Codigo_Parcela);
        String Opercao = (String) JTParcelas_Cadastro.getValueAt(index, 2);
        String Valor_Total = (String) JTParcelas_Cadastro.getValueAt(index, 3);
        String Valor_Pago = (String) JTParcelas_Cadastro.getValueAt(index, 4);
        String Juros = (String) JTParcelas_Cadastro.getValueAt(index, 5);
        String Desconto = (String) JTParcelas_Cadastro.getValueAt(index, 6);
        String Data_Vencimento = (String) JTParcelas_Cadastro.getValueAt(index, 8);
        String Data_Pago = (String) JTParcelas_Cadastro.getValueAt(index, 9);
        String Situacao = (String) JTParcelas_Cadastro.getValueAt(index, 11);
        String Normal = (String) JTParcelas_Cadastro.getValueAt(index, 12);

        JTFCodigo.setText(Codigo);
        JTFDataPago.setText(Data_Pago);
        JTFDataVenc.setText(Data_Vencimento);
        JTFDesconto.setText(Desconto);
        JTFTotal.setText(Valor_Total);
        JTFDescontoFocusLost(null);
        JTFJuros.setText(Juros);
        JTFJurosFocusLost(null);
        JTFSituacao.setText(Situacao);
        JTFValorGeral.setText(Normal);
        JTFTotal.setText(Valor_Total);
        JTFValorPago.setText(Valor_Pago);
        if (Opercao.equals("VENDA")) {
            Classe_Venda.setID_VENDA(Integer.parseInt(Receber.getIDENTIF()));
            Classe_Venda = Controle_Venda.Recuperar_Parcela(Classe_Venda);
            JTFData.setText(Classe_Venda.getDATA_VENDA());
            JTFValorGeral.setText(String.valueOf(Classe_Venda.getTOTAL_VENDA()));
            JCTipo.setSelectedIndex(1);
        } else if (Opercao.equals("ORDEM SERVICO")) {
            Classe_Ordem.setID_ORDEM(Integer.parseInt(Receber.getIDENTIF()));
            Classe_Ordem = Controle_Ordem.Recuperar_Parcela(Classe_Ordem);
            JTFData.setText(Classe_Ordem.getDATA_FECHA());
            JTFValorGeral.setText(String.valueOf(Classe_Ordem.getVALOR_TOTAL()));
            JCTipo.setSelectedIndex(0);
        } else if (Opercao.equals("COMPRA")) {
            JCTipo.setSelectedIndex(2);
            JTFValorGeral.setText(Normal);
        }
        Receber.setSITUACAO(Situacao);
        if (Receber.getSITUACAO().equals("LIQUIDADA")) {
            jBExcluir.setEnabled(true);
            jBAlterar.setEnabled(false);
        } else {
            jBExcluir.setEnabled(false);
            jBAlterar.setEnabled(true);
        }
        JTFDescontoFocusLost(null);
        JTFValor.setEditable(false);
        JTFDataPago.setEditable(false);
        JTFValor.setText(null);
        JCTipo.setEnabled(false);


    }//GEN-LAST:event_JTParcelas_CadastroMouseClicked

    private void JTFValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFValorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFValorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Receber.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Receber.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Receber.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Receber.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Receber().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox JCPerc_Desconto;
    private javax.swing.JCheckBox JCPerc_Juros;
    private javax.swing.JComboBox JCTipo;
    private javax.swing.JCheckBox JCValor_Desconto;
    private javax.swing.JCheckBox JCValor_Juros;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPANEL;
    private javax.swing.JTextField JTFCodigo;
    private javax.swing.JFormattedTextField JTFData;
    private javax.swing.JFormattedTextField JTFDataPago;
    private javax.swing.JFormattedTextField JTFDataVenc;
    private javax.swing.JTextField JTFDesconto;
    private javax.swing.JTextField JTFJuros;
    private javax.swing.JTextField JTFSituacao;
    private javax.swing.JTextField JTFTotal;
    private javax.swing.JTextField JTFValor;
    private javax.swing.JTextField JTFValorGeral;
    private javax.swing.JTextField JTFValorPago;
    private javax.swing.JTextField JTFValor_Desconto;
    private javax.swing.JTextField JTFValor_Juros;
    private javax.swing.JTable JTParcelas_Cadastro;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public boolean Validar_Data() {
        try {
            data = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }
}
