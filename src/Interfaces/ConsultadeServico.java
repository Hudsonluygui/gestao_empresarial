package Interfaces;

import Classes.ClasseServicos;
import Controles.ControleServico;
import Validações.Preencher_JTableGenerico;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class ConsultadeServico extends javax.swing.JFrame {

    String Codigo;
    String Servico;
    String Valor;
    int A = 0;
    ClasseServicos Classe_Servico;
    ControleServico Controle;
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    public static String servico_;

    public ConsultadeServico() {
        initComponents();
        Classe_Servico = new ClasseServicos();
        Controle = new ControleServico();
        servico_ = Ordem.Servico_;
        if (servico_ == null) {
            preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarGeral());
        }else{
        preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarDescr(servico_.toUpperCase()));
        }
        Double_Click();
        Enter(JTPEsquisa);
        Enter2(JTFPesquisar);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTPEsquisa = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consutar Serviços");
        setResizable(false);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Código", "Serviço" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        JTPEsquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Servico", "Valor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPEsquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPEsquisaMouseClicked(evt);
            }
        });
        JTPEsquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JTPEsquisaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(JTPEsquisa);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 872, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed
        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        }
        if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        }
        if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }


    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarGeral());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisar");
                    JTFPesquisar.requestFocus();
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarPorCodigo(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisar.requestFocus();
                        JTFPesquisar.setText("");
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe uma Descrição");
                    JTFPesquisar.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarDescr(JTFPesquisar.getText().toUpperCase()));
                    Ordem.Servico_ = JTFPesquisar.getText();
                }
                break;
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void JTPEsquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPEsquisaMouseClicked

        int Index = JTPEsquisa.getSelectedRow();
        Codigo = (String) JTPEsquisa.getValueAt(Index, 0);
        Servico = (String) JTPEsquisa.getValueAt(Index, 1);
        Valor = (String) JTPEsquisa.getValueAt(Index, 2);
        A = 1;
        servico_ = JTFPesquisar.getText();

// TODO add your handling code here:
    }//GEN-LAST:event_JTPEsquisaMouseClicked

    private void JTPEsquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JTPEsquisaKeyPressed


    }//GEN-LAST:event_JTPEsquisaKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultadeServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultadeServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultadeServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultadeServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultadeServico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTable JTPEsquisa;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public void Double_Click() {
        JTPEsquisa.addMouseListener(
                new java.awt.event.MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        // Se o botão direito do mouse foi pressionado  
                        if (e.getClickCount() == 2) {
                            //Se for 2x
                            dispose();
                        }
                    }
                });
    }

    private void Enter(final JTable Tabela) {
        Tabela.getInputMap(Tabela.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "enterAction");
        Tabela.getActionMap().put("enterAction", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                int Index = JTPEsquisa.getSelectedRow();
                Codigo = (String) JTPEsquisa.getValueAt(Index, 0);
                Servico = (String) JTPEsquisa.getValueAt(Index, 1);
                Valor = (String) JTPEsquisa.getValueAt(Index, 2);
                A = 1;

                dispose();
            }
        });
    }
       private void Enter2(final JTextField campo) {
        campo.getInputMap(campo.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "enterAction");
        campo.getActionMap().put("enterAction", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
               switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarGeral());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisar");
                    JTFPesquisar.requestFocus();
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarPorCodigo(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisar.requestFocus();
                        JTFPesquisar.setText("");
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe uma Descrição");
                    JTFPesquisar.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(JTPEsquisa, Controle.PesquisarDescr(JTFPesquisar.getText().toUpperCase()));
                    Ordem.Servico_ = JTFPesquisar.getText();
                }
                break;
            }
        }
             
            }
        });
    }

}
