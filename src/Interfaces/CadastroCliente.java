package Interfaces;

import Cep.CEP;
import Cep.ViaCEP;
import Cep.ViaCEPException;
import Classes.ClasseCliente;
import Classes.ClasseClienteItem;
import Classes.ClasseFisica;
import Classes.ClasseJuridica;
import Classes.ClassePessoa;
import Controles.ControleCliente;
import Controles.ControleClienteItens;
import Controles.ControleFisica;
import Controles.ControleJuridica;
import Controles.ControlePessoa;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import Validações.ValidarCNPJ;
import Validações.ValidarCPF;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CadastroCliente extends javax.swing.JFrame {

    public int inclusao = 0;
    public boolean Valida_CPF;
    public boolean Valida_CNPJ;
    public int Contador = 0;
    public int Cont_Banco = 0;
    public int teste_remover = 0;
    public int CODIGO = 0;
    public int estado = Rotinas.Padrao;
    public int Vetor_Codigo[];
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    private final ValidarCPF Validar_CPF;
    private final ValidarCNPJ Validar_CNPJ;
    ControlePessoa Controle_Pessoa;
    ControleCliente Controle_CLiente;
    ClassePessoa Classe_Pessoa;
    ClasseFisica Classe_Fisica;
    ClasseJuridica Classe_Juridica;
    ClasseCliente Classe_Cliente;
    ControleFisica Controle_Fisica;
    ControleJuridica Controle_Juridica;
    LimparTabelas Limpar_Tabela;

    public CadastroCliente() {
        initComponents();
        Controle_Juridica = new ControleJuridica();
        Controle_Fisica = new ControleFisica();
        Controle_Pessoa = new ControlePessoa();
        Controle_CLiente = new ControleCliente();
        Classe_Fisica = new ClasseFisica();
        Classe_Juridica = new ClasseJuridica();
        Classe_Pessoa = new ClassePessoa();
        Classe_Cliente = new ClasseCliente();
        Validar_CPF = new ValidarCPF();
        Validar_CNPJ = new ValidarCNPJ();
        Limpar_Tabela = new LimparTabelas();
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);

        Botao();
        Double_Click();
        Tamanho_Jtable(JTPesquisa, 0, 3);
        Tamanho_Jtable(JTPesquisa, 1, 150);
        Tamanho_Jtable(JTPesquisa, 2, 80);
        Tamanho_Jtable(JTPesquisa, 3, 80);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPCadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCodCliente = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFLogradouro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFNum = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFBairro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFCidade = new javax.swing.JTextField();
        JTFCep = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        JTFEmail = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        JTFTelefone = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        JTFTelCel = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        JLCPF = new javax.swing.JLabel();
        JTFCPF = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        JTFDataNasc = new javax.swing.JFormattedTextField();
        JTFRG = new javax.swing.JFormattedTextField();
        jSeparator1 = new javax.swing.JSeparator();
        JLCNPJ = new javax.swing.JLabel();
        JTFCNPJ = new javax.swing.JFormattedTextField();
        jLabel15 = new javax.swing.JLabel();
        JTFRazao = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        JRFisico = new javax.swing.JRadioButton();
        JRJuridico = new javax.swing.JRadioButton();
        jLabel17 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        JTFEstado = new javax.swing.JTextField();
        JPConsulta = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        JBPesquisa = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastar Cliente");
        setResizable(false);

        jLabel1.setText("Código do Cliente");

        JTFCodCliente.setEditable(false);

        jLabel2.setText("Nome");

        jLabel3.setText("Endereço");

        jLabel4.setText("Número");

        jLabel5.setText("Bairro");

        jLabel6.setText("Cidade");

        try {
            JTFCep.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        JTFCep.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCepFocusLost(evt);
            }
        });

        jLabel7.setText("Cep");

        jLabel8.setText("Email");

        jLabel9.setText("Tel. Fixo");

        try {
            JTFTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel10.setText("Tel. Celular");

        try {
            JTFTelCel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)#####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel11.setText("RG                *Apenas Nºs");

        JLCPF.setText("CPF");

        try {
            JTFCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        JTFCPF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCPFFocusLost(evt);
            }
        });

        jLabel13.setText("Data de Nascimento");

        try {
            JTFDataNasc.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JTFRG.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));

        JLCNPJ.setText("CNPJ");

        JTFCNPJ.setEditable(false);
        try {
            JTFCNPJ.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        JTFCNPJ.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCNPJFocusLost(evt);
            }
        });

        jLabel15.setText("Razao Social");

        JTFRazao.setEditable(false);

        jLabel16.setText("Tipo de Cliente");

        buttonGroup1.add(JRFisico);
        JRFisico.setSelected(true);
        JRFisico.setText("Físico");
        JRFisico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRFisicoActionPerformed(evt);
            }
        });

        buttonGroup1.add(JRJuridico);
        JRJuridico.setText("Jurídico");
        JRJuridico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRJuridicoActionPerformed(evt);
            }
        });

        jLabel17.setText("*Jurídico");

        jLabel12.setText("Estado");

        javax.swing.GroupLayout JPCadastroLayout = new javax.swing.GroupLayout(JPCadastro);
        JPCadastro.setLayout(JPCadastroLayout);
        JPCadastroLayout.setHorizontalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(JTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFRazao))
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPCadastroLayout.createSequentialGroup()
                        .addComponent(JTFRG, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(JTFCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFDataNasc))
                    .addComponent(JTFNome, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(JTFCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(JRFisico)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JRJuridico))))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(186, 186, 186)
                                .addComponent(jLabel5))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPCadastroLayout.createSequentialGroup()
                                .addComponent(JTFNum, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11))
                                .addGap(6, 6, 6)
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JLCPF)
                                    .addComponent(JTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JTFTelCel)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 124, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JPCadastroLayout.createSequentialGroup()
                        .addGap(409, 409, 409)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(JTFEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(209, 209, 209)
                                .addComponent(jLabel3))
                            .addComponent(jLabel2)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(207, 207, 207)
                                .addComponent(jLabel9)
                                .addGap(136, 136, 136)
                                .addComponent(jLabel10))
                            .addComponent(jLabel17)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(JLCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(84, 84, 84)
                                .addComponent(jLabel15)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(JTFCep, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFLogradouro)))
                .addContainerGap())
        );
        JPCadastroLayout.setVerticalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel16))
                .addGap(5, 5, 5)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(JTFCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JRFisico)
                        .addComponent(JRJuridico)))
                .addGap(4, 4, 4)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4)
                    .addComponent(jLabel12))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFTelCel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(JLCPF))
                    .addComponent(jLabel13))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel17)))
                .addGap(24, 24, 24)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JLCNPJ)
                    .addComponent(jLabel15))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFRazao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(176, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", JPCadastro);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Cód. Cliente", "Nome", "CPF", "CNPJ" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        JBPesquisa.setText("Pesquisar");
        JBPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Telefone Fixo", "Telefone Celular"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTPesquisa);

        javax.swing.GroupLayout JPConsultaLayout = new javax.swing.GroupLayout(JPConsulta);
        JPConsulta.setLayout(JPConsultaLayout);
        JPConsultaLayout.setHorizontalGroup(
            JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPConsultaLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 918, Short.MAX_VALUE)
                    .addGroup(JPConsultaLayout.createSequentialGroup()
                        .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPConsultaLayout.createSequentialGroup()
                                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(JTFPesquisar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JBPesquisa)))
                .addContainerGap())
        );
        JPConsultaLayout.setVerticalGroup(
            JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPConsultaLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPConsultaLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JBPesquisa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Consulta", JPConsulta);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 604, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        inclusao = 1;
        JTFNome.requestFocus();
        estado = Rotinas.Incluir;
        limpar.LimparCampos(JPCadastro);

        Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
        JLCPF.setText("CPF");
        JLCNPJ.setText("CNPJ");
        JLCNPJ.setForeground(Color.black);
        JLCPF.setForeground(Color.black);
        Valida_CPF = false;
        Valida_CNPJ = false;
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        JRFisico.setSelected(true);
        JRFisicoActionPerformed(null);
    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        inclusao = 2;
        if (JTFCodCliente.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");
        } else {
            Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
            estado = Rotinas.Alterar;
            JRFisico.setEnabled(false);
            JRJuridico.setEnabled(false);
            if (JRFisico.isSelected()) {
                JTFCPF.setEditable(false);
                JTFCPFFocusLost(null);
                JRFisicoActionPerformed(null);
            } else {
                JTFCNPJ.setEditable(false);
                JRJuridicoActionPerformed(null);
                JTFCNPJFocusLost(null);
            }
            JTFNome.requestFocus();
        }
        JTFCPF.setEditable(false);
        JTFCNPJ.setEditable(false);
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed
        int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmetne Excluir?");
        if (opcao == JOptionPane.YES_OPTION) {
            
            if (Controle_Pessoa.Excluir(Classe_Pessoa)) {
                JOptionPane.showMessageDialog(null, "Registro Excluído com Sucesso");
                limpar.LimparCampos(JPCadastro);
                Botao();
            } else {
                JOptionPane.showMessageDialog(null, "Cliente sendo utilizado em algum Cadastro/Movimento");
            }
              limpar.LimparCampos(JPCadastro);
                Botao();
            JLCPF.setText("CPF");
            JLCNPJ.setText("CNPJ");
            JLCNPJ.setForeground(Color.black);
            JLCPF.setForeground(Color.black);
            Valida_CNPJ = false;
            Valida_CPF = false;
        }


    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (JRFisico.isSelected() && (inclusao == 1)) {
            if (Controle_Pessoa.Cadastrar(getCampos())) {
                if (Controle_Fisica.Cadastrar(GetFisica())) {
                    Classe_Cliente.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
                    if (Controle_CLiente.Cadastrar(Classe_Cliente)) {
                        JTFCodCliente.setText(String.valueOf(Classe_Cliente.getID_CLIENTE()));
                        JOptionPane.showMessageDialog(null, "Registro Incluso com Sucesso");
                        limpar.LimparCampos(JPCadastro);
                        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                    } else {
                        JOptionPane.showMessageDialog(null, Classe_Cliente.getERRO());
                        return;
                    }
                }
            }
        } else if (JRJuridico.isSelected() && (inclusao == 1)) {
            if (Controle_Pessoa.Cadastrar(getCampos())) {
                if (Controle_Juridica.Cadastrar(GetJuridica())) {
                    Classe_Cliente.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
                    if (Controle_CLiente.Cadastrar(Classe_Cliente)) {
                        JTFCodCliente.setText(String.valueOf(Classe_Cliente.getID_CLIENTE()));

                        JOptionPane.showMessageDialog(null, "Registro Incluso com Sucesso");
                        limpar.LimparCampos(JPCadastro);
                        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                    } else {
                        JOptionPane.showMessageDialog(null, Classe_Cliente.getERRO());
                        return;
                    }
                }
            }
        } else if (inclusao == 2) {
            if (Controle_Pessoa.Alterar(getCampos())) {
                Classe_Cliente.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
                JTFCodCliente.setText(String.valueOf(Classe_Cliente.getID_CLIENTE()));
                JOptionPane.showMessageDialog(null, "Registro Alterado com Sucesso");
                limpar.LimparCampos(JPCadastro);
            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro");
                return;
            }
        }
          Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed

        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Botao();
        limpar.LimparCampos(JPCadastro);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);

    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JRFisicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRFisicoActionPerformed
        if (JRFisico.isSelected()) {
            JTFRG.setEditable(true);
            JTFCPF.setEditable(true);
            JTFDataNasc.setEditable(true);
            JTFCNPJ.setEditable(false);
            JTFRazao.setEditable(false);
            JTFCNPJ.setText("");
            JTFRazao.setText("");
        } else if (JRJuridico.isSelected()) {
            JTFRG.setEditable(false);
            JTFCPF.setEditable(false);
            JTFDataNasc.setEditable(false);
            JTFCNPJ.setEditable(true);
            JTFRazao.setEditable(true);
            JTFCPF.setText("");
            JTFRG.setText("");
        }


    }//GEN-LAST:event_JRFisicoActionPerformed

    private void JRJuridicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRJuridicoActionPerformed
        if (JRFisico.isSelected()) {
            JTFRG.setEditable(true);
            JTFCPF.setEditable(true);
            JTFDataNasc.setEditable(true);
            JTFCNPJ.setEditable(false);
            JTFRazao.setEditable(false);
            JTFCNPJ.setText("");
            JTFRazao.setText("");
        } else if (JRJuridico.isSelected()) {
            JTFRG.setEditable(false);
            JTFCPF.setText("");
            JTFRG.setText("");
            JTFCPF.setEditable(false);
            JTFDataNasc.setEditable(false);
            JTFCNPJ.setEditable(true);
            JTFRazao.setEditable(true);
        }
    }//GEN-LAST:event_JRJuridicoActionPerformed

    private void JTFCPFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCPFFocusLost
        if (!JTFCPF.getText().equals("   .   .   -  ")) {
            if (JRFisico.isSelected() == true) {
                String CPF = Validar_CPF.Retirar_Pontos(JTFCPF.getText());
                if (Validar_CPF.Validar_CPF(CPF) == true) {
                    JLCPF.setText("CPF OK");
                    JLCPF.setForeground(Color.blue);
                    Valida_CPF = true;
                } else {
                    JLCPF.setForeground(Color.red);
                    JLCPF.setText("CPF Inválido!");
                    Valida_CPF = false;
                    return;
                }
            } else {
                JLCPF.setText("CPF");
                JLCPF.setForeground(Color.black);
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JTFCPFFocusLost

    private void JTFCNPJFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCNPJFocusLost
        if (!JTFCNPJ.getText().equals("  .   .   /    -  ")) {
            if (JRJuridico.isSelected() == true) {
                String CNPJ = Validar_CNPJ.Retirar_Pontos(JTFCNPJ.getText());
                if (Validar_CNPJ.Validar_CNPJ(CNPJ) == true) {
                    JLCNPJ.setText("CNPJ OK");
                    JLCNPJ.setForeground(Color.blue);
                    Valida_CNPJ = true;
                } else {
                    JLCNPJ.setForeground(Color.red);
                    JLCNPJ.setText("*CNPJ Inválido!");
                    Valida_CNPJ = false;
                    return;
                }
            } else {
                JLCNPJ.setText("CNPJ");
                JLCNPJ.setForeground(Color.black);
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFCNPJFocusLost

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 4) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(true);
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setText("");
            JTFPesquisar.setEditable(false);
            JTFPesquisar.requestFocus();
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void JBPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaActionPerformed
        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_CLiente.PesquisaGeral());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisar");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(JTPesquisa, Controle_CLiente.PesquisaCódigo(Integer.parseInt(JTFPesquisar.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Nome ");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Pessoa.PesquisaNome(JTFPesquisar.getText().toUpperCase()));
                }
                break;
            }
            case 3: {
                String Pesquisar, Aux = null;
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o CPF");
                    JTFPesquisar.requestFocus();
                } else {
                    int tamanho = JTFPesquisar.getText().length();
                    if (tamanho > 11) {
                        Pesquisar = Validar_CPF.Retirar_Pontos(JTFPesquisar.getText());
                        Aux = Pesquisar;
                    } else {
                        Pesquisar = JTFPesquisar.getText();
                    }
                    if (Validar_CPF.Validar_CPF(Pesquisar) == false) {
                        JOptionPane.showMessageDialog(null, "CPF Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);

                    } else {
                        if (Aux != null) {
                            Pesquisar = (Aux.substring(0, 3)
                                    + "." + Aux.substring(3, 6)
                                    + "." + Aux.substring(6, 9)
                                    + "-" + Aux.substring(9, 11));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Fisica.PesquisarCPF(Pesquisar));
                        } else {
                            Pesquisar = (JTFPesquisar.getText().substring(0, 3)
                                    + "." + JTFPesquisar.getText().substring(3, 6)
                                    + "." + JTFPesquisar.getText().substring(6, 9)
                                    + "-" + JTFPesquisar.getText().substring(9, 11));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Fisica.PesquisarCPF(Pesquisar));
                        }
                    }
                }
                break;
            }
            case 4: {
                String Pesquisar, Aux = null;
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o CNPJ");
                    JTFPesquisar.requestFocus();
                    return;
                } else {
                    int tamanho = JTFPesquisar.getText().length();
                    if (tamanho > 14) {
                        Pesquisar = Validar_CNPJ.Retirar_Pontos(JTFPesquisar.getText());
                        Aux = Pesquisar;
                    } else {
                        Pesquisar = JTFPesquisar.getText();
                    }
                    if (Validar_CNPJ.Validar_CNPJ(Pesquisar) == false) {
                        JOptionPane.showMessageDialog(null, "CNPJ Inválido");
                        JTFPesquisar.setText("");
                        JTFPesquisar.requestFocus();
                        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
                    } else {
                        if (Aux != null) {
                            Pesquisar = (Aux.substring(0, 2)
                                    + "." + Aux.substring(2, 5)
                                    + "." + Aux.substring(5, 8)
                                    + "/" + Aux.substring(8, 12)
                                    + "-" + Aux.substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Juridica.PesquisarCNPJ(Pesquisar));
                        } else {
                            Pesquisar = (JTFPesquisar.getText().substring(0, 2)
                                    + "." + JTFPesquisar.getText().substring(2, 5)
                                    + "." + JTFPesquisar.getText().substring(5, 8)
                                    + "/" + JTFPesquisar.getText().substring(8, 12)
                                    + "-" + JTFPesquisar.getText().substring(12, 14));
                            preencher.Preencher_JTableGenerico(JTPesquisa, Controle_Juridica.PesquisarCNPJ(Pesquisar));
                        }
                    }
                }
                break;
            }

        }


    }//GEN-LAST:event_JBPesquisaActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked
        int index = JTPesquisa.getSelectedRow();
        String Codigo = (String) JTPesquisa.getValueAt(index, 0);
        Classe_Pessoa.setID_PESSOA(Integer.parseInt(Codigo));
        Classe_Cliente.setID_CLIENTE(Classe_Pessoa.getID_PESSOA());

    }//GEN-LAST:event_JTPesquisaMouseClicked

    private void JTFCepFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCepFocusLost
        if (!JTFCep.getText().equals("     -   ")) {
            ViaCEP VC = new ViaCEP();
            try {
                VC.buscar(JTFCep.getText());
                JTFBairro.setText(VC.getBairro());
                JTFCidade.setText(VC.getLocalidade());
                JTFLogradouro.setText(VC.getLogradouro());
                JTFEstado.setText(VC.getUf());
                JTFNum.requestFocus();
            } catch (ViaCEPException ex) {
                Logger.getLogger(CadastroCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_JTFCepFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBPesquisa;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JLabel JLCNPJ;
    private javax.swing.JLabel JLCPF;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPCadastro;
    private javax.swing.JPanel JPConsulta;
    private javax.swing.JRadioButton JRFisico;
    private javax.swing.JRadioButton JRJuridico;
    private javax.swing.JTextField JTFBairro;
    private javax.swing.JFormattedTextField JTFCNPJ;
    private javax.swing.JFormattedTextField JTFCPF;
    private javax.swing.JFormattedTextField JTFCep;
    private javax.swing.JTextField JTFCidade;
    private javax.swing.JTextField JTFCodCliente;
    private javax.swing.JFormattedTextField JTFDataNasc;
    private javax.swing.JTextField JTFEmail;
    private javax.swing.JTextField JTFEstado;
    private javax.swing.JTextField JTFLogradouro;
    private javax.swing.JTextField JTFNome;
    private javax.swing.JTextField JTFNum;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JFormattedTextField JTFRG;
    private javax.swing.JTextField JTFRazao;
    private javax.swing.JFormattedTextField JTFTelCel;
    private javax.swing.JFormattedTextField JTFTelefone;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Botao() {
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    public void setCampos(ClassePessoa obj) {
        JTFCodCliente.setText(String.valueOf(obj.getID_PESSOA()));
        JTFNome.setText(obj.getNOME());
        JTFBairro.setText(obj.getBAIRRO());
        JTFCep.setText(obj.getCEP());
        if (obj.getTP_PESSOA().equals("F")) {
            JTFCPF.setText(Classe_Fisica.getCPF());
            JTFRG.setText(Classe_Fisica.getRG());
            JTFDataNasc.setText(Classe_Fisica.getDATA_NASC());
            JTFCNPJ.setText(null);
            JTFRazao.setText(null);
            JRFisico.setSelected(true);
        } else if (obj.getTP_PESSOA().equals("J")) {
            JTFCNPJ.setText(Classe_Juridica.getCNPJ());
            JTFRazao.setText(Classe_Juridica.getRAZAO());
            JTFCPF.setText(null);
            JTFRG.setText(null);
            JTFDataNasc.setText(null);
            JRJuridico.setSelected(true);
        }
        JTFTelCel.setText(obj.getTEL_CEL());
        JTFTelefone.setText(obj.getTEL_FIXO());
        JTFEmail.setText(obj.getEMAIL());
        JTFLogradouro.setText(obj.getLOGRADOURO());
        JTFCidade.setText(obj.getCIDADE());
        JTFNum.setText(obj.getNUM_CASA());
    }

    public ClassePessoa getCampos() {
        int tamanho = 0;
        String teste = null;

        if (estado == Rotinas.Alterar) {
            Classe_Cliente.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
        }
        Classe_Pessoa.setCEP(JTFCep.getText());
        Classe_Pessoa.setTEL_CEL(JTFTelCel.getText());
        Classe_Pessoa.setTEL_FIXO(JTFTelefone.getText());
        Classe_Pessoa.setNUM_CASA(JTFNum.getText());
        tamanho = JTFNome.getText().length();
        if (tamanho > 100) {
            teste = JTFNome.getText().substring(0, 60).toUpperCase();
            Classe_Pessoa.setNOME(teste);
        } else {
            Classe_Pessoa.setNOME(JTFNome.getText().toUpperCase());
        }
        tamanho = JTFBairro.getText().length();
        if (tamanho > 40) {
            teste = JTFBairro.getText().substring(0, 40).toUpperCase();
            Classe_Pessoa.setBAIRRO(teste);
        } else {
            Classe_Pessoa.setBAIRRO(JTFBairro.getText().toUpperCase());
        }
        tamanho = JTFEmail.getText().length();
        if (tamanho > 100) {
            teste = JTFEmail.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setEMAIL(teste);
        } else {
            Classe_Pessoa.setEMAIL(JTFEmail.getText().toUpperCase());
        }
        tamanho = JTFLogradouro.getText().length();
        if (tamanho > 100) {
            teste = JTFLogradouro.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setLOGRADOURO(teste);
        } else {
            Classe_Pessoa.setLOGRADOURO(JTFLogradouro.getText().toUpperCase());
        }
        tamanho = JTFCidade.getText().length();
        if (tamanho > 100) {
            teste = JTFCidade.getText().substring(0, 100).toUpperCase();
            Classe_Pessoa.setCIDADE(teste);
        } else {
            Classe_Pessoa.setCIDADE(JTFCidade.getText().toUpperCase());
        }
        Classe_Pessoa.setESTADO(JTFEstado.getText().toUpperCase());
        if (JRFisico.isSelected()) {
            Classe_Fisica.setCPF(JTFCPF.getText());
            Classe_Fisica.setRG(JTFRG.getText());
            Classe_Fisica.setDATA_NASC(JTFDataNasc.getText());
            Classe_Pessoa.setTP_PESSOA("F");
        } else {
            Classe_Juridica.setCNPJ(JTFCNPJ.getText());
            tamanho = JTFRazao.getText().length();
            if (tamanho > 100) {
                teste = JTFRazao.getText().substring(0, 100).toUpperCase();
                Classe_Juridica.setRAZAO(teste);
            } else {
                Classe_Juridica.setRAZAO(JTFRazao.getText().toUpperCase());
            }
            Classe_Pessoa.setTP_PESSOA("J");
        }

        return Classe_Pessoa;
    }

    public ClasseCliente getPosicao(String posicao) {
        Classe_Cliente.setID_CLIENTE(Integer.parseInt(posicao));
        return Classe_Cliente;
    }

    public ClasseFisica GetFisica() {
        Classe_Fisica.setCPF(JTFCPF.getText());
        if (JTFDataNasc.getText().equals("  /  /    ")) {
            Classe_Fisica.setDATA_NASC("");
        } else {
            Classe_Fisica.setDATA_NASC(JTFDataNasc.getText());
        }
        Classe_Fisica.setRG(JTFRG.getText());
        Classe_Fisica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        return Classe_Fisica;
    }

    public ClasseJuridica GetJuridica() {
        Classe_Juridica.setCNPJ(JTFCNPJ.getText());
        Classe_Juridica.setRAZAO(JTFRazao.getText().toUpperCase());
        Classe_Juridica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
        return Classe_Juridica;

    }

    public void Double_Click() {
        JTPesquisa.addMouseListener(
                new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                // Se o botão direito do mouse foi pressionado  
                if (e.getClickCount() == 2) {
                    //Se for 2x
                    retornar();
                    Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
                }
            }
        });
    }

    public void retornar() {
        JTFCodCliente.setText(String.valueOf(Classe_Cliente.getID_CLIENTE()));
        Controle_Pessoa.Recuperar(Classe_Pessoa);
        jTabbedPane1.setSelectedIndex(0);
        JTFPesquisar.setText(null);
        JTFPesquisar.setEditable(false);
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        JCPesquisa.setSelectedIndex(0x0);
        if (Classe_Pessoa.getTP_PESSOA().equals("F")) {
            Classe_Fisica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
            Controle_Fisica.Recuperar(Classe_Fisica);
            JTFCPF.setText(Classe_Fisica.getCPF());
            JTFDataNasc.setText(Classe_Fisica.getDATA_NASC());
            JTFRG.setText(Classe_Fisica.getRG());
            JRFisico.setSelected(true);
        } else {
            Classe_Juridica.setID_PESSOA(Classe_Pessoa.getID_PESSOA());
            Controle_Juridica.Retornar(Classe_Juridica);
            JTFCNPJ.setText(Classe_Juridica.getCNPJ());
            JTFRazao.setText(Classe_Juridica.getRAZAO());
            JRJuridico.setSelected(true);
        }

        JTFBairro.setText(Classe_Pessoa.getBAIRRO());
        JTFCep.setText(Classe_Pessoa.getCEP());
        JTFCidade.setText(Classe_Pessoa.getCIDADE());
        JTFEmail.setText(Classe_Pessoa.getEMAIL());
        JTFLogradouro.setText(Classe_Pessoa.getLOGRADOURO());
        JTFNome.setText(Classe_Pessoa.getNOME());
        JTFNum.setText(Classe_Pessoa.getNUM_CASA());
        JTFTelCel.setText(Classe_Pessoa.getTEL_CEL());
        JTFTelefone.setText(Classe_Pessoa.getTEL_FIXO());
    }
}
