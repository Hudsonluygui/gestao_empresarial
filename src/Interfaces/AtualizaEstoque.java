package Interfaces;

import Classes.ClasseMovProdutos;
import Classes.ClasseProdutos;
import Controles.ControleMovProdutos;
import Controles.ControleProdutos;
import Validações.DataAtual;
import Validações.LimparCampos;
import Validações.Preencher_JTableGenerico;
import Validações.ValidaEstadoBotoes;
import javax.swing.JOptionPane;

/**
 *
 * @author Hudson
 */
public class AtualizaEstoque extends javax.swing.JFrame {

    private int CodigoBanco;
    public int inclusao = 0;
    LimparCampos limpar = new LimparCampos();

    DataAtual data;
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();

    ClasseProdutos Classe_Produtos;
    ClasseMovProdutos Classe_MovProdutos;

    ControleProdutos ControleProdutos;
    ControleMovProdutos ControleMovimento;
    Preencher_JTableGenerico Preencher;

    public AtualizaEstoque() {
        initComponents();
        data = new DataAtual();
        Classe_Produtos = new ClasseProdutos();
        Classe_MovProdutos = new ClasseMovProdutos();
        ControleProdutos = new ControleProdutos();
        ControleMovimento = new ControleMovProdutos();
        Preencher = new Preencher_JTableGenerico();

        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPCadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCodProduto = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        JTFDescr = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFQuant = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFData = new javax.swing.JFormattedTextField();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        JREntrada = new javax.swing.JRadioButton();
        JRSaida = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        JRAlteracao = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Atualizar Estoque");
        setResizable(false);

        jLabel1.setText("Código Produto");

        JTFCodProduto.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Quantidade Movida");

        jLabel3.setText("Data do Movimento");

        try {
            JTFData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        buttonGroup1.add(JREntrada);
        JREntrada.setSelected(true);
        JREntrada.setText("Entrada ");

        buttonGroup1.add(JRSaida);
        JRSaida.setText("Saida de Produtos");

        jLabel4.setText("Tipo de Operação");

        buttonGroup1.add(JRAlteracao);
        JRAlteracao.setText("Alteração");

        javax.swing.GroupLayout JPCadastroLayout = new javax.swing.GroupLayout(JPCadastro);
        JPCadastro.setLayout(JPCadastroLayout);
        JPCadastroLayout.setHorizontalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, 1012, Short.MAX_VALUE)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(JTFQuant)
                            .addComponent(JTFCodProduto, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFDescr))
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFData)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JRAlteracao)
                            .addComponent(JRSaida)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(JREntrada))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        JPCadastroLayout.setVerticalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(JTFDescr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JREntrada)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRSaida)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRAlteracao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 256, Short.MAX_VALUE)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Cadastro", JPCadastro);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        inclusao = 1;
        JTFData.setText(data.Data());
        Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);

    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (JTFCodProduto.getText().equals(" ")) {
            JOptionPane.showMessageDialog(null, "Selecione um Produto");
            return;
        }
        if (JTFQuant.getText().equals("") || (JTFQuant.getText().equals('0'))) {
            JOptionPane.showMessageDialog(null, "Digite uma Quantidade Válida");
            JTFQuant.requestFocus();
            return;
        } else {
            if (ControleMovimento.Cadastrar(GetQuantidade())) {
                Classe_MovProdutos.setEST_ATUAL(ControleMovimento.EstoqueAtual(Classe_MovProdutos));
                ControleMovimento.Atualizar_Estoque(Classe_MovProdutos);
                JOptionPane.showMessageDialog(null, "Registro Atualizado com Sucesso");
                String Data = JTFData.getText();
                limpar.LimparCampos(JPCadastro);
                JTFData.setText(Data);

            } else {
                JOptionPane.showMessageDialog(null, "Ocorreu um Erro: " + Classe_MovProdutos.getERRO());
                return;
            }
        }


    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);

    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        final PesquisarProdutos Pesquisa = new PesquisarProdutos();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodProduto.setText(Pesquisa.Codigo);
                    JTFDescr.setText(Pesquisa.Descr);
                    CodigoBanco = Integer.parseInt(Pesquisa.CodigoBanco);
                }
            }
        });
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AtualizaEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AtualizaEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AtualizaEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AtualizaEstoque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AtualizaEstoque().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPCadastro;
    private javax.swing.JRadioButton JRAlteracao;
    private javax.swing.JRadioButton JREntrada;
    private javax.swing.JRadioButton JRSaida;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JFormattedTextField JTFData;
    private javax.swing.JTextField JTFDescr;
    private javax.swing.JTextField JTFQuant;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public ClasseMovProdutos GetQuantidade() {
        Classe_MovProdutos.setQUANT_MOVIDA(Double.parseDouble(JTFQuant.getText()));
        Classe_MovProdutos.setID_PRODUTO(JTFCodProduto.getText());
        if (JREntrada.isSelected()) {
            Classe_MovProdutos.setDESCR("COMPRA");
        } else if (JRSaida.isSelected()) {
            Classe_MovProdutos.setDESCR("RETIRADA");
        } else if (JRAlteracao.isSelected()) {
            Classe_MovProdutos.setDESCR("CADASTRO");
        }
        Classe_MovProdutos.setCODIGO(CodigoBanco);
        Classe_MovProdutos.setIDENTIF(JTFCodProduto.getText());
        Classe_MovProdutos.setDATA(JTFData.getText());
        return Classe_MovProdutos;
    }
}
