package Interfaces;

import Classes.ClasseMovNF;
import Classes.ClasseMovProdutos;
import Classes.ClasseProdutos;
import Classes.ClasseReceberVendas;
import Classes.NF_PRODUTOS;
import Controles.ControleMovProdutos;
import Controles.ControleNF;
import Controles.ControleNFProdutos;
import Controles.ControleProdutos;
import Controles.ControleReceberVendas;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import Validações.ValidarCNPJ;
import Validações.ValidarCPF;
import java.awt.HeadlessException;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MovNF extends javax.swing.JFrame {

    boolean AVista = false;
    boolean teste_quant = false;
    ClasseMovProdutos Classe_Mov;
    boolean Liquidada = false;
    public int teste_remover = 0;
    public boolean teste_geral;
    ClasseMovNF Classe_NF;
    NF_PRODUTOS Classe_NF_Produtos;
    Date data_atual;
    private SimpleDateFormat sdf;
    int CODIGO;
    public int Quantidade = 0;
    ConverterDecimais Converter;
    int[] Vetor_Produto;
    int[] Vetor_Produto2;
    int[] Vetor_CODIGO;
    ControleProdutos Controle_Produtos;
    ClasseProdutos Classe_Produto;
    public int Contador_Banco;
    public int Contador = 0;
    public int inclusao = 0;
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    LimparTabelas Limpar_Tabela;
    public int estado = Rotinas.Padrao;
    public String TesteData;
    public Date data;
    ControleReceberVendas Controle_ReceberVendas;
    ClasseReceberVendas Receber_Vendas;
    private final ValidarCPF Validar_CPF;
    private final ValidarCNPJ Validar_CNPJ;
    ControleNF Controle_NF;
    ControleNFProdutos Controle_NFPRodutos;
    ControleMovProdutos Controle_MovProdutos = new ControleMovProdutos();

    public MovNF(ValidarCPF Validar_CPF, ValidarCNPJ Validar_CNPJ) throws HeadlessException {
        this.Validar_CPF = Validar_CPF;
        this.Validar_CNPJ = Validar_CNPJ;
    }

    public MovNF() {
        initComponents();
        Classe_NF = new ClasseMovNF();
        Classe_NF_Produtos = new NF_PRODUTOS();
        Validar_CNPJ = new ValidarCNPJ();
        Validar_CPF = new ValidarCPF();
        Controle_NF = new ControleNF();
        Controle_NFPRodutos = new ControleNFProdutos();
        Controle_ReceberVendas = new ControleReceberVendas();
        Receber_Vendas = new ClasseReceberVendas();
        Limpar_Tabela = new LimparTabelas();
        Controle_Produtos = new ControleProdutos();
        Classe_Produto = new ClasseProdutos();
        Converter = new ConverterDecimais();
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
        Vetor_CODIGO = new int[Controle_Produtos.Criar_Vetor()];
        Contador_Banco = Controle_Produtos.Criar_Vetor();

        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JProduto, JPAINELBOTAO);
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);
        Classe_Mov = new ClasseMovProdutos();
        Botao();
        Double_Click();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPCadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFNUM_NF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFData_Nota = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFDataVencimento = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        JTFCodForn = new javax.swing.JTextField();
        JBPesquisaForn = new javax.swing.JButton();
        JTForn = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        JTFCNPJ = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        JTFRazao = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        JProduto = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        JTFCodProduto = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        JTFProduto = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        JTFMarca = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        JTFModelo = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        JTFUnidade = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        JTFTamanho = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        JTFQuant = new javax.swing.JTextField();
        JTFValorCusto = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        JTFTotalProduto = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTProdutos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTPesquisa = new javax.swing.JTable();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        JTPesquisaProd = new javax.swing.JTable();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        JTFTotalGeral = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Lançamento de Notas Fiscais");
        setResizable(false);

        jLabel1.setText("NF Nº");

        jLabel2.setText("Data da Nota");

        try {
            JTFData_Nota.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel3.setText("Data do Vencimento");

        try {
            JTFDataVencimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel4.setText("Cód. Fornecedor");

        JTFCodForn.setEditable(false);

        JBPesquisaForn.setText("Pesquisar");
        JBPesquisaForn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaFornActionPerformed(evt);
            }
        });

        JTForn.setEditable(false);

        jLabel5.setText("CNPJ");

        JTFCNPJ.setEditable(false);

        jLabel6.setText("Razão Social");

        JTFRazao.setEditable(false);

        jLabel8.setText("Nome Fantasia");

        javax.swing.GroupLayout JPCadastroLayout = new javax.swing.GroupLayout(JPCadastro);
        JPCadastro.setLayout(JPCadastroLayout);
        JPCadastroLayout.setHorizontalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(JTFCNPJ, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                        .addComponent(JTFCodForn, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(JTFNUM_NF, javax.swing.GroupLayout.Alignment.LEADING))
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(6, 6, 6)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFRazao)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(JBPesquisaForn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(JTFData_Nota, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(JTFDataVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(JTForn))))
                .addContainerGap())
        );
        JPCadastroLayout.setVerticalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFNUM_NF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDataVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData_Nota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodForn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBPesquisaForn)
                    .addComponent(JTForn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFRazao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(265, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", JPCadastro);

        jLabel7.setText("Cód. Produto");

        JTFCodProduto.setEditable(false);

        jButton2.setText("Pesquisar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel9.setText("Produto");

        JTFProduto.setEditable(false);

        jLabel13.setText("Marca");

        JTFMarca.setEditable(false);

        jLabel10.setText("Modelo");

        JTFModelo.setEditable(false);

        jLabel11.setText("Unidade de Medida");

        JTFUnidade.setEditable(false);

        jLabel12.setText("Tamanho");

        JTFTamanho.setEditable(false);

        jLabel14.setText("Quantidade");

        JTFQuant.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFQuantFocusLost(evt);
            }
        });

        JTFValorCusto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFValorCustoFocusLost(evt);
            }
        });

        jLabel15.setText("Valor Unitário");

        jLabel16.setText("Valor Total");

        JTFTotalProduto.setEditable(false);

        jButton3.setText("Remover");

        jButton4.setText("Adicionar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        JTProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Selecionar", "Cód. Produto", "Produto", "Marca", "Modelo", "Unidade Medida", "Tamanho", "Quantidade", "Valor Unitário", "Valor Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTProdutos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(JTProdutos);

        javax.swing.GroupLayout JProdutoLayout = new javax.swing.GroupLayout(JProduto);
        JProduto.setLayout(JProdutoLayout);
        JProdutoLayout.setHorizontalGroup(
            JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
                    .addGroup(JProdutoLayout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(0, 463, Short.MAX_VALUE))
                    .addGroup(JProdutoLayout.createSequentialGroup()
                        .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(JTFTotalProduto)
                            .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel13)
                                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(JTFTamanho, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFMarca)
                                    .addGroup(JProdutoLayout.createSequentialGroup()
                                        .addComponent(JTFCodProduto, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton2))))
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JProdutoLayout.createSequentialGroup()
                                .addGap(201, 201, 201)
                                .addComponent(jButton4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3))
                            .addGroup(JProdutoLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JTFProduto)
                                    .addGroup(JProdutoLayout.createSequentialGroup()
                                        .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(JTFModelo, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                            .addComponent(JTFQuant))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(JTFUnidade)
                                            .addComponent(JTFValorCusto)))
                                    .addGroup(JProdutoLayout.createSequentialGroup()
                                        .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel9)
                                            .addGroup(JProdutoLayout.createSequentialGroup()
                                                .addComponent(jLabel10)
                                                .addGap(132, 132, 132)
                                                .addComponent(jLabel11))
                                            .addGroup(JProdutoLayout.createSequentialGroup()
                                                .addComponent(jLabel14)
                                                .addGap(110, 110, 110)
                                                .addComponent(jLabel15)))
                                        .addGap(0, 0, Short.MAX_VALUE)))))))
                .addContainerGap())
        );
        JProdutoLayout.setVerticalGroup(
            JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(JTFProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFUnidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFQuant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFValorCusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTotalProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Produtos", JProduto);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Nº NF" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton5.setText("Pesquisar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        JTPesquisa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nº NF", "Fornecedor", "Data Emissao", "Data Vencimento", "Total", "Cód Forn"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTPesquisaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTPesquisa);

        jLabel18.setText("Produtos");

        JTPesquisaProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo Prod.", "Produto", "Marca", "Modelo", "Unidade Medida", "Tamanho", "Quantidade", "Valor Unitario", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTPesquisaProd.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(JTPesquisaProd);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(JTFPesquisar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Consulta", jPanel3);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        JTFTotalGeral.setEditable(false);
        JTFTotalGeral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFTotalGeralActionPerformed(evt);
            }
        });

        jLabel17.setText("*Total NF");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(JTFTotalGeral, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFTotalGeral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        inclusao = 1;
        JTFCodForn.requestFocus();
        estado = Rotinas.Incluir;
        limpar.LimparCampos(JPCadastro);
        Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposIncluir(JProduto, JPAINELBOTAO);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProd);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        JCPesquisa.setSelectedIndex(0);

    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed

        inclusao = 2;
        JCPesquisa.setSelectedIndex(0x0);
        if (Liquidada == true) {
            JOptionPane.showMessageDialog(null, "Não é possivel Fazer Alterações, Venda ja Liquidada");
            return;
        } else {
            if (JTFCodForn.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");
            } else {
                Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
                estado = Rotinas.Alterar;
                JTFCodForn.requestFocus();
            }
        }
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed

        int opcao;
        if (Liquidada == true) {
            JOptionPane.showMessageDialog(null, "Não é possivel Excluir, NF já Liquidada");
            return;
        } else {
            opcao = JOptionPane.showConfirmDialog(null, "NF será completamente removida, Deseja Continuar?");
            if (opcao == JOptionPane.YES_OPTION) {
                Classe_NF.setNR_NF(JTFNUM_NF.getText());
                opcao = JOptionPane.showConfirmDialog(null, "Deseja Reverter o Estoque Anterior a essa COMPRA?");
                if (opcao == JOptionPane.YES_OPTION) {
                    Reverter();
                    JOptionPane.showMessageDialog(null, "Estoque Revertido com Sucesso");
                }
                if (Controle_NF.Excluir(Classe_NF)) {
                    JOptionPane.showMessageDialog(null, "NF Removida");
                    Controle_ReceberVendas.Exluir(Excluir());
                    limpar.LimparCampos(JPCadastro);
                    Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
                    Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                    Botao();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Exclusão Cancelada");
                limpar.LimparCampos(JPCadastro);
                Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);
                Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                Botao();
            }
        }


    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed

        int Tabela = JTProdutos.getRowCount();
        if (Tabela == 0) {
            JOptionPane.showMessageDialog(null, "Nenhum Produto Adicionado");
            return;
        } else if (JTFCodForn.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe Fornecedor");
            return;
        } else if (JTFDataVencimento.getText().equals("  /  /    ")) {
            JOptionPane.showMessageDialog(null, "Informe a Data do Vencimento");
            JTFDataVencimento.requestFocus();
            return;
        } else {
            TesteData = JTFDataVencimento.getText();
            if (!Validar_Data()) {
                JOptionPane.showMessageDialog(null, "Data Inválida");
                JTFDataVencimento.setText(null);
                JTFDataVencimento.requestFocus();
                return;

            } else {
                TesteData = JTFData_Nota.getText();
                Validar_Data2();
                if (data.compareTo(data_atual) < 0) {
                    JOptionPane.showMessageDialog(null, "Data Inferior a Data da NOTA");
                    return;
                } else if (data.compareTo(data_atual) == 0) {
                    int opcao = JOptionPane.showConfirmDialog(null, "Deseja Gerar essa NF a Vista?");
                    if (opcao == JOptionPane.YES_OPTION) {
                        AVista = true;
                    } else {
                        AVista = false;
                    }
                }
            }
            if (inclusao == 1) {
                if (Controle_NF.Cadastrar(GetCampos())) {
                    ControleNFProdutos Gravar = GetProdutos();
                    Controle_ReceberVendas.Cadastrar(GerarRecebimento());
                    JOptionPane.showMessageDialog(null, "Nota Fiscal Lançada com sucesso");
                } else {
                    JOptionPane.showMessageDialog(null, "Ocorreu Algum Erro");
                    return;
                }
            } else if (inclusao == 2) {
                Receber_Vendas.setIDENTIF(JTFNUM_NF.getText());
                int Codigo_Parcela = Controle_ReceberVendas.Codigo(Receber_Vendas);
                Receber_Vendas.setID_PARCELA(Codigo_Parcela);
                Controle_ReceberVendas.Exluir(Receber_Vendas);
                Controle_ReceberVendas.Cadastrar(GerarRecebimento());
                ControleNFProdutos Alterar = GetProdutos();
                if (teste_geral == true) {
                    JOptionPane.showMessageDialog(null, "Alteração Realizada com Sucesso");
                }
            }
            Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
            Vetor_CODIGO = new int[Controle_Produtos.Criar_Vetor()];
            Vetor_Produto2 = new int[Controle_Produtos.Criar_Vetor()];
            Contador_Banco = Controle_Produtos.Criar_Vetor();
            Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
            Botao();
            limpar.LimparCampos(JPCadastro);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProd);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
            Limpar_Tabela.Limpar_tabela_pesquisa(JTProdutos);

        }

    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed

    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        final ConsultaProdutos Pesquisa = new ConsultaProdutos();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodProduto.setText(Pesquisa.Codigo);
                    JTFProduto.setText(Pesquisa.Descr);
                    JTFMarca.setText(Pesquisa.Marca);
                    JTFModelo.setText(Pesquisa.Modelo);
                    JTFValorCusto.setText(Pesquisa.Valor_Unitario);
                    JTFUnidade.setText(Pesquisa.Unidade_Medida);
                    JTFTamanho.setText(Pesquisa.Tamanho);
                    Quantidade = Integer.parseInt(Pesquisa.Quantidade);
                    Classe_Produto.setID_PRODUTO(Pesquisa.Codigo);
                    Classe_Produto.setDATA_CADASTRO(Pesquisa.Data);
                    Classe_Produto.setDS_PRODUTO(Pesquisa.Descr);
                    Classe_Produto.setDS_UNIDADE(Pesquisa.Unidade_Medida);
                    Classe_Produto.setMARCA(Pesquisa.Marca);
                    Classe_Produto.setMODELO(Pesquisa.Modelo);
                    Classe_Produto.setPERC_LUCRO(Double.parseDouble(Pesquisa.Perc_Lucro));
                    Classe_Produto.setQUANT(Double.parseDouble(Pesquisa.Quantidade));
                    Classe_Produto.setTAMANHO(Pesquisa.Tamanho);
                    Classe_Produto.setTOTAL_CUSTO(Double.parseDouble(Pesquisa.Valor_Total));
                    Classe_Produto.setVL_CUSTO(Double.parseDouble(Pesquisa.Valor_Unitario));
                    Classe_Produto.setVL_VENDA(Double.parseDouble(Pesquisa.Valor_Venda));

                    Vetor_Produto[Contador] = Controle_Produtos.Pesquisar_Codigo(Classe_Produto);
                    Contador++;

                }
            }
        });

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        if (JTFCodProduto.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Nada a Ser Adicionado");
            return;
        } else if (JTFQuant.getText().equals("")) {

            JOptionPane.showMessageDialog(null, "Informe a Quantidade");
        } else {
            if (teste_quant == true) {
                Adicionar_Produtos();

            } else {
                return;

            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void JTFQuantFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFQuantFocusLost

        if (!JTFQuant.getText().equals("")) {
            double Quant;
            try {
                Quant = Double.parseDouble(JTFQuant.getText().replaceAll(",", "."));
                if (Quant > Quantidade) {
                    JOptionPane.showMessageDialog(null, "Quantidade Maior que a Existente");
                    JTFQuant.setText("");
                    JTFQuant.requestFocus();
                    JTFTotalProduto.setText("");
                    teste_quant = false;
                } else {
                    teste_quant = true;
                }
            } catch (NumberFormatException ex) {
                JTFQuant.setText("");
                JTFQuant.requestFocus();
                return;
            }

        }

        double A = 0;
        if (!JTFQuant.getText().equals("") && (!JTFValorCusto.getText().equals(""))) {
            double valor = 0;
            try {
                A = Double.parseDouble(JTFValorCusto.getText().replaceAll(",", "."));
                valor = (Double.parseDouble(JTFQuant.getText()) * A);
                A = Converter.converterDoubleDoisDecimais(valor);
                JTFTotalProduto.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JTFValorCusto.setText("");
                JTFValorCusto.requestFocus();
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFQuantFocusLost

    private void JTFValorCustoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFValorCustoFocusLost
        double A = 0;
        if (!JTFQuant.getText().equals("") && (!JTFValorCusto.getText().equals(""))) {
            double valor = 0;
            try {
                A = Double.parseDouble(JTFValorCusto.getText().replaceAll(",", "."));
                valor = (Double.parseDouble(JTFQuant.getText()) * A);
                A = Converter.converterDoubleDoisDecimais(valor);
                JTFTotalProduto.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JTFValorCusto.setText("");
                JTFValorCusto.requestFocus();
            }
        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFValorCustoFocusLost

    private void JTFTotalGeralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFTotalGeralActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFTotalGeralActionPerformed

    private void JBPesquisaFornActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaFornActionPerformed

        final ConsultaFornecedor Pesquisa = new ConsultaFornecedor();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                JTFCodForn.setText(Pesquisa.Codigo);
                JTFRazao.setText(Pesquisa.RAZAO);
                JTFCNPJ.setText(Pesquisa.CNPJ);
                JTForn.setText(Pesquisa.Nome);
            }
        });

// TODO add your handling code here:
    }//GEN-LAST:event_JBPesquisaFornActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed
        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(JTPesquisa, Controle_NFPRodutos.PesquisaGeral());
                break;
            }
            case 1: {
                if (JTFPesquisar.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Numero da Nota Fiscal");
                    JTFPesquisar.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(JTPesquisa, Controle_NFPRodutos.PesquisaNum(JTFPesquisar.getText()));
                }
                break;
            }
        }


    }//GEN-LAST:event_jButton5ActionPerformed

    private void JTPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTPesquisaMouseClicked

        int Index = JTPesquisa.getSelectedRow();
        String NR = (String) JTPesquisa.getValueAt(Index, 0);
        Classe_NF.setNR_NF(NR);
        preencher.Preencher_JTableGenerico(JTPesquisaProd, Controle_NFPRodutos.Pesquisar_Produtos(Classe_NF));
        String Data_Emissao = (String) JTPesquisa.getValueAt(Index, 2);
        String Data_Vencimento = (String) JTPesquisa.getValueAt(Index, 3);
        Classe_NF.setDATA_EMISSAO(Data_Emissao);
        Classe_NF.setDATA_VENCIMENTO(Data_Vencimento);


    }//GEN-LAST:event_JTPesquisaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MovNF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MovNF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MovNF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MovNF.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MovNF().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBPesquisaForn;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPCadastro;
    private javax.swing.JPanel JProduto;
    private javax.swing.JTextField JTFCNPJ;
    private javax.swing.JTextField JTFCodForn;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JFormattedTextField JTFDataVencimento;
    private javax.swing.JFormattedTextField JTFData_Nota;
    private javax.swing.JTextField JTFMarca;
    private javax.swing.JTextField JTFModelo;
    private javax.swing.JTextField JTFNUM_NF;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTextField JTFProduto;
    private javax.swing.JTextField JTFQuant;
    private javax.swing.JTextField JTFRazao;
    private javax.swing.JTextField JTFTamanho;
    private javax.swing.JTextField JTFTotalGeral;
    private javax.swing.JTextField JTFTotalProduto;
    private javax.swing.JTextField JTFUnidade;
    private javax.swing.JTextField JTFValorCusto;
    private javax.swing.JTextField JTForn;
    private javax.swing.JTable JTPesquisa;
    private javax.swing.JTable JTPesquisaProd;
    private javax.swing.JTable JTProdutos;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public void Botao() {
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Double_Click() {
        JTPesquisa.addMouseListener(
                new java.awt.event.MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        // Se o botão direito do mouse foi pressionado  
                        if (e.getClickCount() == 2) {
                            //Se for 2x
                            retornar();
                        }
                    }
                });
    }

    public void retornar() {
        JTFCodForn.setText(String.valueOf(Classe_NF.getID_FORNECEDOR()));
        JTFCodForn.setEditable(false);
        JBPesquisaForn.setEnabled(false);
        preencher.Preencher_JTableGenerico(JTProdutos, Controle_NFPRodutos.Pesquisar_Produtos2(Classe_NF));
        JCPesquisa.setSelectedIndex(0x0);
        jTabbedPane1.setSelectedIndex(0);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisa);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTPesquisaProd);
        JTFNUM_NF.setText(String.valueOf(Classe_NF.getNR_NF()));
        JTFData_Nota.setText(Classe_NF.getDATA_EMISSAO());
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Receber_Vendas.setIDENTIF(JTFNUM_NF.getText());
        Receber_Vendas.setDESCR("COMPRA");
        Liquidada = Controle_ReceberVendas.PesquisarSituacao(Receber_Vendas);
        JTFDataVencimento.setText(Classe_NF.getDATA_VENCIMENTO());
        int linhas = JTProdutos.getRowCount();
        int X;
        double Total = 0;
        for (X = 0; X < linhas; X++) {
            String Valor = (String) JTProdutos.getValueAt(X, 9);
            Total = Total + Double.parseDouble(Valor);
            JTFTotalGeral.setText(String.valueOf(Total));
        }
    }

    public void Adicionar_Produtos() {
        int conta = 0;
        DefaultTableModel TabelaTeste = (DefaultTableModel) JTProdutos.getModel();
        int totlinha = JTProdutos.getRowCount();    // pega quantiade de linhas da grid
        int i, vai = 0;
        int cod, index = 0;
        conta = conta + 1;
        double total;
        int x = JTProdutos.getRowCount();
        if (vai == 0) {
            String Codi = (String.valueOf(0));
            conta = 0;
            for (i = 1; i <= totlinha; i++) {
                Codi = (String) JTProdutos.getValueAt(conta, 1);
                conta++;
            }
            TabelaTeste.setNumRows(x + 1);
            TabelaTeste.setValueAt(JTFCodProduto.getText(), x, 1);
            TabelaTeste.setValueAt(JTFProduto.getText(), x, 2);
            TabelaTeste.setValueAt(JTFMarca.getText(), x, 3);
            TabelaTeste.setValueAt(JTFModelo.getText(), x, 4);
            TabelaTeste.setValueAt(JTFUnidade.getText(), x, 5);
            TabelaTeste.setValueAt(JTFTamanho.getText(), x, 6);
            TabelaTeste.setValueAt(JTFQuant.getText(), x, 7);
            TabelaTeste.setValueAt(JTFValorCusto.getText(), x, 8);
            TabelaTeste.setValueAt(JTFTotalProduto.getText(), x, 9);
            JTFCodProduto.setText("");
            JTFProduto.setText("");
            JTFMarca.setText("");
            JTFModelo.setText("");
            JTFUnidade.setText("");
            JTFTamanho.setText("");
            JTFQuant.setText("");
            JTFValorCusto.setText("");
            JTFTotalProduto.setText("");
            if (x == 0) {
                total = Double.parseDouble(String.valueOf(TabelaTeste.getValueAt(x, 9)));
                total = Converter.converterDoubleDoisDecimais(total);
                JTFTotalGeral.setText(String.valueOf(total));
            } else {
                total = Double.parseDouble(JTFTotalGeral.getText());
                total = total + Double.parseDouble(String.valueOf(TabelaTeste.getValueAt(x, 9)));
                total = Converter.converterDoubleDoisDecimais(total);
                JTFTotalGeral.setText(String.valueOf(total));
            }
        }
    }

    public void Reverter() {
        int X;
        int Linhas;
        Linhas = JTProdutos.getRowCount();
        for (X = 0; X < Linhas; X++) {
            String Codigo = (String) JTProdutos.getValueAt(X, 1);
            Classe_Mov.setIDENTIF(JTFNUM_NF.getText());
            Classe_Mov.setID_PRODUTO(Codigo);
            Controle_MovProdutos.BuscarEstoque_Reversao(Classe_Mov);
        }
    }

    public ClasseReceberVendas Excluir() {
        Receber_Vendas.setIDENTIF(Classe_NF.getNR_NF());
        Receber_Vendas.setDESCR("COMPRA");
        return Receber_Vendas;
    }

    public boolean Validar_Data2() {
        try {
            data_atual = sdf.parse(TesteData);
            return true;

            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }

    }

    public boolean Validar_Data() {
        try {
            data = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }

    public ClasseMovNF GetCampos() {
        Classe_NF.setDATA_VENCIMENTO(JTFDataVencimento.getText());
        Classe_NF.setDATA_EMISSAO(JTFData_Nota.getText());
        Classe_NF.setID_FORNECEDOR(Integer.parseInt(JTFCodForn.getText()));
        Classe_NF.setNR_NF(JTFNUM_NF.getText());
        Classe_NF.setVALOR_NF(Double.parseDouble(JTFTotalGeral.getText()));
        return Classe_NF;
    }

    public ControleNFProdutos GetProdutos() {
        Contador = 0;
        int totlinha = JTProdutos.getRowCount();
        int i = 0;
        int x = 0;
        int Resultado = 0;
        int Codigo = 0;
        for (i = 1; i <= totlinha; i++) {
            String cod = (String) JTProdutos.getValueAt(x, 1);
            String Descr = (String) JTProdutos.getValueAt(x, 2);
            String Marca = (String) JTProdutos.getValueAt(x, 3);
            String Modelo = (String) JTProdutos.getValueAt(x, 4);
            String Unidade = (String) JTProdutos.getValueAt(x, 5);
            String Tamanho = (String) JTProdutos.getValueAt(x, 6);
            String Quant = (String) JTProdutos.getValueAt(x, 7);
            String Valor_Unit = (String) JTProdutos.getValueAt(x, 8);
            String Valor_total = (String) JTProdutos.getValueAt(x, 9);

            Classe_Produto.setDS_PRODUTO(Descr);
            Classe_Produto.setMARCA(Marca);
            Classe_Produto.setMODELO(Modelo);
            Classe_Produto.setDS_UNIDADE(Unidade);;
            Classe_Produto.setTAMANHO(Tamanho);
            Classe_Produto.setID_PRODUTO(cod);
            Codigo = Controle_Produtos.Pesquisar_Codigo2(Classe_Produto);
            if (Codigo == 0) {
                Codigo = Vetor_Produto[Contador];
            }
            Classe_NF_Produtos.setCODIGO(Codigo);
            Classe_NF_Produtos.setID_PRODUTO(cod);
            Classe_NF_Produtos.setNR_NF(JTFNUM_NF.getText());
            Classe_NF_Produtos.setQUANT(Double.parseDouble(Quant));
            Classe_NF_Produtos.setVALOR_UNIT(Double.parseDouble(Valor_Unit));
            Classe_NF_Produtos.setVALOR_TOTAL(Double.parseDouble(Valor_total));
            Classe_NF_Produtos.setMARCA(Marca.toUpperCase());
            Classe_NF_Produtos.setMODELO(Modelo);
            Classe_NF_Produtos.setTAMANHO(Tamanho);
            Classe_NF_Produtos.setUNIDADE(Unidade);

            Resultado = Controle_NFPRodutos.PesquisarExistentes(Classe_NF_Produtos);
            Classe_NF.setNR_NF(JTFNUM_NF.getText());
            Classe_NF.setDATA_EMISSAO(JTFData_Nota.getText());
            if (Resultado != 0) {
                if (Controle_NFPRodutos.Cadastrar(Classe_NF_Produtos, Classe_NF)) {
                    teste_geral = true;
                } else {
                    teste_geral = false;
                    break;
                }
            }

            Contador++;
            x = x + 1;
        }
        if (teste_remover != 0) {
            for (Contador = 0; Contador < Contador_Banco; Contador++) {
                if (Vetor_CODIGO[Contador] != 0) {
                    String Codigo_Produto = String.valueOf(Vetor_Produto2[Contador]);
                    int CODIGO_Gerado = Vetor_CODIGO[Contador];
                    Classe_NF_Produtos.setCODIGO(CODIGO_Gerado);
                    Classe_NF_Produtos.setID_PRODUTO(Codigo_Produto);
                    Classe_NF_Produtos.setNR_NF(JTFNUM_NF.getText());
                    Resultado = Controle_NFPRodutos.PesquisarExistentes(Classe_NF_Produtos);
                    if (Resultado == 0) {
                        if (!Controle_NFPRodutos.Remover_Produto(Classe_NF_Produtos)) {
                            JOptionPane.showMessageDialog(null, "Não foi Possivel Remover algum(uns) Produto(s)");
                            teste_geral = false;
                            break;
                        } else {
                            teste_geral = true;
                        }
                    }
                }
            }
        }
        return Controle_NFPRodutos;
    }

    public ClasseReceberVendas GerarRecebimento() {
        Receber_Vendas.setIDENTIF(JTFNUM_NF.getText());
        Receber_Vendas.setDATA_PAGO(null);
        Receber_Vendas.setDATA_VENCIMENTO(JTFDataVencimento.getText());
        Receber_Vendas.setDESCONTO(0.00);
        Receber_Vendas.setDESCR("COMPRA");
        Receber_Vendas.setDS_FORMA(" ");
        Receber_Vendas.setERRO(" ");
        Receber_Vendas.setJUROS(0.00);
        if (AVista == true) {
            Receber_Vendas.setSITUACAO("LIQUIDADA");
        } else {
            Receber_Vendas.setSITUACAO("ABERTA");
        }
        Receber_Vendas.setVALOR_PAGO(0.00);
        Receber_Vendas.setVALOR_PARCELA(Double.parseDouble(JTFTotalGeral.getText()));
        Receber_Vendas.setVALOR_TOTAL(Double.parseDouble(JTFTotalGeral.getText()));
        return Receber_Vendas;
    }
}
