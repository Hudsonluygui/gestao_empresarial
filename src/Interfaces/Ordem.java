package Interfaces;

import Classes.ClasseCaixa;
import Classes.ClasseCliente;
import Classes.ClasseClienteItem;
import Classes.ClasseMovProdutos;
import Classes.ClasseOrdem;
import Classes.ClasseOrdemAssociativa;
import Classes.ClasseProdutos;
import Classes.ClasseReceberVendas;
import Classes.ClasseServicos;
import Controles.ControleCaixa;
import Controles.ControleCliente;
import Controles.ControleClienteItens;
import Controles.ControleMovProdutos;
import Controles.ControleOrdem;
import Controles.ControleOrdemAssociativa;
import Controles.ControleProdutos;
import Controles.ControleReceberVendas;
import Controles.ControleServico;
import static Interfaces.ParcelaOrdem.Cliente;
import Relatorio.NotaVenda;
import Validações.ConverterDecimais;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.UltimaSequencia;
import Validações.ValidaEstadoBotoes;
import Validações.ValidarCNPJ;
import Validações.ValidarCPF;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class Ordem extends javax.swing.JFrame {

    public boolean Termino = false;
    int codigo_rel;
    public static String Servico_;
    public static String Produto_;
    public static String CodigoVencido = "";
    int opcao;
    int b = 0;
    int sair = 0;
    String Data_Vencimento;
    public Date Data;
    public Date data_Vencimento;
    public boolean AVista = false;
    public Date data_atual;
    public Date data;
    public String TesteData;
    private SimpleDateFormat sdf;
    private final ValidarCPF Validar_CPF;
    private final ValidarCNPJ Validar_CNPJ;
    public boolean teste_geral = false;
    public boolean Liquidada = false;
    public int teste_remover = 0;
    public boolean teste_quant = false;
    public int Quantidade = 0;
    ConverterDecimais Converter;
    int[] Vetor_Servico;
    int[] Vetor_Produto;
    ControleProdutos Controle_Produtos;
    ClasseProdutos Classe_Produto;
    public double Vetor_Valores = 0;
    public int Contador_Banco;
    public int Contador = 0;
    public int inclusao = 0;

    public ConsultaItem ConsultarItens;

    ClasseOrdemAssociativa Classe_OrdemAssociativa;
    ClasseCliente Classe_Cliente = new ClasseCliente();
    ClasseOrdem Classe_Ordem = new ClasseOrdem();
    ControleCliente Controle_Cliente = new ControleCliente();
    ControleClienteItens Controle_ClienteItem;
    ClasseClienteItem Classe_ClienteItem;
    ClasseServicos Classe_Servico = new ClasseServicos();
    ControleServico Controle_Servico;
    ControleOrdemAssociativa Controle_Associativa;
    ClasseReceberVendas Classe_Receber;
    ControleReceberVendas Controle_Receber;
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    LimparTabelas Limpar_Tabela;

    public int estado = Rotinas.Padrao;
    ControleOrdem Controle_Ordem;
    ClasseMovProdutos Classe_Mov;
    ControleMovProdutos Controle_Mov;
    int Teste = 0;
    int X = 0;
    int Quant_Parc;
    double Valor_Entrada;
    double Valor_Aux = 0;
    double Valor_Parcelas;
    int Entrada = 0;
    double valor_Entrada = 0;
    double Valor = 0;
    public String tipo;
    FormaPagamento forma;

    public Ordem() {
        initComponents();

        Classe_Mov = new ClasseMovProdutos();
        Controle_Mov = new ControleMovProdutos();
        Controle_Ordem = new ControleOrdem();
        Classe_OrdemAssociativa = new ClasseOrdemAssociativa();
        Classe_Produto = new ClasseProdutos();
        Controle_Servico = new ControleServico();
        Classe_ClienteItem = new ClasseClienteItem();
        Controle_ClienteItem = new ControleClienteItens();
        Controle_Produtos = new ControleProdutos();
        Limpar_Tabela = new LimparTabelas();
        Controle_Associativa = new ControleOrdemAssociativa();
        Classe_Receber = new ClasseReceberVendas();
        Controle_Receber = new ControleReceberVendas();
        Validar_CNPJ = new ValidarCNPJ();
        Validar_CPF = new ValidarCPF();
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        //**************************************************************************//
        sdf.setLenient(false);
        JCSituacao.setEnabled(false);
        Vetor_Servico = new int[100];
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPDados, JPAINELBOTAO);

        Valida_Botao.ValidaCamposCancelar(JPProduto, JPAINELBOTAO);
        Botao();

        Tamanho_Jtable(JTProduto, 0, 50);
        Tamanho_Jtable(JTProduto, 1, 50);
        Tamanho_Jtable(JTProduto, 2, 250);
        Tamanho_Jtable(JTProduto, 3, 100);
        Tamanho_Jtable(JTProduto, 4, 100);
        Tamanho_Jtable(JTProduto, 5, 100);

        Tamanho_Jtable(JTConsulta, 0, 50);
        Tamanho_Jtable(JTConsulta, 1, 250);
        Tamanho_Jtable(JTConsulta, 2, 100);
        Tamanho_Jtable(JTConsulta, 3, 100);

        Double_Click();
        if (!CodigoVencido.equals("")) {
            jTabbedPane1.setSelectedIndex(3);;
            preencher.Preencher_JTableGenerico(JTConsulta, Controle_Ordem.PesquisarCodigo(Integer.parseInt(CodigoVencido), "VENDA"));

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPCadastro = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCod = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFCodCliente = new javax.swing.JTextField();
        JBPesquisaCliente = new javax.swing.JButton();
        JTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFCPF_CNPJ = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        JTFDescr = new javax.swing.JTextArea();
        JROrcamento = new javax.swing.JRadioButton();
        JROrdem = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        JPProduto = new javax.swing.JPanel();
        JTFTotalProd = new javax.swing.JTextField();
        JTFQuantidade = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        JTFValorUnitario = new javax.swing.JTextField();
        JTFProduto = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        JBPesquisaProduto = new javax.swing.JButton();
        JTFCodProduto = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        JTProduto = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        JTFTotal_Produtos = new javax.swing.JTextField();
        JPDados = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        JCSituacao = new javax.swing.JComboBox();
        JTFData_Aberta = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        JTFEntrada = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        JTFNum_Parc = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        JTFData_Parcela = new javax.swing.JFormattedTextField();
        JPConsulta = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisar = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        JTConsulta = new javax.swing.JTable();
        jLabel28 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        JRPesquisaOrcamento = new javax.swing.JRadioButton();
        JRPesquisaVendas = new javax.swing.JRadioButton();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        JTFTotalGeral = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        JTFDesconto = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        JTFBruto = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("VENDA");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Código da Ordem");

        JTFCod.setEditable(false);

        jLabel2.setText("Código do Cliente");

        JTFCodCliente.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFCodClienteFocusLost(evt);
            }
        });

        JBPesquisaCliente.setText("Pesquisar");
        JBPesquisaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaClienteActionPerformed(evt);
            }
        });

        JTFNome.setEditable(false);

        jLabel3.setText("Nome");

        JTFCPF_CNPJ.setEditable(false);

        jLabel4.setText("CPF/CNPJ");

        jLabel5.setText("Descrição");

        JTFDescr.setColumns(20);
        JTFDescr.setRows(5);
        jScrollPane3.setViewportView(JTFDescr);

        buttonGroup1.add(JROrcamento);
        JROrcamento.setSelected(true);
        JROrcamento.setText("Orçamento");

        buttonGroup1.add(JROrdem);
        JROrdem.setText("Venda");

        jLabel6.setText("Tipo de Movimento");

        javax.swing.GroupLayout JPCadastroLayout = new javax.swing.GroupLayout(JPCadastro);
        JPCadastro.setLayout(JPCadastroLayout);
        JPCadastroLayout.setHorizontalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(JPCadastroLayout.createSequentialGroup()
                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(JTFCodCliente, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(JBPesquisaCliente))
                                    .addComponent(JTFCod))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(JTFCPF_CNPJ)
                                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                                .addComponent(jLabel4)
                                                .addGap(0, 178, Short.MAX_VALUE))))
                                    .addGroup(JPCadastroLayout.createSequentialGroup()
                                        .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(JPCadastroLayout.createSequentialGroup()
                                                .addComponent(JROrcamento)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(JROrdem))
                                            .addComponent(jLabel6))
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addComponent(jScrollPane3))
                        .addContainerGap())))
        );
        JPCadastroLayout.setVerticalGroup(
            JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JROrcamento)
                    .addComponent(JROrdem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFCodCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JBPesquisaCliente)
                    .addComponent(JTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFCPF_CNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(261, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cadastro", JPCadastro);

        JTFTotalProd.setEditable(false);

        JTFQuantidade.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFQuantidadeFocusLost(evt);
            }
        });
        JTFQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFQuantidadeActionPerformed(evt);
            }
        });

        jLabel17.setText("Produto");

        JTFValorUnitario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFValorUnitarioFocusLost(evt);
            }
        });

        JTFProduto.setEditable(false);

        jLabel24.setText("Valor Total(Produtos)");

        jLabel22.setText("Quantidade");

        JBPesquisaProduto.setText("Pesquisar");
        JBPesquisaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPesquisaProdutoActionPerformed(evt);
            }
        });

        JTFCodProduto.setEditable(false);

        jLabel11.setText("Cód. Produto");

        jLabel23.setText("Valor Unitário");

        JTProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sel.", "Código", "Produto", "Quantidade", "Valor Unit", "Valor Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTProduto.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane5.setViewportView(JTProduto);

        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Adicionar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel29.setText("Total Produtos");

        JTFTotal_Produtos.setEditable(false);

        javax.swing.GroupLayout JPProdutoLayout = new javax.swing.GroupLayout(JPProduto);
        JPProduto.setLayout(JPProdutoLayout);
        JPProdutoLayout.setHorizontalGroup(
            JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPProdutoLayout.createSequentialGroup()
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPProdutoLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel11)
                        .addGap(111, 111, 111)
                        .addComponent(jLabel17))
                    .addGroup(JPProdutoLayout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JTFTotal_Produtos, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(JPProdutoLayout.createSequentialGroup()
                        .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, JPProdutoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(JTFQuantidade))
                            .addGroup(JPProdutoLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(JBPesquisaProduto)))
                        .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPProdutoLayout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(JTFProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 710, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(JPProdutoLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(JPProdutoLayout.createSequentialGroup()
                                        .addGap(0, 426, Short.MAX_VALUE)
                                        .addComponent(jButton2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton1))
                                    .addGroup(JPProdutoLayout.createSequentialGroup()
                                        .addComponent(JTFTotalProd, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(JPProdutoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel22)
                        .addGap(122, 122, 122)
                        .addComponent(jLabel23)
                        .addGap(88, 88, 88)
                        .addComponent(jLabel24))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        JPProdutoLayout.setVerticalGroup(
            JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPProdutoLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel17))
                .addGap(7, 7, 7)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(JPProdutoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(JTFCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JBPesquisaProduto)
                    .addComponent(JTFProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFTotalProd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JTFQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(JTFValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTotal_Produtos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Produtos", JPProduto);

        jLabel9.setText("Situação ");

        JCSituacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Aberta", "Fechada" }));
        JCSituacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCSituacaoActionPerformed(evt);
            }
        });

        try {
            JTFData_Aberta.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel13.setText("Data Aberta");

        jLabel14.setText("Dados Financeiros");

        jLabel15.setText("Valor de Entrada");

        jLabel33.setText("Nº de Parcelas");

        jLabel34.setText("Data do Primeiro Vencimento");

        try {
            JTFData_Parcela.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout JPDadosLayout = new javax.swing.GroupLayout(JPDados);
        JPDados.setLayout(JPDadosLayout);
        JPDadosLayout.setHorizontalGroup(
            JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(JPDadosLayout.createSequentialGroup()
                        .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(JPDadosLayout.createSequentialGroup()
                                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JCSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9))
                                .addGap(18, 18, 18)
                                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(JTFData_Aberta, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(JTFEntrada)
                            .addComponent(jLabel33)
                            .addComponent(JTFNum_Parc))
                        .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(JPDadosLayout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jLabel34))
                            .addGroup(JPDadosLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFData_Parcela)))
                        .addGap(0, 557, Short.MAX_VALUE)))
                .addContainerGap())
        );
        JPDadosLayout.setVerticalGroup(
            JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JCSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData_Aberta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFNum_Parc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFData_Parcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(287, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Dados ", JPDados);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Código", "Cód. Cliente", "Nome Cliente" }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisar.setEditable(false);

        jButton5.setText("Pesquisar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        JTConsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Cliente", "Data Aberta", "Operação", "Valor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTConsulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTConsultaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(JTConsulta);

        jLabel28.setForeground(new java.awt.Color(255, 0, 0));
        jLabel28.setText("*As Pesquisas por Data devem ser Seguidas de \"/\" (01/01/2019)");

        jButton3.setText("Imprimir");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        buttonGroup2.add(JRPesquisaOrcamento);
        JRPesquisaOrcamento.setSelected(true);
        JRPesquisaOrcamento.setText("Orçamentos");
        JRPesquisaOrcamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRPesquisaOrcamentoActionPerformed(evt);
            }
        });

        buttonGroup2.add(JRPesquisaVendas);
        JRPesquisaVendas.setText("Vendas");
        JRPesquisaVendas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRPesquisaVendasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout JPConsultaLayout = new javax.swing.GroupLayout(JPConsulta);
        JPConsulta.setLayout(JPConsultaLayout);
        JPConsultaLayout.setHorizontalGroup(
            JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPConsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(JPConsultaLayout.createSequentialGroup()
                        .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JRPesquisaOrcamento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JRPesquisaVendas))
                    .addGroup(JPConsultaLayout.createSequentialGroup()
                        .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 751, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addGap(0, 22, Short.MAX_VALUE)))
                .addContainerGap())
        );
        JPConsultaLayout.setVerticalGroup(
            JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPConsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JRPesquisaOrcamento)
                        .addComponent(JRPesquisaVendas)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(JPConsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Consulta", JPConsulta);

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        JTFTotalGeral.setEditable(false);

        jLabel26.setText("Total Geral");

        JTFDesconto.setEditable(false);
        JTFDesconto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                JTFDescontoFocusLost(evt);
            }
        });

        jCheckBox1.setText("% Desconto");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        JTFBruto.setEditable(false);

        jLabel32.setText("Valor Bruto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jCheckBox1)
                                .addGap(50, 50, 50))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(JTFBruto, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(JTFDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26)
                            .addComponent(JTFTotalGeral, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 536, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox1)
                    .addComponent(jLabel26)
                    .addComponent(jLabel32))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFTotalGeral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(JTFBruto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        limpar.LimparCampos(JPCadastro);
        limpar.LimparCampos(JPProduto);
        limpar.LimparCampos(JPDados);
        UltimaSequencia us = new UltimaSequencia("ID_ORDEM", "ORDEM");
        Classe_Ordem.setID_ORDEM(Integer.parseInt(us.getUtl()));
        int Codigo = Classe_Ordem.getID_ORDEM();
        JTFCod.setText(String.valueOf(Codigo));
        JTFData_Aberta.setText(Controle_Ordem.Data());
        inclusao = 1;
        JTFNome.requestFocus();
        estado = Rotinas.Incluir;

        Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposIncluir(JPDados, JPAINELBOTAO);

        Valida_Botao.ValidaCamposIncluir(JPProduto, JPAINELBOTAO);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);

        Limpar_Tabela.Limpar_tabela_pesquisa(JTProduto);
        JCPesquisa.setSelectedIndex(0);
        JTFDesconto.setText("0.00");
        JTFTotalGeral.setText("");
        JTFBruto.setText("");

        JTFTotalProd.setText("");

    }//GEN-LAST:event_jBIncluirActionPerformed

    private void JBPesquisaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaClienteActionPerformed

        final ConsultaCliente Pesquisa = new ConsultaCliente();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodCliente.setText(Pesquisa.Codigo);
                    JTFNome.setText(Pesquisa.Nome);
                    if (Pesquisa.CPF.equals(" ")) {
                        JTFCPF_CNPJ.setText(Pesquisa.CNPJ);
                    } else if (Pesquisa.CNPJ.equals(" ")) {
                        JTFCPF_CNPJ.setText(Pesquisa.CPF);
                    }
                }
            }
        });
    }//GEN-LAST:event_JBPesquisaClienteActionPerformed

    private void JTFCodClienteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFCodClienteFocusLost

        if (!JTFCodCliente.getText().equals("")) {
            try {
                Classe_Cliente.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
                JTFNome.setText(Controle_Cliente.PesquisarNome(Classe_Cliente));
                JTFCPF_CNPJ.setText(Controle_Cliente.BuscarCPF_CNPJ(Classe_Cliente));

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Código Inválido");
                JTFCodCliente.setText("");
                JTFCodCliente.requestFocus();
                JTFCodCliente.setText("");
                JTFNome.setText("");
                JTFCPF_CNPJ.setText(null);

                return;
            }
        } else {
            JTFNome.setText("");
            JTFCPF_CNPJ.setText("");

        }
    }//GEN-LAST:event_JTFCodClienteFocusLost

    private void JBPesquisaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPesquisaProdutoActionPerformed
        final ConsultaProdutos Pesquisa = new ConsultaProdutos();
        Pesquisa.setVisible(true);
        Pesquisa.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                if (Pesquisa.A != 0) {
                    JTFCodProduto.setText(Pesquisa.Codigo_Banco);
                    JTFProduto.setText(Pesquisa.Descr);

                    JTFValorUnitario.setText(Pesquisa.Valor_Unitario);

                    Quantidade = Integer.parseInt(Pesquisa.Quantidade);
                    Classe_Produto.setCODIGO(Integer.parseInt(Pesquisa.Codigo_Banco));
                    Classe_Produto.setID_PRODUTO(Pesquisa.Codigo);
                    Classe_Produto.setDATA_CADASTRO(Pesquisa.Data);
                    Classe_Produto.setDS_PRODUTO(Pesquisa.Descr);
                    Classe_Produto.setDS_UNIDADE(Pesquisa.Unidade_Medida);
                    Classe_Produto.setMARCA(Pesquisa.Marca);
                    Classe_Produto.setMODELO(Pesquisa.Modelo);
                    Classe_Produto.setPERC_LUCRO(Double.parseDouble(Pesquisa.Perc_Lucro));
                    Classe_Produto.setQUANT(Double.parseDouble(Pesquisa.Quantidade));
                    Classe_Produto.setTAMANHO(Pesquisa.Tamanho);
                    Classe_Produto.setTOTAL_CUSTO(Double.parseDouble(Pesquisa.Valor_Total));
                    Classe_Produto.setVL_CUSTO(Double.parseDouble(Pesquisa.Valor_Unitario));
                    Classe_Produto.setVL_VENDA(Double.parseDouble(Pesquisa.Valor_Venda));
                    JTFValorUnitario.setText(String.valueOf(Classe_Produto.getVL_VENDA()));

                    Produto_ = Pesquisa.Descr;
                    Contador++;

                }
            }
        });

// TODO add your handling code here:
    }//GEN-LAST:event_JBPesquisaProdutoActionPerformed

    private void JTFQuantidadeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFQuantidadeFocusLost
        if (!JTFQuantidade.getText().equals("")) {
            double Quant;
            try {
                if (JROrdem.isSelected()) {
                    Quant = Double.parseDouble(JTFQuantidade.getText().replaceAll(",", "."));
                    if (Quant > Quantidade) {
                        JOptionPane.showMessageDialog(null, "Quantidade Maior que a Existente");
                        JTFQuantidade.setText("");
                        JTFQuantidade.requestFocus();
                        JTFTotalProd.setText("");
                        teste_quant = false;
                        return;
                    } else {
                        teste_quant = true;
                    }
                }
            } catch (NumberFormatException ex) {
                JTFQuantidade.setText("");
                JTFQuantidade.requestFocus();
                return;
            }
        }
        double A = 0;
        if (!JTFQuantidade.getText().equals("") && (!JTFValorUnitario.getText().equals(""))) {
            double valor = 0;
            try {
                A = Double.parseDouble(JTFValorUnitario.getText().replaceAll(",", "."));
                valor = (Double.parseDouble(JTFQuantidade.getText()) * A);
                A = Converter.converterDoubleDoisDecimais(valor);
                JTFTotalProd.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JTFValorUnitario.setText("");
                JTFValorUnitario.requestFocus();
            }
        }
    }//GEN-LAST:event_JTFQuantidadeFocusLost

    private void JTFValorUnitarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFValorUnitarioFocusLost
        double A = 0;
        if (!JTFQuantidade.getText().equals("") && (!JTFValorUnitario.getText().equals(""))) {
            double valor = 0;
            try {
                A = Double.parseDouble(JTFValorUnitario.getText().replaceAll(",", "."));
                valor = (Double.parseDouble(JTFQuantidade.getText()) * A);
                A = Converter.converterDoubleDoisDecimais(valor);
                JTFTotalProd.setText(String.valueOf(A));
            } catch (NumberFormatException ex) {
                JTFValorUnitario.setText("");
                JTFValorUnitario.requestFocus();
            }
        }

// TODO add your handling code here:
    }//GEN-LAST:event_JTFValorUnitarioFocusLost

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (inclusao == 1) {
            if (Controle_Ordem.Cadastrar(GetCampos())) {
                JTFCod.setText(String.valueOf(Classe_Ordem.getID_ORDEM()));
                Classe_Ordem.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
                Classe_OrdemAssociativa.setID_ORDEM(Classe_Ordem.getID_ORDEM());

                int tamanho1 = JTProduto.getRowCount();
                if (tamanho1 != 0) {
                    GetProdutos();
                }

            }
            JOptionPane.showMessageDialog(null, "Registro Salvo com Sucesso");
        }
        if (!JTFData_Parcela.getText().equals("  /  /    ")) {
            TesteData = JTFData_Parcela.getText();
        } else {
            TesteData = JTFData_Aberta.getText();
        }
        Validar_Data();
        if (inclusao == 2) {
            if (Controle_Ordem.Alterar(GetCampos())) {
                GetProdutos();
                RemoverProduto();
                JOptionPane.showMessageDialog(null, "Registro Alterada com Sucesso");
            }
        }
        if (!JTFEntrada.getText().equals("")) {
            Valor_Entrada = Double.parseDouble(JTFEntrada.getText());

        }

        if (!JTFNum_Parc.getText().equals("")) {

            Quant_Parc = Integer.parseInt(JTFNum_Parc.getText()) + 1;
        } else {
            Quant_Parc = 1;
        }
        Valor_Parcelas = Double.parseDouble(JTFTotalGeral.getText());
        Contador = 1;
        somar_datas();
        String Vencimento = Controle_Ordem.Data_Vencimento1(Classe_Ordem.getID_ORDEM());
        if (!Vencimento.equals("")) {
            Classe_Ordem.setDATA_VENCIMENTO(Vencimento);
            Controle_Ordem.AlterarData(Classe_Ordem);
        } else {
            Vencimento = Controle_Ordem.Data_Vencimento2(Classe_Ordem.getID_ORDEM());
            Classe_Ordem.setDATA_VENCIMENTO(Vencimento);
            Controle_Ordem.AlterarData(Classe_Ordem);
        }
        int Contar_Parc = Controle_Receber.Contar_Parcelas(Classe_Receber);
        int Contar_Pago = Controle_Receber.Contar_Pagos(Classe_Receber);
        if (Contar_Pago == Contar_Parc) {
            Classe_Ordem.setPAGO("S");
            Controle_Ordem.Pago(Classe_Ordem);
        } else {
            Classe_Ordem.setPAGO("N");
            Controle_Ordem.Pago(Classe_Ordem);
        }
        if (JTFEntrada.getText().equals("")) {
            int opcao = JOptionPane.showConfirmDialog(null, "Deseja Gerar um arquivo em PDF?");
            if (opcao == JOptionPane.YES_OPTION) {
                NotaVenda Nota = new NotaVenda();
                try {
                    codigo_rel = Classe_Ordem.getID_ORDEM();
                    Nota.OrdemGeral(codigo_rel);
                } catch (Exception e) {
                    System.out.println(e);
                }
                Classe_OrdemAssociativa.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
                int A = Nota.Cont(Classe_OrdemAssociativa);
                if (A != 0) {
                    Nota.OrdemProduto(Classe_Ordem);
                }
            }
        }

        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPDados, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPProduto, JPAINELBOTAO);
        Botao();
        jCheckBox1.setSelected(false);

    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed
        JCSituacao.setEnabled(true);
        inclusao = 2;
        JCPesquisa.setSelectedIndex(0x0);
        if (Liquidada == true) {
            JOptionPane.showMessageDialog(null, "Não é possivel Fazer Alterações, Ordem ja Liquidada");
            return;
        } else if (JTFCodCliente.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");
        } else {
            Valida_Botao.ValidaCamposIncluir(JPCadastro, JPAINELBOTAO);
            Valida_Botao.ValidaCamposIncluir(JPDados, JPAINELBOTAO);

            Valida_Botao.ValidaCamposIncluir(JPProduto, JPAINELBOTAO);
            JTFCodCliente.setEditable(false);
            JBPesquisaCliente.setEnabled(false);
            estado = Rotinas.Alterar;
            JTFNome.requestFocus();
            jCheckBox1.setSelected(false);
            jCheckBox1ActionPerformed(null);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed
        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 3) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 4) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 5) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 6) {
            JTFPesquisar.setEditable(true);
            JTFPesquisar.setText("");
            JTFPesquisar.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisar.setEditable(false);
            JTFPesquisar.setText("");
        }

    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if (JRPesquisaOrcamento.isSelected()) {
            Classe_Ordem.setOPERACAO("ORCAMENTO");
        }else if (JRPesquisaVendas.isSelected()){
            Classe_Ordem.setOPERACAO("VENDA");
        }
            switch (JCPesquisa.getSelectedIndex()) {
                case 0: {
                     preencher.Preencher_JTableGenerico(JTConsulta, Controle_Ordem.PesquisarGeral(Classe_Ordem.getOPERACAO()));
                    break;
                }case 1:{
                    preencher.Preencher_JTableGenerico(JTConsulta, Controle_Ordem.PesquisarCodigo(Integer.parseInt(JTFPesquisar.getText()), Classe_Ordem.getOPERACAO()));
                    break;
                }case 2: {
                  preencher.Preencher_JTableGenerico(JTConsulta, Controle_Ordem.PesquisarCodigoCliente(Integer.parseInt(JTFPesquisar.getText()), Classe_Ordem.getOPERACAO()));
                  break;
                }case 3: {
                preencher.Preencher_JTableGenerico(JTConsulta, Controle_Ordem.PesquisarNomeCliente(JTFPesquisar.getText().toUpperCase(),Classe_Ordem.getOPERACAO()));
                }break;
            }
        


    }//GEN-LAST:event_jButton5ActionPerformed

    private void JTConsultaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTConsultaMouseClicked

        int Index = JTConsulta.getSelectedRow();
        String Codigo = (String) JTConsulta.getValueAt(Index, 0);
        Classe_Ordem.setID_ORDEM(Integer.parseInt(Codigo));
        Classe_Ordem = Controle_Ordem.Retorno(Integer.parseInt(Codigo));
        JTFTotalGeral.setText("");
        codigo_rel = Classe_Ordem.getID_ORDEM();

    }//GEN-LAST:event_JTConsultaMouseClicked

    private void JCSituacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCSituacaoActionPerformed


    }//GEN-LAST:event_JCSituacaoActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed

        int opcao;
        if (Liquidada == true) {
            JOptionPane.showMessageDialog(null, "Não é possivel Excluir, venda já Liquidada");
            return;
        } else {
            opcao = JOptionPane.showConfirmDialog(null, "Ordem de Serviço será completamente removida, Deseja Continuar?");
            if (opcao == JOptionPane.YES_OPTION) {
                Classe_Ordem.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
                opcao = JOptionPane.showConfirmDialog(null, "Deseja Reverter o Estoque Anterior a essa Ordem?");
                if (opcao == JOptionPane.YES_OPTION) {
                    Reverter();
                    JOptionPane.showMessageDialog(null, "Estoque Revertido com Sucesso");
                }
                if (Controle_Ordem.Excluir(Classe_Ordem)) {
                    JOptionPane.showMessageDialog(null, "Ordem de Serviço Removida");
                    Controle_Receber.Exluir(Excluir());
                    limpar.LimparCampos(JPCadastro);
                    limpar.LimparCampos(JPDados);

                    Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                    Botao();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Exclusão Cancelada");
                limpar.LimparCampos(JPCadastro);
                limpar.LimparCampos(JPDados);

                Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
                Botao();
            }
        }
        JTFTotalGeral.setText("");
        JTFBruto.setText("");
        JTFDescr.setText(" ");
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (JTFCodProduto.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o Produto!");
            return;
        } else {
            Adicionar_Produto();
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        int tamanho = JTProduto.getRowCount();
        if (tamanho <= 0) {
            JOptionPane.showMessageDialog(null, "Nada a ser Removido");
            return;
        } else {
            Remover_Produto();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        if (JTConsulta.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "1º Realize a Pesquisa e clique sobre a desejada");
            return;
        } else {
            NotaVenda Nota = new NotaVenda();

            Nota.OrdemGeral(codigo_rel);

            Classe_OrdemAssociativa.setID_ORDEM(Classe_Ordem.getID_ORDEM());

        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void JTFDescontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_JTFDescontoFocusLost

        if (jCheckBox1.isSelected() == true) {
            double Desconto = Double.parseDouble(JTFDesconto.getText());
            double valor = 0;
            double perc = 0;
            try {
                valor = Double.parseDouble(JTFBruto.getText());
                perc = valor * Desconto / 100;
                valor = valor - perc;
                valor = Converter.converterDoubleDoisDecimais(valor);
                JTFTotalGeral.setText(String.valueOf(valor));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor de Percentual Inválido");
                JTFDesconto.setText("");
                JTFDesconto.requestFocus();
            }

        }
// TODO add your handling code here:
    }//GEN-LAST:event_JTFDescontoFocusLost

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed

// TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        Vetor_Servico = new int[100];
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];
        limpar.LimparCampos(JPCadastro);
        limpar.LimparCampos(JPDados);

        limpar.LimparCampos(JPProduto);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);

        Limpar_Tabela.Limpar_tabela_pesquisa(JTProduto);
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPDados, JPAINELBOTAO);

        Valida_Botao.ValidaCamposCancelar(JPProduto, JPAINELBOTAO);
        JTFTotalGeral.setText(null);
        JTFDesconto.setText(null);
        jCheckBox1.setSelected(false);
        jCheckBox1.action(null, evt);

// TODO add your handling code here:
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (JPConsulta.isShowing() == true) {
            Botao();
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void JTFQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFQuantidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JTFQuantidadeActionPerformed

    private void JRPesquisaVendasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRPesquisaVendasActionPerformed
Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);
        // TODO add your handling code here:
    }//GEN-LAST:event_JRPesquisaVendasActionPerformed

    private void JRPesquisaOrcamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRPesquisaOrcamentoActionPerformed
Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);
        // TODO add your handling code here:
    }//GEN-LAST:event_JRPesquisaOrcamentoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ordem.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ordem.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ordem.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ordem.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ordem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBPesquisaCliente;
    private javax.swing.JButton JBPesquisaProduto;
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JComboBox JCSituacao;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPCadastro;
    private javax.swing.JPanel JPConsulta;
    private javax.swing.JPanel JPDados;
    private javax.swing.JPanel JPProduto;
    private javax.swing.JRadioButton JROrcamento;
    private javax.swing.JRadioButton JROrdem;
    private javax.swing.JRadioButton JRPesquisaOrcamento;
    private javax.swing.JRadioButton JRPesquisaVendas;
    private javax.swing.JTable JTConsulta;
    private javax.swing.JTextField JTFBruto;
    private javax.swing.JTextField JTFCPF_CNPJ;
    private javax.swing.JTextField JTFCod;
    private javax.swing.JTextField JTFCodCliente;
    private javax.swing.JTextField JTFCodProduto;
    private javax.swing.JFormattedTextField JTFData_Aberta;
    private javax.swing.JFormattedTextField JTFData_Parcela;
    private javax.swing.JTextField JTFDesconto;
    private javax.swing.JTextArea JTFDescr;
    private javax.swing.JTextField JTFEntrada;
    private javax.swing.JTextField JTFNome;
    private javax.swing.JTextField JTFNum_Parc;
    private javax.swing.JTextField JTFPesquisar;
    private javax.swing.JTextField JTFProduto;
    private javax.swing.JTextField JTFQuantidade;
    private javax.swing.JTextField JTFTotalGeral;
    private javax.swing.JTextField JTFTotalProd;
    private javax.swing.JTextField JTFTotal_Produtos;
    private javax.swing.JTextField JTFValorUnitario;
    private javax.swing.JTable JTProduto;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    public void Botao() {
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

    public void Adicionar_Produto() {

        int conta = 0;
        DefaultTableModel TabelaTeste = (DefaultTableModel) JTProduto.getModel();
        int totlinha = TabelaTeste.getRowCount();
        int i, vai = 0;
        int Cod, Index = 0;
        conta = conta + 1;
        double Total = 0;
        int X = TabelaTeste.getRowCount();
        Double Total_Produto = null;
        if (vai == 0) {
            String Codigo = (String.valueOf(0));
            conta = 0;
            for (i = 1; i <= totlinha; i++) {
                Codigo = (String) TabelaTeste.getValueAt(conta, 1);
                conta++;
            }
            TabelaTeste.setNumRows(X + 1);
            TabelaTeste.setValueAt(JTFCodProduto.getText(), X, 1);
            TabelaTeste.setValueAt(JTFProduto.getText(), X, 2);

            TabelaTeste.setValueAt(JTFQuantidade.getText(), X, 3);
            TabelaTeste.setValueAt(JTFValorUnitario.getText(), X, 4);
            TabelaTeste.setValueAt(JTFTotalProd.getText(), X, 5);

            JTFCodProduto.setText("");
            JTFProduto.setText("");

            JTFQuantidade.setText("");
            JTFValorUnitario.setText("");
            JTFTotalProd.setText("");

            if (X == 0) {
                Total_Produto = Double.parseDouble((String) TabelaTeste.getValueAt(X, 5));
                Total_Produto = Converter.converterDoubleDoisDecimais(Total_Produto);
                JTFTotal_Produtos.setText(String.valueOf(Total_Produto));
                if (!JTFTotalGeral.getText().equals("")) {
                    Total = Double.parseDouble(JTFTotalGeral.getText());
                }
                Total = Total + Total_Produto;
            } else {
                Total_Produto = Double.parseDouble(JTFTotal_Produtos.getText());
                Total_Produto = Total_Produto + Double.parseDouble(String.valueOf(TabelaTeste.getValueAt(X, 5)));
                Total_Produto = Converter.converterDoubleDoisDecimais(Total_Produto);
                Total = Double.parseDouble(JTFTotalGeral.getText()) + Double.parseDouble(String.valueOf(TabelaTeste.getValueAt(X, 5)));
                Total = Converter.converterDoubleDoisDecimais(Total);
                JTFTotal_Produtos.setText(String.valueOf(Total_Produto));
            }
            JTFTotalGeral.setText(String.valueOf(Total));
            JTFBruto.setText(JTFTotalGeral.getText());

        }
    }

    public void Remover_Produto() {
        teste_remover = 1;
        DefaultTableModel tabela = (DefaultTableModel) JTProduto.getModel();
        int totlinha = tabela.getRowCount();    // pega quantiade de linhas da grid   
        int i = 0;
        int x = 0;
        String Cod = null;
        Boolean sel = false;
        double Total = 0;
        double Valor = 0;
        double Bruto = 0;
        for (i = totlinha - 1; i >= 0; i--) {
            Boolean selecionado = (Boolean) tabela.getValueAt(i, 0);
            Cod = (String) tabela.getValueAt(i, 1);
            if (selecionado == null) {
                selecionado = false;
            }
            if (selecionado == true) {
                sel = true;
                String CodigoProduto = (String) tabela.getValueAt(i, 1);
                String Produto = (String) tabela.getValueAt(i, 2);

                String Quant = (String) tabela.getValueAt(i, 3);
                String ValorUnitario = (String) tabela.getValueAt(i, 4);
                String ValorTotalProd = (String) tabela.getValueAt(i, 5);
                if (!JTFCod.getText().equals("")) {
                    Classe_OrdemAssociativa.setCODIGO(Integer.parseInt(CodigoProduto));
                    Classe_OrdemAssociativa.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
                    int Resultado = Controle_Associativa.PesquisarProdutosExistentes(Classe_OrdemAssociativa);
                    if (Resultado == 0) {
                        Vetor_Produto[Contador] = Integer.parseInt(CodigoProduto);
                    }
                    Valor = Double.parseDouble(JTFTotal_Produtos.getText());

                    Total = Double.parseDouble(JTFTotalGeral.getText()) - Double.parseDouble(ValorTotalProd);
                    JTFTotalGeral.setText(String.valueOf(Total));
                    Valor = Valor - Double.parseDouble(ValorTotalProd);
                    Valor = Converter.converterDoubleDoisDecimais(Valor);

                    JTFTotal_Produtos.setText(String.valueOf(Valor));
                    Bruto = Double.parseDouble(JTFBruto.getText());
                    Bruto = Bruto - Double.parseDouble(ValorTotalProd);
                    JTFBruto.setText(String.valueOf(Bruto));
                } else {
                    Valor = Double.parseDouble(JTFTotal_Produtos.getText());
                    Total = Double.parseDouble(JTFTotalGeral.getText()) - Double.parseDouble(ValorTotalProd);
                    JTFTotalGeral.setText(String.valueOf(Total));
                    Valor = Valor - Double.parseDouble(ValorTotalProd);
                    Valor = Converter.converterDoubleDoisDecimais(Valor);
                    JTFTotal_Produtos.setText(String.valueOf(Valor));

                    Bruto = Double.parseDouble(JTFBruto.getText());
                    Bruto = Bruto - Double.parseDouble(ValorTotalProd);
                    JTFBruto.setText(String.valueOf(Bruto));

                }
                Contador++;
                tabela.removeRow(i);
            }
        }

        if (!sel == true) {
            JOptionPane.showMessageDialog(null, "Nao ha nenhum registro selecionado !");
        }
    }

    public ClasseOrdem GetCampos() {
        String Data = Controle_Ordem.Data();
        if (JCSituacao.getSelectedIndex() == 0) {
            JTFData_Aberta.setText(Data);
            Classe_Ordem.setSITUACAO("ABERTA");
        } else {
            Classe_Ordem.setSITUACAO("FECHADA");
        }
        Classe_Ordem.setDATA_ABERTA(JTFData_Aberta.getText());
        Classe_Ordem.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
        Classe_Ordem.setDESCR(JTFDescr.getText());
        Classe_Ordem.setVALOR_TOTAL(Double.parseDouble(JTFTotalGeral.getText()));

        if (JTFCod.getText().equals("") || (JCSituacao.getSelectedIndex() == 0)) {
            Classe_Ordem.setDATA_FECHA(null);
            Classe_Ordem.setDATA_VENCIMENTO(null);
        }

        if (jCheckBox1.isSelected() == true) {
            Classe_Ordem.setVALOR_BRUTO(Double.parseDouble(JTFBruto.getText()));
            Classe_Ordem.setPERC(Double.parseDouble(JTFDesconto.getText()));
        } else {
            Classe_Ordem.setVALOR_BRUTO(Double.parseDouble(JTFBruto.getText()));
            Classe_Ordem.setPERC(0.00);
        }
        if (JROrcamento.isSelected()) {
            Classe_Ordem.setOPERACAO("ORCAMENTO");
            Classe_OrdemAssociativa.setOPERACAO("ORCAMENTO");
        } else if (JROrdem.isSelected()) {
            Classe_Ordem.setOPERACAO("VENDA");
            Classe_OrdemAssociativa.setOPERACAO("VENDA");
        }

        return Classe_Ordem;
    }

    public ControleOrdemAssociativa RemoverProduto() {
        Contador = 0;
        if (teste_remover != 0) {
            int A = Vetor_Produto.length;
            for (Contador = 0; Contador < A; Contador++) {
                if (Vetor_Produto[Contador] != 0) {
                    Classe_OrdemAssociativa.setCODIGO(Vetor_Produto[Contador]);
                    Controle_Associativa.Remover_Produto(Classe_OrdemAssociativa);
                }
            }
        }
        return Controle_Associativa;
    }

    public ControleOrdemAssociativa GetProdutos() {
        int totlinha = JTProduto.getRowCount();
        int i = 0;
        int x = 0;
        int Resultado = 0;
        for (i = 1; i <= totlinha; i++) {
            String Cod_Produto = (String) JTProduto.getValueAt(x, 1);
            String Produto = (String) JTProduto.getValueAt(x, 2);
            String Quantidade = (String) JTProduto.getValueAt(x, 3);
            String ValorUnitario = (String) JTProduto.getValueAt(x, 4);
            String ValorTotal = (String) JTProduto.getValueAt(x, 5);

            Classe_OrdemAssociativa.setCODIGO(Integer.parseInt(Cod_Produto));
            Classe_OrdemAssociativa.setID_ORDEM(Classe_Ordem.getID_ORDEM());
            Classe_OrdemAssociativa.setQUANT(Double.parseDouble(Quantidade));
            Classe_OrdemAssociativa.setVL_UNITARIO(Double.parseDouble(ValorUnitario));
            Classe_OrdemAssociativa.setVL_TOTAL(Double.parseDouble(ValorTotal));
            Classe_OrdemAssociativa.setTOTAL_GERAL(Double.parseDouble(JTFTotal_Produtos.getText()));
            Classe_OrdemAssociativa.setDATA(JTFData_Aberta.getText());
            Resultado = Controle_Associativa.PesquisarProdutosExistentes(Classe_OrdemAssociativa);

            if (Resultado != 0) {
                Controle_Associativa.Cadastrar_Produtos(Classe_OrdemAssociativa);
            }

            x = x + 1;
            //NÃO ESQUECER QUE CÓDIGO É DO PRODUTO GERADO PELO BANCO
            //E ID É O CARA QUE INFORMA

        }
        return Controle_Associativa;
    }

    public void Double_Click() {
        JTConsulta.addMouseListener(
                new java.awt.event.MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                // Se o botão direito do mouse foi pressionado  
                if (e.getClickCount() == 2) {
                    //Se for 2x
                    retornar();
                }
            }
        });
    }

    public void retornar() {
        JTFCodCliente.setText(String.valueOf(Classe_Ordem.getID_CLIENTE()));
        JTFCodCliente.setEditable(false);
        JBPesquisaCliente.setEnabled(false);
        JTFCodClienteFocusLost(null);

        if (Controle_Associativa.VerificarProdutos(Classe_Ordem.getID_ORDEM()) != 0) {
            preencher.Preencher_JTableGenerico(JTProduto, Controle_Associativa.PesquisarRetornoProdutos(Classe_Ordem.getID_ORDEM()));
        }
        JCPesquisa.setSelectedIndex(0x0);
        jTabbedPane1.setSelectedIndex(0);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);
        JTFCod.setText(String.valueOf(Classe_Ordem.getID_ORDEM()));
        JTFData_Aberta.setText(Classe_Ordem.getDATA_ABERTA());
        JTFDescr.setText(Classe_Ordem.getDESCR());

        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Classe_Receber.setIDENTIF(JTFCod.getText());
        Classe_Receber.setDESCR("ORDEM SERVICO");
        Liquidada = Controle_Receber.PesquisarSituacao(Classe_Receber);

        int linhas1 = JTProduto.getRowCount();
        int X;
        double Total = 0, Total_Servico = 0, Total_Produto = 0;

        for (X = 0; X < linhas1; X++) {
            Total_Produto = Total_Produto + Double.parseDouble(String.valueOf(JTProduto.getValueAt(X, 5)));
            Total_Produto = Converter.converterDoubleDoisDecimais(Total_Produto);
            JTFTotal_Produtos.setText(String.valueOf(Total_Produto));
        }
        Total = Total_Produto + Total_Servico;
        Total = Converter.converterDoubleDoisDecimais(Total);
        JTFTotalGeral.setText(String.valueOf(Total));
        Classe_OrdemAssociativa.setID_ORDEM(Classe_Ordem.getID_ORDEM());
        Classe_OrdemAssociativa.setID_CLIENTE(Classe_Ordem.getID_CLIENTE());
        Classe_OrdemAssociativa.setID_CARRO(Controle_Associativa.PesquisarCarro(Classe_OrdemAssociativa));
        Classe_Ordem = Controle_Associativa.RetornoBruto(Classe_Ordem.getID_ORDEM());
        JTFBruto.setText(String.valueOf(Classe_Ordem.getVALOR_BRUTO()));
        Classe_Ordem.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
        Classe_Ordem = Controle_Associativa.RetornoPerc(Classe_Ordem.getID_ORDEM());
        Classe_Ordem.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
        Classe_Ordem = Controle_Associativa.RetornoTotal(Classe_Ordem.getID_ORDEM());
        JTFTotalGeral.setText(String.valueOf(Classe_Ordem.getVALOR_TOTAL()));
        JTFDesconto.setText(String.valueOf(Classe_Ordem.getPERC()));
        if (!JTFDesconto.getText().equals("")) {
            jCheckBox1.setSelected(true);
            jCheckBox1ActionPerformed(null);
        }
    }

    public boolean Validar_Data() {
        try {
            Data = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }

    public boolean Validar_Data2() {
        try {
            data_atual = sdf.parse(TesteData);
            return true;

            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }
    }

    public boolean Validar_Data3() {
        try {
            data_Vencimento = sdf.parse(TesteData);
            return true;
            // se passou pra cá, é porque a data é válida
        } catch (ParseException e) {
            // se cair aqui, a data é inválida
            return false;
        }

    }

    public ClasseReceberVendas Excluir() {
        Classe_Receber.setIDENTIF(String.valueOf(Classe_Ordem.getID_ORDEM()));
        Classe_Receber.setDESCR("ORDEM SERVICO");
        return Classe_Receber;
    }

    public void Reverter() {
        int X;
        int Linhas;
        Linhas = JTProduto.getRowCount();
        for (X = 0; X < Linhas; X++) {
            String Codigo = (String) JTProduto.getValueAt(X, 4);
            Classe_Mov.setIDENTIF(JTFCod.getText());
            Classe_Mov.setID_PRODUTO(Codigo);
            Controle_Mov.BuscarEstoque_Reversao(Classe_Mov);
        }
    }

    public ClasseReceberVendas getParcelas() {
        Classe_Receber.setDESCR("ORDEM SERVICO");
        Classe_Receber.setIDENTIF(String.valueOf(Classe_Ordem.getID_ORDEM()));
        Classe_Receber.setDESCONTO(0.00);
        Classe_Receber.setJUROS(0.00);
        Classe_Receber.setID_CLIENTE(Integer.parseInt(JTFCodCliente.getText()));
        Valor_Entrada = Converter.converterDoubleDoisDecimais(Valor_Entrada);
        if (Valor_Entrada != (0)) {
            Classe_Receber.setDATA_VENCIMENTO(JTFData_Aberta.getText());
            Classe_Receber.setSITUACAO("LIQUIDADA");
            Classe_Receber.setVALOR_PARCELA(Valor_Entrada);
            Classe_Receber.setVALOR_PAGO(Valor_Entrada);
            Classe_Receber.setDATA_PAGO(JTFData_Aberta.getText());
            JOptionPane.showMessageDialog(null, "Selecione a forma de Entrada");
            final FormaPagamento forma = new FormaPagamento();
            forma.setVisible(true);
            forma.addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosed(java.awt.event.WindowEvent evt) {
                    tipo = FormaPagamento.Forma;
                    tipo = tipo.toUpperCase();
                    ControleCaixa Controle_Caixa = new ControleCaixa();
                    ClasseCaixa Classe_Caixa = new ClasseCaixa();
                    Classe_Caixa.setDATA(Classe_Ordem.getDATA_ABERTA());
                    Classe_Caixa.setDESCR(JTFNome.getText().toUpperCase() + " Ordem Nº " + JTFCod.getText());
                    Classe_Caixa.setFORMA(tipo);
                    Classe_Caixa.setOPERACAO("ENTRADA");
                    Classe_Caixa.setVALOR(Double.parseDouble(JTFEntrada.getText()));
                    Controle_Caixa.Cadastrar(Classe_Caixa);
                    int opcao = JOptionPane.showConfirmDialog(null, "Deseja Gerar um arquivo em PDF?");
                    if (opcao == JOptionPane.YES_OPTION) {
                        NotaVenda Nota = new NotaVenda();
                        try {
                            codigo_rel = Classe_Ordem.getID_ORDEM();
                            Nota.OrdemGeral(codigo_rel);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        Classe_OrdemAssociativa.setID_ORDEM(Integer.parseInt(JTFCod.getText()));
                        int A = Nota.Cont(Classe_OrdemAssociativa);
                        if (A != 0) {
                            Nota.OrdemProduto(Classe_Ordem);
                        }
                    }
                }
            });
            Valor_Aux = (Valor_Entrada);
            Valor_Entrada = 0;
            Teste = 1;
        } else {
            if (Teste == 1) {
                Valor_Parcelas = Valor_Parcelas - Valor_Aux;
                Quant_Parc = Quant_Parc - 1;
                Teste = 0;
            }
            Classe_Receber.setVALOR_PAGO(0.00);
            Classe_Receber.setDATA_PAGO("");
            Classe_Receber.setVALOR_TOTAL(Double.parseDouble(JTFTotalGeral.getText()));
            sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.format(Data);
            Classe_Receber.setDATA_VENCIMENTO(String.valueOf(sdf.format(Data)));
            Classe_Receber.setSITUACAO("ABERTA");
            try {
                Valor = Valor_Parcelas / Quant_Parc;
                Vetor_Valores = Vetor_Valores + Valor;
                X = X + 1;
            } catch (NumberFormatException ex) {
                System.out.println(ex);
            }
            if (X != Quant_Parc) {
                Classe_Receber.setVALOR_PARCELA(Valor);
                Classe_Receber.setVALOR_PAGO(0.00);
            } else {
                double a = Converter.converterDoubleDoisDecimais(Valor);
                Classe_Receber.setVALOR_PARCELA(a);
            }
        }
        Classe_Receber.setVALOR_TOTAL(Double.parseDouble(JTFTotalGeral.getText()));
        Classe_Receber.setERRO(null);

        return Classe_Receber;
    }

    public void somar_datas() {
        int parc = Quant_Parc;
        int mes = Data.getMonth();
        int dias = Data.getDay();
        int x = 0;
        int dias_a_avancar = 30;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        for (x = 1; x <= parc; x++) {
            Classe_Receber.setID_PARCELA(x);
            if (x == 1) {
                mes = Data.getMonth();
                if (!JTFEntrada.getText().equals("")) {
                    Controle_Receber.Cadastrar(getParcelas());
                    Classe_Receber.setDATA_PAGO(JTFData_Aberta.getText());
                }
                Classe_Receber.setIDENTIF(JTFCod.getText());
                Classe_Receber.setID_PARCELA(1);
                if (!JTFEntrada.getText().equals("")) {
                    Valor_Entrada = Double.parseDouble(JTFEntrada.getText());
                } else if (!JTFData_Parcela.getText().equals("  /  /    ")) {
                    Data.setMonth(mes);
                } else {
                    Data.setMonth(mes + 1);
                }
                if (Valor_Entrada != 0) {
                    Controle_Receber.AlterarData(Classe_Receber);
                } else {
                    if (inclusao == 2) {
                        Controle_Receber.ExcluirTotal(getParcelas());
                    }

                    Controle_Receber.Cadastrar(getParcelas());
                    Data.setMonth(mes + 1);
                }
                Entrada = 0;
                Valor_Entrada = 0;
            }
            if (x != 1) {
                if (JTFData_Parcela.getText().equals("  /  /    ")) {
                    Data.setMonth(mes + 1);
                }
                mes = Data.getMonth();
                System.out.println(df.format(Data));
                df.format(Data);
                Controle_Receber.Cadastrar(getParcelas());
                Data.setMonth(mes + 1);
            }
        }

    }

    public void limpar() {
        Vetor_Servico = new int[100];
        Vetor_Produto = new int[Controle_Produtos.Criar_Vetor()];

        limpar.LimparCampos(JPCadastro);

        limpar.LimparCampos(JPDados);
        limpar.LimparCampos(JPProduto);

        Limpar_Tabela.Limpar_tabela_pesquisa(JTConsulta);
        Limpar_Tabela.Limpar_tabela_pesquisa(JTProduto);
        Valida_Botao.ValidaCamposCancelar(JPCadastro, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPDados, JPAINELBOTAO);
        Valida_Botao.ValidaCamposCancelar(JPProduto, JPAINELBOTAO);

        JTFTotalGeral.setText("");
        JTFBruto.setText("");
        JTFDesconto.setText("");
        JTFTotalGeral.setText("");
        JTFBruto.setText("");

        sair = 0;
    }

}
