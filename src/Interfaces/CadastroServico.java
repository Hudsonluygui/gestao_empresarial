/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Classes.ClasseServicos;
import Controles.ControleServico;
import Validações.LimparCampos;
import Validações.LimparTabelas;
import Validações.Preencher_JTableGenerico;
import Validações.Rotinas;
import Validações.ValidaEstadoBotoes;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hudson
 */
public class CadastroServico extends javax.swing.JFrame {

    public int estado = Rotinas.Padrao;
    LimparCampos limpar = new LimparCampos();
    ValidaEstadoBotoes Valida_Botao = new ValidaEstadoBotoes();
    Preencher_JTableGenerico preencher = new Preencher_JTableGenerico();
    private ClasseServicos servico;
    private ControleServico controle;
    private LimparTabelas Limpar_Tabela = new LimparTabelas();

    public CadastroServico() {
        initComponents();
        servico = new ClasseServicos();
        controle = new ControleServico();
        Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
        Tamanho_Jtable(jTable1, 0, 10);
        Tamanho_Jtable(jTable1, 1, 200);
        Botao();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        JPANEL = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        JTFCod = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JTFDS_SERVICO = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        JTFVALOR = new javax.swing.JTextField();
        JPAINELBOTAO = new javax.swing.JPanel();
        jBIncluir = new javax.swing.JButton();
        jBAlterar = new javax.swing.JButton();
        jBExcluir = new javax.swing.JButton();
        jBGravar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        JCPesquisa = new javax.swing.JComboBox();
        JTFPesquisa = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Serviço");
        setResizable(false);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jLabel1.setText("Código do Serviço");

        JTFCod.setEditable(false);

        jLabel2.setText("Descrição ");

        jLabel3.setText("Valor");

        JPAINELBOTAO.setBackground(new java.awt.Color(153, 153, 153));

        jBIncluir.setText("Incluir");
        jBIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIncluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBIncluir);

        jBAlterar.setText("Alterar");
        jBAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAlterarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBAlterar);

        jBExcluir.setText("Excluir");
        jBExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExcluirActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBExcluir);

        jBGravar.setText("Gravar");
        jBGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGravarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBGravar);

        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });
        JPAINELBOTAO.add(jBCancelar);

        javax.swing.GroupLayout JPANELLayout = new javax.swing.GroupLayout(JPANEL);
        JPANEL.setLayout(JPANELLayout);
        JPANELLayout.setHorizontalGroup(
            JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPANELLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JTFDS_SERVICO)
                    .addGroup(JPANELLayout.createSequentialGroup()
                        .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(JTFCod))
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(JTFVALOR, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        JPANELLayout.setVerticalGroup(
            JPANELLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(JPANELLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFDS_SERVICO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JTFVALOR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addComponent(JPAINELBOTAO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Cadastro", JPANEL);

        JCPesquisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Geral", "Código", "Descr." }));
        JCPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCPesquisaActionPerformed(evt);
            }
        });

        JTFPesquisa.setEditable(false);

        jButton1.setText("Pesquisar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Serviço", "Valor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JTFPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JCPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Consulta", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jBIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIncluirActionPerformed
        JTFDS_SERVICO.requestFocus();
        estado = Rotinas.Incluir;
        limpar.LimparCampos(JPANEL);
        Valida_Botao.ValidaCamposIncluir(JPANEL, JPAINELBOTAO);

        // TODO add your handling code here:
    }//GEN-LAST:event_jBIncluirActionPerformed

    private void jBAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAlterarActionPerformed

        if (JTFCod.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para ALTERAR");

        } else {
            Valida_Botao.ValidaCamposIncluir(JPANEL, JPAINELBOTAO);
            estado = Rotinas.Alterar;
            JTFDS_SERVICO.requestFocus();
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jBAlterarActionPerformed

    private void jBExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExcluirActionPerformed

        String mensagem;
        estado = Rotinas.Excluir;
        if (JTFCod.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Selecione um registro para EXCLUIR");
            return;
        }
        if (JOptionPane.showConfirmDialog(null, "DESEJA EXCLUIR?") == 0) {
            estado = Rotinas.Excluir;

            if (controle.Excluir(getCampos())) {
                estado = Rotinas.Excluir;
                mensagem = "Registro Excluído com Sucesso";
                JOptionPane.showMessageDialog(null, mensagem);
                Botao();
            } else {
                mensagem = "Registro sendo utilizado em outro Cadastro/Movimento";
                Botao();
            }
            Botao();

            limpar.LimparCampos(JPANEL);
        }

        // TODO add your handling code here:
        //     }
    }//GEN-LAST:event_jBExcluirActionPerformed

    private void jBGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGravarActionPerformed
        if (JTFDS_SERVICO.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Digite a Descrição");
            JTFDS_SERVICO.requestFocus();

        } else if (JTFVALOR.getText().equals("")) {

            JOptionPane.showMessageDialog(null, "Digite o Valor");
            JTFVALOR.requestFocus();

        } else {
            Double valor1, valor2;
            try {
                valor1 = Double.parseDouble(JTFVALOR.getText().replace(",", "."));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Valor Inválido!");
                JTFVALOR.setText("");
                JTFVALOR.grabFocus();
                return;
            }
            String mensagem = "";
            if (estado == Rotinas.Incluir) {
                if (controle.Cadastrar(getCampos())) {
                    estado = Rotinas.Padrao;
                    mensagem = "Registro Incluso com Sucesso!";
                    JTFCod.setText(Integer.toString(servico.getID_SERVICO()));
                } else {
                    mensagem = "Erro na Inclusão";
                }
            } else if (estado == Rotinas.Alterar) {
                if (controle.Alterar(getCampos())) {
                    estado = Rotinas.Padrao;
                    mensagem = "Registro Alterado com Sucesso";
                } else {
                    mensagem = "Erro na Alteração";
                }
            }
            JOptionPane.showMessageDialog(null, mensagem);
            Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
            Botao();
            limpar.LimparCampos(JPANEL);
        }
    }//GEN-LAST:event_jBGravarActionPerformed

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        JTFDS_SERVICO.requestFocus();
        Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
        Botao();
        limpar.LimparCampos(JPANEL);
        // TODO add your handling code here:
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void JCPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCPesquisaActionPerformed

        if (JCPesquisa.getSelectedIndex() == 1) {
            JTFPesquisa.setEditable(true);
            JTFPesquisa.setText(null);
            JTFPesquisa.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 2) {
            JTFPesquisa.setEditable(true);
            JTFPesquisa.setText(null);
            JTFPesquisa.requestFocus();
        } else if (JCPesquisa.getSelectedIndex() == 0) {
            JTFPesquisa.setEditable(false);
            JTFPesquisa.setText(null);
        }
    }//GEN-LAST:event_JCPesquisaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        switch (JCPesquisa.getSelectedIndex()) {
            case 0: {
                preencher.Preencher_JTableGenerico(jTable1, controle.PesquisarGeral());
                break;
            }
            case 1: {
                if (JTFPesquisa.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe o Código para Pesquisa");
                    JTFPesquisa.requestFocus();
                } else {
                    try {
                        preencher.Preencher_JTableGenerico(jTable1, controle.PesquisarPorCodigo(Integer.parseInt(JTFPesquisa.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Código Inválido");
                        JTFPesquisa.setText("");
                        JTFPesquisa.requestFocus();
                    }
                }
                break;
            }
            case 2: {
                if (JTFPesquisa.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Informe uma Descrição");
                    JTFPesquisa.requestFocus();
                } else {
                    preencher.Preencher_JTableGenerico(jTable1, controle.PesquisarDescr(JTFPesquisa.getText().toUpperCase()));
                }
                break;
            }
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int index = jTable1.getSelectedRow();
        String strVetor = (String) jTable1.getValueAt(index, 0);
        controle.Recuperar(getPosicao(strVetor));
        setCampos(servico);
        jTabbedPane1.setSelectedIndex(0);
        Valida_Botao.ValidaCamposCancelar(JPANEL, JPAINELBOTAO);
        JCPesquisa.setSelectedIndex(0x0);
        Limpar_Tabela.Limpar_tabela_pesquisa(jTable1);
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        Limpar_Tabela.Limpar_tabela_pesquisa(jTable1);
// TODO add your handling code here:
    }//GEN-LAST:event_jTabbedPane1StateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroServico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroServico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox JCPesquisa;
    private javax.swing.JPanel JPAINELBOTAO;
    private javax.swing.JPanel JPANEL;
    private javax.swing.JTextField JTFCod;
    private javax.swing.JTextField JTFDS_SERVICO;
    private javax.swing.JTextField JTFPesquisa;
    private javax.swing.JTextField JTFVALOR;
    private javax.swing.JButton jBAlterar;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBExcluir;
    private javax.swing.JButton jBGravar;
    private javax.swing.JButton jBIncluir;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    public void Botao() {
        jBAlterar.setEnabled(false);
        jBExcluir.setEnabled(false);
    }

    public void setCampos(ClasseServicos Sr) {
        JTFCod.setText(String.valueOf(Sr.getID_SERVICO()));
        JTFDS_SERVICO.setText(String.valueOf(Sr.getDS_SERVICO()).toUpperCase());
        JTFVALOR.setText(String.valueOf(Sr.getVALOR()));
        Sr.setERRO(null);
    }

    public ClasseServicos getCampos() {
        int tamanho = 0;
        String teste = null;
        if (estado == Rotinas.Alterar) {
            servico.setID_SERVICO(Integer.parseInt(JTFCod.getText()));
        }
        tamanho = JTFDS_SERVICO.getText().length();
        if (tamanho > 40) {
            teste = JTFDS_SERVICO.getText().substring(0, 40).toUpperCase();
            servico.setDS_SERVICO(teste);
        } else {

            servico.setDS_SERVICO(JTFDS_SERVICO.getText().toUpperCase());
        }

        try {
            servico.setVALOR(Double.parseDouble(JTFVALOR.getText().replace(",", ".")));
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "Valor Inválido");
            JTFVALOR.requestFocus();
        }
        return servico;
    }

    public ClasseServicos getPosicao(String posicao) {
        servico.setID_SERVICO(Integer.parseInt(posicao));
        return servico;
    }

    public void Tamanho_Jtable(JTable tabela, int Coluna, int Tamanho_Desejado) {
        tabela.getColumnModel().getColumn(Coluna).setPreferredWidth(Tamanho_Desejado);
    }

}
