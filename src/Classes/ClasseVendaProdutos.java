
package Classes;
public class ClasseVendaProdutos {
    private int ID_VENDA;
    private int ID_CLIENTE;
    private String ID_PRODUTO;
    private int CODIGO;
    private Double QUANT;
    private Double VL_UNITARIO;
    private Double VL_TOTAL;
    private String ERRO;
    private String MARCA;
    private String MODELO;
    private String TAMANHO;
    private String UNIDADE;

    /**
     * @return the ID_VENDA
     */
    public int getID_VENDA() {
        return ID_VENDA;
    }

    /**
     * @param ID_VENDA the ID_VENDA to set
     */
    public void setID_VENDA(int ID_VENDA) {
        this.ID_VENDA = ID_VENDA;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the ID_PRODUTO
     */
 

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the QUANT
     */
    public Double getQUANT() {
        return QUANT;
    }

    /**
     * @param QUANT the QUANT to set
     */
    public void setQUANT(Double QUANT) {
        this.QUANT = QUANT;
    }

    /**
     * @return the VL_UNITARIO
     */
    public Double getVL_UNITARIO() {
        return VL_UNITARIO;
    }

    /**
     * @param VL_UNITARIO the VL_UNITARIO to set
     */
    public void setVL_UNITARIO(Double VL_UNITARIO) {
        this.VL_UNITARIO = VL_UNITARIO;
    }

    /**
     * @return the VL_TOTAL
     */
    public Double getVL_TOTAL() {
        return VL_TOTAL;
    }

    /**
     * @param VL_TOTAL the VL_TOTAL to set
     */
    public void setVL_TOTAL(Double VL_TOTAL) {
        this.VL_TOTAL = VL_TOTAL;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the MARCA
     */
    public String getMARCA() {
        return MARCA;
    }

    /**
     * @param MARCA the MARCA to set
     */
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    /**
     * @return the MODELO
     */
    public String getMODELO() {
        return MODELO;
    }

    /**
     * @param MODELO the MODELO to set
     */
    public void setMODELO(String MODELO) {
        this.MODELO = MODELO;
    }

    /**
     * @return the TAMANHO
     */
    public String getTAMANHO() {
        return TAMANHO;
    }

    /**
     * @param TAMANHO the TAMANHO to set
     */
    public void setTAMANHO(String TAMANHO) {
        this.TAMANHO = TAMANHO;
    }

    /**
     * @return the UNIDADE
     */
    public String getUNIDADE() {
        return UNIDADE;
    }

    /**
     * @param UNIDADE the UNIDADE to set
     */
    public void setUNIDADE(String UNIDADE) {
        this.UNIDADE = UNIDADE;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }
}
