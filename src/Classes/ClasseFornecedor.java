
package Classes;


public class ClasseFornecedor {
    private int ID_FORNECEDOR;
    private int ID_PESSOA;
    private String ERRO;

    /**
     * @return the ID_FORNECEDOR
     */
    public int getID_FORNECEDOR() {
        return ID_FORNECEDOR;
    }

    /**
     * @param ID_FORNECEDOR the ID_FORNECEDOR to set
     */
    public void setID_FORNECEDOR(int ID_FORNECEDOR) {
        this.ID_FORNECEDOR = ID_FORNECEDOR;
    }

    /**
     * @return the ID_PESSOA
     */
    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
    
    
}
