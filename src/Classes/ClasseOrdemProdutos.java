

package Classes;


public class ClasseOrdemProdutos {
    private int ID_ORDEM;
    private int ID_ITEM;
    private String ID_PRODUTO;
    private int CODIGO;
    private Double QUANT;
    private Double VL_UNITARIO;
    private Double VL_TOTAL;
    private String ERRO;

    /**
     * @return the ID_ORDEM
     */
    public int getID_ORDEM() {
        return ID_ORDEM;
    }

    /**
     * @param ID_ORDEM the ID_ORDEM to set
     */
    public void setID_ORDEM(int ID_ORDEM) {
        this.ID_ORDEM = ID_ORDEM;
    }

    /**
     * @return the ID_ITEM
     */
    public int getID_ITEM() {
        return ID_ITEM;
    }

    /**
     * @param ID_ITEM the ID_ITEM to set
     */
    public void setID_ITEM(int ID_ITEM) {
        this.ID_ITEM = ID_ITEM;
    }

    /**
     * @return the ID_PRODUTO
     */
   

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the QUANT
     */
    public Double getQUANT() {
        return QUANT;
    }

    /**
     * @param QUANT the QUANT to set
     */
    public void setQUANT(Double QUANT) {
        this.QUANT = QUANT;
    }

    /**
     * @return the VL_UNITARIO
     */
    public Double getVL_UNITARIO() {
        return VL_UNITARIO;
    }

    /**
     * @param VL_UNITARIO the VL_UNITARIO to set
     */
    public void setVL_UNITARIO(Double VL_UNITARIO) {
        this.VL_UNITARIO = VL_UNITARIO;
    }

    /**
     * @return the VL_TOTAL
     */
    public Double getVL_TOTAL() {
        return VL_TOTAL;
    }

    /**
     * @param VL_TOTAL the VL_TOTAL to set
     */
    public void setVL_TOTAL(Double VL_TOTAL) {
        this.VL_TOTAL = VL_TOTAL;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }
}
