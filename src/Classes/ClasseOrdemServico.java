

package Classes;

/**
 *
 * @author Hudson
 */
public class ClasseOrdemServico {
    private int ID_ORDEM;
    private int ID_SERVICO;
    private int ID_ITEM;
    private Double VALOR;
    private String ERRO;

    /**
     * @return the ID_ORDEM
     */
    public int getID_ORDEM() {
        return ID_ORDEM;
    }

    /**
     * @param ID_ORDEM the ID_ORDEM to set
     */
    public void setID_ORDEM(int ID_ORDEM) {
        this.ID_ORDEM = ID_ORDEM;
    }

    /**
     * @return the ID_SERVICO
     */
    public int getID_SERVICO() {
        return ID_SERVICO;
    }

    /**
     * @param ID_SERVICO the ID_SERVICO to set
     */
    public void setID_SERVICO(int ID_SERVICO) {
        this.ID_SERVICO = ID_SERVICO;
    }

    /**
     * @return the ID_ITEM
     */
    public int getID_ITEM() {
        return ID_ITEM;
    }

    /**
     * @param ID_ITEM the ID_ITEM to set
     */
    public void setID_ITEM(int ID_ITEM) {
        this.ID_ITEM = ID_ITEM;
    }

    /**
     * @return the VALOR
     */
    public Double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
