

package Classes;

public class ClasseFornecedorProdutos {
    private int ID_FORNECEDOR;
    private int ID_PESSOA;
    private String ID_PRODUTO;
    private int CODIGO;
    private String ERRO;

    /**
     * @return the ID_FORNECEDOR
     */
    public int getID_FORNECEDOR() {
        return ID_FORNECEDOR;
    }

    /**
     * @param ID_FORNECEDOR the ID_FORNECEDOR to set
     */
    public void setID_FORNECEDOR(int ID_FORNECEDOR) {
        this.ID_FORNECEDOR = ID_FORNECEDOR;
    }

    /**
     * @return the ID_PESSOA
     */
    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the ID_PRODUTO
     */
  

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }


}
