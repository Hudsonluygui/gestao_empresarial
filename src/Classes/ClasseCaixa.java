package Classes;

public class ClasseCaixa {

    private String DESCR;
    private String DATA;
    private String OPERACAO;
    private String ERRO;
    private String FORMA;
    private Double VALOR;
    private int ID_CAIXA;

    /**
     * @return the DESCR
     */
    public String getDESCR() {
        return DESCR;
    }

    /**
     * @param DESCR the DESCR to set
     */
    public void setDESCR(String DESCR) {
        this.DESCR = DESCR;
    }

    /**
     * @return the DATA
     */
    public String getDATA() {
        return DATA;
    }

    /**
     * @param DATA the DATA to set
     */
    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    /**
     * @return the OPERACAO
     */
    public String getOPERACAO() {
        return OPERACAO;
    }

    /**
     * @param OPERACAO the OPERACAO to set
     */
    public void setOPERACAO(String OPERACAO) {
        this.OPERACAO = OPERACAO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the VALOR
     */
    public Double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    /**
     * @return the ID_CAIXA
     */
    public int getID_CAIXA() {
        return ID_CAIXA;
    }

    /**
     * @param ID_CAIXA the ID_CAIXA to set
     */
    public void setID_CAIXA(int ID_CAIXA) {
        this.ID_CAIXA = ID_CAIXA;
    }

    /**
     * @return the FORMA
     */
    public String getFORMA() {
        return FORMA;
    }

    /**
     * @param FORMA the FORMA to set
     */
    public void setFORMA(String FORMA) {
        this.FORMA = FORMA;
    }

}
