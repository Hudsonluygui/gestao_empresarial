

package Classes;

public class ClasseVenda {
    private int ID_VENDA;
    private int ID_CLIENTE;
    private String DATA_VENDA;
    private String DATA_VENCIMENTO;
    private String ERRO;
    private Double TOTAL_VENDA;
    private String DESCR;
    private String PAGO;

    /**
     * @return the ID_VENDA
     */
    public int getID_VENDA() {
        return ID_VENDA;
    }

    /**
     * @param ID_VENDA the ID_VENDA to set
     */
    public void setID_VENDA(int ID_VENDA) {
        this.ID_VENDA = ID_VENDA;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the DATA_VENDA
     */
    public String getDATA_VENDA() {
        return DATA_VENDA;
    }

    /**
     * @param DATA_VENDA the DATA_VENDA to set
     */
    public void setDATA_VENDA(String DATA_VENDA) {
        this.DATA_VENDA = DATA_VENDA;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the TOTAL_VENDA
     */
    public Double getTOTAL_VENDA() {
        return TOTAL_VENDA;
    }

    /**
     * @param TOTAL_VENDA the TOTAL_VENDA to set
     */
    public void setTOTAL_VENDA(Double TOTAL_VENDA) {
        this.TOTAL_VENDA = TOTAL_VENDA;
    }

    /**
     * @return the DESCR
     */
    public String getDESCR() {
        return DESCR;
    }

    /**
     * @param DESCR the DESCR to set
     */
    public void setDESCR(String DESCR) {
        this.DESCR = DESCR;
    }

    /**
     * @return the PAGO
     */
    public String getPAGO() {
        return PAGO;
    }

    /**
     * @param PAGO the PAGO to set
     */
    public void setPAGO(String PAGO) {
        this.PAGO = PAGO;
    }
            
}
