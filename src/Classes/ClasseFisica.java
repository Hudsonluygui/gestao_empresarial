

package Classes;


public class ClasseFisica {
private int ID_PESSOA;
private String RG;
private String CPF;
private String DATA_NASC;
private String ERRO;

    /**
     * @return the ID_PESSOA
     */
    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the RG
     */
    public String getRG() {
        return RG;
    }

    /**
     * @param RG the RG to set
     */
    public void setRG(String RG) {
        this.RG = RG;
    }

    /**
     * @return the CPF
     */
    public String getCPF() {
        return CPF;
    }

    /**
     * @param CPF the CPF to set
     */
    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    /**
     * @return the DATA_NASC
     */
    public String getDATA_NASC() {
        return DATA_NASC;
    }

    /**
     * @param DATA_NASC the DATA_NASC to set
     */
    public void setDATA_NASC(String DATA_NASC) {
        this.DATA_NASC = DATA_NASC;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }




}
