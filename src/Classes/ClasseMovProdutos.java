package Classes;
public class ClasseMovProdutos {
    private String ID_PRODUTO;
    private int ID_MOV;
    private String IDENTIF;
    private Double EST_ANTERIOR;
    private Double EST_ATUAL;
    private Double QUANT_MOVIDA;
    private String MARCA;
    private String MODELO;
    private String TAMANHO;
    private String ERRO;
    private String DS_UNIDADE;
    private String DATA;
    private String DESCR;
    private int CODIGO;

    /**
     * @return the ID_PRODUTO
     */
   

    /**
     * @return the ID_MOV
     */
    public int getID_MOV() {
        return ID_MOV;
    }

    /**
     * @param ID_MOV the ID_MOV to set
     */
    public void setID_MOV(int ID_MOV) {
        this.ID_MOV = ID_MOV;
    }

    /**
     * @return the IDENTIF
     */
   

    /**
     * @return the EST_ANTERIOR
     */
    public Double getEST_ANTERIOR() {
        return EST_ANTERIOR;
    }

    /**
     * @param EST_ANTERIOR the EST_ANTERIOR to set
     */
    public void setEST_ANTERIOR(Double EST_ANTERIOR) {
        this.EST_ANTERIOR = EST_ANTERIOR;
    }

    /**
     * @return the EST_ATUAL
     */
    public Double getEST_ATUAL() {
        return EST_ATUAL;
    }

    /**
     * @param EST_ATUAL the EST_ATUAL to set
     */
    public void setEST_ATUAL(Double EST_ATUAL) {
        this.EST_ATUAL = EST_ATUAL;
    }

    /**
     * @return the QUANT_MOVIDA
     */
    public Double getQUANT_MOVIDA() {
        return QUANT_MOVIDA;
    }

    /**
     * @param QUANT_MOVIDA the QUANT_MOVIDA to set
     */
    public void setQUANT_MOVIDA(Double QUANT_MOVIDA) {
        this.QUANT_MOVIDA = QUANT_MOVIDA;
    }

    /**
     * @return the MARCA
     */
    public String getMARCA() {
        return MARCA;
    }

    /**
     * @param MARCA the MARCA to set
     */
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    /**
     * @return the MODELO
     */
    public String getMODELO() {
        return MODELO;
    }

    /**
     * @param MODELO the MODELO to set
     */
    public void setMODELO(String MODELO) {
        this.MODELO = MODELO;
    }

    /**
     * @return the TAMANHO
     */
    public String getTAMANHO() {
        return TAMANHO;
    }

    /**
     * @param TAMANHO the TAMANHO to set
     */
    public void setTAMANHO(String TAMANHO) {
        this.TAMANHO = TAMANHO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the DS_UNIDADE
     */
    public String getDS_UNIDADE() {
        return DS_UNIDADE;
    }

    /**
     * @param DS_UNIDADE the DS_UNIDADE to set
     */
    public void setDS_UNIDADE(String DS_UNIDADE) {
        this.DS_UNIDADE = DS_UNIDADE;
    }

    /**
     * @return the DATA
     */
    public String getDATA() {
        return DATA;
    }

    /**
     * @param DATA the DATA to set
     */
    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    /**
     * @return the DESCR
     */
    public String getDESCR() {
        return DESCR;
    }

    /**
     * @param DESCR the DESCR to set
     */
    public void setDESCR(String DESCR) {
        this.DESCR = DESCR;
    }

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the IDENTIF
     */
    public String getIDENTIF() {
        return IDENTIF;
    }

    /**
     * @param IDENTIF the IDENTIF to set
     */
    public void setIDENTIF(String IDENTIF) {
        this.IDENTIF = IDENTIF;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }

}
