package Classes;
public class Classe_de_Carros_Compativeis_Produtos {
    private int ID_CARRO;
    private String ID_PRODUTO; //Referencias de Cadastro de Produtos
    private String MARCA_CARRO;
    private String MODELO_CARRO;
    private int ANO_FAB;
    private String ERRO;
    private int CODIGO;

    /**
     * @return the ID_CARRO
     */
    public int getID_CARRO() {
        return ID_CARRO;
    }

    /**
     * @param ID_CARRO the ID_CARRO to set
     */
    public void setID_CARRO(int ID_CARRO) {
        this.ID_CARRO = ID_CARRO;
    }

    /**
     * @return the ID_PRODUTO
     */
   

    /**
     * @return the MARCA_CARRO
     */
    public String getMARCA_CARRO() {
        return MARCA_CARRO;
    }

    /**
     * @param MARCA_CARRO the MARCA_CARRO to set
     */
    public void setMARCA_CARRO(String MARCA_CARRO) {
        this.MARCA_CARRO = MARCA_CARRO;
    }

    /**
     * @return the MODELO_CARRO
     */
    public String getMODELO_CARRO() {
        return MODELO_CARRO;
    }

    /**
     * @param MODELO_CARRO the MODELO_CARRO to set
     */
    public void setMODELO_CARRO(String MODELO_CARRO) {
        this.MODELO_CARRO = MODELO_CARRO;
    }

    /**
     * @return the ANO_FAB
     */
    public int getANO_FAB() {
        return ANO_FAB;
    }

    /**
     * @param ANO_FAB the ANO_FAB to set
     */
    public void setANO_FAB(int ANO_FAB) {
        this.ANO_FAB = ANO_FAB;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }
}
