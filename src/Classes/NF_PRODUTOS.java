

package Classes;

public class NF_PRODUTOS {
    private String NR_NF;
    private String ID_PRODUTO;
    private int CODIGO;
    private double VALOR_UNIT;
    private double QUANT;
    private double VALOR_TOTAL;
    private String ERRO;
        private String MARCA;
    private String MODELO;
    private String TAMANHO;
    private String UNIDADE;

    /**
     * @return the NR_NF
     */
    public String getNR_NF() {
        return NR_NF;
    }

    /**
     * @param NR_NF the NR_NF to set
     */
    public void setNR_NF(String NR_NF) {
        this.NR_NF = NR_NF;
    }

    /**
     * @return the ID_PRODUTO
     */
   

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the VALOR_UNIT
     */
    public double getVALOR_UNIT() {
        return VALOR_UNIT;
    }

    /**
     * @param VALOR_UNIT the VALOR_UNIT to set
     */
    public void setVALOR_UNIT(double VALOR_UNIT) {
        this.VALOR_UNIT = VALOR_UNIT;
    }

    /**
     * @return the QUANT
     */
    public double getQUANT() {
        return QUANT;
    }

    /**
     * @param QUANT the QUANT to set
     */
    public void setQUANT(double QUANT) {
        this.QUANT = QUANT;
    }

    /**
     * @return the VALOR_TOTAL
     */
    public double getVALOR_TOTAL() {
        return VALOR_TOTAL;
    }

    /**
     * @param VALOR_TOTAL the VALOR_TOTAL to set
     */
    public void setVALOR_TOTAL(double VALOR_TOTAL) {
        this.VALOR_TOTAL = VALOR_TOTAL;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the MARCA
     */
    public String getMARCA() {
        return MARCA;
    }

    /**
     * @param MARCA the MARCA to set
     */
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    /**
     * @return the MODELO
     */
    public String getMODELO() {
        return MODELO;
    }

    /**
     * @param MODELO the MODELO to set
     */
    public void setMODELO(String MODELO) {
        this.MODELO = MODELO;
    }

    /**
     * @return the TAMANHO
     */
    public String getTAMANHO() {
        return TAMANHO;
    }

    /**
     * @param TAMANHO the TAMANHO to set
     */
    public void setTAMANHO(String TAMANHO) {
        this.TAMANHO = TAMANHO;
    }

    /**
     * @return the UNIDADE
     */
    public String getUNIDADE() {
        return UNIDADE;
    }

    /**
     * @param UNIDADE the UNIDADE to set
     */
    public void setUNIDADE(String UNIDADE) {
        this.UNIDADE = UNIDADE;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }
}
