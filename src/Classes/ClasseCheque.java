package Classes;

public class ClasseCheque {

    private int ID_CHEQUE;
    private String TITULAR;
    private String CLIENTE;
    private String TELEFONE;
    private String CIDADE;
    private Double VALOR;
    private String DATA_EMISSAO;
    private String DATA_VENCIMENTO;
    private String PAGO;
    private String NOME_RECEBIDO;
    private Double VALOR_RECEBIDO;
    private String DATA_RECEBIDO;
    private String ERRO;

    /**
     * @return the ID_CHEQUE
     */
    public int getID_CHEQUE() {
        return ID_CHEQUE;
    }

    /**
     * @param ID_CHEQUE the ID_CHEQUE to set
     */
    public void setID_CHEQUE(int ID_CHEQUE) {
        this.ID_CHEQUE = ID_CHEQUE;
    }

    /**
     * @return the TITULAR
     */
    public String getTITULAR() {
        return TITULAR;
    }

    /**
     * @param TITULAR the TITULAR to set
     */
    public void setTITULAR(String TITULAR) {
        this.TITULAR = TITULAR;
    }

    /**
     * @return the CLIENTE
     */
    public String getCLIENTE() {
        return CLIENTE;
    }

    /**
     * @param CLIENTE the CLIENTE to set
     */
    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }

    /**
     * @return the TELEFONE
     */
    public String getTELEFONE() {
        return TELEFONE;
    }

    /**
     * @param TELEFONE the TELEFONE to set
     */
    public void setTELEFONE(String TELEFONE) {
        this.TELEFONE = TELEFONE;
    }

    /**
     * @return the CIDADE
     */
    public String getCIDADE() {
        return CIDADE;
    }

    /**
     * @param CIDADE the CIDADE to set
     */
    public void setCIDADE(String CIDADE) {
        this.CIDADE = CIDADE;
    }

    /**
     * @return the VALOR
     */
    public Double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    /**
     * @return the DATA_EMISSAO
     */
    public String getDATA_EMISSAO() {
        return DATA_EMISSAO;
    }

    /**
     * @param DATA_EMISSAO the DATA_EMISSAO to set
     */
    public void setDATA_EMISSAO(String DATA_EMISSAO) {
        this.DATA_EMISSAO = DATA_EMISSAO;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the PAGO
     */
    public String getPAGO() {
        return PAGO;
    }

    /**
     * @param PAGO the PAGO to set
     */
    public void setPAGO(String PAGO) {
        this.PAGO = PAGO;
    }

    /**
     * @return the NOME_RECEBIDO
     */
    public String getNOME_RECEBIDO() {
        return NOME_RECEBIDO;
    }

    /**
     * @param NOME_RECEBIDO the NOME_RECEBIDO to set
     */
    public void setNOME_RECEBIDO(String NOME_RECEBIDO) {
        this.NOME_RECEBIDO = NOME_RECEBIDO;
    }

    /**
     * @return the VALOR_RECEBIDO
     */
    public Double getVALOR_RECEBIDO() {
        return VALOR_RECEBIDO;
    }

    /**
     * @param VALOR_RECEBIDO the VALOR_RECEBIDO to set
     */
    public void setVALOR_RECEBIDO(Double VALOR_RECEBIDO) {
        this.VALOR_RECEBIDO = VALOR_RECEBIDO;
    }

    /**
     * @return the DATA_RECEBIDO
     */
    public String getDATA_RECEBIDO() {
        return DATA_RECEBIDO;
    }

    /**
     * @param DATA_RECEBIDO the DATA_RECEBIDO to set
     */
    public void setDATA_RECEBIDO(String DATA_RECEBIDO) {
        this.DATA_RECEBIDO = DATA_RECEBIDO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
