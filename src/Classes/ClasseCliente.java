
package Classes;

public class ClasseCliente {
    private int ID_CLIENTE;
    private int ID_PESSOA;
    private String ERRO;

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the ID_PESSOA
     */
    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
    
}
