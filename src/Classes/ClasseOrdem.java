package Classes;
public class ClasseOrdem {
 private int ID_ORDEM;
 private int ID_CLIENTE;
 private int ID_CARRO;
 private Double VALOR_TOTAL;
 private String DESCR;
 private Double VALOR_BRUTO;
 private String DATA_ABERTA;
 private String DATA_FECHA;
 private String SITUACAO;
 private String NUMERO;
private String ERRO;
private String DATA_VENCIMENTO;
private Double PERC;
private String PAGO;
private String OPERACAO;


    /**
     * @return the ID_ORDEM
     */
    public int getID_ORDEM() {
        return ID_ORDEM;
    }

    /**
     * @param ID_ORDEM the ID_ORDEM to set
     */
    public void setID_ORDEM(int ID_ORDEM) {
        this.ID_ORDEM = ID_ORDEM;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the VALOR_TOTAL
     */
    public Double getVALOR_TOTAL() {
        return VALOR_TOTAL;
    }

    /**
     * @param VALOR_TOTAL the VALOR_TOTAL to set
     */
    public void setVALOR_TOTAL(Double VALOR_TOTAL) {
        this.VALOR_TOTAL = VALOR_TOTAL;
    }

    /**
     * @return the DESCR
     */
    public String getDESCR() {
        return DESCR;
    }

    /**
     * @param DESCR the DESCR to set
     */
    public void setDESCR(String DESCR) {
        this.DESCR = DESCR;
    }

    /**
     * @return the DATA_ABERTA
     */
    public String getDATA_ABERTA() {
        return DATA_ABERTA;
    }

    /**
     * @param DATA_ABERTA the DATA_ABERTA to set
     */
    public void setDATA_ABERTA(String DATA_ABERTA) {
        this.DATA_ABERTA = DATA_ABERTA;
    }

    /**
     * @return the DATA_FECHA
     */
    public String getDATA_FECHA() {
        return DATA_FECHA;
    }

    /**
     * @param DATA_FECHA the DATA_FECHA to set
     */
    public void setDATA_FECHA(String DATA_FECHA) {
        this.DATA_FECHA = DATA_FECHA;
    }

    /**
     * @return the SITUACAO
     */
    public String getSITUACAO() {
        return SITUACAO;
    }

    /**
     * @param SITUACAO the SITUACAO to set
     */
    public void setSITUACAO(String SITUACAO) {
        this.SITUACAO = SITUACAO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the ID_CARRO
     */
    public int getID_CARRO() {
        return ID_CARRO;
    }

    /**
     * @param ID_CARRO the ID_CARRO to set
     */
    public void setID_CARRO(int ID_CARRO) {
        this.ID_CARRO = ID_CARRO;
    }

    /**
     * @return the VALOR_BRUTO
     */
    public Double getVALOR_BRUTO() {
        return VALOR_BRUTO;
    }

    /**
     * @param VALOR_BRUTO the VALOR_BRUTO to set
     */
    public void setVALOR_BRUTO(Double VALOR_BRUTO) {
        this.VALOR_BRUTO = VALOR_BRUTO;
    }

    /**
     * @return the PERC
     */
    public Double getPERC() {
        return PERC;
    }

    /**
     * @param PERC the PERC to set
     */
    public void setPERC(Double PERC) {
        this.PERC = PERC;
    }

    /**
     * @return the PAGO
     */
    public String getPAGO() {
        return PAGO;
    }

    /**
     * @param PAGO the PAGO to set
     */
    public void setPAGO(String PAGO) {
        this.PAGO = PAGO;
    }

    /**
     * @return the NUMERO
     */
    public String getNUMERO() {
        return NUMERO;
    }

    /**
     * @param NUMERO the NUMERO to set
     */
    public void setNUMERO(String NUMERO) {
        this.NUMERO = NUMERO;
    }

    /**
     * @return the OPERACAO
     */
    public String getOPERACAO() {
        return OPERACAO;
    }

    /**
     * @param OPERACAO the OPERACAO to set
     */
    public void setOPERACAO(String OPERACAO) {
        this.OPERACAO = OPERACAO;
    }
}
