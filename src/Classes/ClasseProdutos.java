package Classes;
public class ClasseProdutos {
    private String ID_PRODUTO;
    private String DS_PRODUTO;
    private Double QUANT;
    private Double VL_CUSTO;
    private Double VL_VENDA;
    private Double PERC_LUCRO;
    private String DS_UNIDADE;
    private String TAMANHO;
    private Double TOTAL_CUSTO;
    private String MARCA;
    private String MODELO;
    private String ERRO;
    private String DATA_CADASTRO;
    private int CODIGO;

   

    /**
     * @return the DS_PRODUTO
     */
    public String getDS_PRODUTO() {
        return DS_PRODUTO;
    }

    /**
     * @param DS_PRODUTO the DS_PRODUTO to set
     */
    public void setDS_PRODUTO(String DS_PRODUTO) {
        this.DS_PRODUTO = DS_PRODUTO;
    }

    /**
     * @return the QUANT
     */
    public Double getQUANT() {
        return QUANT;
    }

    /**
     * @param QUANT the QUANT to set
     */
    public void setQUANT(Double QUANT) {
        this.QUANT = QUANT;
    }

    /**
     * @return the VL_CUSTO
     */
    public Double getVL_CUSTO() {
        return VL_CUSTO;
    }

    /**
     * @param VL_CUSTO the VL_CUSTO to set
     */
    public void setVL_CUSTO(Double VL_CUSTO) {
        this.VL_CUSTO = VL_CUSTO;
    }

    /**
     * @return the VL_VENDA
     */
    public Double getVL_VENDA() {
        return VL_VENDA;
    }

    /**
     * @param VL_VENDA the VL_VENDA to set
     */
    public void setVL_VENDA(Double VL_VENDA) {
        this.VL_VENDA = VL_VENDA;
    }

    /**
     * @return the PERC_LUCRO
     */
    public Double getPERC_LUCRO() {
        return PERC_LUCRO;
    }

    /**
     * @param PERC_LUCRO the PERC_LUCRO to set
     */
    public void setPERC_LUCRO(Double PERC_LUCRO) {
        this.PERC_LUCRO = PERC_LUCRO;
    }

    /**
     * @return the DS_UNIDADE
     */
    public String getDS_UNIDADE() {
        return DS_UNIDADE;
    }

    /**
     * @param DS_UNIDADE the DS_UNIDADE to set
     */
    public void setDS_UNIDADE(String DS_UNIDADE) {
        this.DS_UNIDADE = DS_UNIDADE;
    }

    /**
     * @return the TAMANHO
     */
    public String getTAMANHO() {
        return TAMANHO;
    }

    /**
     * @param TAMANHO the TAMANHO to set
     */
    public void setTAMANHO(String TAMANHO) {
        this.TAMANHO = TAMANHO;
    }

    /**
     * @return the TOTAL_CUSTO
     */
    public Double getTOTAL_CUSTO() {
        return TOTAL_CUSTO;
    }

    /**
     * @param TOTAL_CUSTO the TOTAL_CUSTO to set
     */
    public void setTOTAL_CUSTO(Double TOTAL_CUSTO) {
        this.TOTAL_CUSTO = TOTAL_CUSTO;
    }

    /**
     * @return the MARCA
     */
    public String getMARCA() {
        return MARCA;
    }

    /**
     * @param MARCA the MARCA to set
     */
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    /**
     * @return the MODELO
     */
    public String getMODELO() {
        return MODELO;
    }

    /**
     * @param MODELO the MODELO to set
     */
    public void setMODELO(String MODELO) {
        this.MODELO = MODELO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the DATA_CADASTRO
     */
    public String getDATA_CADASTRO() {
        return DATA_CADASTRO;
    }

    /**
     * @param DATA_CADASTRO the DATA_CADASTRO to set
     */
    public void setDATA_CADASTRO(String DATA_CADASTRO) {
        this.DATA_CADASTRO = DATA_CADASTRO;
    }

    /**
     * @return the CODIGO
     */
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }
    
}
