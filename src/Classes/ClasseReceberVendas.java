package Classes;

public class ClasseReceberVendas {
private int ID_CLIENTE;
    private int ID_PARCELA;
    private String IDENTIF;
    private double VALOR_PARCELA;
    private double VALOR_PAGO;
    private double JUROS;
    private double DESCONTO;
    private double VALOR_TOTAL;
    private String DATA_VENCIMENTO;
    private String DATA_PAGO;
    private String SITUACAO;
    private String DS_FORMA;
    private String DESCR;
    private String ERRO;

    /**
     * @return the ID_PARCELA
     */
    public int getID_PARCELA() {
        return ID_PARCELA;
    }

    /**
     * @param ID_PARCELA the ID_PARCELA to set
     */
    public void setID_PARCELA(int ID_PARCELA) {
        this.ID_PARCELA = ID_PARCELA;
    }

    /**
     * @return the ID_VENDA
     */
    /**
     * @return the VALOR_PARCELA
     */
    public double getVALOR_PARCELA() {
        return VALOR_PARCELA;
    }

    /**
     * @param VALOR_PARCELA the VALOR_PARCELA to set
     */
    public void setVALOR_PARCELA(double VALOR_PARCELA) {
        this.VALOR_PARCELA = VALOR_PARCELA;
    }

    /**
     * @return the VALOR_PAGO
     */
    public double getVALOR_PAGO() {
        return VALOR_PAGO;
    }

    /**
     * @param VALOR_PAGO the VALOR_PAGO to set
     */
    public void setVALOR_PAGO(double VALOR_PAGO) {
        this.VALOR_PAGO = VALOR_PAGO;
    }

    /**
     * @return the JUROS
     */
    public double getJUROS() {
        return JUROS;
    }

    /**
     * @param JUROS the JUROS to set
     */
    public void setJUROS(double JUROS) {
        this.JUROS = JUROS;
    }

    /**
     * @return the DESCONTO
     */
    public double getDESCONTO() {
        return DESCONTO;
    }

    /**
     * @param DESCONTO the DESCONTO to set
     */
    public void setDESCONTO(double DESCONTO) {
        this.DESCONTO = DESCONTO;
    }

    /**
     * @return the VALOR_TOTAL
     */
    public double getVALOR_TOTAL() {
        return VALOR_TOTAL;
    }

    /**
     * @param VALOR_TOTAL the VALOR_TOTAL to set
     */
    public void setVALOR_TOTAL(double VALOR_TOTAL) {
        this.VALOR_TOTAL = VALOR_TOTAL;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the DATA_PAGO
     */
    public String getDATA_PAGO() {
        return DATA_PAGO;
    }

    /**
     * @param DATA_PAGO the DATA_PAGO to set
     */
    public void setDATA_PAGO(String DATA_PAGO) {
        this.DATA_PAGO = DATA_PAGO;
    }

    /**
     * @return the SITUACAO
     */
    public String getSITUACAO() {
        return SITUACAO;
    }

    /**
     * @param SITUACAO the SITUACAO to set
     */
    public void setSITUACAO(String SITUACAO) {
        this.SITUACAO = SITUACAO;
    }

    /**
     * @return the DS_FORMA
     */
    public String getDS_FORMA() {
        return DS_FORMA;
    }

    /**
     * @param DS_FORMA the DS_FORMA to set
     */
    public void setDS_FORMA(String DS_FORMA) {
        this.DS_FORMA = DS_FORMA;
    }

    /**
     * @return the DESCR
     */
    public String getDESCR() {
        return DESCR;
    }

    /**
     * @param DESCR the DESCR to set
     */
    public void setDESCR(String DESCR) {
        this.DESCR = DESCR;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the IDENTIF
     */
    /**
     * @param IDENTIF the IDENTIF to set
     */
    /**
     * @param IDENTIF the IDENTIF to set
     */
    public void setIDENTIF(String IDENTIF) {
        this.IDENTIF = IDENTIF;
    }

    /**
     * @return the IDENTIF
     */
    public String getIDENTIF() {
        return IDENTIF;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

}
