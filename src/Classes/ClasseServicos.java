

package Classes;


public class ClasseServicos {
    private int ID_SERVICO;
    private String DS_SERVICO;
    private String ERRO;
    private double VALOR;

    /**
     * @return the ID_SERVICO
     */
    public int getID_SERVICO() {
        return ID_SERVICO;
    }

    /**
     * @param ID_SERVICO the ID_SERVICO to set
     */
    public void setID_SERVICO(int ID_SERVICO) {
        this.ID_SERVICO = ID_SERVICO;
    }

    /**
     * @return the DS_SERVICO
     */
    public String getDS_SERVICO() {
        return DS_SERVICO;
    }

    /**
     * @param DS_SERVICO the DS_SERVICO to set
     */
    public void setDS_SERVICO(String DS_SERVICO) {
        this.DS_SERVICO = DS_SERVICO;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the VALOR
     */
    public double getVALOR() {
        return VALOR;
    }

    /**
     * @param VALOR the VALOR to set
     */
    public void setVALOR(double VALOR) {
        this.VALOR = VALOR;
    }
    
    
}
