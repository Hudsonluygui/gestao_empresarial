
package Classes;


public class ClasseOrcamento {
    private int ID_ORCAMENTO;
    private int ID_PRODUTO;
    private double VALOR_TOTAL;
    private double VL_PRODUTO;
    private double VL_SERVICO;
    private String DS_SERVICO;
    private String OBS;
    private String ERRO;

    /**
     * @return the ID_ORCAMENTO
     */
    public int getID_ORCAMENTO() {
        return ID_ORCAMENTO;
    }

    /**
     * @param ID_ORCAMENTO the ID_ORCAMENTO to set
     */
    public void setID_ORCAMENTO(int ID_ORCAMENTO) {
        this.ID_ORCAMENTO = ID_ORCAMENTO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public int getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(int ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }

    /**
     * @return the VALOR_TOTAL
     */
    public double getVALOR_TOTAL() {
        return VALOR_TOTAL;
    }

    /**
     * @param VALOR_TOTAL the VALOR_TOTAL to set
     */
    public void setVALOR_TOTAL(double VALOR_TOTAL) {
        this.VALOR_TOTAL = VALOR_TOTAL;
    }

    /**
     * @return the VL_PRODUTO
     */
    public double getVL_PRODUTO() {
        return VL_PRODUTO;
    }

    /**
     * @param VL_PRODUTO the VL_PRODUTO to set
     */
    public void setVL_PRODUTO(double VL_PRODUTO) {
        this.VL_PRODUTO = VL_PRODUTO;
    }

    /**
     * @return the VL_SERVICO
     */
    public double getVL_SERVICO() {
        return VL_SERVICO;
    }

    /**
     * @param VL_SERVICO the VL_SERVICO to set
     */
    public void setVL_SERVICO(double VL_SERVICO) {
        this.VL_SERVICO = VL_SERVICO;
    }

    /**
     * @return the DS_SERVICO
     */
    public String getDS_SERVICO() {
        return DS_SERVICO;
    }

    /**
     * @param DS_SERVICO the DS_SERVICO to set
     */
    public void setDS_SERVICO(String DS_SERVICO) {
        this.DS_SERVICO = DS_SERVICO;
    }

    /**
     * @return the OBS
     */
    public String getOBS() {
        return OBS;
    }

    /**
     * @param OBS the OBS to set
     */
    public void setOBS(String OBS) {
        this.OBS = OBS;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
