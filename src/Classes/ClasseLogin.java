
package Classes;

public class ClasseLogin {
    private int ID_LOGIN;
    private String USUARIO;
    private String DATA;
    private String ERRO;

    /**
     * @return the ID_LOGIN
     */


    /**
     * @return the USUARIO
     */
    public String getUSUARIO() {
        return USUARIO;
    }

    /**
     * @param USUARIO the USUARIO to set
     */
    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }

    /**
     * @return the DATA
     */
    public String getDATA() {
        return DATA;
    }

    /**
     * @param DATA the DATA to set
     */
    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the ID_LOGIN
     */
    public int getID_LOGIN() {
        return ID_LOGIN;
    }

    /**
     * @param ID_LOGIN the ID_LOGIN to set
     */
    public void setID_LOGIN(int ID_LOGIN) {
        this.ID_LOGIN = ID_LOGIN;
    }
    
}
