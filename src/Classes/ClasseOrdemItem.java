

package Classes;

public class ClasseOrdemItem {
    private int ID_ORDEM;
    private int ID_CLIENTE;
    private int ID_ITEM;
    private int SEQUENCIA;
    private String ERRO;

    /**
     * @return the ID_ORDEM
     */
    public int getID_ORDEM() {
        return ID_ORDEM;
    }

    /**
     * @param ID_ORDEM the ID_ORDEM to set
     */
    public void setID_ORDEM(int ID_ORDEM) {
        this.ID_ORDEM = ID_ORDEM;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the ID_ITEM
     */
    public int getID_ITEM() {
        return ID_ITEM;
    }

    /**
     * @param ID_ITEM the ID_ITEM to set
     */
    public void setID_ITEM(int ID_ITEM) {
        this.ID_ITEM = ID_ITEM;
    }

    /**
     * @return the SEQUENCIA
     */
    public int getSEQUENCIA() {
        return SEQUENCIA;
    }

    /**
     * @param SEQUENCIA the SEQUENCIA to set
     */
    public void setSEQUENCIA(int SEQUENCIA) {
        this.SEQUENCIA = SEQUENCIA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
