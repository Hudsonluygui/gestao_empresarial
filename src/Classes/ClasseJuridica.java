package Classes;
public class ClasseJuridica {
private int ID_PESSOA;
private String RAZAO;
private String CNPJ;
private String ERRO;

    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the RAZAO
     */
    public String getRAZAO() {
        return RAZAO;
    }

    /**
     * @param RAZAO the RAZAO to set
     */
    public void setRAZAO(String RAZAO) {
        this.RAZAO = RAZAO;
    }

    /**
     * @return the CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }

    /**
     * @param CNPJ the CNPJ to set
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }



}
