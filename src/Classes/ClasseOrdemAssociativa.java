

package Classes;


public class ClasseOrdemAssociativa {
    private int ID_ORDEM;
    private int ID_ITEM;
    private String ID_PRODUTO;
    private int CODIGO;
    private int ID_CLIENTE;
    private int ID_SERVICO;
    private Double VALOR_SERVICO;
    private Double QUANT;
    private Double VL_UNITARIO;
    private Double VL_TOTAL;
    private String ERRO;
    private String UNIDADE;
    private String MARCA;
    private String MODELO;
    private String TAMANHO;
    private String DATA;
    private  String OPERACAO;
    private int ID_CARRO;
    private Double QuantServico;
    private Double TotalServico;
    private Double TOTAL_GERAL_SERVICO;
    private Double TOTAL_GERAL;
    
    

    /**
     * @return the ID_ORDEM
     */
    public int getID_ORDEM() {
        return ID_ORDEM;
    }

    /**
     * @param ID_ORDEM the ID_ORDEM to set
     */
    public void setID_ORDEM(int ID_ORDEM) {
        this.ID_ORDEM = ID_ORDEM;
    }

    /**
     * @return the ID_ITEM
     */
    public int getID_ITEM() {
        return ID_ITEM;
    }

    /**
     * @param ID_ITEM the ID_ITEM to set
     */
    public void setID_ITEM(int ID_ITEM) {
        this.ID_ITEM = ID_ITEM;
    }

  
    public int getCODIGO() {
        return CODIGO;
    }

    /**
     * @param CODIGO the CODIGO to set
     */
    public void setCODIGO(int CODIGO) {
        this.CODIGO = CODIGO;
    }

    /**
     * @return the ID_CLIENTE
     */
    public int getID_CLIENTE() {
        return ID_CLIENTE;
    }

    /**
     * @param ID_CLIENTE the ID_CLIENTE to set
     */
    public void setID_CLIENTE(int ID_CLIENTE) {
        this.ID_CLIENTE = ID_CLIENTE;
    }

    /**
     * @return the ID_SERVICO
     */
    public int getID_SERVICO() {
        return ID_SERVICO;
    }

    /**
     * @param ID_SERVICO the ID_SERVICO to set
     */
    public void setID_SERVICO(int ID_SERVICO) {
        this.ID_SERVICO = ID_SERVICO;
    }

    /**
     * @return the VALOR_SERVICO
     */
    public Double getVALOR_SERVICO() {
        return VALOR_SERVICO;
    }

    /**
     * @param VALOR_SERVICO the VALOR_SERVICO to set
     */
    public void setVALOR_SERVICO(Double VALOR_SERVICO) {
        this.VALOR_SERVICO = VALOR_SERVICO;
    }

    /**
     * @return the QUANT
     */
    public Double getQUANT() {
        return QUANT;
    }

    /**
     * @param QUANT the QUANT to set
     */
    public void setQUANT(Double QUANT) {
        this.QUANT = QUANT;
    }

    /**
     * @return the VL_UNITARIO
     */
    public Double getVL_UNITARIO() {
        return VL_UNITARIO;
    }

    /**
     * @param VL_UNITARIO the VL_UNITARIO to set
     */
    public void setVL_UNITARIO(Double VL_UNITARIO) {
        this.VL_UNITARIO = VL_UNITARIO;
    }

    /**
     * @return the VL_TOTAL
     */
    public Double getVL_TOTAL() {
        return VL_TOTAL;
    }

    /**
     * @param VL_TOTAL the VL_TOTAL to set
     */
    public void setVL_TOTAL(Double VL_TOTAL) {
        this.VL_TOTAL = VL_TOTAL;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the UNIDADE
     */
    public String getUNIDADE() {
        return UNIDADE;
    }

    /**
     * @param UNIDADE the UNIDADE to set
     */
    public void setUNIDADE(String UNIDADE) {
        this.UNIDADE = UNIDADE;
    }

    /**
     * @return the MARCA
     */
    public String getMARCA() {
        return MARCA;
    }

    /**
     * @param MARCA the MARCA to set
     */
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    /**
     * @return the MODELO
     */
    public String getMODELO() {
        return MODELO;
    }

    /**
     * @param MODELO the MODELO to set
     */
    public void setMODELO(String MODELO) {
        this.MODELO = MODELO;
    }

    /**
     * @return the TAMANHO
     */
    public String getTAMANHO() {
        return TAMANHO;
    }

    /**
     * @param TAMANHO the TAMANHO to set
     */
    public void setTAMANHO(String TAMANHO) {
        this.TAMANHO = TAMANHO;
    }

    /**
     * @return the DATA
     */
    public String getDATA() {
        return DATA;
    }

    /**
     * @param DATA the DATA to set
     */
    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    /**
     * @return the ID_CARRO
     */
    public int getID_CARRO() {
        return ID_CARRO;
    }

    /**
     * @param ID_CARRO the ID_CARRO to set
     */
    public void setID_CARRO(int ID_CARRO) {
        this.ID_CARRO = ID_CARRO;
    }

    /**
     * @return the ID_PRODUTO
     */
    public String getID_PRODUTO() {
        return ID_PRODUTO;
    }

    /**
     * @param ID_PRODUTO the ID_PRODUTO to set
     */
    public void setID_PRODUTO(String ID_PRODUTO) {
        this.ID_PRODUTO = ID_PRODUTO;
    }

    /**
     * @return the QuantServico
     */
    public Double getQuantServico() {
        return QuantServico;
    }

    /**
     * @param QuantServico the QuantServico to set
     */
    public void setQuantServico(Double QuantServico) {
        this.QuantServico = QuantServico;
    }

    /**
     * @return the TotalServico
     */
    public Double getTotalServico() {
        return TotalServico;
    }

    /**
     * @param TotalServico the TotalServico to set
     */
    public void setTotalServico(Double TotalServico) {
        this.TotalServico = TotalServico;
    }

    /**
     * @return the TOTAL_GERAL_SERVICO
     */
    public Double getTOTAL_GERAL_SERVICO() {
        return TOTAL_GERAL_SERVICO;
    }

    /**
     * @param TOTAL_GERAL_SERVICO the TOTAL_GERAL_SERVICO to set
     */
    public void setTOTAL_GERAL_SERVICO(Double TOTAL_GERAL_SERVICO) {
        this.TOTAL_GERAL_SERVICO = TOTAL_GERAL_SERVICO;
    }

    /**
     * @return the TOTAL_GERAL
     */
    public Double getTOTAL_GERAL() {
        return TOTAL_GERAL;
    }

    /**
     * @param TOTAL_GERAL the TOTAL_GERAL to set
     */
    public void setTOTAL_GERAL(Double TOTAL_GERAL) {
        this.TOTAL_GERAL = TOTAL_GERAL;
    }

    /**
     * @return the OPERACAO
     */
    public String getOPERACAO() {
        return OPERACAO;
    }

    /**
     * @param OPERACAO the OPERACAO to set
     */
    public void setOPERACAO(String OPERACAO) {
        this.OPERACAO = OPERACAO;
    }
            
            
}
