package Classes;

public class ClassePessoa {
private int ID_PESSOA;
private String NOME;
private String TP_PESSOA;
private String TEL_FIXO;
private String TEL_CEL;
private String CEP;
private String BAIRRO;
private String EMAIL;
private String NUM_CASA;
private String ERRO;
private String LOGRADOURO;
private String CIDADE;
private String ESTADO;
    /**
     * @return the ID_PESSOA
     */
    public int getID_PESSOA() {
        return ID_PESSOA;
    }

    /**
     * @param ID_PESSOA the ID_PESSOA to set
     */
    public void setID_PESSOA(int ID_PESSOA) {
        this.ID_PESSOA = ID_PESSOA;
    }

    /**
     * @return the NOME
     */
    public String getNOME() {
        return NOME;
    }

    /**
     * @param NOME the NOME to set
     */
    public void setNOME(String NOME) {
        this.NOME = NOME;
    }

    /**
     * @return the TP_PESSOA
     */
    public String getTP_PESSOA() {
        return TP_PESSOA;
    }

    /**
     * @param TP_PESSOA the TP_PESSOA to set
     */
    public void setTP_PESSOA(String TP_PESSOA) {
        this.TP_PESSOA = TP_PESSOA;
    }

    /**
     * @return the TEL_FIXO
     */
    public String getTEL_FIXO() {
        return TEL_FIXO;
    }

    /**
     * @param TEL_FIXO the TEL_FIXO to set
     */
    public void setTEL_FIXO(String TEL_FIXO) {
        this.TEL_FIXO = TEL_FIXO;
    }

    /**
     * @return the TEL_CEL
     */
    public String getTEL_CEL() {
        return TEL_CEL;
    }

    /**
     * @param TEL_CEL the TEL_CEL to set
     */
    public void setTEL_CEL(String TEL_CEL) {
        this.TEL_CEL = TEL_CEL;
    }

    /**
     * @return the CEP
     */
    public String getCEP() {
        return CEP;
    }

    /**
     * @param CEP the CEP to set
     */
    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    /**
     * @return the BAIRRO
     */
    public String getBAIRRO() {
        return BAIRRO;
    }

    /**
     * @param BAIRRO the BAIRRO to set
     */
    public void setBAIRRO(String BAIRRO) {
        this.BAIRRO = BAIRRO;
    }

    /**
     * @return the EMAIL
     */
    public String getEMAIL() {
        return EMAIL;
    }

    /**
     * @param EMAIL the EMAIL to set
     */
    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    /**
     * @return the NUM_CASA
     */
    public String getNUM_CASA() {
        return NUM_CASA;
    }

    /**
     * @param NUM_CASA the NUM_CASA to set
     */
    public void setNUM_CASA(String NUM_CASA) {
        this.NUM_CASA = NUM_CASA;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }

    /**
     * @return the LOGRADOURO
     */
    public String getLOGRADOURO() {
        return LOGRADOURO;
    }

    /**
     * @param LOGRADOURO the LOGRADOURO to set
     */
    public void setLOGRADOURO(String LOGRADOURO) {
        this.LOGRADOURO = LOGRADOURO;
    }

    /**
     * @return the CIDADE
     */
    public String getCIDADE() {
        return CIDADE;
    }

    /**
     * @param CIDADE the CIDADE to set
     */
    public void setCIDADE(String CIDADE) {
        this.CIDADE = CIDADE;
    }

    /**
     * @return the ESTADO
     */
    public String getESTADO() {
        return ESTADO;
    }

    /**
     * @param ESTADO the ESTADO to set
     */
    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }



}
