
package Classes;

public class ClasseMovNF {
    private String NR_NF;
    private int ID_FORNECEDOR;
    private String DATA_EMISSAO;
    private String DATA_VENCIMENTO;
    private double VALOR_NF;
    private String ERRO;

    /**
     * @return the NR_NF
     */
    public String getNR_NF() {
        return NR_NF;
    }

    /**
     * @param NR_NF the NR_NF to set
     */
    public void setNR_NF(String NR_NF) {
        this.NR_NF = NR_NF;
    }

    /**
     * @return the ID_FORNECEDOR
     */
    public int getID_FORNECEDOR() {
        return ID_FORNECEDOR;
    }

    /**
     * @param ID_FORNECEDOR the ID_FORNECEDOR to set
     */
    public void setID_FORNECEDOR(int ID_FORNECEDOR) {
        this.ID_FORNECEDOR = ID_FORNECEDOR;
    }

    /**
     * @return the DATA_EMISSAO
     */
    public String getDATA_EMISSAO() {
        return DATA_EMISSAO;
    }

    /**
     * @param DATA_EMISSAO the DATA_EMISSAO to set
     */
    public void setDATA_EMISSAO(String DATA_EMISSAO) {
        this.DATA_EMISSAO = DATA_EMISSAO;
    }

    /**
     * @return the DATA_VENCIMENTO
     */
    public String getDATA_VENCIMENTO() {
        return DATA_VENCIMENTO;
    }

    /**
     * @param DATA_VENCIMENTO the DATA_VENCIMENTO to set
     */
    public void setDATA_VENCIMENTO(String DATA_VENCIMENTO) {
        this.DATA_VENCIMENTO = DATA_VENCIMENTO;
    }

    /**
     * @return the VALOR_NF
     */
    public double getVALOR_NF() {
        return VALOR_NF;
    }

    /**
     * @param VALOR_NF the VALOR_NF to set
     */
    public void setVALOR_NF(double VALOR_NF) {
        this.VALOR_NF = VALOR_NF;
    }

    /**
     * @return the ERRO
     */
    public String getERRO() {
        return ERRO;
    }

    /**
     * @param ERRO the ERRO to set
     */
    public void setERRO(String ERRO) {
        this.ERRO = ERRO;
    }
}
