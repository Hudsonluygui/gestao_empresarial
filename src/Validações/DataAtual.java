package Validações;

import CONEXAO.ConexaoOracle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DataAtual {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;

    public DataAtual() {
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);

    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            SQL = "";
        }

        return SQL;

    }

}
