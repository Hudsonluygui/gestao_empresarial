package Validações;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class LimparCampos {

    public static void LimparCampos(Container container) {
        Component components[] = container.getComponents();
        for (Component component : components) {
            if (component instanceof JFormattedTextField) {
                JFormattedTextField field = (JFormattedTextField) component;
                field.setValue(null);
                field.setText("");
            } else if (component instanceof JTextField) {
                JTextField field = (JTextField) component;
                field.setText("");
            } else if (component instanceof JTextArea) {
                JTextArea area = (JTextArea) component;
                area.setText("");
            } else if (component instanceof JComboBox) {
                JComboBox combo = (JComboBox) component;
                String conteudo = (String) combo.getModel().getSelectedItem();
                if ((conteudo == null) || (combo.TOOL_TIP_TEXT_KEY.equals("A"))) {
                    combo.removeAllItems();
                }
            }
        }
    }
}
