/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Validações;

import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;


public class LookAndFeelWindows {
public static void setLookAndFeel(Component tela){
        try{
            //com.sun.java.swing.plaf.windows.WindowsLookAndFeel
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
          //  UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            SwingUtilities.updateComponentTreeUI(tela);
        }

        catch(Exception erro){
            JOptionPane.showMessageDialog(null, "Não foi possível aplicar 'look and feel'", "Aviso", JOptionPane.WARNING_MESSAGE);
        }
    }
}
