package Validações;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author acsantana
 */
public class ValidaEstadoBotoes {

    public void ValidaCamposIncluir(Container container, Container container1) {


        Component components[] = container.getComponents();

        for (Component component : components) {

            if (component instanceof JComboBox) {
                JComboBox field = (JComboBox) component;
                field.setEnabled(true);
            }

            if (component instanceof JTextField) {
                JTextField field = (JTextField) component;
                field.setEnabled(true);

            }


            if (component instanceof JTable) {
                JTable field = (JTable) component;
                field.setEnabled(true);

            }

            if (component instanceof JTextArea) {
                JTextArea field = (JTextArea) component;
                if (field.isEditable()) {
                    field.setEnabled(true);
                }
                //field.enable(true);

            }

            if (component instanceof JFormattedTextField) {
                JFormattedTextField field = (JFormattedTextField) component;
                if (field.isEditable()) {
                    field.setEnabled(true);
                }
            }

            if (component instanceof JButton) {
                JButton field = (JButton) component;
                field.setEnabled(true);
            }

            if (component instanceof JRadioButton) {
                JRadioButton field = (JRadioButton) component;
                field.setEnabled(true);
            }

            if (component instanceof JTextArea) {
                JTextArea field = (JTextArea) component;
                field.setEnabled(true);
            }

        }

// testa painel de botoes        
        Component components1[] = container1.getComponents();
        for (Component component1 : components1) {

            if (component1 instanceof JButton) {
                JButton field = (JButton) component1;
                String nome = field.getText();
                if ("Incluir".equals(nome) | "Alterar".equals(nome) | "Excluir".equals(nome)) {
                    field.setEnabled(false);
                } else {
                    field.setEnabled(true);
                }
            }
        }
    }

    public void ValidaCamposCancelar(Container container, Container container1) {

        Component components[] = container.getComponents();
        for (Component component : components) {
            if (component instanceof JComboBox) {
                JComboBox field = (JComboBox) component;
                field.setEnabled(false);

            }

            if (component instanceof JTextField) {
                JTextField field = (JTextField) component;
                if (field.isEditable()) {
                    field.setEnabled(false);

                }
            }



            if (component instanceof JTextArea) {
                JTextArea field = (JTextArea) component;
                field.setEnabled(false);// --- não valida ass. ferty.
                //field.enable(false);

            }

            if (component instanceof JFormattedTextField) {
                JFormattedTextField field = (JFormattedTextField) component;
                if (field.isEditable()) {
                    field.setEnabled(false);
                }
            }
            if (component instanceof JButton) {
                JButton field = (JButton) component;
                field.setEnabled(false);
            }

            if (component instanceof JRadioButton) {
                JRadioButton field = (JRadioButton) component;
                field.setEnabled(false);
            }

            if (component instanceof JTextArea) {
                JTextArea field = (JTextArea) component;
                field.setEnabled(false);
            }


        }

        // testa painel de botoes       
        Component components1[] = container1.getComponents();
        for (Component component1 : components1) {

            if (component1 instanceof JButton) {
                JButton field = (JButton) component1;
                String nome = field.getText();
                if ("Incluir".equals(nome) | "Alterar".equals(nome) | "Excluir".equals(nome)) {
                    field.setEnabled(true);
                } else {
                    field.setEnabled(false);
                }
            }

        }
    }
}
