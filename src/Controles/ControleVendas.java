package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseReceberVendas;
import Classes.ClasseVenda;
import Classes.ClasseVendaProdutos;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleVendas {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseVenda Classe_Venda;
    ClasseVendaProdutos Classe_Venda_Produtos;

    public ControleVendas() {
        conecta_oracle = new ConexaoOracle();
        Classe_Venda = new ClasseVenda();
        Classe_Venda_Produtos = new ClasseVendaProdutos();
    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return SQL;
    }

    public boolean Cadastrar(ClasseVenda obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_VENDA", "VENDA");
            obj.setID_VENDA(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO VENDA "
                    + "(ID_VENDA,"
                    + "ID_CLIENTE,"
                    + "DATA_VENDA,"
                    + "DATA_VENCIMENTO,"
                    + "TOTAL_VENDA,DESCR)"
                    + " VALUES ("
                    + obj.getID_VENDA() + ","
                    + obj.getID_CLIENTE() + ",'"
                    + obj.getDATA_VENDA() + "',"
                    + obj.getDATA_VENCIMENTO() + ","
                    + obj.getTOTAL_VENDA() + ",'"
                    + obj.getDESCR() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Alterar(ClasseVenda obj) {
        try {
            pst = conn.prepareStatement(" UPDATE VENDA SET NOME = ?, "
                    + " DATA_VENDA = ?,"
                    + " DATA_VENCIMENTO = ?,"
                    + " TOTAL_VENDA = ?,"
                    + " ID_CLIENTE = ?"
                    + " WHERE ID_VENDA = ?");
            pst.setString(1, obj.getDATA_VENDA());
            pst.setString(2, obj.getDATA_VENCIMENTO());
            pst.setDouble(3, obj.getTOTAL_VENDA());
            pst.setInt(4, obj.getID_CLIENTE());
            pst.setInt(5, obj.getID_VENDA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ResultSet PesquisaGeral() {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), "
                + " TO_CHAR(V.DATA_VENCIMENTO, 'DD/MM/YYYY'), V.TOTAL_VENDA,V.DESCR"
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaCodigo(int Cod) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), TO_CHAR(V.DATA_VENCIMENTO, 'DD/MM/YYYY'), V.TOTAL_VENDA"
                + ",V.DESCR "
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA "
                + " WHERE V.ID_VENDA = " + Cod;
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaCodigoCliente(int Cod) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), TO_CHAR(V.DATA_VENCIMENTO, 'DD/MM/YYYY'), V.TOTAL_VENDA"
                + ",V.DESCR "
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = " + Cod + " ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaNomeCliente(String Nome) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, "
                + " TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), "
                + " TO_CHAR(V.DATA_VENCIMENTO, 'DD/MM/YYYY'), V.TOTAL_VENDA,V.DESCR"
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA "
                + " WHERE P.NOME LIKE '%" + Nome + "%' ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCPF(String CPF) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), "
                + " TO_CHAR(V.DATA_VENCIMENTO,'DD/MM/YYYY'), V.TOTAL_VENDA,V.DESCR "
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " JOIN CAD_FISICA F ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE F.CPF LIKE '%" + CPF + "%' ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCNPJ(String CNPJ) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY'), "
                + " TO_CHAR(V.DATA_VENCIMENTO,'DD/MM/YYYY'), V.TOTAL_VENDA,V.DESCR"
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON P.ID_PESSOA = J.ID_PESSOA"
                + " WHERE J.CNPJ LIKE '%" + CNPJ + "%' ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarData(String Data) {
        String SQL = "SELECT V.ID_VENDA, P.NOME, TO_CHAR(V.DATA_VENDA, 'DD/MM/YYYY') AS DATA , TO_CHAR(V.DATA_VENCIMENTO,'DD/MM/YYYY'), V.TOTAL_VENDA"
                + " FROM VENDA V JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " WHERE DATA = '" + Data + "' ";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public int PesquisarCodigoCliente(int Cod) {
        String SQL = "SELECT ID_CLIENTE FROM VENDA WHERE ID_VENDA = " + Cod;
        int Retorno = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Retorno = conecta_oracle.resultSet.getInt("ID_CLIENTE");
        } catch (Exception ex) {
            Retorno = 0;
        }
        return Retorno;
    }

    public boolean Excluir(ClasseVenda obj) {
        ControleVendaProdutos venda = new ControleVendaProdutos();
        String SQL = "DELETE FROM VENDA WHERE ID_VENDA = " + obj.getID_VENDA();
        try {
            Classe_Venda_Produtos.setID_VENDA(obj.getID_VENDA());
            venda.Excluir_Venda(Classe_Venda_Produtos);
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ClasseVenda Recuperar_Parcela(ClasseVenda obj) {
        String SQL = " SELECT ID_VENDA,ID_CLIENTE,TO_CHAR(DATA_VENDA, 'DD/MM/YYYY') AS DATA, TOTAL_VENDA "
                + " FROM VENDA WHERE ID_VENDA = " + obj.getID_VENDA();
        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                obj.setID_VENDA(conecta_oracle.resultSet.getInt("ID_VENDA"));
                obj.setID_CLIENTE(conecta_oracle.resultSet.getInt("ID_CLIENTE"));
                obj.setDATA_VENDA(conecta_oracle.resultSet.getString("DATA"));
                obj.setTOTAL_VENDA(conecta_oracle.resultSet.getDouble("TOTAL_VENDA"));
            }
        } catch (Exception e) {
            obj.setERRO("OCORREU ALGUM ERRO");
        }
        return obj;
    }

    public String Data_Vencimento3(int Codigo) {
        String SQL = "SELECT TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA FROM RECEBER_VENDAS WHERE ID_PARCELA = 1 "
                + " AND SITUACAO = 'ABERTA' AND IDENTIF = '" + Codigo + "' AND DESCR = 'VENDA'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Data_Vencimento2(int Codigo) {
        String SQL = "SELECT TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA FROM RECEBER_VENDAS "
                + "WHERE ID_PARCELA = 2 "
                + " AND SITUACAO = 'ABERTA' AND IDENTIF = '" + Codigo + "' AND DESCR = 'VENDA'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public boolean AlterarData(ClasseVenda obj) {
        try {
            pst = conn.prepareStatement(" UPDATE VENDA SET "
                    + "DATA_VENCIMENTO = ?"
                    + " WHERE ID_VENDA = ?");
            pst.setString(1, obj.getDATA_VENCIMENTO());
            pst.setInt(2, obj.getID_VENDA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Pago(ClasseVenda obj) {
        try {
            pst = conn.prepareStatement(" UPDATE VENDA SET PAGO = ? "
                    + " WHERE ID_VENDA = ?");
            pst.setString(1, obj.getPAGO());
            pst.setInt(2, obj.getID_VENDA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }
}
