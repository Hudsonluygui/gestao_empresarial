package Controles;



import CONEXAO.ConexaoOracle;
import Classes.ClasseCheque;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleCheque {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseCheque Cheque;

    public ControleCheque() {
        Cheque = new ClasseCheque();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean CadastrarComData(ClasseCheque obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_CHEQUE", "CAD_CHEQUE");
            obj.setID_CHEQUE(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAD_CHEQUE1 ("
                    + " ID_CHEQUE,"
                    + " TITULAR,"
                    + " CLIENTE,"
                    + " TELEFONE,"
                    + " CIDADE,"
                    + " VALOR,"
                    + " DATA_EMISSAO,"
                    + " DATA_VENCIMENTO,"
                    + " PAGO,"
                    + " NOME_RECEBIDO,"
                    + " VALOR_RECEBIDO,DATA_RECEBIDO) "
                    + " VALUES ("
                    + obj.getID_CHEQUE() + ",'"
                    + obj.getTITULAR() + "','"
                    + obj.getCLIENTE() + "','"
                    + obj.getTELEFONE() + "','"
                    + obj.getCIDADE() + "',"
                    + obj.getVALOR() + ",'"
                    + obj.getDATA_EMISSAO() + "','"
                    + obj.getDATA_VENCIMENTO() + "','"
                    + obj.getPAGO() + "','"
                    + obj.getNOME_RECEBIDO() + "',"
                    + obj.getVALOR_RECEBIDO() + ",'"
                    + obj.getDATA_RECEBIDO() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            return false;
        }
    }
       public boolean CadastrarSemData(ClasseCheque obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_CHEQUE", "CAD_CHEQUE");
            obj.setID_CHEQUE(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CAD_CHEQUE1 ("
                    + " ID_CHEQUE,"
                    + " TITULAR,"
                    + " CLIENTE,"
                    + " TELEFONE,"
                    + " CIDADE,"
                    + " VALOR,"
                    + " DATA_EMISSAO,"
                    + " DATA_VENCIMENTO,"
                    + " PAGO,"
                    + " NOME_RECEBIDO,"
                    + " VALOR_RECEBIDO,DATA_RECEBIDO) "
                    + " VALUES ("
                    + obj.getID_CHEQUE() + ",'"
                    + obj.getTITULAR() + "','"
                    + obj.getCLIENTE() + "','"
                    + obj.getTELEFONE() + "','"
                    + obj.getCIDADE() + "',"
                    + obj.getVALOR() + ",'"
                    + obj.getDATA_EMISSAO() + "','"
                    + obj.getDATA_VENCIMENTO() + "','"
                    + obj.getPAGO() + "','"
                    + obj.getNOME_RECEBIDO() + "',"
                    + obj.getVALOR_RECEBIDO() + ","
                    + obj.getDATA_RECEBIDO() + ")");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            return false;
        }
    }

    public ResultSet PesquisarGeral() {
        conecta_oracle.executeSQL("SELECT TITULAR,CLIENTE,CIDADE,VALOR,TELEFONE,"
                + "TO_CHAR(DATA_EMISSAO,'DD/MM/YYYY'),"
                + "TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY'),"
                + "PAGO,NOME_RECEBIDO,VALOR_RECEBIDO,"
                + "TO_CHAR(DATA_RECEBIDO,'DD/MM/YYYY'),"
                + " ID_CHEQUE FROM CAD_CHEQUE1");
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarTitular(String Nome) {
        conecta_oracle.executeSQL("SELECT TITULAR,CLIENTE,CIDADE,VALOR,TELEFONE,"
                + "TO_CHAR(DATA_EMISSAO,'DD/MM/YYYY'),"
                + "TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY'),"
                + "PAGO,NOME_RECEBIDO,VALOR_RECEBIDO,"
                + "TO_CHAR(DATA_RECEBIDO,'DD/MM/YYYY'),"
                + " ID_CHEQUE FROM CAD_CHEQUE1 WHERE TITULAR LIKE '%" + Nome + "%'");
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCliente(String Nome) {
        conecta_oracle.executeSQL("SELECT TITULAR,CLIENTE,CIDADE,VALOR,TELEFONE,"
                + "TO_CHAR(DATA_EMISSAO,'DD/MM/YYYY'),"
                + "TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY'),"
                + "PAGO,NOME_RECEBIDO,VALOR_RECEBIDO,"
                + "TO_CHAR(DATA_RECEBIDO,'DD/MM/YYYY'),"
                + " ID_CHEQUE FROM CAD_CHEQUE1 WHERE CLIENTE LIKE '%" + Nome + "%'");
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCidade(String Nome) {
        conecta_oracle.executeSQL("SELECT TITULAR,CLIENTE,CIDADE,VALOR,TELEFONE,"
                + "TO_CHAR(DATA_EMISSAO,'DD/MM/YYYY'),"
                + "TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY'),"
                + "PAGO,NOME_RECEBIDO,VALOR_RECEBIDO,"
                + "TO_CHAR(DATA_RECEBIDO,'DD/MM/YYYY'),"
                + " ID_CHEQUE FROM CAD_CHEQUE1 WHERE Cidade LIKE '%" + Nome + "%'");
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public boolean Excluir(ClasseCheque obj) {
        String SQL = "DELETE FROM CAD_CHEQUE1 WHERE ID_CHEQUE = " + obj.getID_CHEQUE();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Alterar(ClasseCheque obj) {
        try {
            pst = conn.prepareStatement("UPDATE CAD_CHEQUE1 SET "
                    + "TITULAR = ?,"
                    + "CLIENTE = ?,"
                    + "TELEFONE = ?,"
                    + "CIDADE = ?,"
                    + "VALOR = ?,"
                    + "DATA_EMISSAO = ?,"
                    + "DATA_VENCIMENTO = ?,"
                    + "NOME_RECEBIDO = ?,"
                    + "VALOR_RECEBIDO = ?,"
                    + "DATA_RECEBIDO = ?,"
                    + "PAGO = ? WHERE ID_CHEQUE = ?");
            pst.setString(1, obj.getTITULAR());
            pst.setString(2, obj.getCLIENTE());
            pst.setString(3, obj.getTELEFONE());
            pst.setString(4, obj.getCIDADE());
            pst.setDouble(5, obj.getVALOR());
            pst.setString(6, obj.getDATA_EMISSAO());
            pst.setString(7, obj.getDATA_VENCIMENTO());
            pst.setString(8, obj.getNOME_RECEBIDO());
            pst.setDouble(9, obj.getVALOR_RECEBIDO());
            pst.setString(10, obj.getDATA_RECEBIDO());
            pst.setString(11, obj.getPAGO());
            pst.setInt(12, obj.getID_CHEQUE());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            return false;
        }
    }
}
