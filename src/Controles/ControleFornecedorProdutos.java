package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseFornecedor;
import Classes.ClasseFornecedorProdutos;
import Classes.ClassePessoa;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleFornecedorProdutos {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;
    ClasseFornecedorProdutos Classe_FornProdutos;
    ControleFornecedor Controle_Fornecedor;
    ClasseFornecedor Classe_Fornecedor;

    public ControleFornecedorProdutos() {
        conecta_oracle = new ConexaoOracle();
        Classe_Pessoa = new ClassePessoa();
        Classe_FornProdutos = new ClasseFornecedorProdutos();
        Controle_Fornecedor = new ControleFornecedor();
        Classe_Fornecedor = new ClasseFornecedor();
    }

    public boolean Cadastrar(ClasseFornecedorProdutos obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO FORNECEDOR_PRODUTOS "
                    + "(ID_PESSOA,"
                    + "ID_FORNECEDOR,"
                    + "ID_PRODUTO, CODIGO)"
                    + " VALUES ("
                    + obj.getID_PESSOA() + ","
                    + obj.getID_FORNECEDOR() + ",'"
                    + obj.getID_PRODUTO() + "',"
                    + obj.getCODIGO() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean RemoverProdeuto(ClasseFornecedorProdutos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM FORNECEDOR_PRODUTOS WHERE "
                    + " ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR()
                    + " AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "'"
                    + " AND CODIGO = " + obj.getCODIGO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public int PesquisarExistentes(ClasseFornecedorProdutos obj) {
        String SQL = "SELECT * FROM FORNECEDOR_PRODUTOS WHERE ID_PESSOA = " + obj.getID_PESSOA()
                + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "'"
                + " AND CODIGO = " + obj.getCODIGO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            int Codigo_Pessoa = conecta_oracle.resultSet.getInt("ID_PESSOA");
            String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            int Codigo_Forn = conecta_oracle.resultSet.getInt("ID_FORNECEDOR");
            int CODIGO = conecta_oracle.resultSet.getInt("CODIGO");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public boolean Exluir_TUDO(ClasseFornecedorProdutos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM FORNECEDOR_PRODUTOS WHERE ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR());
            pst.executeUpdate();
            Classe_Fornecedor.setID_FORNECEDOR(obj.getID_FORNECEDOR());
            Classe_Fornecedor.setID_PESSOA(obj.getID_PESSOA());
            Controle_Fornecedor.Exluir_Fornecedor(Classe_Fornecedor);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean ExcluirFornecedor(ClasseFornecedorProdutos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM FORNECEDOR_PRODUTOS WHERE ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR()
                    + " AND CODIGO = " + obj.getCODIGO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean PesquisarExistente(ClasseFornecedorProdutos obj) {
        String SQL = "SELECT ID_PRODUTO FROM FORNECEDOR_PRODUTOS WHERE ID_FORNECEDOR = " + obj.getID_FORNECEDOR();
        int A = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            A = conecta_oracle.resultSet.getInt("ID_PRODUTO");
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public ResultSet PesquisarProdutos(int Codigo) {
        String SQL = "SELECT DISTINCT FP.ID_PRODUTO, P.DS_PRODUTO,P.DS_UNIDADE, P.TAMANHO,"
                + " P.MARCA,P.MODELO FROM FORNECEDOR_PRODUTOS FP"
                + " JOIN CAD_PRODUTO P ON P.ID_PRODUTO = FP.ID_PRODUTO"
                + " WHERE FP.ID_FORNECEDOR = " + Codigo + " ORDER BY FP.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public int Pesquisar_CodigoFornecedor(ClasseFornecedorProdutos obj) {
        int Codigo = 0;
        String SQL = ("SELECT CODIGO FROM FORNECEDOR_PRODUTOS WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' "
                + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR()
                + " AND ID_PESSOA = " + obj.getID_PESSOA());
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Codigo = conecta_oracle.resultSet.getInt("CODIGO");
            } else {
                Codigo = 0;
                obj.setERRO("Código não Localizado");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }

    public ResultSet PesquisarProdutos2(int Codigo) {
        String SQL = "SELECT DISTINCT NULL,FP.ID_PRODUTO, P.DS_PRODUTO,P.DS_UNIDADE, P.TAMANHO,"
                + " P.MARCA,P.MODELO FROM FORNECEDOR_PRODUTOS FP"
                + " JOIN CAD_PRODUTO P ON P.ID_PRODUTO = FP.ID_PRODUTO"
                + " WHERE FP.ID_FORNECEDOR = " + Codigo + " ORDER BY FP.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public int Contador(ClasseFornecedorProdutos obj) {
        int Resultado = 0;
        String SQL = "SELECT COUNT (CODIGO) AS CONTADOR FROM FORNECEDOR_PRODUTOS WHERE ID_FORNECEDOR = " + obj.getID_FORNECEDOR()
                + " AND ID_PESSOA = " + obj.getID_PESSOA();
        conecta_oracle.executeSQL(SQL);
        try {
            if (conecta_oracle.resultSet.first()) {
                Resultado = conecta_oracle.resultSet.getInt("CONTADOR");
            }
        } catch (Exception ex) {
            System.out.println(ex);

        }
        return Resultado;
    }
}
