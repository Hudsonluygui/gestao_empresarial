package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseFisica;
import Classes.ClassePessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleFisica {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;
    ClasseFisica Classe_Fisica;

    public ControleFisica() {
        Classe_Fisica = new ClasseFisica();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseFisica obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO CAD_FISICA "
                    + "(ID_PESSOA,"
                    + "RG,"
                    + "CPF,"
                    + "DATA_NASC)"
                    + "VALUES ("
                    + obj.getID_PESSOA() + ", '"
                    + obj.getRG() + "','"
                    + obj.getCPF() + "','"
                    + obj.getDATA_NASC() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }
 public ResultSet PesquisarCPF(String CPF) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(P.TEL_FIXO, ' ') AS TELEFONE, P.TEL_CEL, P.EMAIL"
                + " FROM CLIENTE C "
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " JOIN CAD_FISICA F ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE F.CPF LIKE '%"+CPF+"'";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }
 
 public void Recuperar(ClasseFisica obj){
   String SQL = "SELECT CPF,RG,DATA_NASC FROM CAD_FISICA WHERE ID_PESSOA = "+obj.getID_PESSOA();
   conecta_oracle.executeSQL(SQL);
   try{
        conecta_oracle.resultSet.next();
        obj.setCPF(conecta_oracle.resultSet.getString("CPF"));
        obj.setRG(conecta_oracle.resultSet.getString("RG"));
        obj.setDATA_NASC(conecta_oracle.resultSet.getString("DATA_NASC"));
   }catch (Exception ex){
       System.out.println(ex);
       obj.setERRO("OCORREU ALGUM ERRO");
   }
 }
}
