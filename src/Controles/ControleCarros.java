package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseProdutos;
import Classes.Classe_de_Carros_Compativeis_Produtos;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;
import javax.swing.JOptionPane;

public class ControleCarros {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseProdutos Classe_Produtos;
    Classe_de_Carros_Compativeis_Produtos Classe_Carros;

    public ControleCarros() {
        conecta_oracle = new ConexaoOracle();
        Classe_Carros = new Classe_de_Carros_Compativeis_Produtos();
        Classe_Produtos = new ClasseProdutos();
    }

    public int Criar_Vetor() {
        String SQL = "SELECT COUNT (DISTINCT ID_CARRO) AS CONT FROM CARROS_COMP";
        int Tamanho = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Tamanho = conecta_oracle.resultSet.getInt("CONT");
            } else {
                Tamanho = 0;
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Tamanho;
    }

    public boolean Cadastrar(Classe_de_Carros_Compativeis_Produtos obj) {
        try {
            // UltimaSequencia us = new UltimaSequencia("ID_CARRO", "CARROS_COMP");
            //     obj.setID_CARRO(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO CARROS_COMP (ID_CARRO,ID_PRODUTO,MARCA_CARRO,MODELO_CARRO,CODIGO) VALUES ("
                    + obj.getID_CARRO() + ",'"
                    + obj.getID_PRODUTO() + "','"
                    + obj.getMARCA_CARRO() + "','"
                    + obj.getMODELO_CARRO() + "',"
                    + obj.getCODIGO() + ")");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int PesquisaCarro(Classe_de_Carros_Compativeis_Produtos obj) {
        String SQL = "SELECT * FROM CARROS_COMP WHERE ID_PRODUTO = '" + obj.getID_PRODUTO()
                + "' AND ID_CARRO = " + obj.getID_CARRO() + " AND CODIGO =" + obj.getCODIGO();
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            int Codigo_Gerado = conecta_oracle.resultSet.getInt("CODIGO"); // CODIGO é um Código Gerado como PK UNICO
            String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            int Codigo_Carro = conecta_oracle.resultSet.getInt("ID_CARRO");
            return 0;
        } catch (SQLException ex) {
            return -1;
        }

    }

    public boolean ExcluirCarros(ClasseProdutos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CARROS_COMP WHERE "
                    + " ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                    + " AND CODIGO = " + obj.getCODIGO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Classe_Carros.setERRO(String.valueOf(ex));
            return false;
        }
    }

    public ResultSet Pesquisar_Carros(Classe_de_Carros_Compativeis_Produtos obj) {
        String SQL = "SELECT DISTINCT ID_CARRO, MARCA_CARRO, MODELO_CARRO FROM CARROS_COMP WHERE ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                + " ORDER BY ID_CARRO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar_Retorno(String Cod) {
        String SQL = "SELECT DISTINCT NULL, C.ID_CARRO,C.MARCA_CARRO, C.MODELO_CARRO FROM CARROS_COMP C"
                + " WHERE C.ID_PRODUTO = '" + Cod + "' ORDER BY C.ID_CARRO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public int Pesquisar_Codigo(Classe_de_Carros_Compativeis_Produtos obj) {
        int Codigo = 0;
        String SQL = "SELECT CODIGO FROM CARROS_COMP WHERE ID_CARRO = " + obj.getID_CARRO()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+"'";
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Codigo = conecta_oracle.resultSet.getInt("CODIGO");

            } else {
                Codigo = 0;
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return Codigo;
    }

    public int PesquisaCarros(Classe_de_Carros_Compativeis_Produtos obj) {
        String SQL = "SELECT * FROM CARROS_COMP WHERE ID_PRODUTO = '" + obj.getID_PRODUTO()+"'"
                + " AND CODIGO = " + obj.getCODIGO() + "AND ID_CARRO = " + obj.getID_CARRO();
        conecta_oracle.executeSQL(SQL);
        try {
            if (conecta_oracle.resultSet.next()) {
                int Codigo_Gerado = conecta_oracle.resultSet.getInt("CODIGO"); // CODIGO é um Código Gerado como PK UNICO
                String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
                int Codigo_Carro = conecta_oracle.resultSet.getInt("ID_CARRO");
            }
            return 0;
        } catch (SQLException ex) {
            return -1;
        }
    }

    public boolean ExcluirCarros(Classe_de_Carros_Compativeis_Produtos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CARROS_COMP WHERE "
                    + " ID_PRODUTO = '" + obj.getID_PRODUTO()+"'"
                    + " AND CODIGO = " + obj.getCODIGO()
                    + " AND ID_CARRO = " + obj.getID_CARRO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            return false;
        }
    }

    public int Codigo(Classe_de_Carros_Compativeis_Produtos obj) {
        String SQL = "SELECT CODIGO FROM CARROS_COMP WHERE ID_CARRO =" + obj.getID_CARRO()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+"'"
                + " AND MARCA_CARRO = '" + obj.getMARCA_CARRO()+ "'"
                + " AND MODELO_CARRO = '" + obj.getMODELO_CARRO()+ "'";
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                obj.setCODIGO(conecta_oracle.resultSet.getInt("CODIGO"));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return obj.getCODIGO();
    }

    public int Contador(Classe_de_Carros_Compativeis_Produtos obj) {
        int Contador = 0;
        String SQL = "SELECT COUNT(CODIGO) AS CONT FROM CARROS_COMP WHERE ID_CARRO =" + obj.getID_CARRO()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                + " AND MARCA_CARRO = '" + obj.getMARCA_CARRO()
                + "' AND MODELO_CARRO = '" + obj.getMODELO_CARRO()+ "' ";
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Contador = (conecta_oracle.resultSet.getInt("CONT"));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return Contador;
    }
}
