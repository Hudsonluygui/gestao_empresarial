package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseCaixa;
import Validações.ConverterDecimais;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleFolha {

    ConexaoOracle conecta_oracle;
    ConverterDecimais converter;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseFolha Classe;

    public ControleFolha() {
        Classe = new ClasseFolha();
        conecta_oracle = new ConexaoOracle();
        conecta_oracle.conecta(0);
        converter = new ConverterDecimais();
    }

    public boolean Cadastrar(ClasseFolha obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("CODIGO", "FOLHA_PAGAMENTO");
            obj.setCod(Integer.parseInt(us.getUtl()));
            String SQL = ("INSERT INTO FOLHA_PAGAMENTO(CODIGO,DATA,DESCR,VALOR,NOME)"
                    + "VALUES ("
                    + obj.getCod() + ",'"
                    + obj.getData() + "','"
                    + obj.getDesc() + "',"
                    + obj.getValor() + ",'"
                    + obj.getNome() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Classe.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, Classe.getERRO());
            return false;
        }
    }
       public boolean Excluir(ClasseFolha obj) {
       String SQL = "DELETE FROM FOLHA_PAGAMENTO WHERE CODIGO = " + obj.getCod();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
      public ResultSet Pesquisar() {
        String SQL = "SELECT NOME, DESCR,VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, CODIGO FROM FOLHA_PAGAMENTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
      
          public ResultSet Pesquisar(String Nome) {
        String SQL = "SELECT NOME, DESCR,VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, CODIGO "
                + "FROM FOLHA_PAGAMENTO WHERE NOME LIKE '%"+Nome+"%'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
        public ResultSet Pesquisar_Data(String Data) {
        String SQL = "SELECT NOME, DESCR,VALOR,TO_CHAR(DATA,'DD/MM/YYYY') AS DATA, CODIGO "
                + "FROM FOLHA_PAGAMENTO WHERE DATA = '"+Data+"'";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
}
