package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovNF;
import Classes.ClasseMovProdutos;
import Classes.ClasseVenda;
import Classes.ClasseVendaProdutos;
import Classes.NF_PRODUTOS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleNFProdutos {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseMovNF Classe_NF;
    NF_PRODUTOS Classe_ProdutosNF;
    ClasseMovProdutos Classe_MovProdutos;
    ControleMovProdutos Controle_MovProdutos;

    public ControleNFProdutos() {
        conecta_oracle = new ConexaoOracle();
        Classe_NF = new ClasseMovNF();
        Classe_ProdutosNF = new NF_PRODUTOS();
        Classe_MovProdutos = new ClasseMovProdutos();
        Controle_MovProdutos = new ControleMovProdutos();
    }

    public double Pesquisar_Estoque(NF_PRODUTOS obj) {
        double EST_ANTERIOR = 0;
        conecta_oracle.executeSQL("SELECT QUANT FROM CAD_PRODUTO"
                + " WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' AND CODIGO  =" + obj.getCODIGO());
        try {
            if (ConexaoOracle.resultSet.next()) {
                EST_ANTERIOR = (ConexaoOracle.resultSet.getInt("QUANT"));
            }
        } catch (Exception e) {
            obj.getERRO();
        }
        return EST_ANTERIOR;

    }

    public boolean Cadastrar(NF_PRODUTOS obj, ClasseMovNF obj2) {
        try {
            pst = conn.prepareStatement("INSERT INTO MOV_NF_PRODUTO "
                    + "(NR_NF,"
                    + "ID_PRODUTO,"
                    + "VALOR_TOTAL,"
                    + "CODIGO, QUANT, VALOR_UNIT)"
                    + " VALUES ('"
                    + obj.getNR_NF() + "','"
                    + obj.getID_PRODUTO() + "',"
                    + obj.getVALOR_TOTAL() + ","
                    + obj.getCODIGO() + ","
                    + obj.getQUANT() + ","
                    + obj.getVALOR_UNIT() + ")");
            pst.executeUpdate();
            Double QUANT_MOV = obj.getQUANT();
            Double QUANT_ANT = Pesquisar_Estoque(obj);
            Classe_MovProdutos.setCODIGO(obj.getCODIGO());
            Classe_MovProdutos.setDATA(obj2.getDATA_EMISSAO());
            Classe_MovProdutos.setDESCR("COMPRAS");
            Classe_MovProdutos.setDS_UNIDADE(obj.getUNIDADE());
            Classe_MovProdutos.setERRO(null);
            Classe_MovProdutos.setEST_ANTERIOR(QUANT_ANT);
            Classe_MovProdutos.setIDENTIF(obj2.getNR_NF());
            Classe_MovProdutos.setID_PRODUTO(obj.getID_PRODUTO());
            Classe_MovProdutos.setMARCA(obj.getMARCA());
            Classe_MovProdutos.setMODELO(obj.getMODELO());
            Classe_MovProdutos.setQUANT_MOVIDA(QUANT_MOV);
            Classe_MovProdutos.setTAMANHO(obj.getTAMANHO());
            QUANT_ANT = QUANT_ANT + QUANT_MOV;
            Classe_MovProdutos.setEST_ATUAL(QUANT_ANT);
            Controle_MovProdutos.Cadastrar(Classe_MovProdutos);
            Controle_MovProdutos.Atualizar_Estoque(Classe_MovProdutos);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int PesquisarExistentes(NF_PRODUTOS obj) {
        String SQL = "SELECT * FROM MOV_NF_PRODUTO WHERE NR_NF = '" + obj.getNR_NF() + "'"
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                + " AND CODIGO = " + obj.getCODIGO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            String NUM_NF = conecta_oracle.resultSet.getString("NR_NF");
            int CODIGO = conecta_oracle.resultSet.getInt("CODIGO");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public double Pesquisar_Estoque(ClasseVendaProdutos obj) {
        double EST_ANTERIOR = 0;
        conecta_oracle.executeSQL("SELECT QUANT FROM CAD_PRODUTO"
                + " WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' AND CODIGO  =" + obj.getCODIGO());
        try {
            if (ConexaoOracle.resultSet.next()) {
                EST_ANTERIOR = (ConexaoOracle.resultSet.getInt("QUANT"));
            }
        } catch (Exception e) {
            obj.getERRO();
        }
        return EST_ANTERIOR;

    }

    public ResultSet Pesquisar_Produtos(NF_PRODUTOS obj) {
        String SQL = "SELECT VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VL_UNIT, VP.VL_TOTAL"
                + " FROM MOV_NF_PRODUTO VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.NR_NF = '" + obj.getNR_NF() + "'"
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar_ProdutosRetorno(NF_PRODUTOS obj) {
        String SQL = "SELECT NULL,VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VALOR_UNIT, VP.VALOR_TOTAL"
                + " FROM MOV_NF_PRODUTO VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.NR_NF = " + obj.getNR_NF() 
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public boolean Excluir_Venda(NF_PRODUTOS obj) {
        String SQL = "DELETE FROM MOV_NF_PRODUTO WHERE NR_NF = " + obj.getNR_NF();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Remover_Mov(obj);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Remover_Mov(NF_PRODUTOS obj) {
        String SQL = "DELETE FROM MOV_PRODUTOS WHERE IDENTIF = '" + obj.getNR_NF() + "'";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Remover_Produto(NF_PRODUTOS obj) {
        String SQL = "DELETE FROM MOV_NF_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                + " AND NR_NF = " + obj.getNR_NF()
                + " AND CODIGO = " + obj.getCODIGO();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Remover_Mov(obj);
            return true;
        } catch (SQLException ex) {
            return false;
        }

    }

    public ResultSet PesquisaGeral() {
        String SQL = "SELECT N.NR_NF, P.NOME, TO_CHAR(N.DATA_EMISSAO, 'DD/MM/YYYY'), TO_CHAR(N.DATA_VENCIMENTO, 'DD/MM/YYYY'), N.VALOR_NF, N.ID_FORNECEDOR"
                + " FROM MOV_NF N "
                + " JOIN FORNECEDOR F ON F.ID_FORNECEDOR = N.ID_FORNECEDOR"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = F.ID_PESSOA"
                + " ORDER BY N.DATA_EMISSAO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
     public ResultSet PesquisaNum(String NR) {
        String SQL = "SELECT N.NR_NF, P.NOME, TO_CHAR(N.DATA_EMISSAO, 'DD/MM/YYYY'), TO_CHAR(N.DATA_VENCIMENTO, 'DD/MM/YYYY'), N.VALOR_NF, N.ID_FORNECEDOR"
                + " FROM MOV_NF N "
                + " JOIN FORNECEDOR F ON F.ID_FORNECEDOR = N.ID_FORNECEDOR"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE N.NR_NF = '"+NR+"' "
                + " ORDER BY N.DATA_EMISSAO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
        
    }
     
         public ResultSet Pesquisar_Produtos(ClasseMovNF obj) {
        String SQL = "SELECT VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VALOR_UNIT, VP.VALOR_TOTAL"
                + " FROM MOV_NF_PRODUTO VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.NR_NF = '" + obj.getNR_NF()+ "' "
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
          public ResultSet Pesquisar_Produtos2(ClasseMovNF obj) {
        String SQL = "SELECT NULL,VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VALOR_UNIT, VP.VALOR_TOTAL"
                + " FROM MOV_NF_PRODUTO VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.NR_NF = '" + obj.getNR_NF()+ "' "
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
}
