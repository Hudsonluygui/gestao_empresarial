package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseLogin;
import Classes.ClasseUsuario;
import Validações.UltimaSequencia;
import groovy.io.GroovyPrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class ControleUsuario {

    ResultSet Rs;
    ConexaoOracle conecta_Oracle;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    private ClasseUsuario Classe_User;
    private ClasseLogin Login;

    public ControleUsuario() {
        conecta_Oracle = new ConexaoOracle();
        conecta_Oracle.conecta(0);
        Classe_User = new ClasseUsuario();
        Login = new ClasseLogin();
    }

    public boolean RetornaUsuario(ClasseUsuario obj) {
        String SQL = "SELECT * FROM CAD_USUARIO WHERE USUARIO = '" + obj.getUSUARIO() + "' AND SENHA = '" + obj.getSENHA() + "'";
        conecta_Oracle.executeSQL(SQL);
        try {
            conecta_Oracle.resultSet.first();
            String Senha = conecta_Oracle.resultSet.getString("SENHA");
            String Usuario = conecta_Oracle.resultSet.getString("USUARIO");
            return true;
        } catch (SQLException ex) {
            obj.setERRO("Usuário e/ou Senha Incorretos");
            return false;
        }
    }

    public boolean Cadastrar(ClasseUsuario obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO CAD_USUARIO (USUARIO,SENHA) "
                    + "VALUES ('" + obj.getUSUARIO() + "','"
                    + obj.getSENHA() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean BuscarUsuario(ClasseUsuario obj) {
        String SQL = "SELECT * FROM CAD_USUARIO WHERE USUARIO = '" + obj.getUSUARIO() + "'";
        conecta_Oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                return true;
            } else {
                obj.setERRO("Usuário já Cadastrado");
                return false;
            }
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean GravarLogin(ClasseLogin obj) {
        try {

            UltimaSequencia us = new UltimaSequencia("ID_LOGIN", "LOGIN");
            obj.setID_LOGIN(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO LOGIN (ID_LOGIN,USUARIO,DATA) "
                    + "VALUES ("
                    + obj.getID_LOGIN() + ",'"
                    + obj.getUSUARIO() + "','"
                    + obj.getDATA() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ClasseLogin RetornaLogin(ClasseLogin obj) {
        String SQL = "SELECT USUARIO, DATA FROM LOGIN WHERE ID_LOGIN =  " + obj.getID_LOGIN();
        conecta_Oracle.executeSQL(SQL);
        try {
            conecta_Oracle.resultSet.first();
            obj.setUSUARIO(conecta_Oracle.resultSet.getString("USUARIO"));
            obj.setDATA(conecta_Oracle.resultSet.getString("DATA"));
            return obj;
        } catch (SQLException ex) {
            obj.setERRO("Usuário e/ou Senha Incorretos");
            return obj;
        }
    }

    public int Contador() {
        int Contador = 0;
        String SQL = "SELECT COUNT(USUARIO)AS CONTADOR FROM LOGIN";
        conecta_Oracle.executeSQL(SQL);
        try {
            conecta_Oracle.resultSet.first();
            Contador = conecta_Oracle.resultSet.getInt("CONTADOR");
        } catch (SQLException ex) {
            Contador = 0;
        }
        return Contador;
    }

    public void GravarTxt(ClasseLogin obj) {

        String newLine = System.getProperty("line.separator");
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter("teste.txt"));
            for (int i = 0; i < 10; i++) {
                writer.write("" + newLine);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GravarLogin(obj);
        int Contador = Contador();
        int X = 1;
        try {
            FileWriter Arquivo = new FileWriter("C:\\Sistema-Calixto\\src\\Logs.txt");
            Arquivo.write("Usuário");              
            Arquivo.write("                 Horário " + newLine);
            do {
                obj.setID_LOGIN(X);
                RetornaLogin(obj);
                Arquivo.write(obj.getUSUARIO()+"   ");
                Arquivo.write("               " );
                Arquivo.write(obj.getDATA());
                Arquivo.write(newLine);
                X++;
            } while (X <= Contador);
            Arquivo.close();

        } catch (IOException io) {
            JOptionPane.showMessageDialog(null, "" + io);
        }

        //Buscar data e hora
    }

    public String Data(ClasseLogin obj) {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI') AS DATA FROM DUAL ";
        conecta_Oracle.executeSQL(SQL);
        try {
            conecta_Oracle.resultSet.first();
            obj.setDATA(conecta_Oracle.resultSet.getString("DATA"));
            return obj.getDATA();
        } catch (SQLException ex) {
            obj.setERRO("" + ex);
            return obj.getERRO();
        }
    }
}
