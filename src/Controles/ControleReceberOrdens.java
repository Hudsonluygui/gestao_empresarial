package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseOrdem;
import Classes.ClasseReceberVendas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleReceberOrdens {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseReceberVendas Classe_Receber;

    public ControleReceberOrdens() {
        conecta_oracle = new ConexaoOracle();
        Classe_Receber = new ClasseReceberVendas();
    }

    public ResultSet PesquisarOrdensPagas() {
        String SQL = "SELECT O.ID_ORDEM, P.NOME, O.VALOR_TOTAL, "
                + " TO_CHAR (O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA"
                + " FROM ORDEM O"
                + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE O.PAGO = 'S' "
                + " ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarOrdens() {
        String SQL = "SELECT O.ID_ORDEM, P.NOME, O.VALOR_TOTAL, "
                + " TO_CHAR (O.DATA_ABERTA, 'DD/MM/YYYY') AS DATA_ABERTA"
                + " FROM ORDEM O"
                + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE O.PAGO = 'N' "
                + " ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public Double ValorOrdem(ClasseReceberVendas obj) {
        double Resultado = 0;
        String SQL = "SELECT SUM (VALOR_PARCELA) AS VALOR_PARCELA "
                + " FROM RECEBER_VENDAS WHERE IDENTIF = '" + obj.getIDENTIF() + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Resultado = conecta_oracle.resultSet.getDouble("VALOR_PARCELA");

        } catch (Exception e) {
            Resultado = 0;
        }
        return Resultado;
    }

    public Double ValorPago(ClasseReceberVendas obj) {
        double Resultado = 0;
        String SQL = "SELECT SUM (VALOR_PAGO) as VALOR_PAGO "
                + " FROM RECEBER_VENDAS WHERE IDENTIF = '" + obj.getIDENTIF() + "'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Resultado = conecta_oracle.resultSet.getDouble("VALOR_PAGO");

        } catch (Exception e) {
            Resultado = 0;
        }
        return Resultado;

    }

    public ResultSet PesquisarParcelas(ClasseReceberVendas obj) {
        String SQL = "SELECT ID_PARCELA,VALOR_PARCELA,VALOR_PAGO,SITUACAO, "
                + "TO_CHAR (DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA_VENCIMENTO,"
                + " TO_CHAR(DATA_PAGO,'DD/MM/YYYY') AS DATA_PAGO"
                + " FROM RECEBER_VENDAS "
                + " WHERE IDENTIF = '" + obj.getIDENTIF() + "' AND DESCR = '" + obj.getDESCR() + "' ORDER BY ID_PARCELA ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;

    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        String Data = null;
        try {
            conecta_oracle.resultSet.next();
            Data = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }
        return Data;
    }
 public boolean Pago(ClasseOrdem obj) {
        try {
            pst = conn.prepareStatement(" UPDATE ORDEM SET PAGO = ? "
                    + " WHERE ID_ORDEM = ?");
            pst.setString(1, obj.getPAGO());
            pst.setInt(2, obj.getID_ORDEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }
    public boolean Liquidar(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement("UPDATE RECEBER_VENDAS SET "
                    + "VALOR_PAGO = ?,"
                    + "SITUACAO = ?,"
                    + "DATA_PAGO = ? WHERE ID_PARCELA = ? AND IDENTIF = ? AND DESCR = ?");
            pst.setDouble(1, obj.getVALOR_PAGO());
            pst.setString(2, obj.getSITUACAO());
            pst.setString(3, obj.getDATA_PAGO());
            pst.setInt(4, obj.getID_PARCELA());
            pst.setString(5, obj.getIDENTIF());
            pst.setString(6, obj.getDESCR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarSituacao(ClasseOrdem obj) {
        try {
            pst = conn.prepareStatement("UPDATE ORDEM SET "
                    + "SITUACAO = ?"
                    + " WHERE ID_ORDEM = ? AND PAGO = ? ");
            pst.setString(1, obj.getSITUACAO());
            pst.setInt(2, obj.getID_ORDEM());
            pst.setString(3, obj.getPAGO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public String ConsultarPago(int Cod) {
        String SQL = "SELECT PAGO FROM ORDEM WHERE ID_ORDEM = " + Cod;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("PAGO");

        } catch (Exception e) {
            SQL = "N";
        }
        return SQL;
    }

    public boolean Alterar(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement("UPDATE RECEBER_VENDAS SET "
                    + "VALOR_PARCELA = ?,"
                    + "SITUACAO = ?,"
                    + "DATA_PAGO = ?, "
                    + "VALOR_PAGO = ? "
                    + "WHERE ID_PARCELA = ? AND IDENTIF = ? AND DESCR = ?");
            pst.setDouble(1, obj.getVALOR_PARCELA());
            pst.setString(2, obj.getSITUACAO());
            pst.setString(3, obj.getDATA_PAGO());
            pst.setDouble(4, obj.getVALOR_PAGO());
            pst.setInt(5, obj.getID_PARCELA());
            pst.setString(6, obj.getIDENTIF());
            pst.setString(7, obj.getDESCR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarValor(Double Valor, int Codigo) {
        try {
            pst = conn.prepareStatement("UPDATE ORDEM SET "
                    + "VALOR_TOTAL = ?"
                    + " WHERE ID_ORDEM = ?");
            pst.setDouble(1, Valor);
            pst.setInt(2, Codigo);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarSituacao(String Situacao, int Codigo) {
        try {
            pst = conn.prepareStatement("UPDATE ORDEM SET "
                    + " SITUACAO = ?"
                    + " WHERE ID_ORDEM = ? AND PAGO = ?");
            pst.setString(1, Situacao);
            pst.setInt(2, Codigo);
            pst.setString(3, "S");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int Contar_Parcelas(ClasseReceberVendas obj) {
        String SQL = "SELECT COUNT(ID_PARCELA) AS CONTADOR FROM RECEBER_VENDAS "
                + "WHERE IDENTIF = " + obj.getIDENTIF() + " AND DESCR = '" + obj.getDESCR() + "'";
        int Contador;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Contador = conecta_oracle.resultSet.getInt("CONTADOR");

        } catch (Exception e) {
            Contador = -1;
        }
        return Contador;
    }

    public int Contar_Pagos(ClasseReceberVendas obj) {
        String SQL = "SELECT COUNT (SITUACAO) AS CONTADOR FROM RECEBER_VENDAS WHERE "
                + " IDENTIF = " + obj.getIDENTIF()
                + " AND SITUACAO = 'LIQUIDADA' AND DESCR = '" + obj.getDESCR() + "'";
        int Contador;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Contador = conecta_oracle.resultSet.getInt("CONTADOR");

        } catch (Exception e) {
            Contador = -1;
        }
        return Contador;
    }
}
