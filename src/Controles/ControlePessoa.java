package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseCliente;
import Classes.ClassePessoa;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControlePessoa {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;

    public ControlePessoa() {
        Classe_Pessoa = new ClassePessoa();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClassePessoa obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_PESSOA", "CAD_PESSOA");
            obj.setID_PESSOA(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO CAD_PESSOA "
                    + "(ID_PESSOA,"
                    + "NOME,"
                    + "LOGRADOURO,"
                    + "NUM_CASA,"
                    + "TP_PESSOA,"
                    + "CEP,"
                    + "TEL_FIXO,"
                    + "TEL_CEL,"
                    + "EMAIL,"
                    + "BAIRRO,"
                    + "CIDADE,"
                    + "ESTADO )"
                    + "VALUES ("
                    + obj.getID_PESSOA()  + ",'"
                    + obj.getNOME()       + "','"
                    + obj.getLOGRADOURO() + "','"
                    + obj.getNUM_CASA()   + "','"
                    + obj.getTP_PESSOA()  + "','"
                    + obj.getCEP()        + "','"
                    + obj.getTEL_FIXO()   + "','"
                    + obj.getTEL_CEL()    + "','"
                    + obj.getEMAIL()      + "','"
                    + obj.getBAIRRO()     + "','"
                    + obj.getCIDADE()     + "','"
                    + obj.getESTADO()     + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Alterar(ClassePessoa obj) {
        try {
            pst = conn.prepareStatement(" UPDATE CAD_PESSOA SET NOME = ?, "
                    + "LOGRADOURO = ?,"
                    + "NUM_CASA = ?,"
                    + "TP_PESSOA = ?,"
                    + "TEL_FIXO = ?,"
                    + "TEL_CEL = ?,"
                    + "EMAIL = ?,"
                    + "CEP = ?,"
                    + "BAIRRO = ?,"
                    + "CIDADE = ?,"
                    + "ESTADO = ?"
                    + " WHERE ID_PESSOA = ?");
            pst.setString(1, obj.getNOME());
            pst.setString(2, obj.getLOGRADOURO());
            pst.setString(3, obj.getNUM_CASA());
            pst.setString(4, obj.getTP_PESSOA());
            pst.setString(5, obj.getTEL_FIXO());
            pst.setString(6, obj.getTEL_CEL());
            pst.setString(7, obj.getEMAIL());
            pst.setString(8, obj.getCEP());
            pst.setString(9, obj.getBAIRRO());
            pst.setString(10, obj.getCIDADE());
            pst.setString(11, obj.getESTADO());
            pst.setInt(12, obj.getID_PESSOA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Excluir(ClassePessoa obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CAD_PESSOA WHERE ID_PESSOA = " + obj.getID_PESSOA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ResultSet PesquisaNome(String NOME) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME,P.TEL_FIXO,P.TEL_CEL,P.EMAIL"
                + " FROM CLIENTE C"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE P.NOME LIKE '%" + NOME + "%'"
                + " ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
     public void Recuperar(ClassePessoa obj) {
         String SQL = "SELECT ID_PESSOA,NOME,TP_PESSOA,TEL_FIXO,TEL_CEL,CEP,BAIRRO,EMAIL,NUM_CASA,LOGRADOURO, CIDADE,ESTADO FROM CAD_PESSOA "
                 + " WHERE ID_PESSOA = "+obj.getID_PESSOA();
        conecta_oracle.executeSQL(SQL);
        try {
            if (conecta_oracle.resultSet.next()) {
                obj.setID_PESSOA(conecta_oracle.resultSet.getInt("ID_PESSOA"));
                obj.setBAIRRO(conecta_oracle.resultSet.getString("BAIRRO"));
                obj.setCEP(conecta_oracle.resultSet.getString("CEP"));
                obj.setCIDADE(conecta_oracle.resultSet.getString("CIDADE"));
                obj.setEMAIL(conecta_oracle.resultSet.getString("EMAIL"));
                obj.setLOGRADOURO(conecta_oracle.resultSet.getString("LOGRADOURO"));
                obj.setNOME(conecta_oracle.resultSet.getString("NOME"));
                obj.setNUM_CASA(conecta_oracle.resultSet.getString("NUM_CASA"));
                obj.setTEL_CEL(conecta_oracle.resultSet.getString("TEL_CEL"));
                obj.setTEL_FIXO(conecta_oracle.resultSet.getString("TEL_FIXO"));
                obj.setTP_PESSOA(conecta_oracle.resultSet.getString("TP_PESSOA"));
                obj.setESTADO(conecta_oracle.resultSet.getString("ESTADO"));
                obj.setERRO(null);
            }
        } catch (Exception e) {
            obj.setERRO("OCORREU ALGUM ERRO");
            System.out.println(e);
        }
    }
public ResultSet PesquisaNomeFantasia(String NOME) {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, J.RAZAO_SOCIAL,"
                + "P.TEL_FIXO, P.TEL_CEL,P.EMAIL FROM FORNECEDOR F"
                + " JOIN CAD_PESSOA P ON F.ID_PESSOA = P.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON P.ID_PESSOA = J.ID_PESSOA"
                + " WHERE P.NOME LIKE '%"+NOME+"%'"
                + " ORDER BY F.ID_FORNECEDOR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }     
}
