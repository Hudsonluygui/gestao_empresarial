package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseReceberVendas;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleReceberVendas {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseReceberVendas Receber_Vendas;

    public ControleReceberVendas() {
        Receber_Vendas = new ClasseReceberVendas();
        conecta_oracle = new ConexaoOracle();
    }

    public int Codigo(ClasseReceberVendas obj) {
        String SQL = "SELECT ID_PARCELA FROM RECEBER_VENDAS WHERE IDENTIF = " + obj.getIDENTIF() + " AND DESCR = 'VENDA'";
        int Codigo = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Codigo = conecta_oracle.resultSet.getInt("ID_PARCELA");
        } catch (Exception ex) {
            System.out.println(ex);
            Codigo = 0;
        }
        return Codigo;
    }

    public int Codigo2(ClasseReceberVendas obj) {
        String SQL = "SELECT ID_PARCELA FROM RECEBER_VENDAS WHERE IDENTIF = " + obj.getIDENTIF() + " AND DESCR = 'ORDEM SERVICO'";
        int Codigo = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Codigo = conecta_oracle.resultSet.getInt("ID_PARCELA");
        } catch (Exception ex) {
            System.out.println(ex);
            Codigo = 0;
        }
        return Codigo;
    }

    public boolean Exluir(ClasseReceberVendas obj) {
        String SQL = "DELETE FROM RECEBER_VENDAS WHERE IDENTIF = "
                + obj.getIDENTIF() + " AND DESCR = '" + obj.getDESCR() + "'";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean Cadastrar(ClasseReceberVendas obj) {
        //    UltimaSequencia us = new UltimaSequencia("ID_PARCELA", "RECEBER_VENDAS");
        //  obj.setID_PARCELA(Integer.parseInt(us.getUtl()));
        try {
            String SQL = ("INSERT INTO RECEBER_VENDAS (ID_PARCELA,ID_CLIENTE,"
                    + "VALOR_PARCELA,"
                    + "VALOR_PAGO,"
                    + "JUROS,"
                    + "DESCONTO,"
                    + "DATA_VENCIMENTO,"
                    + "DATA_PAGO,"
                    + "SITUACAO,"
                    + "DS_FORMA,"
                    + "VALOR_TOTAL,"
                    + "DESCR,"
                    + "IDENTIF) VALUES ("
                    + obj.getID_PARCELA() + ","
                    + obj.getID_CLIENTE() + ","
                    + obj.getVALOR_PARCELA() + ","
                    + obj.getVALOR_PAGO() + ","
                    + obj.getJUROS() + ","
                    + obj.getDESCONTO() + ",'"
                    + obj.getDATA_VENCIMENTO() + "','"
                    + obj.getDATA_PAGO() + "','"
                    + obj.getSITUACAO() + "','"
                    + obj.getDS_FORMA() + "',"
                    + obj.getVALOR_TOTAL() + ",'"
                    + obj.getDESCR() + "','"
                    + obj.getIDENTIF() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean PesquisarSituacao(ClasseReceberVendas obj) {
        String SQL = "SELECT SITUACAO FROM RECEBER_VENDAS WHERE IDENTIF = " + obj.getIDENTIF()
                + " AND DESCR = '" + obj.getDESCR() + "' ";
        boolean teste = false;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            if (conecta_oracle.resultSet.getString("SITUACAO").equals("LIQUIDADA")) {
                teste = true;
            } else {
                teste = false;
            }

        } catch (Exception ex) {
            System.out.println(ex);
            teste = false;
        }
        return teste;
    }

    public boolean Excluir_Liquidação(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement(" UPDATE RECEBER_VENDAS SET "
                    + " SITUACAO = ?,"
                    + " JUROS = ?,"
                    + " DESCONTO = ?,"
                    + " VALOR_TOTAL =?,"
                    + " VALOR_PAGO = ?,"
                    + " DS_FORMA = ?,"
                    + " DATA_PAGO = ?,"
                    + " DS_FORMA = ?"
                    + " WHERE IDENTIF = ? AND ID_PARCELA = ?");
            pst.setString(1, "ABERTA");
            pst.setDouble(2, 0.00);
            pst.setDouble(3, 0.00);
            pst.setDouble(4, obj.getVALOR_TOTAL());
            pst.setDouble(5, 0.00);
            pst.setString(6, null);
            pst.setDate(7, null);
            pst.setString(8, null);
            pst.setString(9, obj.getIDENTIF());
            pst.setInt(10, obj.getID_PARCELA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ResultSet Pesquisar_ParcelasGeral() {
        String SQL = "SELECT DISTINCT R.ID_PARCELA, R.IDENTIF, R.DESCR,"
                + " R.VALOR_TOTAL,R.VALOR_PAGO,R.JUROS,R.DESCONTO,"
                + " P.NOME,TO_CHAR(R.DATA_VENCIMENTO, 'DD/MM/YYYY'), "
                + " TO_CHAR(R.DATA_PAGO, 'DD/MM/YYYY'),"
                + " R.DS_FORMA,R.SITUACAO,R.VALOR_PARCELA FROM RECEBER_VENDAS R"
                + " LEFT JOIN VENDA V ON V.ID_VENDA = R.IDENTIF"
                + " FULL JOIN ORDEM O ON O.ID_ORDEM = R.IDENTIF "
                + " FULL JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE OR C.ID_CLIENTE = O.ID_CLIENTE"
                + " LEFT JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " WHERE R.IDENTIF IS NOT NULL"
                + " ORDER BY R.ID_PARCELA ASC";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public boolean Alterar(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement(" UPDATE RECEBER_VENDAS SET "
                    + " VALOR_PARCELA =?,"
                    + " DATA_VENCIMENTO = ?,"
                    + " SITUACAO = ?,"
                    + " JUROS = ?,"
                    + " DATA_PAGO = ?,"
                    + " DESCONTO = ?,"
                    + " VALOR_TOTAL =?,"
                    + " VALOR_PAGO = ?,"
                    + "DS_FORMA = ?"
                    + " WHERE IDENTIF= ? AND ID_PARCELA = ?");
            pst.setDouble(1, obj.getVALOR_PARCELA());
            pst.setString(2, obj.getDATA_VENCIMENTO());
            pst.setString(3, obj.getSITUACAO());
            pst.setDouble(4, obj.getJUROS());
            pst.setString(5, obj.getDATA_PAGO());
            pst.setDouble(6, obj.getDESCONTO());
            pst.setDouble(7, obj.getVALOR_TOTAL());
            pst.setDouble(8, obj.getVALOR_PAGO());
            pst.setString(9, obj.getDS_FORMA());
            pst.setString(10, obj.getIDENTIF());
            pst.setInt(11, obj.getID_PARCELA());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarData(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement(" UPDATE RECEBER_VENDAS SET "
                    + " DATA_PAGO = ?"
                    + " WHERE IDENTIF = ? AND ID_PARCELA = ? AND DESCR = ?");
            pst.setString(1, obj.getDATA_PAGO());
            pst.setString(2, obj.getIDENTIF());
            pst.setInt(3, obj.getID_PARCELA());
            pst.setString(4, obj.getDESCR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean ExcluirTotal(ClasseReceberVendas obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM RECEBER_VENDAS WHERE IDENTIF = " + obj.getIDENTIF()
            + "AND DESCR = '"+obj.getDESCR()+ "' ");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public int Contar_Parcelas(ClasseReceberVendas obj) {
        String SQL = "SELECT COUNT(ID_PARCELA) AS CONTADOR FROM RECEBER_VENDAS "
                + "WHERE IDENTIF = " + obj.getIDENTIF()+ "AND DESCR = '"+obj.getDESCR()+ "'";
        int Contador;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Contador = conecta_oracle.resultSet.getInt("CONTADOR");

        } catch (Exception e) {
            Contador = -1;
        }
        return Contador;
    }

    public int Contar_Pagos(ClasseReceberVendas obj) {
        String SQL = "SELECT COUNT (SITUACAO) AS CONTADOR FROM RECEBER_VENDAS "
                + " WHERE IDENTIF = " + obj.getIDENTIF()
                + " AND SITUACAO = 'LIQUIDADA' AND DESCR = '"+obj.getDESCR()+"'";
        int Contador;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Contador = conecta_oracle.resultSet.getInt("CONTADOR");

        } catch (Exception e) {
            Contador = -1;
        }
        return Contador;
    }
      public ResultSet PesquisarVendas() {
        String SQL = "SELECT V.ID_VENDA, P.NOME, V.TOTAL_VENDA, "
                + " TO_CHAR (V.DATA_VENDA, 'DD/MM/YYYY') AS DATA_VENDA"
                + " FROM VENDA V"
                + " JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE V.PAGO = 'N' "
                + " ORDER BY V.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }
      
       public ResultSet PesquisarVendasPagas() {
        String SQL = "SELECT O.ID_VENDA, P.NOME, O.TOTAL_VENDA, "
                + " TO_CHAR (O.DATA_VENDA, 'DD/MM/YYYY') AS DATA_VENDA"
                + " FROM VENDA O"
                + " JOIN CLIENTE C ON O.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE O.PAGO = 'S' "
                + " ORDER BY O.ID_VENDA ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }
}
