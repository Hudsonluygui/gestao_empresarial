package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseCliente;
import Classes.ClassePessoa;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleCliente {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;
    ClasseCliente Classe_Cliente;

    public ControleCliente() {
        conecta_oracle = new ConexaoOracle();
        Classe_Pessoa = new ClassePessoa();
        Classe_Cliente = new ClasseCliente();

    }

    public boolean Cadastrar(ClasseCliente obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_CLIENTE", "CLIENTE");
            obj.setID_CLIENTE(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO CLIENTE "
                    + "(ID_PESSOA,"
                    + "ID_CLIENTE)"
                    + " VALUES ("
                    + obj.getID_PESSOA() + ","
                    + obj.getID_CLIENTE() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int Pesquisar_PessoaCliente(ClasseCliente obj) {
        String SQL = "SELECT ID_PESSOA FROM CLIENTE WHERE ID_CLIENTE = " + obj.getID_CLIENTE();
        int Codigo = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            Codigo = conecta_oracle.resultSet.getInt("ID_PESSOA");
            return Codigo;
        } catch (Exception e) {
            obj.setERRO(String.valueOf(e));
            return Codigo;
        }
    }

    public ResultSet PesquisaGeral() {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, P.TEL_FIXO, P.TEL_CEL,P.EMAIL"
                + " FROM CLIENTE C"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaCódigo(int Cod) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME,P.TEL_FIXO,P.TEL_CEL,P.EMAIL"
                + " FROM CLIENTE C"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE C.ID_CLIENTE = " + Cod
                + " ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaGeral2() {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " ORDER BY P.NOME ASC";

        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisaCodigo(int Codigo) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " AND C.ID_CLIENTE = " + Codigo;
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarNome(String Nome) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " AND P.NOME LIKE '%" + Nome + "%' ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCPF(String CPF) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " AND F.CPF LIKE '%" + CPF + "%' ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCNPJ(String CNPJ) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " AND J.CNPJ LIKE '%" + CNPJ + "%' ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarRG(String RG) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, NVL(F.CPF, ' '), NVL(J.CNPJ, ' '), NVL(F.RG, ' ')"
                + "                 FROM CLIENTE C JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_FISICA F ON F.ID_PESSOA = C.ID_PESSOA"
                + "                 FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE IS NOT NULL"
                + " AND F.RG LIKE '%" + RG + "%' ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public String PesquisarNome(ClasseCliente obj) {
        String SQL = "SELECT P.NOME FROM CAD_PESSOA P"
                + " JOIN CLIENTE C ON P.ID_PESSOA = C.ID_PESSOA WHERE C.ID_CLIENTE = " + obj.getID_CLIENTE();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            SQL = conecta_oracle.resultSet.getString("NOME");
            return SQL;
        } catch (Exception ex) {
            System.out.println(ex);
            SQL = "ERRO";
        }
        return SQL;
    }

    public String BuscarCPF_CNPJ(ClasseCliente obj) {
        String SQL = "SELECT F.CPF AS CPF,J.CNPJ AS CNPJ FROM CAD_FISICA F"
                + "                FULL JOIN CAD_PESSOA P ON F.ID_PESSOA = P.ID_PESSOA"
                + "                FULL JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA"
                + "                FULL JOIN CLIENTE C ON C.ID_PESSOA = P.ID_PESSOA WHERE C.ID_CLIENTE = " + obj.getID_CLIENTE();
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("CPF");
            if (SQL == null) {
                SQL = conecta_oracle.resultSet.getString("CNPJ");
            }
            obj.setERRO(null);
            return SQL;
        } catch (Exception ex) {
            System.out.println(ex);
            obj.setERRO(String.valueOf(ex));
            JOptionPane.showMessageDialog(null, "Registro não Encontrado");
            return obj.getERRO();
        }
    }

    public ResultSet PesquisarClienteVendas() {

        String SQL = " SELECT C.ID_CLIENTE,P.NOME FROM CLIENTE C"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " FULL JOIN VENDA V ON V.ID_CLIENTE = C.ID_CLIENTE ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarRelatorioORdem(ClasseCliente obj) {
        String SQL = "SELECT DISTINCT C.ID_CLIENTE,P.NOME FROM CLIENTE C"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " JOIN ORDEM O ON O.ID_CLIENTE = C.ID_CLIENTE ORDER BY C.ID_CLIENTE ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }
}
