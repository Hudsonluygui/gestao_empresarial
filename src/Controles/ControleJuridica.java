package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseJuridica;
import Classes.ClassePessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleJuridica {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;
    ClasseJuridica Classe_Juridica;

    public ControleJuridica() {
        Classe_Juridica = new ClasseJuridica();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseJuridica obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO CAD_JURIDICA "
                    + "(ID_PESSOA,"
                    + "CNPJ,"
                    + "RAZAO_SOCIAL) VALUES ("
                    + obj.getID_PESSOA() + ",'"
                    + obj.getCNPJ() + "','"
                    + obj.getRAZAO() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ResultSet PesquisarCNPJ(String CNPJ) {
        String SQL = "SELECT C.ID_CLIENTE, P.NOME, P.TEL_FIXO, P.TEL_CEL, P.EMAIL"
                + " FROM CLIENTE C "
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON P.ID_PESSOA = J.ID_PESSOA"
                + " WHERE J.CNPJ LIKE '%" + CNPJ + "'";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public void Retornar(ClasseJuridica obj) {
        String SQL = "SELECT CNPJ,RAZAO_SOCIAL FROM CAD_JURIDICA WHERE ID_PESSOA = " + obj.getID_PESSOA();
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            obj.setCNPJ(conecta_oracle.resultSet.getString("CNPJ"));
            obj.setRAZAO(conecta_oracle.resultSet.getString("RAZAO_SOCIAL"));
            obj.setERRO(null);
        } catch (Exception ex) {
            System.out.println(ex);
            obj.setERRO("Ocorreu algum Erro");
        }
    }

    public ResultSet PesquisarRazao(String Razao) {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, J.RAZAO_SOCIAL,P.TEL_FIXO, P.TEL_CEL, P.EMAIL FROM FORNECEDOR F "
                + " JOIN CAD_PESSOA P ON P.ID_pESSOA = F.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA"
                + " WHERE J.RAZAO_SOCIAL LIKE '%" + Razao + "%'"
                + " ORDER BY F.ID_FORNECEDOR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public ResultSet Pesquisar_CNPJ_Forn(String CNPJ) {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, J.RAZAO_SOCIAL, P.TEL_FIXO, P.TEL_CEL, P.EMAIL"
                + " FROM FORNECEDOR F JOIN CAD_PESSOA P ON P.ID_PESSOA = F.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA"
                + " WHERE J.CNPJ LIKE '%" + CNPJ + "%'";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public ClasseJuridica RetornarCNPJ_RAZAO(int Cod, ClasseJuridica obj) {
        String SQL = "SELECT J.CNPJ,J.RAZAO_SOCIAL FROM CAD_JURIDICA J"
                + " JOIN CAD_PESSOA P ON P.ID_PESSOA = J.ID_PESSOA"
                + " JOIN FORNECEDOR F ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE F.ID_FORNECEDOR = " + Cod;
        conecta_oracle.executeSQL(SQL);
        try {
            if (conecta_oracle.resultSet.first()) {
                obj.setCNPJ(conecta_oracle.resultSet.getString("CNPJ"));
                obj.setRAZAO(conecta_oracle.resultSet.getString("RAZAO_SOCIAL"));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return obj;
    }
}
