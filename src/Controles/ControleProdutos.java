package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovProdutos;
import Classes.ClasseProdutos;
import Classes.Classe_de_Carros_Compativeis_Produtos;
import Validações.UltimaSequencia;
import com.sun.corba.se.spi.ior.Identifiable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleProdutos {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseProdutos Classe_Produtos;
    Classe_de_Carros_Compativeis_Produtos Classe_Carros;
    ClasseMovProdutos Classe_Movimentacao;
    ControleMovProdutos Controle_Mov;
    ControleCarros Controle_Carros;

    public ControleProdutos() {
        conecta_oracle = new ConexaoOracle();
        Classe_Carros = new Classe_de_Carros_Compativeis_Produtos();
        Classe_Movimentacao = new ClasseMovProdutos();
        Classe_Produtos = new ClasseProdutos();
        Controle_Mov = new ControleMovProdutos();
        Controle_Carros = new ControleCarros();

    }

    public int Criar_Vetor() {
        String SQL = "SELECT COUNT (CODIGO) AS CONT FROM CAD_PRODUTO";
        int Tamanho = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Tamanho = conecta_oracle.resultSet.getInt("CONT");
            } else {
                Tamanho = 0;
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Tamanho;
    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                SQL = conecta_oracle.resultSet.getString("DATA");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return SQL;
    }

    public int Codigo(ClasseProdutos obj) {
        UltimaSequencia us = new UltimaSequencia("CODIGO", "CAD_PRODUTO");
        obj.setCODIGO(Integer.parseInt(us.getUtl()));
        return obj.getCODIGO();
    }
    public UltimaSequencia Ultima(){
    UltimaSequencia us = new UltimaSequencia("CODIGO", "CAD_PRODUTO");
    return us;
    }

    public boolean Cadastrar(ClasseProdutos obj) {
        try {
            
           
            String SQL = ("INSERT INTO CAD_PRODUTO (ID_PRODUTO, DS_PRODUTO,QUANT,VL_CUSTO,VL_VENDA,PERC_LUCRO,"
                    + "DS_UNIDADE,TAMANHO,TOTAL_CUSTO, MARCA,MODELO,DATA_CADASTRO,CODIGO) VALUES ("
                    + "'"
                    + obj.getID_PRODUTO() + "','"
                    + obj.getDS_PRODUTO() + "',"
                    + obj.getQUANT() + ","
                    + obj.getVL_CUSTO() + ","
                    + obj.getVL_VENDA() + ","
                    + obj.getPERC_LUCRO() + ",'"
                    + obj.getDS_UNIDADE() + "','"
                    + obj.getTAMANHO() + "',"
                    + obj.getTOTAL_CUSTO() + ",'"
                    + obj.getMARCA() + "','"
                    + obj.getMODELO() + "','"
                    + obj.getDATA_CADASTRO() + "',"
                    + obj.getCODIGO() + ")");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Controle_Mov.Cadastrar(GetMov(obj));
            obj.setERRO(null);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            obj.setERRO("ERRO");
            return false;
        }
    }

    public ClasseMovProdutos GetMov(ClasseProdutos obj) {
        Classe_Movimentacao.setDATA(obj.getDATA_CADASTRO());
        Classe_Movimentacao.setDESCR("CADASTRO");
        Classe_Movimentacao.setDS_UNIDADE(obj.getDS_UNIDADE());
        Classe_Movimentacao.setERRO(null);
        Classe_Movimentacao.setIDENTIF(String.valueOf(obj.getID_PRODUTO()));
        Classe_Movimentacao.setID_PRODUTO(obj.getID_PRODUTO());
        Classe_Movimentacao.setMARCA(obj.getMARCA());
        Classe_Movimentacao.setMODELO(obj.getMODELO());
        Classe_Movimentacao.setQUANT_MOVIDA(obj.getQUANT());
        Classe_Movimentacao.setTAMANHO(obj.getTAMANHO());
        Classe_Movimentacao.setCODIGO(obj.getCODIGO());
        return Classe_Movimentacao;
    }

    public Classe_de_Carros_Compativeis_Produtos getCarros(Classe_de_Carros_Compativeis_Produtos obj) {
        Classe_Carros.setANO_FAB(obj.getANO_FAB());
        Classe_Carros.setERRO(null);
        Classe_Carros.setID_PRODUTO(obj.getID_PRODUTO());
        Classe_Carros.setMARCA_CARRO(obj.getMARCA_CARRO());
        Classe_Carros.setMODELO_CARRO(obj.getMODELO_CARRO());
        return Classe_Carros;
    }

    public int pesquisaproduto(int CODIGO, String ID) {
        String SQL = "SELECT * FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + ID + "'  "
                + " AND CODIGO = " + CODIGO;
        conecta_oracle.executeSQL(SQL);
        try {
            if (conecta_oracle.resultSet.next()) {
                int Codigo_Gerado = conecta_oracle.resultSet.getInt("CODIGO"); // CODIGO é um Código Gerado como PK UNICO
                String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            }
            return 0;
        } catch (SQLException ex) {
            return -1;
        }
    }

    public int Pesquisar_Codigo(ClasseProdutos obj) {
        int Codigo = 0;
        String SQL = ("SELECT CODIGO FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "'"
                + " AND DS_PRODUTO = '" + obj.getDS_PRODUTO() + "' "
                + " AND QUANT = " + obj.getQUANT()
                + " AND VL_CUSTO = " + obj.getVL_CUSTO()
                + " AND VL_VENDA = " + obj.getVL_VENDA()
                + " AND PERC_LUCRO = " + obj.getPERC_LUCRO()
                + " AND DS_UNIDADE = '" + obj.getDS_UNIDADE() + "' "
                + " AND TAMANHO = '" + obj.getTAMANHO() + "' "
                + " AND TOTAL_CUSTO = " + obj.getTOTAL_CUSTO()
                + " AND MARCA = '" + obj.getMARCA() + "' "
                + " AND MODELO = '" + obj.getMODELO() + "' "
                + " AND DATA_CADASTRO = '" + obj.getDATA_CADASTRO() + "' ");
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Codigo = conecta_oracle.resultSet.getInt("CODIGO");
            } else {
                Codigo = 0;
                obj.setERRO("Código não Localizado");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }

    public ResultSet Pesquisar() {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT,VL_CUSTO,TOTAL_CUSTO,PERC_LUCRO,VL_VENDA,"
                + " TO_CHAR(DATA_CADASTRO,'DD/MM/YYYY'),CODIGO FROM CAD_PRODUTO ORDER BY DS_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar(int Codigo) {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT,VL_CUSTO,TOTAL_CUSTO,PERC_LUCRO,VL_VENDA,"
                + " TO_CHAR(DATA_CADASTRO,'DD/MM/YYYY'),CODIGO FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + Codigo + "' ORDER BY DS_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar(String Descr) {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT,VL_CUSTO,TOTAL_CUSTO,PERC_LUCRO,VL_VENDA,"
                + " TO_CHAR(DATA_CADASTRO,'DD/MM/YYYY'),CODIGO FROM CAD_PRODUTO WHERE DS_PRODUTO LIKE '%" + Descr + "%' ORDER BY DS_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

   

    public void Recuperar(ClasseProdutos obj) {
        String SQL = "SELECT ID_PRODUTO, DS_PRODUTO, QUANT, VL_CUSTO, VL_VENDA,PERC_LUCRO,TOTAL_CUSTO,"
                + "TO_CHAR(DATA_CADASTRO,'DD/MM/YYYY') AS DATA FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' ";
        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                obj.setID_PRODUTO(ConexaoOracle.resultSet.getString("ID_PRODUTO"));
                obj.setDS_PRODUTO(conecta_oracle.resultSet.getString("DS_PRODUTO"));
                obj.setDATA_CADASTRO(conecta_oracle.resultSet.getString("DATA"));
            
                obj.setERRO(null);
              
                obj.setPERC_LUCRO(conecta_oracle.resultSet.getDouble("PERC_LUCRO"));
                obj.setQUANT(conecta_oracle.resultSet.getDouble("QUANT"));
             
                obj.setTOTAL_CUSTO(conecta_oracle.resultSet.getDouble("TOTAL_CUSTO"));
                obj.setVL_CUSTO(conecta_oracle.resultSet.getDouble("VL_CUSTO"));
                obj.setVL_VENDA(conecta_oracle.resultSet.getDouble("VL_VENDA"));
            }
        } catch (Exception e) {
            obj.setERRO("OCORREU ALGUM ERRO");
            System.out.println(e);
        }
    }

    public ResultSet Pesquisar_Retorno(String Cod) {
        String SQL = "SELECT NULL,ID_PRODUTO, DS_PRODUTO, QUANT, VL_CUSTO, TOTAL_CUSTO,PERC_LUCRO,"
                + "VL_VENDA FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + Cod + "' "
                + " ORDER BY ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public String Pesquisar_Data(String Cod) {
        String SQL = "SELECT TO_CHAR (DATA_CADASTRO,'DD/MM/YYYY') AS DATA FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + Cod + "' ";
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                SQL = conecta_oracle.resultSet.getString("DATA");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return SQL;
    }

    public boolean ExcluirProduto(ClasseProdutos obj) {
        try {
            Controle_Mov.PesquisarIdentificador(obj);
            if (Controle_Mov.ExcluirMov(obj)) {
                pst = conn.prepareStatement("DELETE FROM CAD_PRODUTO WHERE CODIGO = " + obj.getCODIGO()
                        + "AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "'"
                );
                Controle_Mov.ExcluirMov(obj);
                pst.executeUpdate();
            }
            return true;
        } catch (SQLException ex) {
            obj.setERRO(String.valueOf(ex));
            return false;
        }
    }

    public int Pesquisar_CodigoFornecedor(ClasseProdutos obj) {
        int Codigo = 0;
        String SQL = ("SELECT CODIGO FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' "
                + " AND DS_PRODUTO = '" + obj.getDS_PRODUTO() + "' "
                + " AND DS_UNIDADE = '" + obj.getDS_UNIDADE() + "' "
                + " AND TAMANHO = '" + obj.getTAMANHO() + "' "
                + " AND MARCA = '" + obj.getMARCA() + "' "
                + " AND MODELO = '" + obj.getMODELO() + "' ");
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Codigo = conecta_oracle.resultSet.getInt("CODIGO");
            } else {
                Codigo = 0;
                obj.setERRO("Código não Localizado");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }

    public int Pesquisar_Codigo2(ClasseProdutos obj) {
        int Codigo = 0;
        String SQL = ("SELECT CODIGO FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' "
                + " AND DS_PRODUTO = '" + obj.getDS_PRODUTO() + "' "
                + " AND DS_UNIDADE = '" + obj.getDS_UNIDADE() + "' "
                + " AND TAMANHO = '" + obj.getTAMANHO() + "' "
                + " AND MARCA = '" + obj.getMARCA() + "' "
                + " AND MODELO = '" + obj.getMODELO() + "' ");
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Codigo = conecta_oracle.resultSet.getInt("CODIGO");
            } else {
                Codigo = 0;
                obj.setERRO("Código não Localizado");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Codigo;
    }

    public boolean Existe(String Codigo) {
        boolean Resultado = false;
        String SQL = ("SELECT * FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + Codigo + "' ");
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Resultado = true;
            } else {
                Resultado = false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Resultado;
    }

    public boolean Alterar(ClasseProdutos obj) {
        try {
            pst = conn.prepareStatement("UPDATE CAD_PRODUTO SET "
                    + "DS_PRODUTO  = ?,"
                    + "DS_UNIDADE  = ?,"
                    + "TAMANHO     = ?,"
                    + "MARCA       = ?,"
                    + "MODELO      = ?,"
                    + "QUANT       = ?,"
                    + "VL_CUSTO    = ?,"
                    + "VL_VENDA    = ?,"
                    + "PERC_LUCRO  = ?,"
                    + "TOTAL_CUSTO = ?  "
                    + "WHERE ID_PRODUTO = ? AND CODIGO = ?");

            pst.setString(1, obj.getDS_PRODUTO());
            pst.setString(2, obj.getDS_UNIDADE());
            pst.setString(3, obj.getTAMANHO());
            pst.setString(4, obj.getMARCA());
            pst.setString(5, obj.getMODELO());
            pst.setDouble(6, obj.getQUANT());
            pst.setDouble(7, obj.getVL_CUSTO());
            pst.setDouble(8, obj.getVL_VENDA());
            pst.setDouble(9, obj.getPERC_LUCRO());
            pst.setDouble(10, obj.getTOTAL_CUSTO());
            pst.setString(11, obj.getID_PRODUTO());
            pst.setInt(12, obj.getCODIGO());

            Controle_Mov.AlterarCadastro(GetMov(obj));
            pst.executeUpdate();

            obj.setERRO(null);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int Contador(String Codigo_Produto, int Codigo) {
        int Cont = 0;
        String SQL = "SELECT COUNT(ID_MOV) AS CONTADOR FROM MOV_PRODUTOS WHERE ID_PRODUTO = '" + Codigo_Produto + "' "
                + "AND CODIGO_PROD = " + Codigo;
        try {
            conecta_oracle.executeSQL(SQL);
            if (conecta_oracle.resultSet.first()) {
                Cont = conecta_oracle.resultSet.getInt("CONTADOR");
            } else {
                Cont = 0;

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Cont;

    }
public ResultSet PesquisarProduto() {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT, VL_CUSTO, VL_VENDA,CODIGO FROM CAD_PRODUTO ORDER BY DS_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
public ResultSet PesquisarProduto(String Codigo) {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT, VL_CUSTO, VL_VENDA,CODIGO FROM CAD_PRODUTO"
                + " WHERE ID_PRODUTO = '"+Codigo+"' ORDER BY DS_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
public ResultSet PesquisarProdutoNome(String Descr) {
        String SQL = "SELECT ID_PRODUTO,DS_PRODUTO,QUANT, VL_CUSTO, VL_VENDA,CODIGO FROM CAD_PRODUTO"
                + " WHERE DS_PRODUTO LIKE '%" + Descr + "%' ORDER BY DS_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
}
