package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovNF;
import Classes.ClasseVenda;
import Classes.NF_PRODUTOS;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleNF {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseMovNF Classe_NF;
    NF_PRODUTOS Classe_ProdutosNF;
    ControleNFProdutos ControleNFProd = new ControleNFProdutos();

    public ControleNF() {
        conecta_oracle = new ConexaoOracle();
        Classe_NF = new ClasseMovNF();
        Classe_ProdutosNF = new NF_PRODUTOS();
    }

    public boolean Cadastrar(ClasseMovNF obj) {
        try {

            pst = conn.prepareStatement("INSERT INTO MOV_NF "
                    + "(NR_NF,"
                    + "ID_FORNECEDOR,"
                    + "DATA_EMISSAO,"
                    + "DATA_VENCIMENTO,"
                    + "VALOR_NF)"
                    + " VALUES ('"
                    + obj.getNR_NF() + "',"
                    + obj.getID_FORNECEDOR() + ",'"
                    + obj.getDATA_EMISSAO() + "','"
                    + obj.getDATA_VENCIMENTO() + "',"
                    + obj.getVALOR_NF() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Excluir(ClasseMovNF obj) {
        String SQL = "DELETE FROM MOV_NF WHERE NR_NF = " + obj.getNR_NF();
        try {
            Classe_ProdutosNF.setNR_NF(obj.getNR_NF());
            ControleNFProd.Excluir_Venda(Classe_ProdutosNF);
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();

            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
}
