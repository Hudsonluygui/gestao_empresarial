package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovProdutos;
import Classes.ClasseVenda;
import Classes.ClasseVendaProdutos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleVendaProdutos {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseVendaProdutos Classe_VendaProduto;
    ClasseMovProdutos Classe_MovProdutos = new ClasseMovProdutos();
   public ControleMovProdutos Controle_MovProdutos = new ControleMovProdutos();
    public double QUANT;

    public ControleVendaProdutos() {
        Classe_VendaProduto = new ClasseVendaProdutos();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseVendaProdutos obj, ClasseVenda obj2) {
        try {
            pst = conn.prepareStatement("INSERT INTO VENDA_PRODUTOS "
                    + "(ID_VENDA,"
                    + "ID_CLIENTE,"
                    + "ID_PRODUTO,"
                    + "VL_TOTAL,"
                    + "CODIGO, QUANT, VL_UNITARIO)"
                    + " VALUES ("
                    + obj.getID_VENDA() + ","
                    + obj.getID_CLIENTE() + ",'"
                    + obj.getID_PRODUTO() + "',"
                    + obj.getVL_TOTAL() + ","
                    + obj.getCODIGO() + ","
                    + obj.getQUANT() + ","
                    + obj.getVL_UNITARIO() + ")");
            pst.executeUpdate();
            Double QUANT_MOV = obj.getQUANT();
            Double QUANT_ANT = Pesquisar_Estoque(obj);
            Classe_MovProdutos.setCODIGO(obj.getCODIGO());
            Classe_MovProdutos.setDATA(obj2.getDATA_VENDA());
            Classe_MovProdutos.setDESCR("VENDAS");
            Classe_MovProdutos.setDS_UNIDADE(obj.getUNIDADE());
            Classe_MovProdutos.setERRO(null);
            Classe_MovProdutos.setEST_ANTERIOR(QUANT_ANT);
            Classe_MovProdutos.setIDENTIF(String.valueOf(obj2.getID_VENDA()));
            Classe_MovProdutos.setID_PRODUTO(obj.getID_PRODUTO());
            Classe_MovProdutos.setMARCA(obj.getMARCA());
            Classe_MovProdutos.setMODELO(obj.getMODELO());
            Classe_MovProdutos.setQUANT_MOVIDA(QUANT_MOV);
            Classe_MovProdutos.setTAMANHO(obj.getTAMANHO());
            QUANT_ANT = QUANT_ANT - QUANT_MOV;
            Classe_MovProdutos.setEST_ATUAL(QUANT_ANT);
            Controle_MovProdutos.Cadastrar(Classe_MovProdutos);
            Controle_MovProdutos.Atualizar_Estoque(Classe_MovProdutos);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int PesquisarExistentes(ClasseVendaProdutos obj) {
        String SQL = "SELECT * FROM VENDA_PRODUTOS WHERE ID_CLIENTE = " + obj.getID_CLIENTE()
                + " AND ID_VENDA = " + obj.getID_VENDA()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+ "' "
                + " AND CODIGO = " + obj.getCODIGO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            int Codigo_Pessoa = conecta_oracle.resultSet.getInt("ID_CLIENTE");
            String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            int Codigo_Venda = conecta_oracle.resultSet.getInt("ID_VENDA");
            int CODIGO = conecta_oracle.resultSet.getInt("CODIGO");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public double Pesquisar_Estoque(ClasseVendaProdutos obj) {
        double EST_ANTERIOR = 0;
        conecta_oracle.executeSQL("SELECT QUANT FROM CAD_PRODUTO"
                + " WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' AND CODIGO  =" + obj.getCODIGO());
        try {
            if (ConexaoOracle.resultSet.next()) {
                EST_ANTERIOR = (ConexaoOracle.resultSet.getInt("QUANT"));
            }
        } catch (Exception e) {
            obj.getERRO();
        }
        return EST_ANTERIOR;

    }

    public boolean Remover_MOv(ClasseVendaProdutos obj) {
        String SQL = "DELETE FROM MOV_PRODUTOS WHERE IDENTIF = " + obj.getID_VENDA()
                + " AND CODIGO_PROD = " + obj.getCODIGO()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO()+"' ";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Remover_Produto(ClasseVendaProdutos obj) {
        String SQL = "DELETE FROM VENDA_PRODUTOS WHERE ID_PRODUTO = '" + obj.getID_PRODUTO()+"' "
                + " AND ID_VENDA = " + obj.getID_VENDA()
                + " AND CODIGO = " + obj.getCODIGO();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Remover_MOv(obj);
            return true;
        } catch (SQLException ex) {
            return false;
        }

    }

    public ResultSet Pesquisar_Produtos(ClasseVendaProdutos obj) {
        String SQL = "SELECT VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VL_UNITARIO, VP.VL_TOTAL"
                + " FROM VENDA_PRODUTOS VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.ID_VENDA = " + obj.getID_VENDA()
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet Pesquisar_ProdutosRetorno(ClasseVendaProdutos obj) {
        String SQL = "SELECT NULL,VP.ID_PRODUTO, C.DS_PRODUTO,C.MARCA, C.MODELO,"
                + " C.DS_UNIDADE, C.TAMANHO, VP.QUANT, VP.VL_UNITARIO, VP.VL_TOTAL"
                + " FROM VENDA_PRODUTOS VP "
                + " JOIN CAD_PRODUTO C ON VP.ID_PRODUTO = C.ID_PRODUTO"
                + " WHERE VP.ID_VENDA = " + obj.getID_VENDA()
                + " AND C.CODIGO = VP.CODIGO AND C.ID_PRODUTO = VP.ID_PRODUTO";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public boolean Excluir_Venda(ClasseVendaProdutos obj) {
        String SQL = "DELETE FROM VENDA_PRODUTOS WHERE ID_VENDA = " + obj.getID_VENDA();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Remover_MOvVenda(obj);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
  public boolean Remover_MOvVenda(ClasseVendaProdutos obj) {
        String SQL = "DELETE FROM MOV_PRODUTOS WHERE IDENTIF = '" + obj.getID_VENDA()+ "'";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
 
}
