package Controles;

import CONEXAO.ConexaoOracle;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class RelatorioEstoque {

    ConexaoOracle conecta_oracle;

    public RelatorioEstoque() {
        conecta_oracle = new ConexaoOracle();

    }

    public void Geral() {
        try {
            conecta_oracle.conecta(0);
            conecta_oracle.executeSQL("SELECT M.ID_PRODUTO, P.DS_PRODUTO, P.MARCA, P.MODELO, P.TAMANHO, M.EST_ANTERIOR, M.EST_ATUAL,M.QUANT_MOVIDA, M.DESCR,TO_CHAR(M.DATA,'DD/MM/YYYY') AS DATA,"
                    + " P.DS_UNIDADE"
                    + " FROM MOV_PRODUTOS M JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO");
            JRResultSetDataSource jrRs = new JRResultSetDataSource(ConexaoOracle.resultSet);
            JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Sistema-2019\\src\\Relatorio"
                    + "\\02.Jasper", new HashMap(), jrRs);
            JasperViewer viewer = new JasperViewer(jasperPrint, false);
            viewer.setVisible(true);
        } catch (Exception erro) {
            System.out.println(erro);
        }
    }
}
