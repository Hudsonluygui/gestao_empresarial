package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseFornecedor;
import Classes.ClassePessoa;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleFornecedor {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClassePessoa Classe_Pessoa;
    ClasseFornecedor Classe_Fornecedor;

    public ControleFornecedor() {
        Classe_Fornecedor = new ClasseFornecedor();
        Classe_Pessoa = new ClassePessoa();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseFornecedor obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_FORNECEDOR", "FORNECEDOR");
            obj.setID_FORNECEDOR(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO FORNECEDOR "
                    + "(ID_PESSOA,"
                    + "ID_FORNECEDOR)"
                    + " VALUES ("
                    + obj.getID_PESSOA() + ","
                    + obj.getID_FORNECEDOR() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public int Pesquisar_PessoaCliente(ClasseFornecedor obj) {
        String SQL = "SELECT ID_PESSOA FROM FORNECEDOR WHERE ID_FORNECEDOR = " + obj.getID_FORNECEDOR();
        int Codigo = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            Codigo = conecta_oracle.resultSet.getInt("ID_PESSOA");
            return Codigo;
        } catch (Exception e) {
            obj.setERRO(String.valueOf(e));
            return Codigo;
        }
    }
    public ResultSet Consultar(){
    String SQL = "SELECT ID_FORNECEDOR, P.NOME, J.CNPJ, J.RAZAO_SOCIAL FROM FORNECEDOR F "
            + " JOIN CAD_PESSOA P ON F.ID_PESSOA = P.ID_PESSOA"
            + " JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA ORDER BY F.ID_FORNECEDOR ASC";
    conecta_oracle.executeSQL(SQL);
    rs = conecta_oracle.resultSet;
    return rs;
    }

    public ResultSet PesquisarGeral() {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, J.RAZAO_SOCIAL, P.TEL_FIXO, P.TEL_CEL,P.EMAIL "
                + " FROM FORNECEDOR F JOIN CAD_PESSOA P ON F.ID_PESSOA = P.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA"
                + " ORDER BY F.ID_FORNECEDOR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCodigo(int Cod) {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, J.RAZAO_SOCIAL, P.TEL_FIXO, P.TEL_CEL,P.EMAIL "
                + " FROM FORNECEDOR F JOIN CAD_PESSOA P ON F.ID_PESSOA = P.ID_PESSOA"
                + " JOIN CAD_JURIDICA J ON J.ID_PESSOA = P.ID_PESSOA"
                + " WHERE F.ID_FORNECEDOR = " + Cod
                + " ORDER BY F.ID_FORNECEDOR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public int Pesquisar_Pessoa(ClasseFornecedor obj) {
        String SQL = "SELECT ID_PESSOA FROM FORNECEDOR WHERE ID_FORNECEDOR = " + obj.getID_FORNECEDOR();
        int Codigo = 0;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            Codigo = conecta_oracle.resultSet.getInt("ID_PESSOA");
            return Codigo;
        } catch (Exception e) {
            obj.setERRO(String.valueOf(e));
            return Codigo;
        }
    }

    public void RecuperarFornecedor(ClasseFornecedor obj, ClassePessoa obj2) {
        String SQL = "SELECT F.ID_FORNECEDOR, P.NOME, P.TEL_FIXO, P.TEL_CEL, P.CEP, P.BAIRRO, P.EMAIL,P.NUM_CASA, P.LOGRADOURO,"
                + " P.CIDADE FROM FORNECEDOR F  JOIN CAD_PESSOA P ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE F.ID_PESSOA = " + obj.getID_PESSOA();
        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.first()) {
                obj.setID_FORNECEDOR(conecta_oracle.resultSet.getInt("ID_FORNECEDOR"));
                obj2.setBAIRRO(conecta_oracle.resultSet.getString("BAIRRO"));
                obj2.setCEP(conecta_oracle.resultSet.getString("CEP"));
                obj2.setCIDADE(conecta_oracle.resultSet.getString("CIDADE"));
                obj2.setEMAIL(conecta_oracle.resultSet.getString("EMAIL"));
                obj2.setERRO(null);
                obj2.setLOGRADOURO(conecta_oracle.resultSet.getString("LOGRADOURO"));
                obj2.setNOME(conecta_oracle.resultSet.getString("NOME"));
                obj2.setNUM_CASA(conecta_oracle.resultSet.getString("NUM_CASA"));
                obj2.setTEL_CEL(conecta_oracle.resultSet.getString("TEL_CEL"));
                obj2.setTEL_FIXO(conecta_oracle.resultSet.getString("TEL_FIXO"));
            }
        } catch (Exception ex) {
            obj.setERRO("OCORREU ALGUM ERRO");
            System.out.println(ex);
        }
    }
    public boolean Exluir_Fornecedor(ClasseFornecedor obj){
        try {
            pst = conn.prepareStatement("DELETE FROM FORNECEDOR WHERE ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_FORNECEDOR = " + obj.getID_FORNECEDOR());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
}
}
