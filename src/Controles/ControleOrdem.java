package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseOrdem;
import Classes.ClasseOrdemAssociativa;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleOrdem {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseOrdem Classe = new ClasseOrdem();
    ClasseOrdemAssociativa Classe_Associativa;
    ControleOrdemAssociativa Controle_Associativa;

    public ControleOrdem() {
        Classe_Associativa = new ClasseOrdemAssociativa();
        Controle_Associativa = new ControleOrdemAssociativa();
        conecta_oracle = new ConexaoOracle();
    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return SQL;
    }

    public boolean Pago(ClasseOrdem obj) {
        try {
            pst = conn.prepareStatement(" UPDATE ORDEM SET PAGO = ? "
                    + " WHERE ID_ORDEM = ?");
            pst.setString(1, obj.getPAGO());
            pst.setInt(2, obj.getID_ORDEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Cadastrar(ClasseOrdem obj) {
        try {
         
            pst = conn.prepareStatement("INSERT INTO ORDEM "
                    + "(ID_ORDEM,"
                    + "ID_CLIENTE,"
                    + "DATA_ABERTA,"
                    + "DATA_FECHA,"
                    + "VALOR_TOTAL,DESCR,SITUACAO,DATA_VENCIMENTO,VALOR_BRUTO,PERC,OPERACAO)"
                    + " VALUES ("
                    + obj.getID_ORDEM() + ","
                    + obj.getID_CLIENTE() + ",'"
                    + obj.getDATA_ABERTA() + "',"
                    + obj.getDATA_FECHA() + ","
                    + obj.getVALOR_TOTAL() + ",'"
                    + obj.getDESCR() + "','"
                    + obj.getSITUACAO() + "',"
                    + obj.getDATA_VENCIMENTO() + ","
                    + obj.getVALOR_BRUTO() + ","
                    + obj.getPERC() + ",'"
                    + obj.getOPERACAO() +"')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ResultSet PesquisarGeral(String Operacao) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, CP.NOME,TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " O.OPERACAO,O.VALOR_TOTAL"
                + " FROM ORDEM O "
                + " JOIN CLIENTE C ON C.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA CP ON CP.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND O.OPERACAO = '"+Operacao+"' ORDER BY CP.NOME ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

      public ResultSet PesquisarCodigo(int Codigo, String Operacao) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, CP.NOME,TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " O.OPERACAO,O.VALOR_TOTAL"
                + " FROM ORDEM O "
                + " JOIN CLIENTE C ON C.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA CP ON CP.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND O.ID_ORDEM = "+Codigo+" AND O.OPERACAO = '"+Operacao+"'"
                + " ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCodigoCliente(int Codigo, String Operacao) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, CP.NOME,TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " O.OPERACAO,O.VALOR_TOTAL"
                + " FROM ORDEM O "
                + " JOIN CLIENTE C ON C.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA CP ON CP.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND C.ID_CLIENTE = "+Codigo+" AND O.OPERACAO = '"+Operacao+"' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

     public ResultSet PesquisarNomeCliente(String Nome, String Operacao) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, CP.NOME,TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " O.OPERACAO,O.VALOR_TOTAL"
                + " FROM ORDEM O "
                + " JOIN CLIENTE C ON C.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA CP ON CP.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND CP.NOME LIKE '%"+Nome+"%' AND O.OPERACAO = '"+Operacao+"' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }
       public ResultSet PesquisarNumeroOrdem(String Numero) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, CP.NOME,TO_CHAR(O.DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " O.OPERACAO,O.VALOR_TOTAL"
                + " FROM ORDEM O "
                + " JOIN CLIENTE C ON C.ID_CLIENTE = C.ID_CLIENTE"
                + " JOIN CAD_PESSOA CP ON CP.ID_PESSOA = C.ID_PESSOA "
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND O.NUMERO LIKE '%"+Numero+"%' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCPF(String CPF) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, P.NOME, TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY'), TO_CHAR(O.DATA_FECHA,'DD/MM/YYYY'), "
                + " TO_CHAR(o.data_vencimento, 'DD/MM/YYYY'), "
                + " O.OPERACAO, O.valor_total FROM ORDEM O "
                + " JOIN ORDEM_ASSOCIATIVA OA ON O.ID_ORDEM = OA.ID_ORDEM "
                + " JOIN CLIENTE C ON OA.ID_CLIENTE = O.ID_CLIENTE "
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " FULL JOIN CAD_FISICA F ON P.ID_PESSOA = F.ID_PESSOA"
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND F.CPF = '" + CPF + "' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarCNPJ(String CNPJ) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, P.NOME, TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY'), TO_CHAR(O.DATA_FECHA,'DD/MM/YYYY'), "
                + " TO_CHAR(o.data_vencimento, 'DD/MM/YYYY'), "
                + " O.OPERACAO, O.valor_total FROM ORDEM O "
                + " JOIN ORDEM_ASSOCIATIVA OA ON O.ID_ORDEM = OA.ID_ORDEM "
                + " JOIN CLIENTE C ON OA.ID_CLIENTE = O.ID_CLIENTE "
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " FULL JOIN CAD_JURIDICA J ON P.ID_PESSOA = J.ID_PESSOA"
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND J.CNPJ = '" + CNPJ + "' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarData(String Data) {
        String SQL = "SELECT DISTINCT O.ID_ORDEM, P.NOME, TO_CHAR(O.DATA_ABERTA, 'DD/MM/YYYY'), TO_CHAR(O.DATA_FECHA,'DD/MM/YYYY'), "
                + " TO_CHAR(o.data_vencimento, 'DD/MM/YYYY'), "
                + " O.OPERACAO, O.valor_total FROM ORDEM O "
                + " JOIN ORDEM_ASSOCIATIVA OA ON O.ID_ORDEM = OA.ID_ORDEM "
                + " JOIN CLIENTE C ON OA.ID_CLIENTE = O.ID_CLIENTE "
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE C.ID_CLIENTE = O.ID_CLIENTE AND O.DATA_ABERTA = '" + Data + "' ORDER BY O.ID_ORDEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ClasseOrdem Retorno(int Cod) {
        String SQL = "SELECT ID_ORDEM,ID_CLIENTE, TO_CHAR(DATA_ABERTA,'DD/MM/YYYY') AS DATA_ABERTA,"
                + " TO_CHAR(DATA_FECHA, 'DD/MM/YYYY') AS DATA_FECHA,"
                + " VALOR_TOTAL, DESCR, SITUACAO, TO_CHAR(DATA_VENCIMENTO, 'DD/MM/YYYY') AS DATA_VENC "
                + " FROM ORDEM WHERE ID_ORDEM = " + Cod;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            Classe.setID_ORDEM(conecta_oracle.resultSet.getInt("ID_ORDEM"));
            Classe.setID_CLIENTE(conecta_oracle.resultSet.getInt("ID_CLIENTE"));
            Classe.setDATA_ABERTA(conecta_oracle.resultSet.getString("DATA_ABERTA"));
            Classe.setDATA_FECHA(conecta_oracle.resultSet.getString("DATA_FECHA"));
            Classe.setVALOR_TOTAL(conecta_oracle.resultSet.getDouble("VALOR_TOTAL"));
            Classe.setDESCR(conecta_oracle.resultSet.getString("DESCR"));
            Classe.setSITUACAO(conecta_oracle.resultSet.getString("SITUACAO"));
            Classe.setDATA_VENCIMENTO(conecta_oracle.resultSet.getString("DATA_VENC"));
            Classe.setERRO(null);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            Classe.setERRO("ERRO");
        }
        return Classe;
    }

    public boolean Alterar(ClasseOrdem obj) {
        try {
            pst = conn.prepareStatement(" UPDATE ORDEM SET DATA_FECHA = ?, "
                    + " VALOR_TOTAL = ?,"
                    + " DESCR = ?,"
                    + " SITUACAO = ?,"
                    + " DATA_VENCIMENTO = ?,"
                    + " PERC = ?,"
                    + " VALOR_BRUTO = ?"
                    + " WHERE ID_ORDEM = ?");
            pst.setString(1, obj.getDATA_FECHA());
            pst.setDouble(2, obj.getVALOR_TOTAL());
            pst.setString(3, obj.getDESCR());
            pst.setString(4, obj.getSITUACAO());
            pst.setString(5, obj.getDATA_VENCIMENTO());
            pst.setDouble(6, obj.getPERC());
            pst.setDouble(7, obj.getVALOR_BRUTO());
            pst.setInt(8, obj.getID_ORDEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean AlterarData(ClasseOrdem obj) {
        try {
            pst = conn.prepareStatement(" UPDATE ORDEM SET "
                    + "DATA_VENCIMENTO = ?"
                    + " WHERE ID_ORDEM = ?");
            pst.setString(1, obj.getDATA_VENCIMENTO());
            pst.setInt(2, obj.getID_ORDEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Excluir(ClasseOrdem obj) {
        ControleOrdemAssociativa Associativa = new ControleOrdemAssociativa();
        String SQL = "DELETE FROM ORDEM WHERE ID_ORDEM = " + obj.getID_ORDEM();
        try {
            Classe_Associativa.setID_ORDEM(obj.getID_ORDEM());
            Controle_Associativa.Excluir_Produtos(obj);
            Controle_Associativa.Excluir_Servicos(obj);
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public ClasseOrdem Recuperar_Parcela(ClasseOrdem obj) {
        String SQL = " SELECT ID_ORDEM,ID_CLIENTE,TO_CHAR(DATA_FECHA, 'DD/MM/YYYY') AS DATA, VALOR_TOTAL "
                + " FROM ORDEM WHERE ID_ORDEM = " + obj.getID_ORDEM();
        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                obj.setID_ORDEM(conecta_oracle.resultSet.getInt("ID_ORDEM"));
                obj.setID_CLIENTE(conecta_oracle.resultSet.getInt("ID_CLIENTE"));
                obj.setDATA_FECHA(conecta_oracle.resultSet.getString("DATA"));
                obj.setVALOR_TOTAL(conecta_oracle.resultSet.getDouble("VALOR_TOTAL"));
            }
        } catch (Exception e) {
            obj.setERRO("OCORREU ALGUM ERRO");
        }
        return obj;
    }

    public String Data_Vencimento1(int Codigo) {
        String SQL = "SELECT TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA FROM RECEBER_VENDAS WHERE ID_PARCELA = 1 "
                + " AND SITUACAO = 'ABERTA' AND IDENTIF = '" + Codigo + "' AND DESCR = 'ORDEM SERVICO'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Data_Vencimento2(int Codigo) {
        String SQL = "SELECT TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA FROM RECEBER_VENDAS "
                + " WHERE ID_PARCELA = 2 "
                + " AND SITUACAO = 'ABERTA' AND IDENTIF = '" + Codigo + "' AND DESCR = 'ORDEM SERVICO'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }

    public String Data_Vencimento3(int Codigo) {
        String SQL = "SELECT TO_CHAR(DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA FROM RECEBER_VENDAS WHERE ID_PARCELA = 1 "
                + " AND SITUACAO = 'ABERTA' AND IDENTIF = '" + Codigo + "' AND DESCR = 'VENDA'";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception e) {
            SQL = "";
        }
        return SQL;
    }
}
