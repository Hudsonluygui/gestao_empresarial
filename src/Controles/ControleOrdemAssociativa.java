package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovProdutos;
import Classes.ClasseOrdem;
import Classes.ClasseOrdemAssociativa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleOrdemAssociativa {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseOrdemAssociativa Classe_OrdemAssociativa;
    ClasseMovProdutos Classe_MovProdutos;
    ControleMovProdutos Controle_MovProdutos;
    ClasseOrdem Classe;

    public ControleOrdemAssociativa() {
        conecta_oracle = new ConexaoOracle();
        Classe_OrdemAssociativa = new ClasseOrdemAssociativa();
        Classe_MovProdutos = new ClasseMovProdutos();
        Controle_MovProdutos = new ControleMovProdutos();
        Classe = new ClasseOrdem();
    }

    public int PesquisarProdutosExistentes(ClasseOrdemAssociativa obj) {
        String SQL = "SELECT * FROM ORDEM_PRODUTO WHERE ID_ORDEM = " + obj.getID_ORDEM()
                + " AND ID_PRODUTO = " + obj.getCODIGO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            int Codigo_Produto = conecta_oracle.resultSet.getInt("ID_PRODUTO");
            int Codigo_Ordem = conecta_oracle.resultSet.getInt("ID_ORDEM");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public int PesquisarServicosExistentes(ClasseOrdemAssociativa obj) {
        String SQL = "SELECT ID_SERVICO,ID_ORDEM FROM ORDEM_SERVICO WHERE ID_ORDEM = " + obj.getID_ORDEM()
                + " AND ID_SERVICO = " + obj.getID_SERVICO();
        conecta_oracle.executeSQL(SQL);
        try {

            conecta_oracle.resultSet.next();
            int Codigo_Servico = conecta_oracle.resultSet.getInt("ID_SERVICO");
            int Codigo_Ordem = conecta_oracle.resultSet.getInt("ID_ORDEM");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }
public boolean AlterarValor(ClasseOrdemAssociativa obj) {
        try {
            pst = conn.prepareStatement(" UPDATE ORDEM_SERVICO SET TOTAL_GERAL_SERVICO = ? "
                    + " WHERE ID_ORDEM = ?");
            pst.setDouble(1, obj.getTOTAL_GERAL_SERVICO());
            pst.setInt(2, obj.getID_ORDEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }
    public int PesquisarExistentes(ClasseOrdemAssociativa obj) {
        String SQL = "SELECT * FROM ORDEM_ASSOCIATIVA WHERE ID_CLIENTE = " + obj.getID_CLIENTE()
                + " AND ID_ORDEM = " + obj.getID_ORDEM()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "' "
                + " AND CODIGO = " + obj.getCODIGO()
                + " AND ID_SERVICO = " + obj.getID_SERVICO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            int Codigo_Servico = conecta_oracle.resultSet.getInt("ID_SERVICO");
            int Codigo_Pessoa = conecta_oracle.resultSet.getInt("ID_CLIENTE");
            String Codigo_Produto = conecta_oracle.resultSet.getString("ID_PRODUTO");
            int Codigo_Ordem = conecta_oracle.resultSet.getInt("ID_ORDEM");
            int CODIGO = conecta_oracle.resultSet.getInt("CODIGO");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public double Pesquisar_Estoque(ClasseOrdemAssociativa obj) {
        double EST_ANTERIOR = 0;
        conecta_oracle.executeSQL("SELECT QUANT FROM CAD_PRODUTO"
                + " WHERE CODIGO = " + obj.getCODIGO());
        try {
            if (ConexaoOracle.resultSet.next()) {
                EST_ANTERIOR = (ConexaoOracle.resultSet.getInt("QUANT"));
            }
        } catch (Exception e) {
            obj.getERRO();
        }
        return EST_ANTERIOR;

    }

    public boolean Cadastrar_Servicos(ClasseOrdemAssociativa obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO ORDEM_SERVICO ("
                    + "ID_ORDEM,"
                    + "ID_SERVICO,QUANT,TOTAL,"
                    + "VALOR_SERVICO,TOTAL_GERAL_SERVICO) VALUES ("
                    + obj.getID_ORDEM() + ","
                    + obj.getID_SERVICO() + ","
                    + obj.getQuantServico() + ","
                    + obj.getTotalServico() + ","
                    + obj.getVALOR_SERVICO() + ","
                    + obj.getTOTAL_GERAL_SERVICO() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Cadastrar_Produtos(ClasseOrdemAssociativa obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO ORDEM_PRODUTO ("
                    + "ID_ORDEM,"
                    + "ID_PRODUTO,"
                    + "QUANT,"
                    + "VL_UNITARIO,"
                    + "VL_TOTAL,TOTAL_GERAL)"
                    + "VALUES ('"
                    + obj.getID_ORDEM() + "',"
                    + obj.getCODIGO() + ","
                    + obj.getQUANT() + ","
                    + obj.getVL_UNITARIO() + ","
                    + obj.getVL_TOTAL() + ","
                    + obj.getTOTAL_GERAL() + ")");
            pst.executeUpdate();
           if (obj.getOPERACAO().equals("VENDA")){
            Double Quant_Mov = obj.getQUANT();
            Double Quant_Ant = Pesquisar_Estoque(obj);
            Classe_MovProdutos.setCODIGO(obj.getCODIGO());
            Classe_MovProdutos.setDATA(obj.getDATA());
            Classe_MovProdutos.setDESCR("VENDA");
            Classe_MovProdutos.setDS_UNIDADE(obj.getUNIDADE());
            Classe_MovProdutos.setERRO(null);
            Classe_MovProdutos.setEST_ANTERIOR(Quant_Ant);
            Classe_MovProdutos.setIDENTIF(String.valueOf(obj.getID_ORDEM()));
            Classe_MovProdutos.setID_PRODUTO(obj.getID_PRODUTO());
            Classe_MovProdutos.setMARCA(obj.getMARCA());
            Classe_MovProdutos.setMODELO(obj.getMODELO());
            Classe_MovProdutos.setQUANT_MOVIDA(obj.getQUANT());
            Classe_MovProdutos.setTAMANHO(obj.getTAMANHO());
            Classe_MovProdutos.setID_PRODUTO(String.valueOf(obj.getCODIGO()));

            Quant_Ant = Quant_Ant - Quant_Mov;
          
            Classe_MovProdutos.setEST_ATUAL(Quant_Ant);
            Controle_MovProdutos.Cadastrar(Classe_MovProdutos);
            Controle_MovProdutos.Atualizar_Estoque(Classe_MovProdutos);
           }
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Remover_Produto(ClasseOrdemAssociativa obj) {
        String SQL = "DELETE FROM ORDEM_PRODUTO WHERE ID_PRODUTO = " + obj.getCODIGO()
                + " AND ID_ORDEM = " + obj.getID_ORDEM();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            Remover_MOv(obj);
            return true;
        } catch (SQLException ex) {
            return false;
        }

    }

    public boolean Remover_Servico(ClasseOrdemAssociativa obj) {
        String SQL = "DELETE FROM ORDEM_SERVICO WHERE ID_SERVICO = " + obj.getID_SERVICO() + " AND"
                + " ID_ORDEM = " + obj.getID_ORDEM();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
    
    

    public boolean Remover_MOv(ClasseOrdemAssociativa obj) {
        String SQL = "DELETE FROM MOV_PRODUTOS WHERE IDENTIF = " + obj.getID_ORDEM()
                + " AND CODIGO_PROD = " + obj.getCODIGO()
                + " AND DESCR = 'ORDEM SERVICO'";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public int VerificarProdutos(int Cod) {
        int Cont = 0;
        String SQL = "SELECT COUNT (ID_PRODUTO) AS CONT FROM ORDEM_PRODUTO WHERE ID_ORDEM =" + Cod;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            Cont = conecta_oracle.resultSet.getInt("CONT");
            return Cont;
        } catch (Exception ex) {
            return Cont;
        }
    }

    public ResultSet PesquisarProdutos(int Cod) {
        String SQL = "SELECT OA.ID_SERVICO, CS.DS_SERVICO, OA.VALOR_SERVICO, "
                + " OA.ID_PRODUTO, CP.DS_PRODUTO,CP.MARCA,"
                + " CP.MODELO,CP.DS_UNIDADE,CP.TAMANHO, OA.QUANT,OA.VL_UNITARIO,"
                + " OA.VL_TOTAL FROM ORDEM_ASSOCIATIVA OA"
                + " JOIN CAD_SERVICO CS ON OA.ID_SERVICO = CS.ID_SERVICO"
                + " JOIN CAD_PRODUTO CP ON OA.ID_PRODUTO = CP.ID_PRODUTO"
                + " WHERE OA.ID_ORDEM = " + Cod + " AND OA.CODIGO = CP.CODIGO";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarRetornoProdutos(int Cod) {
        String SQL = "SELECT NULL,OP.ID_PRODUTO,P.DS_PRODUTO,P.MARCA,P.MODELO,P.DS_UNIDADE,P.TAMANHO,"
                + "OP.QUANT,OP.VL_UNITARIO, OP.VL_TOTAL FROM ORDEM_PRODUTO OP "
                + "JOIN CAD_PRODUTO P ON OP.ID_PRODUTO = P.CODIGO "
                + "WHERE OP.ID_ORDEM = " + Cod + " ORDER BY OP.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ResultSet PesquisarRetornoServico(int Cod) {
        String SQL = "SELECT NULL, O.ID_SERVICO,S.DS_SERVICO, O.VALOR_SERVICO,O.QUANT,O.TOTAL FROM ORDEM_SERVICO O"
                + " JOIN CAD_SERVICO S ON S.ID_SERVICO = O.ID_SERVICO WHERE O.ID_ORDEM = " + Cod
                + " ORDER BY O.ID_SERVICO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }

    public ClasseOrdem RetornoBruto(int Codigo) {
        String SQL = "SELECT VALOR_BRUTO FROM ORDEM WHERE ID_ORDEM = " + Codigo;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Classe.setVALOR_BRUTO(conecta_oracle.resultSet.getDouble("VALOR_BRUTO"));
        } catch (Exception ex) {
            Classe.setERRO(String.valueOf(ex));
        }
        return Classe;
    }
       public ClasseOrdem RetornoPerc(int Codigo) {
        String SQL = "SELECT PERC FROM ORDEM WHERE ID_ORDEM = " + Codigo;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Classe.setPERC(conecta_oracle.resultSet.getDouble("PERC"));
        } catch (Exception ex) {
            Classe.setERRO(String.valueOf(ex));
        }
        return Classe;
    }
            public ClasseOrdem RetornoTotal(int Codigo) {
        String SQL = "SELECT VALOR_TOTAL FROM ORDEM WHERE ID_ORDEM = " + Codigo;
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.next();
            Classe.setVALOR_TOTAL(conecta_oracle.resultSet.getDouble("VALOR_TOTAL"));
        } catch (Exception ex) {
            Classe.setERRO(String.valueOf(ex));
        }
        return Classe;
    }

    public int PesquisarCarro(ClasseOrdemAssociativa obj) {
        String SQL = "SELECT ID_CARRO FROM ORDEM WHERE ID_ORDEM = " + obj.getID_ORDEM()
                + " AND ID_CLIENTE = " + obj.getID_CLIENTE();
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            obj.setID_CARRO(conecta_oracle.resultSet.getInt("ID_CARRO"));
        } catch (Exception ex) {
            System.out.println(ex);
            obj.setID_CARRO(0);
        }
        return obj.getID_CARRO();
    }

    public boolean Excluir_Servicos(ClasseOrdem obj) {
        String SQL = "DELETE FROM ORDEM_SERVICO WHERE ID_ORDEM = " + obj.getID_ORDEM();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }

    }

    public boolean Excluir_Produtos(ClasseOrdem obj) {
        String SQL = "DELETE FROM ORDEM_PRODUTO WHERE ID_ORDEM = " + obj.getID_ORDEM();
        try {
            pst = conn.prepareStatement(SQL);

            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Excluir_Ordem(ClasseOrdemAssociativa obj) {
        String SQL = "DELETE FROM ORDEM_ASSOCIATIVA WHERE ID_ORDEM = " + obj.getID_ORDEM();
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Remover_MovGeral(ClasseOrdemAssociativa obj) {
        String SQL = "DELETE FROM MOV_PRODUTOS WHERE IDENTIF = '" + obj.getID_ORDEM() + "'";
        try {
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

}
