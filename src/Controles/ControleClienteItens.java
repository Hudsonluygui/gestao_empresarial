package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseCliente;
import Classes.ClasseClienteItem;
import Classes.ClassePessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleClienteItens {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseCliente Classe_Cliente;
    ClassePessoa Classe_Pessoa;
    ClasseClienteItem Classe_ClienteItem;

    public ControleClienteItens() {
        Classe_Cliente = new ClasseCliente();
        Classe_Pessoa = new ClassePessoa();
        Classe_ClienteItem = new ClasseClienteItem();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseClienteItem obj) {
        try {
            pst = conn.prepareStatement("INSERT INTO CLIENTE_ITEM "
                    + "(ID_PESSOA,"
                    + "ID_CLIENTE,"
                    + "ID_ITEM,"
                    + "MARCA,"
                    + "MODELO,"
                    + "DESCR) VALUES ("
                    + obj.getID_PESSOA() + ","
                    + obj.getID_CLIENTE() + ","
                    + obj.getID_ITEM() + ",'"
                    + obj.getMARCA() + "','"
                    + obj.getMODELO() + "','"
                    + obj.getDESCR() + "')");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean RemoverItem(ClasseClienteItem obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CLIENTE_ITEM WHERE "
                    + " ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_CLIENTE = " + obj.getID_CLIENTE()
                    + " AND ID_ITEM = " + obj.getID_ITEM());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public int PesquisarExistentes(ClasseClienteItem obj) {
        String SQL = "SELECT * FROM CLIENTE_ITEM WHERE ID_PESSOA = " + obj.getID_PESSOA()
                + " AND ID_CLIENTE = " + obj.getID_CLIENTE()
                + " AND ID_ITEM = " + obj.getID_ITEM();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.next();
            int Codigo_Pessoa = conecta_oracle.resultSet.getInt("ID_PESSOA");
            int Codigo_Item = conecta_oracle.resultSet.getInt("ID_ITEM");
            int Codigo_Cliente = conecta_oracle.resultSet.getInt("ID_CLIENTE");
            return 0;
        } catch (Exception ex) {
            return -1;
        }
    }

    public boolean ExcluirCliente(ClasseClienteItem obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CLIENTE_ITEM WHERE ID_PESSOA = " + obj.getID_PESSOA()
                    + " AND ID_CLIENTE = " + obj.getID_CLIENTE());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public int Contador(ClasseClienteItem obj) {
        int Resultado = 0;
        String SQL = "SELECT COUNT (ID_ITEM) AS CONTADOR FROM CLIENTE_ITEM WHERE ID_PESSOA = " + obj.getID_PESSOA()
                + " AND ID_CLIENTE = " + obj.getID_CLIENTE();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Resultado = conecta_oracle.resultSet.getInt("CONTADOR");
            return Resultado;
        } catch (Exception ex) {
            obj.setERRO(String.valueOf(ex));
            return Resultado;
        }
    }

    public boolean PesquisarExistente(ClasseClienteItem obj) {
        int ID = 0;
        String SQL = "SELECT ID_ITEM FROM CLIENTE_ITEM WHERE ID_CLIENTE = " + obj.getID_CLIENTE();
        try {
            conecta_oracle.executeSQL(SQL);
             conecta_oracle.resultSet.first();
            ID = conecta_oracle.resultSet.getInt("ID_ITEM");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public ResultSet PesquisarItens(ClasseClienteItem obj) {
        String SQL = "SELECT ID_ITEM, DESCR, MARCA,MODELO FROM CLIENTE_ITEM"
                + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE();
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;

        return rs;
    }

    public ResultSet PesquisarRetorno(ClasseClienteItem obj) {
        String SQL = "SELECT NULL, ID_ITEM, DESCR, MARCA,MODELO FROM CLIENTE_ITEM"
                + " WHERE ID_CLIENTE = " + obj.getID_CLIENTE();
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarCodigo(ClasseClienteItem obj) {
        String SQL = "SELECT CI.ID_ITEM, CI.DESCR, CI.MARCA,CI.MODELO FROM CLIENTE_ITEM CI"
                + " JOIN CLIENTE C ON CI.ID_CLIENTE = C.ID_CLIENTE "
                + " WHERE CI.ID_ITEM = " + obj.getID_ITEM() + " AND C.ID_CLIENTE = " + obj.getID_CLIENTE() + "ORDER BY CI.ID_ITEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarMarca(ClasseClienteItem obj) {
        String SQL = "SELECT CI.ID_ITEM, CI.DESCR, CI.MARCA,CI.MODELO FROM CLIENTE_ITEM CI"
                + " JOIN CLIENTE C ON CI.ID_CLIENTE = C.ID_CLIENTE "
                + " WHERE CI.MARCA LIKE '%" + obj.getMARCA() + "%'  AND C.ID_CLIENTE = " + obj.getID_CLIENTE() + "ORDER BY CI.ID_ITEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarModelo(ClasseClienteItem obj) {
        String SQL = "SELECT CI.ID_ITEM, CI.DESCR, CI.MARCA,CI.MODELO FROM CLIENTE_ITEM CI"
                + " JOIN CLIENTE C ON CI.ID_CLIENTE = C.ID_CLIENTE "
                + " WHERE CI.MODELO LIKE '%" + obj.getMODELO() + "%'  AND C.ID_CLIENTE = " + obj.getID_CLIENTE() + "ORDER BY CI.ID_ITEM ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ClasseClienteItem Pesquisar(ClasseClienteItem obj) {
        String SQL = "SELECT * FROM CLIENTE_ITEM WHERE ID_ITEM = " + obj.getID_ITEM() + " AND ID_CLIENTE = " + obj.getID_CLIENTE();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Classe_ClienteItem.setDESCR(conecta_oracle.resultSet.getString("DESCR"));
            Classe_ClienteItem.setMARCA(conecta_oracle.resultSet.getString("MARCA"));
            Classe_ClienteItem.setMODELO(conecta_oracle.resultSet.getString("MODELO"));
        } catch (Exception ex) {
            System.out.println(ex);
            Classe_ClienteItem.setERRO("Registro não Localizado");
        }
        return Classe_ClienteItem;
    }
}
