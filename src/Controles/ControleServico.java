package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseServicos;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleServico {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    private ClasseServicos Services;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;

    public ControleServico() {
        Services = new ClasseServicos();
        conecta_oracle = new ConexaoOracle();
    }

    public boolean Cadastrar(ClasseServicos obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_SERVICO", "CAD_SERVICO");
            obj.setID_SERVICO(Integer.parseInt(us.getUtl()));
            pst = conn.prepareStatement("INSERT INTO CAD_SERVICO (ID_SERVICO, DS_SERVICO, VALOR) VALUES "
                    + "("
                    + obj.getID_SERVICO() + ",'"
                    + obj.getDS_SERVICO() + "',"
                    + obj.getVALOR() + ")");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public boolean Excluir(ClasseServicos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM CAD_SERVICO WHERE ID_SERVICO = " + obj.getID_SERVICO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean Alterar(ClasseServicos obj) {
        try {
            pst = conn.prepareStatement(" UPDATE CAD_SERVICO SET DS_SERVICO = ?, "
                    + "VALOR = ?"
                    + " WHERE ID_SERVICO = ?");
            pst.setString(1, obj.getDS_SERVICO());
            pst.setDouble(2, obj.getVALOR());
            pst.setInt(3, obj.getID_SERVICO());
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ResultSet PesquisarGeral() {
        String SQL = "SELECT * FROM CAD_SERVICO ORDER BY DS_SERVICO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public ResultSet PesquisarPorCodigo(int Cod) {
        String SQL = "SELECT * FROM CAD_SERVICO WHERE ID_SERVICO = " + Cod + " ORDER BY DS_SERVICO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

    public void Recuperar(ClasseServicos obj) {
        String SQL = " SELECT * FROM CAD_SERVICO WHERE ID_SERVICO = " + obj.getID_SERVICO();
        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                obj.setID_SERVICO(ConexaoOracle.resultSet.getInt("ID_SERVICO"));
                obj.setDS_SERVICO(ConexaoOracle.resultSet.getString("DS_SERVICO"));
                obj.setVALOR(ConexaoOracle.resultSet.getDouble("VALOR"));
            }
        } catch (Exception e) {
            obj.setERRO("Registro não Localizado");
        }
    }

    public void Pesquisar(ClasseServicos obj) {
        String SQL = "SELECT * FROM CAD_SERVICO WHERE ID_SERVICO = " + obj.getID_SERVICO();
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            obj.setDS_SERVICO(conecta_oracle.resultSet.getString("DS_SERVICO"));
            obj.setVALOR(conecta_oracle.resultSet.getDouble("VALOR"));
            obj.setERRO(" ");
        }catch (Exception ex){
            obj.setERRO("Registro não Localizado");
        }
    }
    public ResultSet PesquisarDescr(String Descr) {
        String SQL = "SELECT * FROM CAD_SERVICO WHERE DS_SERVICO LIKE  '%" + Descr + "%' ORDER BY DS_SERVICO ASC";
        conecta_oracle.executeSQL(SQL);
        rs = conecta_oracle.resultSet;
        return rs;
    }

}
