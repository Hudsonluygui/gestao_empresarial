package Controles;

import CONEXAO.ConexaoOracle;
import Classes.ClasseMovProdutos;
import Classes.ClasseProdutos;
import Classes.Classe_de_Carros_Compativeis_Produtos;
import Validações.UltimaSequencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ControleMovProdutos {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;
    ClasseProdutos Classe_Produtos;
    Classe_de_Carros_Compativeis_Produtos Classe_Carros;
    ClasseMovProdutos Classe_Movimentacao;

    public ControleMovProdutos() {
        conecta_oracle = new ConexaoOracle();
        Classe_Carros = new Classe_de_Carros_Compativeis_Produtos();
        Classe_Movimentacao = new ClasseMovProdutos();
        Classe_Produtos = new ClasseProdutos();
    }

    public String PesquisarData() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        String data = null;
        try {
            conecta_oracle.resultSet.first();
            data = conecta_oracle.resultSet.getString("DATA");
            return data;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            return data;
        }
    }

    public boolean Cadastrar(ClasseMovProdutos obj) {
        try {
            UltimaSequencia us = new UltimaSequencia("ID_MOV", "MOV_PRODUTOS");
            obj.setID_MOV(Integer.parseInt(us.getUtl()));
            if (obj.getDESCR().equals("VENDA")) {
             
                 double Qt = obj.getQUANT_MOVIDA();
                double EST = EstoqueCompra(obj);
                Qt = 0 - Qt;
                obj.setQUANT_MOVIDA(Qt);
                obj.setEST_ANTERIOR(EST);
                obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());
            }
            if (obj.getDESCR().equals("RETIRADA")) {

                double Qt = obj.getQUANT_MOVIDA();
                double EST = EstoqueCompra(obj);
                Qt = 0 - Qt;
                obj.setQUANT_MOVIDA(Qt);
                obj.setEST_ANTERIOR(EST);
                obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());
            }
            if (obj.getDESCR().equals("ORDEM SERVICO")) {

                 double Qt = obj.getQUANT_MOVIDA();
                double EST = EstoqueCompra(obj);
                Qt = 0 - Qt;
                obj.setQUANT_MOVIDA(Qt);
                obj.setEST_ANTERIOR(EST);
                obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());

            }
            if (obj.getDESCR().equals("COMPRA")) {
                double Qt = obj.getQUANT_MOVIDA();
                double EST = EstoqueCompra(obj);
                obj.setQUANT_MOVIDA(Qt);
                obj.setEST_ANTERIOR(EST);
                obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());

            }
            if (obj.getDESCR().equals("CADASTRO")) {
                double Qt = obj.getQUANT_MOVIDA();
                double EST_ANTERIOR = Estoque(obj);
                if (EST_ANTERIOR != 0) {
                    if (JOptionPane.showConfirmDialog(null, "Produto já Existente, Deseja Continuar? (*Estoque poderá ser Substituído)") != 0) {
                        return false;
                    } else {
                        obj.setQUANT_MOVIDA(Qt);
                        obj.setEST_ANTERIOR(EST_ANTERIOR);
                        obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());
                    }
                } else {
                    obj.setEST_ATUAL(EST_ANTERIOR + obj.getQUANT_MOVIDA());
                    obj.setEST_ANTERIOR(0.00);
                }

            }
            String SQL = ("INSERT INTO MOV_PRODUTOS (ID_MOV,"
                    + "CODIGO_PROD,"
                    + "ID_PRODUTO,"
                    + " DS_UNIDADE,"
                    + " DATA,"
                    + " IDENTIF,"
                    + " EST_ANTERIOR,"
                    + " EST_ATUAL,"
                    + " QUANT_MOVIDA,"
                    + " MARCA,"
                    + " MODELO,"
                    + " TAMANHO,"
                    + " DESCR) VALUES ("
                    + obj.getID_MOV() + ","
                    + obj.getCODIGO() + ",'"
                    + obj.getID_PRODUTO() + "','"
                    + obj.getDS_UNIDADE() + "','"
                    + obj.getDATA() + "','"
                    + obj.getIDENTIF() + "',"
                    + obj.getEST_ANTERIOR() + ","
                    + obj.getEST_ATUAL() + ","
                    + obj.getQUANT_MOVIDA() + ",'"
                    + obj.getMARCA() + "','"
                    + obj.getMODELO() + "','"
                    + obj.getTAMANHO() + "','"
                    + obj.getDESCR() + "')");
            pst = conn.prepareStatement(SQL);
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public Double Estoque(ClasseMovProdutos obj) {
        String SQL = "SELECT EST_ANTERIOR FROM MOV_PRODUTOS WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' AND CODIGO_PROD  = " + obj.getCODIGO();
        double Estoque;
        try {
            conecta_oracle.executeSQL(SQL);
            Estoque = conecta_oracle.resultSet.getDouble("EST_ANTERIOR");
            return Estoque;
        } catch (SQLException ex) {
            System.out.println(ex);
            Estoque = 0;
            return Estoque;
        }
    }

    public Double EstoqueCompra(ClasseMovProdutos obj) {
        double EST = 0;
        String SQL = "SELECT QUANT FROM CAD_PRODUTO WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "' AND CODIGO  = " + obj.getCODIGO();

        conecta_oracle.executeSQL(SQL);
        try {
            if (ConexaoOracle.resultSet.next()) {
                EST = (ConexaoOracle.resultSet.getInt("QUANT"));
            }
        } catch (Exception e) {
            obj.getERRO();
        }
        return EST;
    }

    public boolean ExcluirMov(ClasseProdutos obj) {
        try {
            pst = conn.prepareStatement("DELETE FROM MOV_PRODUTOS WHERE CODIGO_PROD = " + obj.getCODIGO()
                    + " AND IDENTIF = '" + obj.getID_PRODUTO() + "' ");
            pst.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Classe_Movimentacao.setERRO(String.valueOf(ex));
            return false;
        }
    }

    public boolean Atualizar_Estoque(ClasseMovProdutos obj) {
        try {
            pst = conn.prepareStatement(" UPDATE CAD_PRODUTO SET QUANT = ? "
                    + " WHERE ID_PRODUTO = ?");
            pst.setDouble(1, obj.getEST_ATUAL());
            pst.setInt(2, obj.getCODIGO());
            pst.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;

        }
    }

    public ClasseMovProdutos BuscarEstoque(ClasseMovProdutos obj) {
        String SQL = "SELECT EST_ANTERIOR,CODIGO_PROD FROM MOV_PRODUTOS WHERE IDENTIF = '" + obj.getIDENTIF() + "' AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "' ";
        int Estoque;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            obj.setEST_ANTERIOR(conecta_oracle.resultSet.getDouble("EST_ANTERIOR"));
            obj.setCODIGO(conecta_oracle.resultSet.getInt("CODIGO_PROD"));
        } catch (Exception ex) {
            System.out.println(ex);
            Estoque = 0;
        }
        return obj;
    }

    public ClasseMovProdutos BuscarEstoque_Reversao(ClasseMovProdutos obj) {
        String SQL = "SELECT EST_ANTERIOR,CODIGO_PROD FROM MOV_PRODUTOS WHERE IDENTIF = '" + obj.getIDENTIF() + "' AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "' ";
        int Estoque;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            obj.setEST_ANTERIOR(conecta_oracle.resultSet.getDouble("EST_ANTERIOR"));
            obj.setCODIGO(conecta_oracle.resultSet.getInt("CODIGO_PROD"));
            Reverter_Estoque(obj);
        } catch (Exception ex) {
            System.out.println(ex);
            Estoque = 0;
        }
        return obj;
    }

    public boolean Reverter_Estoque(ClasseMovProdutos obj) {
        try {
            pst = conn.prepareStatement("UPDATE CAD_PRODUTO SET QUANT = ?"
                    + " WHERE ID_PRODUTO = ? AND CODIGO = ?");
            pst.setDouble(1, obj.getEST_ANTERIOR());
            pst.setString(2, obj.getID_PRODUTO());
            pst.setInt(3, obj.getCODIGO());
            pst.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            return false;

        }

    }

    public boolean AlterarCadastro(ClasseMovProdutos obj) {
        try {
            double Qt = obj.getQUANT_MOVIDA();
            double EST_ANTERIOR = Estoque(obj);
            obj.setQUANT_MOVIDA(Qt);
            obj.setEST_ANTERIOR(EST_ANTERIOR);
            obj.setEST_ATUAL(obj.getEST_ANTERIOR() + obj.getQUANT_MOVIDA());
            pst = conn.prepareStatement("UPDATE MOV_PRODUTOS SET "
                    + " DS_UNIDADE = ?,"
                    + " DATA = ?,"
                    + " EST_ANTERIOR = ?,"
                    + " EST_ATUAL = ?,"
                    + " QUANT_MOVIDA = ?,"
                    + " MARCA = ?,"
                    + " MODELO = ?,"
                    + " TAMANHO = ? "
                    + " WHERE CODIGO_PROD = ? AND IDENTIF = ? AND DESCR = ?");

            pst.setString(1, obj.getDS_UNIDADE());
            pst.setString(2, obj.getDATA());
            pst.setDouble(3, obj.getEST_ANTERIOR());
            pst.setDouble(4, obj.getEST_ATUAL());
            pst.setDouble(5, obj.getQUANT_MOVIDA());
            pst.setString(6, obj.getMARCA());
            pst.setString(7, obj.getMODELO());
            pst.setString(8, obj.getTAMANHO());
            pst.setInt(9, obj.getCODIGO());
            pst.setString(10, obj.getIDENTIF());
            pst.setString(11, "CADASTRO");

            pst.executeUpdate();

            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return false;
        }
    }

    public ClasseMovProdutos PesquisarIdentificador(ClasseProdutos obj) {
        String SQL = ("SELECT ID_MOV, IDENTIF FROM MOV_PRODUTOS WHERE CODIGO_PROD = " + obj.getCODIGO()
                + " AND ID_PRODUTO = '" + obj.getID_PRODUTO() + "'");
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Classe_Movimentacao.setID_MOV(conecta_oracle.resultSet.getInt("ID_MOV"));
            Classe_Movimentacao.setIDENTIF(conecta_oracle.resultSet.getString("IDENTIF"));

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return Classe_Movimentacao;
    }

    public int UltimaMov(String CodProduto) {
        int Mov = 0;
        String SQL = "SELECT MAX(ID_MOV) AS MOV FROM MOV_PRODUTOS WHERE ID_PRODUTO = '" + CodProduto + "' ";
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Mov = conecta_oracle.resultSet.getInt("MOV");

        } catch (SQLException ex) {
            System.out.println(ex);
            Mov = 0;

        }
        return Mov;

    }

    public Double EstoqueAtual(ClasseMovProdutos obj) {
        String SQL = "SELECT EST_ATUAL FROM MOV_PRODUTOS WHERE ID_PRODUTO = '" + obj.getID_PRODUTO() + "'"
                + " AND ID_MOV = " + UltimaMov(obj.getID_PRODUTO());
        double Estoque;
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            Estoque = conecta_oracle.resultSet.getDouble("EST_ATUAL");
            return Estoque;
        } catch (SQLException ex) {
            System.out.println(ex);
            Estoque = 0;
            return Estoque;
        }
    }
//**********************************************************************************//
    public ResultSet PesquisarAtualiza() {
        String SQL = "SELECT DISTINCT M.ID_PRODUTO, P.DS_PRODUTO, M.EST_ATUAL, TO_CHAR(M.DATA,'DD/MM/YYY') AS DATA FROM MOV_PRODUTOS M "
                + "JOIN CAD_PRODUTO P ON M.ID_PRODUTO = P.ID_PRODUTO ORDER BY P.DS_PRODUTO ASC";
        try {
            conecta_oracle.executeSQL(SQL);
            conecta_oracle.resultSet.first();
            rs = conecta_oracle.resultSet;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return rs;
    }
}
