package Controles;

import CONEXAO.ConexaoOracle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class ControleGeral {

    ConexaoOracle conecta_oracle;
    public ResultSet rs;
    Connection conn = ConexaoOracle.conecta(0);
    PreparedStatement pst;

    public ControleGeral() {
        conecta_oracle = new ConexaoOracle();
    }

    public String Data() {
        String SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') AS DATA FROM DUAL";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            SQL = conecta_oracle.resultSet.getString("DATA");
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return SQL;
    }

    public boolean Buscar() {
        String data = Data();
        String SQL;
        SQL = "SELECT * FROM RECEBER_VENDAS WHERE DATA_VENCIMENTO <= '" + data + "' AND SITUACAO = 'ABERTA' ";
        conecta_oracle.executeSQL(SQL);
        try {
            conecta_oracle.resultSet.first();
            String teste = conecta_oracle.resultSet.getString("DATA_VENCIMENTO");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public ResultSet Pesquisar(String Data) {
        String SQL = "SELECT DISTINCT R.ID_PARCELA, R.IDENTIF, R.DESCR,"
                + " R.VALOR_TOTAL,R.VALOR_PAGO,R.JUROS,R.DESCONTO,"
                + " P.NOME,TO_CHAR(R.DATA_VENCIMENTO, 'DD/MM/YYYY'), "
                + " TO_CHAR(R.DATA_PAGO, 'DD/MM/YYYY'),"
                + " R.DS_FORMA,R.SITUACAO,R.VALOR_PARCELA FROM RECEBER_VENDAS R"
                + " LEFT JOIN VENDA V ON V.ID_VENDA = R.IDENTIF"
                + " FULL JOIN ORDEM O ON O.ID_ORDEM = R.IDENTIF "
                + " FULL JOIN CLIENTE C ON V.ID_CLIENTE = C.ID_CLIENTE OR C.ID_CLIENTE = O.ID_CLIENTE"
                + " LEFT JOIN CAD_PESSOA P ON P.ID_PESSOA = C.ID_PESSOA"
                + " WHERE R.IDENTIF IS NOT NULL AND R.DATA_VENCIMENTO <='" + Data + "' AND R.SITUACAO LIKE 'ABERTA' "
                + " ORDER BY R.DESCR ASC";
        conecta_oracle.executeSQL(SQL);
        rs = (conecta_oracle.resultSet);
        return rs;
    }

    public ResultSet PesquisarProdutosGeral() {
        String SQL = "SELECT DISTINCT M.ID_PRODUTO, P.DS_PRODUTO FROM MOV_PRODUTOS M"
                + " JOIN CAD_PRODUTO P ON P.ID_PRODUTO = M.ID_PRODUTO AND"
                + " M.CODIGO_PROD = P.CODIGO ORDER BY M.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = (conecta_oracle.resultSet);
    }

    public ResultSet PesquisarProdutosCodigo(int Codigo) {
        String SQL = "SELECT DISTINCT M.ID_PRODUTO, P.DS_PRODUTO FROM MOV_PRODUTOS M"
                + " JOIN CAD_PRODUTO P ON P.ID_PRODUTO = M.ID_PRODUTO AND"
                + " M.CODIGO_PROD = P.CODIGO WHERE M.ID_PRODUTO = " + Codigo + " ORDER BY M.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = (conecta_oracle.resultSet);
    }

    public ResultSet PesquisarProdutosDescr(String Produto) {
        String SQL = "SELECT DISTINCT M.ID_PRODUTO, P.DS_PRODUTO FROM MOV_PRODUTOS M"
                + " JOIN CAD_PRODUTO P ON P.ID_PRODUTO = M.ID_PRODUTO AND"
                + " M.CODIGO_PROD = P.CODIGO WHERE P.DS_PRODUTO LIKE '%" + Produto + "%' ORDER BY M.ID_PRODUTO ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = (conecta_oracle.resultSet);
    }

    public ResultSet PesquisarParcelasVencidas(String Data) {
        String SQL = "SELECT DISTINCT P.NOME,R.VALOR_TOTAL, R.VALOR_PARCELA,"
                + " TO_CHAR(R.DATA_VENCIMENTO,'DD/MM/YYYY') AS DATA, R.DESCR, R.IDENTIF"
                + " FROM RECEBER_VENDAS R"
                + " JOIN CLIENTE C ON C.ID_CLIENTE = R.ID_CLIENTE"
                + " JOIN CAD_PESSOA P ON C.ID_PESSOA = P.ID_PESSOA"
                + " WHERE DATA_VENCIMENTO <= '"+Data+"' AND R.SITUACAO = 'ABERTA' ORDER BY P.NOME ASC";
        conecta_oracle.executeSQL(SQL);
        return rs = conecta_oracle.resultSet;
    }
}
